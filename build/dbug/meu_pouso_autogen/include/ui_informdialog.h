/********************************************************************************
** Form generated from reading UI file 'informdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INFORMDIALOG_H
#define UI_INFORMDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>

QT_BEGIN_NAMESPACE

class Ui_InformDialog
{
public:
    QDialogButtonBox *buttonBox;
    QPushButton *btn_error;
    QPushButton *btn_warning;
    QTextBrowser *textBrowser;

    void setupUi(QDialog *InformDialog)
    {
        if (InformDialog->objectName().isEmpty())
            InformDialog->setObjectName(QString::fromUtf8("InformDialog"));
        InformDialog->resize(493, 195);
        buttonBox = new QDialogButtonBox(InformDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(10, 150, 471, 40));
        buttonBox->setMinimumSize(QSize(0, 40));
        QFont font;
        font.setFamily(QString::fromUtf8("URW Bookman L"));
        font.setPointSize(15);
        font.setBold(true);
        font.setWeight(75);
        buttonBox->setFont(font);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Ok);
        btn_error = new QPushButton(InformDialog);
        btn_error->setObjectName(QString::fromUtf8("btn_error"));
        btn_error->setGeometry(QRect(10, 10, 89, 61));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(40);
        sizePolicy.setVerticalStretch(40);
        sizePolicy.setHeightForWidth(btn_error->sizePolicy().hasHeightForWidth());
        btn_error->setSizePolicy(sizePolicy);
        btn_error->setMinimumSize(QSize(40, 40));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/remove.png"), QSize(), QIcon::Normal, QIcon::Off);
        btn_error->setIcon(icon);
        btn_error->setIconSize(QSize(40, 40));
        btn_warning = new QPushButton(InformDialog);
        btn_warning->setObjectName(QString::fromUtf8("btn_warning"));
        btn_warning->setGeometry(QRect(10, 80, 89, 61));
        sizePolicy.setHeightForWidth(btn_warning->sizePolicy().hasHeightForWidth());
        btn_warning->setSizePolicy(sizePolicy);
        btn_warning->setMinimumSize(QSize(40, 40));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/warning.png"), QSize(), QIcon::Normal, QIcon::Off);
        btn_warning->setIcon(icon1);
        btn_warning->setIconSize(QSize(40, 40));
        textBrowser = new QTextBrowser(InformDialog);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setGeometry(QRect(110, 10, 371, 131));

        retranslateUi(InformDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), InformDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), InformDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(InformDialog);
    } // setupUi

    void retranslateUi(QDialog *InformDialog)
    {
        InformDialog->setWindowTitle(QApplication::translate("InformDialog", "Dialog", nullptr));
        btn_error->setText(QString());
        btn_warning->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class InformDialog: public Ui_InformDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INFORMDIALOG_H
