/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *act_salvar;
    QAction *act_sair;
    QAction *act_backup;
    QAction *act_carregar;
    QWidget *centralWidget;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGroupBox *groupBox;
    QTableWidget *tb_check_ins;
    QWidget *horizontalLayoutWidget_6;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *btn_checkin;
    QGroupBox *groupBox_2;
    QTableWidget *tb_check_outs;
    QWidget *horizontalLayoutWidget_7;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *btn_checkout;
    QLabel *lbl_date;
    QLabel *lbl_time;
    QFrame *line;
    QGroupBox *groupBox_8;
    QTableWidget *tb_confirmations;
    QWidget *horizontalLayoutWidget_8;
    QHBoxLayout *horizontalLayout_8;
    QPushButton *btn_confirma;
    QPushButton *btn_add_reserva_inicial;
    QWidget *tab_3;
    QWidget *gridLayoutWidget_5;
    QGridLayout *gridLayout_5;
    QTableWidget *table_reserva;
    QVBoxLayout *verticalLayout_6;
    QPushButton *btn_add_reserva;
    QPushButton *btn_remove_reserva;
    QPushButton *btn_edit_reserva;
    QPushButton *btn_print_reserva;
    QGroupBox *groupBox_7;
    QLabel *label_5;
    QTextEdit *txt_cliente;
    QLabel *label_10;
    QLabel *label_11;
    QDateEdit *dt_inicio_lower;
    QDateEdit *dt_inicio_higher;
    QLabel *label_12;
    QLabel *label_13;
    QLabel *label_14;
    QDateEdit *dt_fim_lower;
    QLabel *label_15;
    QDateEdit *dt_fim_higher;
    QCheckBox *chk_inicio_lower;
    QCheckBox *chk_fim_lower;
    QCheckBox *chk_inicio_higher;
    QCheckBox *chk_fim_higher;
    QTextEdit *txt_quarto;
    QLabel *label_19;
    QLabel *label_20;
    QComboBox *cbx_status_reserva;
    QWidget *tab_4;
    QWidget *gridLayoutWidget_3;
    QGridLayout *gridLayout_3;
    QTableWidget *table_cliente;
    QGroupBox *groupBox_6;
    QLabel *label_4;
    QTextEdit *txt_cliente_nome;
    QLabel *label_6;
    QTextEdit *txt_cliente_cpf;
    QVBoxLayout *verticalLayout_4;
    QPushButton *btn_add_cliente;
    QPushButton *btn_remove_cliente;
    QPushButton *btn_edit_cliente;
    QWidget *tab_6;
    QWidget *gridLayoutWidget_4;
    QGridLayout *gridLayout_4;
    QGroupBox *groupBox_5;
    QTextEdit *txt_hospede_cpf;
    QLabel *label_9;
    QLabel *label_8;
    QTextEdit *txt_hospede_nome;
    QTableWidget *table_hospede;
    QVBoxLayout *verticalLayout_5;
    QPushButton *btn_add_hospede;
    QPushButton *btn_remove_hospede;
    QPushButton *btn_edit_hospede;
    QWidget *tab_2;
    QWidget *gridLayoutWidget_2;
    QGridLayout *gridLayout_2;
    QGroupBox *groupBox_4;
    QLabel *label_2;
    QLabel *label_7;
    QComboBox *cbx_quarto_tipo_quarto_filtro;
    QLineEdit *txt_quarto_numero;
    QCheckBox *chk_disponivel_higher;
    QLabel *label_18;
    QLabel *label_16;
    QDateEdit *dt_disponivel_higher;
    QLabel *label_17;
    QCheckBox *chk_disponivel_lower;
    QDateEdit *dt_disponivel_lower;
    QVBoxLayout *verticalLayout_3;
    QPushButton *btn_add_quarto;
    QPushButton *btn_remove_quarto;
    QPushButton *btn_edit_quarto;
    QTableWidget *table_quarto;
    QWidget *tab_5;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout_2;
    QPushButton *btn_add_tipoquarto;
    QPushButton *btn_remove_tipoquarto;
    QPushButton *btn_edit_tipoquarto;
    QGroupBox *groupBox_3;
    QLabel *label;
    QLineEdit *txt_tipo_quarto_nome;
    QTableWidget *table_tipoquarto;
    QWidget *tab_7;
    QTextEdit *txt_log;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *btn_update;
    QMenuBar *menuBar;
    QMenu *menuArquivo;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1311, 794);
        MainWindow->setTabShape(QTabWidget::Rounded);
        act_salvar = new QAction(MainWindow);
        act_salvar->setObjectName(QString::fromUtf8("act_salvar"));
        act_sair = new QAction(MainWindow);
        act_sair->setObjectName(QString::fromUtf8("act_sair"));
        act_backup = new QAction(MainWindow);
        act_backup->setObjectName(QString::fromUtf8("act_backup"));
        act_carregar = new QAction(MainWindow);
        act_carregar->setObjectName(QString::fromUtf8("act_carregar"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        QFont font;
        font.setFamily(QString::fromUtf8("URW Bookman L"));
        font.setPointSize(15);
        font.setBold(true);
        font.setWeight(75);
        centralWidget->setFont(font);
        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(-1, -1, 1311, 771));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        tabWidget = new QTabWidget(horizontalLayoutWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setEnabled(true);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(100);
        sizePolicy.setVerticalStretch(100);
        sizePolicy.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy);
        tabWidget->setFont(font);
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        groupBox = new QGroupBox(tab);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(470, 20, 391, 451));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy1);
        tb_check_ins = new QTableWidget(groupBox);
        tb_check_ins->setObjectName(QString::fromUtf8("tb_check_ins"));
        tb_check_ins->setGeometry(QRect(0, 30, 391, 341));
        tb_check_ins->setSelectionBehavior(QAbstractItemView::SelectRows);
        tb_check_ins->horizontalHeader()->setStretchLastSection(true);
        horizontalLayoutWidget_6 = new QWidget(groupBox);
        horizontalLayoutWidget_6->setObjectName(QString::fromUtf8("horizontalLayoutWidget_6"));
        horizontalLayoutWidget_6->setGeometry(QRect(0, 370, 391, 80));
        horizontalLayout_6 = new QHBoxLayout(horizontalLayoutWidget_6);
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(0, 0, 0, 0);
        btn_checkin = new QPushButton(horizontalLayoutWidget_6);
        btn_checkin->setObjectName(QString::fromUtf8("btn_checkin"));
        btn_checkin->setEnabled(false);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/checked.png"), QSize(), QIcon::Normal, QIcon::Off);
        btn_checkin->setIcon(icon);
        btn_checkin->setIconSize(QSize(40, 40));

        horizontalLayout_6->addWidget(btn_checkin);

        groupBox_2 = new QGroupBox(tab);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(880, 20, 401, 451));
        tb_check_outs = new QTableWidget(groupBox_2);
        tb_check_outs->setObjectName(QString::fromUtf8("tb_check_outs"));
        tb_check_outs->setGeometry(QRect(0, 30, 401, 341));
        tb_check_outs->setSelectionBehavior(QAbstractItemView::SelectRows);
        tb_check_outs->horizontalHeader()->setStretchLastSection(true);
        horizontalLayoutWidget_7 = new QWidget(groupBox_2);
        horizontalLayoutWidget_7->setObjectName(QString::fromUtf8("horizontalLayoutWidget_7"));
        horizontalLayoutWidget_7->setGeometry(QRect(0, 370, 401, 80));
        horizontalLayout_7 = new QHBoxLayout(horizontalLayoutWidget_7);
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(0, 0, 0, 0);
        btn_checkout = new QPushButton(horizontalLayoutWidget_7);
        btn_checkout->setObjectName(QString::fromUtf8("btn_checkout"));
        btn_checkout->setEnabled(false);
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/close.png"), QSize(), QIcon::Normal, QIcon::Off);
        btn_checkout->setIcon(icon1);
        btn_checkout->setIconSize(QSize(40, 40));

        horizontalLayout_7->addWidget(btn_checkout);

        lbl_date = new QLabel(tab);
        lbl_date->setObjectName(QString::fromUtf8("lbl_date"));
        lbl_date->setGeometry(QRect(90, 480, 1141, 91));
        QFont font1;
        font1.setFamily(QString::fromUtf8("URW Bookman L"));
        font1.setPointSize(50);
        font1.setBold(true);
        font1.setItalic(true);
        font1.setWeight(75);
        lbl_date->setFont(font1);
        lbl_date->setAlignment(Qt::AlignCenter);
        lbl_time = new QLabel(tab);
        lbl_time->setObjectName(QString::fromUtf8("lbl_time"));
        lbl_time->setGeometry(QRect(90, 560, 1141, 91));
        QFont font2;
        font2.setFamily(QString::fromUtf8("URW Bookman L"));
        font2.setPointSize(36);
        font2.setBold(true);
        font2.setItalic(true);
        font2.setWeight(75);
        lbl_time->setFont(font2);
        lbl_time->setTextFormat(Qt::AutoText);
        lbl_time->setScaledContents(false);
        lbl_time->setAlignment(Qt::AlignCenter);
        lbl_time->setWordWrap(false);
        line = new QFrame(tab);
        line->setObjectName(QString::fromUtf8("line"));
        line->setGeometry(QRect(10, 560, 1281, 20));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        groupBox_8 = new QGroupBox(tab);
        groupBox_8->setObjectName(QString::fromUtf8("groupBox_8"));
        groupBox_8->setGeometry(QRect(80, 20, 371, 451));
        sizePolicy1.setHeightForWidth(groupBox_8->sizePolicy().hasHeightForWidth());
        groupBox_8->setSizePolicy(sizePolicy1);
        tb_confirmations = new QTableWidget(groupBox_8);
        tb_confirmations->setObjectName(QString::fromUtf8("tb_confirmations"));
        tb_confirmations->setGeometry(QRect(0, 30, 371, 341));
        tb_confirmations->setAlternatingRowColors(true);
        tb_confirmations->setSelectionBehavior(QAbstractItemView::SelectRows);
        tb_confirmations->setTextElideMode(Qt::ElideLeft);
        tb_confirmations->horizontalHeader()->setStretchLastSection(true);
        horizontalLayoutWidget_8 = new QWidget(groupBox_8);
        horizontalLayoutWidget_8->setObjectName(QString::fromUtf8("horizontalLayoutWidget_8"));
        horizontalLayoutWidget_8->setGeometry(QRect(0, 370, 371, 80));
        horizontalLayout_8 = new QHBoxLayout(horizontalLayoutWidget_8);
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(0, 0, 0, 0);
        btn_confirma = new QPushButton(horizontalLayoutWidget_8);
        btn_confirma->setObjectName(QString::fromUtf8("btn_confirma"));
        btn_confirma->setEnabled(false);
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/checked_1.png"), QSize(), QIcon::Normal, QIcon::Off);
        btn_confirma->setIcon(icon2);
        btn_confirma->setIconSize(QSize(40, 40));

        horizontalLayout_8->addWidget(btn_confirma);

        btn_add_reserva_inicial = new QPushButton(tab);
        btn_add_reserva_inicial->setObjectName(QString::fromUtf8("btn_add_reserva_inicial"));
        btn_add_reserva_inicial->setGeometry(QRect(20, 50, 52, 421));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/add.png"), QSize(), QIcon::Normal, QIcon::Off);
        btn_add_reserva_inicial->setIcon(icon3);
        btn_add_reserva_inicial->setIconSize(QSize(40, 40));
        tabWidget->addTab(tab, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        gridLayoutWidget_5 = new QWidget(tab_3);
        gridLayoutWidget_5->setObjectName(QString::fromUtf8("gridLayoutWidget_5"));
        gridLayoutWidget_5->setGeometry(QRect(0, 0, 1281, 231671));
        gridLayout_5 = new QGridLayout(gridLayoutWidget_5);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gridLayout_5->setSizeConstraint(QLayout::SetMinimumSize);
        gridLayout_5->setContentsMargins(0, 0, 0, 0);
        table_reserva = new QTableWidget(gridLayoutWidget_5);
        if (table_reserva->columnCount() < 3)
            table_reserva->setColumnCount(3);
        if (table_reserva->rowCount() < 10)
            table_reserva->setRowCount(10);
        table_reserva->setObjectName(QString::fromUtf8("table_reserva"));
        sizePolicy.setHeightForWidth(table_reserva->sizePolicy().hasHeightForWidth());
        table_reserva->setSizePolicy(sizePolicy);
        table_reserva->setAutoFillBackground(true);
        table_reserva->setFrameShape(QFrame::Box);
        table_reserva->setFrameShadow(QFrame::Raised);
        table_reserva->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        table_reserva->setAlternatingRowColors(true);
        table_reserva->setSelectionBehavior(QAbstractItemView::SelectRows);
        table_reserva->setTextElideMode(Qt::ElideMiddle);
        table_reserva->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
        table_reserva->setSortingEnabled(false);
        table_reserva->setRowCount(10);
        table_reserva->setColumnCount(3);
        table_reserva->horizontalHeader()->setCascadingSectionResizes(true);
        table_reserva->horizontalHeader()->setMinimumSectionSize(32);
        table_reserva->horizontalHeader()->setProperty("showSortIndicator", QVariant(false));
        table_reserva->horizontalHeader()->setStretchLastSection(true);
        table_reserva->verticalHeader()->setStretchLastSection(false);

        gridLayout_5->addWidget(table_reserva, 1, 0, 1, 1);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        btn_add_reserva = new QPushButton(gridLayoutWidget_5);
        btn_add_reserva->setObjectName(QString::fromUtf8("btn_add_reserva"));
        btn_add_reserva->setIcon(icon3);
        btn_add_reserva->setIconSize(QSize(40, 40));

        verticalLayout_6->addWidget(btn_add_reserva);

        btn_remove_reserva = new QPushButton(gridLayoutWidget_5);
        btn_remove_reserva->setObjectName(QString::fromUtf8("btn_remove_reserva"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/remove.png"), QSize(), QIcon::Normal, QIcon::Off);
        btn_remove_reserva->setIcon(icon4);
        btn_remove_reserva->setIconSize(QSize(40, 40));

        verticalLayout_6->addWidget(btn_remove_reserva);

        btn_edit_reserva = new QPushButton(gridLayoutWidget_5);
        btn_edit_reserva->setObjectName(QString::fromUtf8("btn_edit_reserva"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/edit.png"), QSize(), QIcon::Normal, QIcon::Off);
        btn_edit_reserva->setIcon(icon5);
        btn_edit_reserva->setIconSize(QSize(40, 40));

        verticalLayout_6->addWidget(btn_edit_reserva);

        btn_print_reserva = new QPushButton(gridLayoutWidget_5);
        btn_print_reserva->setObjectName(QString::fromUtf8("btn_print_reserva"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/images/file-2.png"), QSize(), QIcon::Normal, QIcon::Off);
        btn_print_reserva->setIcon(icon6);
        btn_print_reserva->setIconSize(QSize(40, 40));

        verticalLayout_6->addWidget(btn_print_reserva);


        gridLayout_5->addLayout(verticalLayout_6, 0, 1, 1, 1);

        groupBox_7 = new QGroupBox(gridLayoutWidget_5);
        groupBox_7->setObjectName(QString::fromUtf8("groupBox_7"));
        groupBox_7->setFont(font);
        label_5 = new QLabel(groupBox_7);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(0, 40, 81, 31));
        txt_cliente = new QTextEdit(groupBox_7);
        txt_cliente->setObjectName(QString::fromUtf8("txt_cliente"));
        txt_cliente->setGeometry(QRect(88, 40, 1111, 30));
        QFont font3;
        font3.setFamily(QString::fromUtf8("URW Bookman L"));
        font3.setPointSize(12);
        font3.setBold(false);
        font3.setWeight(50);
        txt_cliente->setFont(font3);
        label_10 = new QLabel(groupBox_7);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(0, 160, 151, 31));
        label_11 = new QLabel(groupBox_7);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setGeometry(QRect(0, 220, 151, 31));
        dt_inicio_lower = new QDateEdit(groupBox_7);
        dt_inicio_lower->setObjectName(QString::fromUtf8("dt_inicio_lower"));
        dt_inicio_lower->setGeometry(QRect(260, 190, 171, 30));
        QFont font4;
        font4.setFamily(QString::fromUtf8("URW Bookman L"));
        font4.setPointSize(12);
        font4.setBold(true);
        font4.setWeight(75);
        dt_inicio_lower->setFont(font4);
        dt_inicio_lower->setCalendarPopup(true);
        dt_inicio_higher = new QDateEdit(groupBox_7);
        dt_inicio_higher->setObjectName(QString::fromUtf8("dt_inicio_higher"));
        dt_inicio_higher->setGeometry(QRect(700, 190, 171, 30));
        dt_inicio_higher->setFont(font4);
        dt_inicio_higher->setCalendarPopup(true);
        label_12 = new QLabel(groupBox_7);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setGeometry(QRect(150, 190, 91, 31));
        QFont font5;
        font5.setFamily(QString::fromUtf8("URW Bookman L"));
        font5.setPointSize(12);
        font5.setBold(true);
        font5.setUnderline(true);
        font5.setWeight(75);
        label_12->setFont(font5);
        label_13 = new QLabel(groupBox_7);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setGeometry(QRect(600, 190, 91, 31));
        label_13->setFont(font5);
        label_14 = new QLabel(groupBox_7);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setGeometry(QRect(150, 250, 91, 31));
        label_14->setFont(font5);
        dt_fim_lower = new QDateEdit(groupBox_7);
        dt_fim_lower->setObjectName(QString::fromUtf8("dt_fim_lower"));
        dt_fim_lower->setGeometry(QRect(260, 250, 171, 30));
        dt_fim_lower->setFont(font4);
        dt_fim_lower->setCalendarPopup(true);
        label_15 = new QLabel(groupBox_7);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setGeometry(QRect(600, 250, 91, 31));
        label_15->setFont(font5);
        dt_fim_higher = new QDateEdit(groupBox_7);
        dt_fim_higher->setObjectName(QString::fromUtf8("dt_fim_higher"));
        dt_fim_higher->setGeometry(QRect(700, 250, 171, 30));
        dt_fim_higher->setFont(font4);
        dt_fim_higher->setCalendarPopup(true);
        chk_inicio_lower = new QCheckBox(groupBox_7);
        chk_inicio_lower->setObjectName(QString::fromUtf8("chk_inicio_lower"));
        chk_inicio_lower->setGeometry(QRect(450, 190, 16, 23));
        chk_fim_lower = new QCheckBox(groupBox_7);
        chk_fim_lower->setObjectName(QString::fromUtf8("chk_fim_lower"));
        chk_fim_lower->setGeometry(QRect(450, 250, 16, 23));
        chk_inicio_higher = new QCheckBox(groupBox_7);
        chk_inicio_higher->setObjectName(QString::fromUtf8("chk_inicio_higher"));
        chk_inicio_higher->setGeometry(QRect(880, 190, 16, 23));
        chk_fim_higher = new QCheckBox(groupBox_7);
        chk_fim_higher->setObjectName(QString::fromUtf8("chk_fim_higher"));
        chk_fim_higher->setGeometry(QRect(880, 250, 16, 23));
        txt_quarto = new QTextEdit(groupBox_7);
        txt_quarto->setObjectName(QString::fromUtf8("txt_quarto"));
        txt_quarto->setGeometry(QRect(88, 80, 1111, 30));
        txt_quarto->setFont(font3);
        label_19 = new QLabel(groupBox_7);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setGeometry(QRect(0, 80, 81, 31));

        gridLayout_5->addWidget(groupBox_7, 0, 0, 1, 1);

        gridLayout_5->setRowMinimumHeight(0, 280);
        label_20 = new QLabel(tab_3);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setGeometry(QRect(0, 120, 156, 35));
        cbx_status_reserva = new QComboBox(tab_3);
        cbx_status_reserva->setObjectName(QString::fromUtf8("cbx_status_reserva"));
        cbx_status_reserva->setGeometry(QRect(160, 120, 1041, 30));
        sizePolicy1.setHeightForWidth(cbx_status_reserva->sizePolicy().hasHeightForWidth());
        cbx_status_reserva->setSizePolicy(sizePolicy1);
        cbx_status_reserva->setFont(font3);
        tabWidget->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        gridLayoutWidget_3 = new QWidget(tab_4);
        gridLayoutWidget_3->setObjectName(QString::fromUtf8("gridLayoutWidget_3"));
        gridLayoutWidget_3->setGeometry(QRect(0, 0, 1301, 691));
        gridLayout_3 = new QGridLayout(gridLayoutWidget_3);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        table_cliente = new QTableWidget(gridLayoutWidget_3);
        if (table_cliente->columnCount() < 6)
            table_cliente->setColumnCount(6);
        if (table_cliente->rowCount() < 10)
            table_cliente->setRowCount(10);
        table_cliente->setObjectName(QString::fromUtf8("table_cliente"));
        sizePolicy.setHeightForWidth(table_cliente->sizePolicy().hasHeightForWidth());
        table_cliente->setSizePolicy(sizePolicy);
        table_cliente->setAutoFillBackground(true);
        table_cliente->setFrameShape(QFrame::Box);
        table_cliente->setFrameShadow(QFrame::Raised);
        table_cliente->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        table_cliente->setSelectionBehavior(QAbstractItemView::SelectRows);
        table_cliente->setGridStyle(Qt::DotLine);
        table_cliente->setRowCount(10);
        table_cliente->setColumnCount(6);
        table_cliente->horizontalHeader()->setCascadingSectionResizes(true);
        table_cliente->horizontalHeader()->setDefaultSectionSize(193);
        table_cliente->horizontalHeader()->setStretchLastSection(true);

        gridLayout_3->addWidget(table_cliente, 1, 0, 1, 1);

        groupBox_6 = new QGroupBox(gridLayoutWidget_3);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        groupBox_6->setFont(font);
        label_4 = new QLabel(groupBox_6);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 50, 59, 27));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy2);
        txt_cliente_nome = new QTextEdit(groupBox_6);
        txt_cliente_nome->setObjectName(QString::fromUtf8("txt_cliente_nome"));
        txt_cliente_nome->setGeometry(QRect(78, 40, 1121, 30));
        txt_cliente_nome->setFont(font3);
        label_6 = new QLabel(groupBox_6);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(10, 90, 42, 27));
        sizePolicy2.setHeightForWidth(label_6->sizePolicy().hasHeightForWidth());
        label_6->setSizePolicy(sizePolicy2);
        txt_cliente_cpf = new QTextEdit(groupBox_6);
        txt_cliente_cpf->setObjectName(QString::fromUtf8("txt_cliente_cpf"));
        txt_cliente_cpf->setGeometry(QRect(80, 80, 1121, 30));
        txt_cliente_cpf->setFont(font3);

        gridLayout_3->addWidget(groupBox_6, 0, 0, 1, 1);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        btn_add_cliente = new QPushButton(gridLayoutWidget_3);
        btn_add_cliente->setObjectName(QString::fromUtf8("btn_add_cliente"));
        btn_add_cliente->setIcon(icon3);
        btn_add_cliente->setIconSize(QSize(40, 40));

        verticalLayout_4->addWidget(btn_add_cliente);

        btn_remove_cliente = new QPushButton(gridLayoutWidget_3);
        btn_remove_cliente->setObjectName(QString::fromUtf8("btn_remove_cliente"));
        btn_remove_cliente->setIcon(icon4);
        btn_remove_cliente->setIconSize(QSize(40, 40));

        verticalLayout_4->addWidget(btn_remove_cliente);

        btn_edit_cliente = new QPushButton(gridLayoutWidget_3);
        btn_edit_cliente->setObjectName(QString::fromUtf8("btn_edit_cliente"));
        btn_edit_cliente->setIcon(icon5);
        btn_edit_cliente->setIconSize(QSize(40, 40));

        verticalLayout_4->addWidget(btn_edit_cliente);


        gridLayout_3->addLayout(verticalLayout_4, 0, 1, 1, 1);

        gridLayout_3->setColumnMinimumWidth(0, 800);
        gridLayout_3->setRowMinimumHeight(0, 200);
        tabWidget->addTab(tab_4, QString());
        tab_6 = new QWidget();
        tab_6->setObjectName(QString::fromUtf8("tab_6"));
        gridLayoutWidget_4 = new QWidget(tab_6);
        gridLayoutWidget_4->setObjectName(QString::fromUtf8("gridLayoutWidget_4"));
        gridLayoutWidget_4->setGeometry(QRect(0, 0, 1301, 571));
        gridLayout_4 = new QGridLayout(gridLayoutWidget_4);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        groupBox_5 = new QGroupBox(gridLayoutWidget_4);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        groupBox_5->setFont(font);
        txt_hospede_cpf = new QTextEdit(groupBox_5);
        txt_hospede_cpf->setObjectName(QString::fromUtf8("txt_hospede_cpf"));
        txt_hospede_cpf->setGeometry(QRect(82, 80, 1121, 30));
        txt_hospede_cpf->setFont(font3);
        label_9 = new QLabel(groupBox_5);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(10, 90, 42, 27));
        sizePolicy2.setHeightForWidth(label_9->sizePolicy().hasHeightForWidth());
        label_9->setSizePolicy(sizePolicy2);
        label_8 = new QLabel(groupBox_5);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(10, 50, 59, 27));
        sizePolicy2.setHeightForWidth(label_8->sizePolicy().hasHeightForWidth());
        label_8->setSizePolicy(sizePolicy2);
        txt_hospede_nome = new QTextEdit(groupBox_5);
        txt_hospede_nome->setObjectName(QString::fromUtf8("txt_hospede_nome"));
        txt_hospede_nome->setGeometry(QRect(80, 40, 1121, 30));
        txt_hospede_nome->setFont(font3);

        gridLayout_4->addWidget(groupBox_5, 0, 0, 1, 1);

        table_hospede = new QTableWidget(gridLayoutWidget_4);
        if (table_hospede->columnCount() < 5)
            table_hospede->setColumnCount(5);
        if (table_hospede->rowCount() < 10)
            table_hospede->setRowCount(10);
        table_hospede->setObjectName(QString::fromUtf8("table_hospede"));
        sizePolicy.setHeightForWidth(table_hospede->sizePolicy().hasHeightForWidth());
        table_hospede->setSizePolicy(sizePolicy);
        table_hospede->setAutoFillBackground(true);
        table_hospede->setFrameShape(QFrame::Box);
        table_hospede->setFrameShadow(QFrame::Raised);
        table_hospede->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        table_hospede->setAlternatingRowColors(true);
        table_hospede->setSelectionBehavior(QAbstractItemView::SelectRows);
        table_hospede->setGridStyle(Qt::DotLine);
        table_hospede->setRowCount(10);
        table_hospede->setColumnCount(5);
        table_hospede->horizontalHeader()->setCascadingSectionResizes(true);
        table_hospede->horizontalHeader()->setDefaultSectionSize(235);
        table_hospede->horizontalHeader()->setStretchLastSection(true);

        gridLayout_4->addWidget(table_hospede, 1, 0, 1, 1);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        btn_add_hospede = new QPushButton(gridLayoutWidget_4);
        btn_add_hospede->setObjectName(QString::fromUtf8("btn_add_hospede"));
        btn_add_hospede->setIcon(icon3);
        btn_add_hospede->setIconSize(QSize(40, 40));

        verticalLayout_5->addWidget(btn_add_hospede);

        btn_remove_hospede = new QPushButton(gridLayoutWidget_4);
        btn_remove_hospede->setObjectName(QString::fromUtf8("btn_remove_hospede"));
        btn_remove_hospede->setIcon(icon4);
        btn_remove_hospede->setIconSize(QSize(40, 40));

        verticalLayout_5->addWidget(btn_remove_hospede);

        btn_edit_hospede = new QPushButton(gridLayoutWidget_4);
        btn_edit_hospede->setObjectName(QString::fromUtf8("btn_edit_hospede"));
        btn_edit_hospede->setIcon(icon5);
        btn_edit_hospede->setIconSize(QSize(40, 40));

        verticalLayout_5->addWidget(btn_edit_hospede);


        gridLayout_4->addLayout(verticalLayout_5, 0, 1, 1, 1);

        gridLayout_4->setRowMinimumHeight(0, 200);
        tabWidget->addTab(tab_6, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        gridLayoutWidget_2 = new QWidget(tab_2);
        gridLayoutWidget_2->setObjectName(QString::fromUtf8("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(0, 0, 1271, 561));
        gridLayout_2 = new QGridLayout(gridLayoutWidget_2);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        groupBox_4 = new QGroupBox(gridLayoutWidget_2);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setFont(font);
        label_2 = new QLabel(groupBox_4);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(0, 40, 82, 27));
        QSizePolicy sizePolicy3(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy3);
        label_7 = new QLabel(groupBox_4);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(0, 80, 156, 35));
        cbx_quarto_tipo_quarto_filtro = new QComboBox(groupBox_4);
        cbx_quarto_tipo_quarto_filtro->setObjectName(QString::fromUtf8("cbx_quarto_tipo_quarto_filtro"));
        cbx_quarto_tipo_quarto_filtro->setGeometry(QRect(180, 80, 1021, 30));
        sizePolicy1.setHeightForWidth(cbx_quarto_tipo_quarto_filtro->sizePolicy().hasHeightForWidth());
        cbx_quarto_tipo_quarto_filtro->setSizePolicy(sizePolicy1);
        cbx_quarto_tipo_quarto_filtro->setFont(font3);
        txt_quarto_numero = new QLineEdit(groupBox_4);
        txt_quarto_numero->setObjectName(QString::fromUtf8("txt_quarto_numero"));
        txt_quarto_numero->setGeometry(QRect(90, 40, 1111, 30));
        txt_quarto_numero->setFont(font3);
        chk_disponivel_higher = new QCheckBox(groupBox_4);
        chk_disponivel_higher->setObjectName(QString::fromUtf8("chk_disponivel_higher"));
        chk_disponivel_higher->setGeometry(QRect(880, 150, 16, 23));
        label_18 = new QLabel(groupBox_4);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setGeometry(QRect(150, 150, 91, 31));
        label_18->setFont(font5);
        label_16 = new QLabel(groupBox_4);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setGeometry(QRect(0, 120, 151, 31));
        dt_disponivel_higher = new QDateEdit(groupBox_4);
        dt_disponivel_higher->setObjectName(QString::fromUtf8("dt_disponivel_higher"));
        dt_disponivel_higher->setGeometry(QRect(700, 150, 171, 30));
        dt_disponivel_higher->setFont(font4);
        dt_disponivel_higher->setCalendarPopup(true);
        label_17 = new QLabel(groupBox_4);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setGeometry(QRect(600, 150, 91, 31));
        label_17->setFont(font5);
        chk_disponivel_lower = new QCheckBox(groupBox_4);
        chk_disponivel_lower->setObjectName(QString::fromUtf8("chk_disponivel_lower"));
        chk_disponivel_lower->setGeometry(QRect(450, 150, 16, 23));
        dt_disponivel_lower = new QDateEdit(groupBox_4);
        dt_disponivel_lower->setObjectName(QString::fromUtf8("dt_disponivel_lower"));
        dt_disponivel_lower->setGeometry(QRect(260, 150, 171, 30));
        dt_disponivel_lower->setFont(font4);
        dt_disponivel_lower->setCalendarPopup(true);

        gridLayout_2->addWidget(groupBox_4, 0, 0, 1, 1);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        btn_add_quarto = new QPushButton(gridLayoutWidget_2);
        btn_add_quarto->setObjectName(QString::fromUtf8("btn_add_quarto"));
        btn_add_quarto->setIcon(icon3);
        btn_add_quarto->setIconSize(QSize(40, 40));

        verticalLayout_3->addWidget(btn_add_quarto);

        btn_remove_quarto = new QPushButton(gridLayoutWidget_2);
        btn_remove_quarto->setObjectName(QString::fromUtf8("btn_remove_quarto"));
        btn_remove_quarto->setIcon(icon4);
        btn_remove_quarto->setIconSize(QSize(40, 40));

        verticalLayout_3->addWidget(btn_remove_quarto);

        btn_edit_quarto = new QPushButton(gridLayoutWidget_2);
        btn_edit_quarto->setObjectName(QString::fromUtf8("btn_edit_quarto"));
        btn_edit_quarto->setIcon(icon5);
        btn_edit_quarto->setIconSize(QSize(40, 40));

        verticalLayout_3->addWidget(btn_edit_quarto);


        gridLayout_2->addLayout(verticalLayout_3, 0, 1, 1, 1);

        table_quarto = new QTableWidget(gridLayoutWidget_2);
        if (table_quarto->columnCount() < 6)
            table_quarto->setColumnCount(6);
        if (table_quarto->rowCount() < 10)
            table_quarto->setRowCount(10);
        table_quarto->setObjectName(QString::fromUtf8("table_quarto"));
        sizePolicy.setHeightForWidth(table_quarto->sizePolicy().hasHeightForWidth());
        table_quarto->setSizePolicy(sizePolicy);
        table_quarto->setAutoFillBackground(true);
        table_quarto->setFrameShape(QFrame::Box);
        table_quarto->setFrameShadow(QFrame::Raised);
        table_quarto->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        table_quarto->setAlternatingRowColors(true);
        table_quarto->setSelectionBehavior(QAbstractItemView::SelectRows);
        table_quarto->setGridStyle(Qt::DotLine);
        table_quarto->setRowCount(10);
        table_quarto->setColumnCount(6);
        table_quarto->horizontalHeader()->setCascadingSectionResizes(true);
        table_quarto->horizontalHeader()->setDefaultSectionSize(193);
        table_quarto->horizontalHeader()->setStretchLastSection(true);

        gridLayout_2->addWidget(table_quarto, 1, 0, 1, 1);

        gridLayout_2->setColumnMinimumWidth(0, 800);
        gridLayout_2->setRowMinimumHeight(0, 200);
        tabWidget->addTab(tab_2, QString());
        tab_5 = new QWidget();
        tab_5->setObjectName(QString::fromUtf8("tab_5"));
        gridLayoutWidget = new QWidget(tab_5);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 0, 1291, 561));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        btn_add_tipoquarto = new QPushButton(gridLayoutWidget);
        btn_add_tipoquarto->setObjectName(QString::fromUtf8("btn_add_tipoquarto"));
        btn_add_tipoquarto->setIcon(icon3);
        btn_add_tipoquarto->setIconSize(QSize(40, 40));

        verticalLayout_2->addWidget(btn_add_tipoquarto);

        btn_remove_tipoquarto = new QPushButton(gridLayoutWidget);
        btn_remove_tipoquarto->setObjectName(QString::fromUtf8("btn_remove_tipoquarto"));
        btn_remove_tipoquarto->setIcon(icon4);
        btn_remove_tipoquarto->setIconSize(QSize(40, 40));

        verticalLayout_2->addWidget(btn_remove_tipoquarto);

        btn_edit_tipoquarto = new QPushButton(gridLayoutWidget);
        btn_edit_tipoquarto->setObjectName(QString::fromUtf8("btn_edit_tipoquarto"));
        btn_edit_tipoquarto->setIcon(icon5);
        btn_edit_tipoquarto->setIconSize(QSize(40, 40));

        verticalLayout_2->addWidget(btn_edit_tipoquarto);


        gridLayout->addLayout(verticalLayout_2, 1, 1, 1, 1);

        groupBox_3 = new QGroupBox(gridLayoutWidget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setFont(font);
        label = new QLabel(groupBox_3);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 40, 67, 31));
        txt_tipo_quarto_nome = new QLineEdit(groupBox_3);
        txt_tipo_quarto_nome->setObjectName(QString::fromUtf8("txt_tipo_quarto_nome"));
        txt_tipo_quarto_nome->setGeometry(QRect(80, 40, 1111, 30));
        txt_tipo_quarto_nome->setFont(font3);

        gridLayout->addWidget(groupBox_3, 1, 0, 1, 1);

        table_tipoquarto = new QTableWidget(gridLayoutWidget);
        if (table_tipoquarto->columnCount() < 3)
            table_tipoquarto->setColumnCount(3);
        if (table_tipoquarto->rowCount() < 10)
            table_tipoquarto->setRowCount(10);
        table_tipoquarto->setObjectName(QString::fromUtf8("table_tipoquarto"));
        sizePolicy.setHeightForWidth(table_tipoquarto->sizePolicy().hasHeightForWidth());
        table_tipoquarto->setSizePolicy(sizePolicy);
        table_tipoquarto->setAutoFillBackground(true);
        table_tipoquarto->setFrameShape(QFrame::Box);
        table_tipoquarto->setFrameShadow(QFrame::Raised);
        table_tipoquarto->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        table_tipoquarto->setAlternatingRowColors(true);
        table_tipoquarto->setSelectionBehavior(QAbstractItemView::SelectRows);
        table_tipoquarto->setGridStyle(Qt::DotLine);
        table_tipoquarto->setRowCount(10);
        table_tipoquarto->setColumnCount(3);
        table_tipoquarto->horizontalHeader()->setCascadingSectionResizes(true);
        table_tipoquarto->horizontalHeader()->setDefaultSectionSize(387);
        table_tipoquarto->horizontalHeader()->setStretchLastSection(true);
        table_tipoquarto->verticalHeader()->setCascadingSectionResizes(true);
        table_tipoquarto->verticalHeader()->setStretchLastSection(false);

        gridLayout->addWidget(table_tipoquarto, 2, 0, 1, 1);

        gridLayout->setColumnMinimumWidth(0, 800);
        gridLayout->setRowMinimumHeight(1, 200);
        tabWidget->addTab(tab_5, QString());
        tab_7 = new QWidget();
        tab_7->setObjectName(QString::fromUtf8("tab_7"));
        txt_log = new QTextEdit(tab_7);
        txt_log->setObjectName(QString::fromUtf8("txt_log"));
        txt_log->setGeometry(QRect(0, 0, 1301, 651));
        txt_log->setReadOnly(true);
        verticalLayoutWidget = new QWidget(tab_7);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(-1, 660, 1301, 61));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        btn_update = new QPushButton(verticalLayoutWidget);
        btn_update->setObjectName(QString::fromUtf8("btn_update"));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/images/repeat.png"), QSize(), QIcon::Normal, QIcon::Off);
        btn_update->setIcon(icon7);
        btn_update->setIconSize(QSize(50, 50));

        verticalLayout->addWidget(btn_update);

        tabWidget->addTab(tab_7, QString());

        horizontalLayout->addWidget(tabWidget);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1311, 31));
        QFont font6;
        font6.setFamily(QString::fromUtf8("URW Bookman L"));
        font6.setPointSize(15);
        font6.setBold(true);
        font6.setItalic(false);
        font6.setWeight(75);
        menuBar->setFont(font6);
        menuArquivo = new QMenu(menuBar);
        menuArquivo->setObjectName(QString::fromUtf8("menuArquivo"));
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuArquivo->menuAction());
        menuArquivo->addAction(act_salvar);
        menuArquivo->addAction(act_carregar);
        menuArquivo->addSeparator();
        menuArquivo->addAction(act_backup);
        menuArquivo->addSeparator();
        menuArquivo->addAction(act_sair);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(6);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        act_salvar->setText(QApplication::translate("MainWindow", "Salvar..", nullptr));
        act_sair->setText(QApplication::translate("MainWindow", "Sair", nullptr));
        act_backup->setText(QApplication::translate("MainWindow", "Carregar backup", nullptr));
        act_carregar->setText(QApplication::translate("MainWindow", "Carregar..", nullptr));
        groupBox->setTitle(QApplication::translate("MainWindow", "Check-ins", nullptr));
        btn_checkin->setText(QString());
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Check-outs", nullptr));
        btn_checkout->setText(QString());
        lbl_date->setText(QApplication::translate("MainWindow", "Data", nullptr));
        lbl_time->setText(QApplication::translate("MainWindow", "Hora", nullptr));
        groupBox_8->setTitle(QApplication::translate("MainWindow", "Confirma\303\247\303\265es", nullptr));
        btn_confirma->setText(QString());
        btn_add_reserva_inicial->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "Inicial", nullptr));
        btn_add_reserva->setText(QString());
        btn_remove_reserva->setText(QString());
        btn_edit_reserva->setText(QString());
        btn_print_reserva->setText(QString());
        groupBox_7->setTitle(QApplication::translate("MainWindow", "Pesquisa", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "Cliente", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "Data de In\303\255cio", nullptr));
        label_11->setText(QApplication::translate("MainWindow", "Data do Fim", nullptr));
        label_12->setText(QApplication::translate("MainWindow", "Depois de", nullptr));
        label_13->setText(QApplication::translate("MainWindow", "Antes de", nullptr));
        label_14->setText(QApplication::translate("MainWindow", "Depois de", nullptr));
        label_15->setText(QApplication::translate("MainWindow", "Antes de", nullptr));
        chk_inicio_lower->setText(QString());
        chk_fim_lower->setText(QString());
        chk_inicio_higher->setText(QString());
        chk_fim_higher->setText(QString());
        label_19->setText(QApplication::translate("MainWindow", "Quarto", nullptr));
        label_20->setText(QApplication::translate("MainWindow", "Status Reserva", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("MainWindow", "Reservas", nullptr));
        groupBox_6->setTitle(QApplication::translate("MainWindow", "Pesquisa", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "Nome", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "CPF", nullptr));
        btn_add_cliente->setText(QString());
        btn_remove_cliente->setText(QString());
        btn_edit_cliente->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("MainWindow", "Clientes", nullptr));
        groupBox_5->setTitle(QApplication::translate("MainWindow", "Pesquisa", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "CPF", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "Nome", nullptr));
        btn_add_hospede->setText(QString());
        btn_remove_hospede->setText(QString());
        btn_edit_hospede->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_6), QApplication::translate("MainWindow", "H\303\263spedes", nullptr));
        groupBox_4->setTitle(QApplication::translate("MainWindow", "Pesquisa", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "N\303\272mero", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "Tipo de Quarto", nullptr));
        chk_disponivel_higher->setText(QString());
        label_18->setText(QApplication::translate("MainWindow", "Come\303\247o", nullptr));
        label_16->setText(QApplication::translate("MainWindow", "Dispon\303\255vel", nullptr));
        label_17->setText(QApplication::translate("MainWindow", "Fim", nullptr));
        chk_disponivel_lower->setText(QString());
        btn_add_quarto->setText(QString());
        btn_remove_quarto->setText(QString());
        btn_edit_quarto->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "Quartos", nullptr));
        btn_add_tipoquarto->setText(QString());
        btn_remove_tipoquarto->setText(QString());
        btn_edit_tipoquarto->setText(QString());
        groupBox_3->setTitle(QApplication::translate("MainWindow", "Pesquisa", nullptr));
        label->setText(QApplication::translate("MainWindow", "Nome", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_5), QApplication::translate("MainWindow", "Tipos de Quarto", nullptr));
        btn_update->setText(QString());
#ifndef QT_NO_SHORTCUT
        btn_update->setShortcut(QApplication::translate("MainWindow", "F5", nullptr));
#endif // QT_NO_SHORTCUT
        tabWidget->setTabText(tabWidget->indexOf(tab_7), QApplication::translate("MainWindow", "Log", nullptr));
        menuArquivo->setTitle(QApplication::translate("MainWindow", "Arquivo", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
