/********************************************************************************
** Form generated from reading UI file 'quartodialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QUARTODIALOG_H
#define UI_QUARTODIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QuartoDialog
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QTextEdit *txt_nome;
    QLabel *label_2;
    QComboBox *cbx_tipo_quarto;
    QLabel *label_3;
    QSpinBox *spn_PAX;
    QCheckBox *chk_disp;
    QTextEdit *txt_descricao;
    QLabel *label_4;

    void setupUi(QDialog *QuartoDialog)
    {
        if (QuartoDialog->objectName().isEmpty())
            QuartoDialog->setObjectName(QString::fromUtf8("QuartoDialog"));
        QuartoDialog->resize(676, 638);
        buttonBox = new QDialogButtonBox(QuartoDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(0, 600, 671, 32));
        QFont font;
        font.setFamily(QString::fromUtf8("URW Bookman L"));
        font.setPointSize(15);
        font.setBold(false);
        font.setWeight(50);
        buttonBox->setFont(font);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        formLayoutWidget = new QWidget(QuartoDialog);
        formLayoutWidget->setObjectName(QString::fromUtf8("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(10, 10, 659, 581));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::ExpandingFieldsGrow);
        formLayout->setLabelAlignment(Qt::AlignCenter);
        formLayout->setFormAlignment(Qt::AlignHCenter|Qt::AlignTop);
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(formLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("URW Bookman L"));
        font1.setPointSize(15);
        font1.setBold(true);
        font1.setWeight(75);
        label->setFont(font1);

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        txt_nome = new QTextEdit(formLayoutWidget);
        txt_nome->setObjectName(QString::fromUtf8("txt_nome"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(txt_nome->sizePolicy().hasHeightForWidth());
        txt_nome->setSizePolicy(sizePolicy);
        txt_nome->setMinimumSize(QSize(495, 0));
        txt_nome->setMaximumSize(QSize(495, 40));
        QFont font2;
        font2.setFamily(QString::fromUtf8("URW Bookman L"));
        font2.setPointSize(12);
        txt_nome->setFont(font2);

        formLayout->setWidget(0, QFormLayout::FieldRole, txt_nome);

        label_2 = new QLabel(formLayoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font1);

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        cbx_tipo_quarto = new QComboBox(formLayoutWidget);
        cbx_tipo_quarto->setObjectName(QString::fromUtf8("cbx_tipo_quarto"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(cbx_tipo_quarto->sizePolicy().hasHeightForWidth());
        cbx_tipo_quarto->setSizePolicy(sizePolicy1);

        formLayout->setWidget(1, QFormLayout::FieldRole, cbx_tipo_quarto);

        label_3 = new QLabel(formLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font1);

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        spn_PAX = new QSpinBox(formLayoutWidget);
        spn_PAX->setObjectName(QString::fromUtf8("spn_PAX"));
        sizePolicy1.setHeightForWidth(spn_PAX->sizePolicy().hasHeightForWidth());
        spn_PAX->setSizePolicy(sizePolicy1);
        spn_PAX->setMaximumSize(QSize(200, 16777215));
        spn_PAX->setAlignment(Qt::AlignCenter);

        formLayout->setWidget(2, QFormLayout::FieldRole, spn_PAX);

        chk_disp = new QCheckBox(formLayoutWidget);
        chk_disp->setObjectName(QString::fromUtf8("chk_disp"));
        chk_disp->setFont(font1);
        chk_disp->setContextMenuPolicy(Qt::CustomContextMenu);
        chk_disp->setAcceptDrops(true);
        chk_disp->setLayoutDirection(Qt::LeftToRight);

        formLayout->setWidget(3, QFormLayout::FieldRole, chk_disp);

        txt_descricao = new QTextEdit(formLayoutWidget);
        txt_descricao->setObjectName(QString::fromUtf8("txt_descricao"));
        txt_descricao->setMinimumSize(QSize(0, 420));
        QFont font3;
        font3.setPointSize(12);
        font3.setBold(false);
        font3.setWeight(50);
        txt_descricao->setFont(font3);

        formLayout->setWidget(4, QFormLayout::FieldRole, txt_descricao);

        label_4 = new QLabel(formLayoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font1);

        formLayout->setWidget(4, QFormLayout::LabelRole, label_4);


        retranslateUi(QuartoDialog);

        QMetaObject::connectSlotsByName(QuartoDialog);
    } // setupUi

    void retranslateUi(QDialog *QuartoDialog)
    {
        QuartoDialog->setWindowTitle(QApplication::translate("QuartoDialog", "Dialog", nullptr));
        label->setText(QApplication::translate("QuartoDialog", "N\303\272mero", nullptr));
        label_2->setText(QApplication::translate("QuartoDialog", "Tipo de Quarto", nullptr));
        label_3->setText(QApplication::translate("QuartoDialog", "PAX", nullptr));
        chk_disp->setText(QApplication::translate("QuartoDialog", "Disponibilidade", nullptr));
        label_4->setText(QApplication::translate("QuartoDialog", "Descri\303\247\303\243o", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QuartoDialog: public Ui_QuartoDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QUARTODIALOG_H
