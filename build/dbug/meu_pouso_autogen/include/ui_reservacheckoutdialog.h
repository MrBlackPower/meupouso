/********************************************************************************
** Form generated from reading UI file 'reservacheckoutdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RESERVACHECKOUTDIALOG_H
#define UI_RESERVACHECKOUTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDateTimeEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ReservaCheckoutDialog
{
public:
    QDialogButtonBox *buttonBox;
    QLabel *label_4;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QDateTimeEdit *dt_current;
    QDateTimeEdit *dt_insert;
    QLabel *label_9;
    QListWidget *lst_hospedes;

    void setupUi(QDialog *ReservaCheckoutDialog)
    {
        if (ReservaCheckoutDialog->objectName().isEmpty())
            ReservaCheckoutDialog->setObjectName(QString::fromUtf8("ReservaCheckoutDialog"));
        ReservaCheckoutDialog->resize(625, 468);
        buttonBox = new QDialogButtonBox(ReservaCheckoutDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(10, 420, 601, 40));
        buttonBox->setMinimumSize(QSize(0, 40));
        QFont font;
        font.setFamily(QString::fromUtf8("URW Bookman L"));
        font.setPointSize(15);
        font.setBold(true);
        font.setWeight(75);
        buttonBox->setFont(font);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        label_4 = new QLabel(ReservaCheckoutDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 0, 81, 40));
        label_4->setFont(font);
        gridLayoutWidget = new QWidget(ReservaCheckoutDialog);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 40, 601, 80));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        radioButton = new QRadioButton(gridLayoutWidget);
        radioButton->setObjectName(QString::fromUtf8("radioButton"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("URW Bookman L"));
        font1.setPointSize(15);
        radioButton->setFont(font1);
        radioButton->setAutoFillBackground(false);
        radioButton->setChecked(true);

        gridLayout->addWidget(radioButton, 0, 0, 1, 1);

        radioButton_2 = new QRadioButton(gridLayoutWidget);
        radioButton_2->setObjectName(QString::fromUtf8("radioButton_2"));
        radioButton_2->setFont(font1);

        gridLayout->addWidget(radioButton_2, 0, 1, 1, 1);

        dt_current = new QDateTimeEdit(gridLayoutWidget);
        dt_current->setObjectName(QString::fromUtf8("dt_current"));
        dt_current->setCalendarPopup(true);

        gridLayout->addWidget(dt_current, 1, 0, 1, 1);

        dt_insert = new QDateTimeEdit(gridLayoutWidget);
        dt_insert->setObjectName(QString::fromUtf8("dt_insert"));
        dt_insert->setCalendarPopup(true);

        gridLayout->addWidget(dt_insert, 1, 1, 1, 1);

        label_9 = new QLabel(ReservaCheckoutDialog);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(10, 120, 111, 40));
        label_9->setFont(font);
        lst_hospedes = new QListWidget(ReservaCheckoutDialog);
        lst_hospedes->setObjectName(QString::fromUtf8("lst_hospedes"));
        lst_hospedes->setGeometry(QRect(10, 160, 601, 251));

        retranslateUi(ReservaCheckoutDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ReservaCheckoutDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ReservaCheckoutDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ReservaCheckoutDialog);
    } // setupUi

    void retranslateUi(QDialog *ReservaCheckoutDialog)
    {
        ReservaCheckoutDialog->setWindowTitle(QApplication::translate("ReservaCheckoutDialog", "Dialog", nullptr));
        label_4->setText(QApplication::translate("ReservaCheckoutDialog", "Hor\303\241rio", nullptr));
        radioButton->setText(QApplication::translate("ReservaCheckoutDialog", "Hor\303\241rio Atual", nullptr));
        radioButton_2->setText(QApplication::translate("ReservaCheckoutDialog", "Inserir", nullptr));
        label_9->setText(QApplication::translate("ReservaCheckoutDialog", "H\303\263spedes", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ReservaCheckoutDialog: public Ui_ReservaCheckoutDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RESERVACHECKOUTDIALOG_H
