/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[83];
    char stringdata0[2210];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 11), // "update_time"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 13), // "update_tables"
QT_MOC_LITERAL(4, 38, 22), // "update_tipoquarto_list"
QT_MOC_LITERAL(5, 61, 9), // "do_backup"
QT_MOC_LITERAL(6, 71, 10), // "update_log"
QT_MOC_LITERAL(7, 82, 16), // "confirma_reserva"
QT_MOC_LITERAL(8, 99, 2), // "id"
QT_MOC_LITERAL(9, 102, 29), // "on_btn_add_tipoquarto_clicked"
QT_MOC_LITERAL(10, 132, 30), // "on_btn_edit_tipoquarto_clicked"
QT_MOC_LITERAL(11, 163, 35), // "on_txt_tipo_quarto_nome_textC..."
QT_MOC_LITERAL(12, 199, 32), // "on_btn_remove_tipoquarto_clicked"
QT_MOC_LITERAL(13, 232, 37), // "on_table_tipoquarto_itemDoubl..."
QT_MOC_LITERAL(14, 270, 17), // "QTableWidgetItem*"
QT_MOC_LITERAL(15, 288, 4), // "item"
QT_MOC_LITERAL(16, 293, 25), // "on_btn_add_quarto_clicked"
QT_MOC_LITERAL(17, 319, 28), // "on_btn_remove_quarto_clicked"
QT_MOC_LITERAL(18, 348, 26), // "on_btn_edit_quarto_clicked"
QT_MOC_LITERAL(19, 375, 51), // "on_cbx_quarto_tipo_quarto_fil..."
QT_MOC_LITERAL(20, 427, 4), // "arg1"
QT_MOC_LITERAL(21, 432, 26), // "on_btn_add_cliente_clicked"
QT_MOC_LITERAL(22, 459, 29), // "on_btn_remove_cliente_clicked"
QT_MOC_LITERAL(23, 489, 27), // "on_btn_edit_cliente_clicked"
QT_MOC_LITERAL(24, 517, 30), // "on_txt_cliente_cpf_textChanged"
QT_MOC_LITERAL(25, 548, 27), // "on_table_quarto_cellPressed"
QT_MOC_LITERAL(26, 576, 3), // "row"
QT_MOC_LITERAL(27, 580, 6), // "column"
QT_MOC_LITERAL(28, 587, 31), // "on_table_tipoquarto_cellPressed"
QT_MOC_LITERAL(29, 619, 28), // "on_table_cliente_cellPressed"
QT_MOC_LITERAL(30, 648, 26), // "on_btn_add_hospede_clicked"
QT_MOC_LITERAL(31, 675, 29), // "on_btn_remove_hospede_clicked"
QT_MOC_LITERAL(32, 705, 27), // "on_btn_edit_hospede_clicked"
QT_MOC_LITERAL(33, 733, 33), // "on_table_quarto_itemDoubleCli..."
QT_MOC_LITERAL(34, 767, 31), // "on_txt_cliente_nome_textChanged"
QT_MOC_LITERAL(35, 799, 31), // "on_txt_hospede_nome_textChanged"
QT_MOC_LITERAL(36, 831, 30), // "on_txt_hospede_cpf_textChanged"
QT_MOC_LITERAL(37, 862, 26), // "on_txt_cliente_textChanged"
QT_MOC_LITERAL(38, 889, 25), // "on_txt_quarto_textChanged"
QT_MOC_LITERAL(39, 915, 27), // "on_chk_inicio_lower_clicked"
QT_MOC_LITERAL(40, 943, 28), // "on_chk_inicio_higher_clicked"
QT_MOC_LITERAL(41, 972, 34), // "on_dt_inicio_lower_userDateCh..."
QT_MOC_LITERAL(42, 1007, 4), // "date"
QT_MOC_LITERAL(43, 1012, 31), // "on_dt_inicio_higher_dateChanged"
QT_MOC_LITERAL(44, 1044, 24), // "on_chk_fim_lower_clicked"
QT_MOC_LITERAL(45, 1069, 25), // "on_chk_fim_higher_clicked"
QT_MOC_LITERAL(46, 1095, 27), // "on_dt_fim_lower_dateChanged"
QT_MOC_LITERAL(47, 1123, 32), // "on_dt_fim_higher_userDateChanged"
QT_MOC_LITERAL(48, 1156, 31), // "on_tb_confirmations_cellClicked"
QT_MOC_LITERAL(49, 1188, 37), // "on_tb_confirmations_cellDoubl..."
QT_MOC_LITERAL(50, 1226, 27), // "on_tb_check_ins_cellClicked"
QT_MOC_LITERAL(51, 1254, 33), // "on_tb_check_ins_cellDoubleCli..."
QT_MOC_LITERAL(52, 1288, 28), // "on_tb_check_outs_cellClicked"
QT_MOC_LITERAL(53, 1317, 34), // "on_tb_check_outs_cellDoubleCl..."
QT_MOC_LITERAL(54, 1352, 23), // "on_btn_confirma_clicked"
QT_MOC_LITERAL(55, 1376, 22), // "on_btn_checkin_clicked"
QT_MOC_LITERAL(56, 1399, 34), // "on_btn_add_reserva_inicial_cl..."
QT_MOC_LITERAL(57, 1434, 26), // "on_btn_add_reserva_clicked"
QT_MOC_LITERAL(58, 1461, 29), // "on_btn_remove_reserva_clicked"
QT_MOC_LITERAL(59, 1491, 27), // "on_btn_edit_reserva_clicked"
QT_MOC_LITERAL(60, 1519, 23), // "on_btn_checkout_clicked"
QT_MOC_LITERAL(61, 1543, 28), // "on_table_reserva_itemClicked"
QT_MOC_LITERAL(62, 1572, 34), // "on_table_reserva_itemDoubleCl..."
QT_MOC_LITERAL(63, 1607, 32), // "on_txt_quarto_numero_textChanged"
QT_MOC_LITERAL(64, 1640, 31), // "on_chk_disponivel_lower_clicked"
QT_MOC_LITERAL(65, 1672, 32), // "on_chk_disponivel_higher_clicked"
QT_MOC_LITERAL(66, 1705, 38), // "on_dt_disponivel_lower_userDa..."
QT_MOC_LITERAL(67, 1744, 39), // "on_dt_disponivel_higher_userD..."
QT_MOC_LITERAL(68, 1784, 23), // "on_act_backup_triggered"
QT_MOC_LITERAL(69, 1808, 25), // "on_act_carregar_triggered"
QT_MOC_LITERAL(70, 1834, 21), // "on_act_sair_triggered"
QT_MOC_LITERAL(71, 1856, 23), // "on_act_salvar_triggered"
QT_MOC_LITERAL(72, 1880, 34), // "on_table_cliente_itemDoubleCl..."
QT_MOC_LITERAL(73, 1915, 34), // "on_table_hospede_itemDoubleCl..."
QT_MOC_LITERAL(74, 1950, 28), // "on_table_hospede_cellPressed"
QT_MOC_LITERAL(75, 1979, 37), // "on_tb_confirmations_itemDoubl..."
QT_MOC_LITERAL(76, 2017, 33), // "on_tb_check_ins_itemDoubleCli..."
QT_MOC_LITERAL(77, 2051, 34), // "on_tb_check_outs_itemDoubleCl..."
QT_MOC_LITERAL(78, 2086, 28), // "on_btn_print_reserva_clicked"
QT_MOC_LITERAL(79, 2115, 40), // "on_cbx_status_reserva_current..."
QT_MOC_LITERAL(80, 2156, 23), // "on_MainWindow_destroyed"
QT_MOC_LITERAL(81, 2180, 21), // "on_btn_update_toggled"
QT_MOC_LITERAL(82, 2202, 7) // "checked"

    },
    "MainWindow\0update_time\0\0update_tables\0"
    "update_tipoquarto_list\0do_backup\0"
    "update_log\0confirma_reserva\0id\0"
    "on_btn_add_tipoquarto_clicked\0"
    "on_btn_edit_tipoquarto_clicked\0"
    "on_txt_tipo_quarto_nome_textChanged\0"
    "on_btn_remove_tipoquarto_clicked\0"
    "on_table_tipoquarto_itemDoubleClicked\0"
    "QTableWidgetItem*\0item\0on_btn_add_quarto_clicked\0"
    "on_btn_remove_quarto_clicked\0"
    "on_btn_edit_quarto_clicked\0"
    "on_cbx_quarto_tipo_quarto_filtro_currentTextChanged\0"
    "arg1\0on_btn_add_cliente_clicked\0"
    "on_btn_remove_cliente_clicked\0"
    "on_btn_edit_cliente_clicked\0"
    "on_txt_cliente_cpf_textChanged\0"
    "on_table_quarto_cellPressed\0row\0column\0"
    "on_table_tipoquarto_cellPressed\0"
    "on_table_cliente_cellPressed\0"
    "on_btn_add_hospede_clicked\0"
    "on_btn_remove_hospede_clicked\0"
    "on_btn_edit_hospede_clicked\0"
    "on_table_quarto_itemDoubleClicked\0"
    "on_txt_cliente_nome_textChanged\0"
    "on_txt_hospede_nome_textChanged\0"
    "on_txt_hospede_cpf_textChanged\0"
    "on_txt_cliente_textChanged\0"
    "on_txt_quarto_textChanged\0"
    "on_chk_inicio_lower_clicked\0"
    "on_chk_inicio_higher_clicked\0"
    "on_dt_inicio_lower_userDateChanged\0"
    "date\0on_dt_inicio_higher_dateChanged\0"
    "on_chk_fim_lower_clicked\0"
    "on_chk_fim_higher_clicked\0"
    "on_dt_fim_lower_dateChanged\0"
    "on_dt_fim_higher_userDateChanged\0"
    "on_tb_confirmations_cellClicked\0"
    "on_tb_confirmations_cellDoubleClicked\0"
    "on_tb_check_ins_cellClicked\0"
    "on_tb_check_ins_cellDoubleClicked\0"
    "on_tb_check_outs_cellClicked\0"
    "on_tb_check_outs_cellDoubleClicked\0"
    "on_btn_confirma_clicked\0on_btn_checkin_clicked\0"
    "on_btn_add_reserva_inicial_clicked\0"
    "on_btn_add_reserva_clicked\0"
    "on_btn_remove_reserva_clicked\0"
    "on_btn_edit_reserva_clicked\0"
    "on_btn_checkout_clicked\0"
    "on_table_reserva_itemClicked\0"
    "on_table_reserva_itemDoubleClicked\0"
    "on_txt_quarto_numero_textChanged\0"
    "on_chk_disponivel_lower_clicked\0"
    "on_chk_disponivel_higher_clicked\0"
    "on_dt_disponivel_lower_userDateChanged\0"
    "on_dt_disponivel_higher_userDateChanged\0"
    "on_act_backup_triggered\0"
    "on_act_carregar_triggered\0"
    "on_act_sair_triggered\0on_act_salvar_triggered\0"
    "on_table_cliente_itemDoubleClicked\0"
    "on_table_hospede_itemDoubleClicked\0"
    "on_table_hospede_cellPressed\0"
    "on_tb_confirmations_itemDoubleClicked\0"
    "on_tb_check_ins_itemDoubleClicked\0"
    "on_tb_check_outs_itemDoubleClicked\0"
    "on_btn_print_reserva_clicked\0"
    "on_cbx_status_reserva_currentTextChanged\0"
    "on_MainWindow_destroyed\0on_btn_update_toggled\0"
    "checked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      73,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  379,    2, 0x0a /* Public */,
       3,    0,  380,    2, 0x0a /* Public */,
       4,    0,  381,    2, 0x0a /* Public */,
       5,    0,  382,    2, 0x0a /* Public */,
       6,    0,  383,    2, 0x0a /* Public */,
       7,    1,  384,    2, 0x0a /* Public */,
       9,    0,  387,    2, 0x08 /* Private */,
      10,    0,  388,    2, 0x08 /* Private */,
      11,    0,  389,    2, 0x08 /* Private */,
      12,    0,  390,    2, 0x08 /* Private */,
      13,    1,  391,    2, 0x08 /* Private */,
      16,    0,  394,    2, 0x08 /* Private */,
      17,    0,  395,    2, 0x08 /* Private */,
      18,    0,  396,    2, 0x08 /* Private */,
      19,    1,  397,    2, 0x08 /* Private */,
      21,    0,  400,    2, 0x08 /* Private */,
      22,    0,  401,    2, 0x08 /* Private */,
      23,    0,  402,    2, 0x08 /* Private */,
      24,    0,  403,    2, 0x08 /* Private */,
      25,    2,  404,    2, 0x08 /* Private */,
      28,    2,  409,    2, 0x08 /* Private */,
      29,    2,  414,    2, 0x08 /* Private */,
      30,    0,  419,    2, 0x08 /* Private */,
      31,    0,  420,    2, 0x08 /* Private */,
      32,    0,  421,    2, 0x08 /* Private */,
      33,    1,  422,    2, 0x08 /* Private */,
      34,    0,  425,    2, 0x08 /* Private */,
      35,    0,  426,    2, 0x08 /* Private */,
      36,    0,  427,    2, 0x08 /* Private */,
      37,    0,  428,    2, 0x08 /* Private */,
      38,    0,  429,    2, 0x08 /* Private */,
      39,    0,  430,    2, 0x08 /* Private */,
      40,    0,  431,    2, 0x08 /* Private */,
      41,    1,  432,    2, 0x08 /* Private */,
      43,    1,  435,    2, 0x08 /* Private */,
      44,    0,  438,    2, 0x08 /* Private */,
      45,    0,  439,    2, 0x08 /* Private */,
      46,    1,  440,    2, 0x08 /* Private */,
      47,    1,  443,    2, 0x08 /* Private */,
      48,    2,  446,    2, 0x08 /* Private */,
      49,    2,  451,    2, 0x08 /* Private */,
      50,    2,  456,    2, 0x08 /* Private */,
      51,    2,  461,    2, 0x08 /* Private */,
      52,    2,  466,    2, 0x08 /* Private */,
      53,    2,  471,    2, 0x08 /* Private */,
      54,    0,  476,    2, 0x08 /* Private */,
      55,    0,  477,    2, 0x08 /* Private */,
      56,    0,  478,    2, 0x08 /* Private */,
      57,    0,  479,    2, 0x08 /* Private */,
      58,    0,  480,    2, 0x08 /* Private */,
      59,    0,  481,    2, 0x08 /* Private */,
      60,    0,  482,    2, 0x08 /* Private */,
      61,    1,  483,    2, 0x08 /* Private */,
      62,    1,  486,    2, 0x08 /* Private */,
      63,    1,  489,    2, 0x08 /* Private */,
      64,    0,  492,    2, 0x08 /* Private */,
      65,    0,  493,    2, 0x08 /* Private */,
      66,    1,  494,    2, 0x08 /* Private */,
      67,    1,  497,    2, 0x08 /* Private */,
      68,    0,  500,    2, 0x08 /* Private */,
      69,    0,  501,    2, 0x08 /* Private */,
      70,    0,  502,    2, 0x08 /* Private */,
      71,    0,  503,    2, 0x08 /* Private */,
      72,    1,  504,    2, 0x08 /* Private */,
      73,    1,  507,    2, 0x08 /* Private */,
      74,    2,  510,    2, 0x08 /* Private */,
      75,    1,  515,    2, 0x08 /* Private */,
      76,    1,  518,    2, 0x08 /* Private */,
      77,    1,  521,    2, 0x08 /* Private */,
      78,    0,  524,    2, 0x08 /* Private */,
      79,    1,  525,    2, 0x08 /* Private */,
      80,    0,  528,    2, 0x08 /* Private */,
      81,    1,  529,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 14,   15,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   26,   27,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   26,   27,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   26,   27,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 14,   15,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QDate,   42,
    QMetaType::Void, QMetaType::QDate,   42,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QDate,   42,
    QMetaType::Void, QMetaType::QDate,   42,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   26,   27,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   26,   27,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   26,   27,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   26,   27,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   26,   27,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   26,   27,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 14,   15,
    QMetaType::Void, 0x80000000 | 14,   15,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QDate,   42,
    QMetaType::Void, QMetaType::QDate,   42,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 14,   15,
    QMetaType::Void, 0x80000000 | 14,   15,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   26,   27,
    QMetaType::Void, 0x80000000 | 14,   15,
    QMetaType::Void, 0x80000000 | 14,   15,
    QMetaType::Void, 0x80000000 | 14,   15,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   82,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->update_time(); break;
        case 1: _t->update_tables(); break;
        case 2: _t->update_tipoquarto_list(); break;
        case 3: _t->do_backup(); break;
        case 4: _t->update_log(); break;
        case 5: _t->confirma_reserva((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->on_btn_add_tipoquarto_clicked(); break;
        case 7: _t->on_btn_edit_tipoquarto_clicked(); break;
        case 8: _t->on_txt_tipo_quarto_nome_textChanged(); break;
        case 9: _t->on_btn_remove_tipoquarto_clicked(); break;
        case 10: _t->on_table_tipoquarto_itemDoubleClicked((*reinterpret_cast< QTableWidgetItem*(*)>(_a[1]))); break;
        case 11: _t->on_btn_add_quarto_clicked(); break;
        case 12: _t->on_btn_remove_quarto_clicked(); break;
        case 13: _t->on_btn_edit_quarto_clicked(); break;
        case 14: _t->on_cbx_quarto_tipo_quarto_filtro_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 15: _t->on_btn_add_cliente_clicked(); break;
        case 16: _t->on_btn_remove_cliente_clicked(); break;
        case 17: _t->on_btn_edit_cliente_clicked(); break;
        case 18: _t->on_txt_cliente_cpf_textChanged(); break;
        case 19: _t->on_table_quarto_cellPressed((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 20: _t->on_table_tipoquarto_cellPressed((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 21: _t->on_table_cliente_cellPressed((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 22: _t->on_btn_add_hospede_clicked(); break;
        case 23: _t->on_btn_remove_hospede_clicked(); break;
        case 24: _t->on_btn_edit_hospede_clicked(); break;
        case 25: _t->on_table_quarto_itemDoubleClicked((*reinterpret_cast< QTableWidgetItem*(*)>(_a[1]))); break;
        case 26: _t->on_txt_cliente_nome_textChanged(); break;
        case 27: _t->on_txt_hospede_nome_textChanged(); break;
        case 28: _t->on_txt_hospede_cpf_textChanged(); break;
        case 29: _t->on_txt_cliente_textChanged(); break;
        case 30: _t->on_txt_quarto_textChanged(); break;
        case 31: _t->on_chk_inicio_lower_clicked(); break;
        case 32: _t->on_chk_inicio_higher_clicked(); break;
        case 33: _t->on_dt_inicio_lower_userDateChanged((*reinterpret_cast< const QDate(*)>(_a[1]))); break;
        case 34: _t->on_dt_inicio_higher_dateChanged((*reinterpret_cast< const QDate(*)>(_a[1]))); break;
        case 35: _t->on_chk_fim_lower_clicked(); break;
        case 36: _t->on_chk_fim_higher_clicked(); break;
        case 37: _t->on_dt_fim_lower_dateChanged((*reinterpret_cast< const QDate(*)>(_a[1]))); break;
        case 38: _t->on_dt_fim_higher_userDateChanged((*reinterpret_cast< const QDate(*)>(_a[1]))); break;
        case 39: _t->on_tb_confirmations_cellClicked((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 40: _t->on_tb_confirmations_cellDoubleClicked((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 41: _t->on_tb_check_ins_cellClicked((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 42: _t->on_tb_check_ins_cellDoubleClicked((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 43: _t->on_tb_check_outs_cellClicked((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 44: _t->on_tb_check_outs_cellDoubleClicked((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 45: _t->on_btn_confirma_clicked(); break;
        case 46: _t->on_btn_checkin_clicked(); break;
        case 47: _t->on_btn_add_reserva_inicial_clicked(); break;
        case 48: _t->on_btn_add_reserva_clicked(); break;
        case 49: _t->on_btn_remove_reserva_clicked(); break;
        case 50: _t->on_btn_edit_reserva_clicked(); break;
        case 51: _t->on_btn_checkout_clicked(); break;
        case 52: _t->on_table_reserva_itemClicked((*reinterpret_cast< QTableWidgetItem*(*)>(_a[1]))); break;
        case 53: _t->on_table_reserva_itemDoubleClicked((*reinterpret_cast< QTableWidgetItem*(*)>(_a[1]))); break;
        case 54: _t->on_txt_quarto_numero_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 55: _t->on_chk_disponivel_lower_clicked(); break;
        case 56: _t->on_chk_disponivel_higher_clicked(); break;
        case 57: _t->on_dt_disponivel_lower_userDateChanged((*reinterpret_cast< const QDate(*)>(_a[1]))); break;
        case 58: _t->on_dt_disponivel_higher_userDateChanged((*reinterpret_cast< const QDate(*)>(_a[1]))); break;
        case 59: _t->on_act_backup_triggered(); break;
        case 60: _t->on_act_carregar_triggered(); break;
        case 61: _t->on_act_sair_triggered(); break;
        case 62: _t->on_act_salvar_triggered(); break;
        case 63: _t->on_table_cliente_itemDoubleClicked((*reinterpret_cast< QTableWidgetItem*(*)>(_a[1]))); break;
        case 64: _t->on_table_hospede_itemDoubleClicked((*reinterpret_cast< QTableWidgetItem*(*)>(_a[1]))); break;
        case 65: _t->on_table_hospede_cellPressed((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 66: _t->on_tb_confirmations_itemDoubleClicked((*reinterpret_cast< QTableWidgetItem*(*)>(_a[1]))); break;
        case 67: _t->on_tb_check_ins_itemDoubleClicked((*reinterpret_cast< QTableWidgetItem*(*)>(_a[1]))); break;
        case 68: _t->on_tb_check_outs_itemDoubleClicked((*reinterpret_cast< QTableWidgetItem*(*)>(_a[1]))); break;
        case 69: _t->on_btn_print_reserva_clicked(); break;
        case 70: _t->on_cbx_status_reserva_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 71: _t->on_MainWindow_destroyed(); break;
        case 72: _t->on_btn_update_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 73)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 73;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 73)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 73;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
