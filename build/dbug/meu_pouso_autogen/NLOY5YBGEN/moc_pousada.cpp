/****************************************************************************
** Meta object code from reading C++ file 'pousada.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "model/pousada.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'pousada.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Pousada_t {
    QByteArrayData data[57];
    char stringdata0[707];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Pousada_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Pousada_t qt_meta_stringdata_Pousada = {
    {
QT_MOC_LITERAL(0, 0, 7), // "Pousada"
QT_MOC_LITERAL(1, 8, 8), // "push_log"
QT_MOC_LITERAL(2, 17, 0), // ""
QT_MOC_LITERAL(3, 18, 4), // "line"
QT_MOC_LITERAL(4, 23, 6), // "origin"
QT_MOC_LITERAL(5, 30, 7), // "updated"
QT_MOC_LITERAL(6, 38, 15), // "add_tipo_quarto"
QT_MOC_LITERAL(7, 54, 4), // "nome"
QT_MOC_LITERAL(8, 59, 9), // "descricao"
QT_MOC_LITERAL(9, 69, 16), // "edit_tipo_quarto"
QT_MOC_LITERAL(10, 86, 2), // "ID"
QT_MOC_LITERAL(11, 89, 18), // "remove_tipo_quarto"
QT_MOC_LITERAL(12, 108, 22), // "remove_all_tipo_quarto"
QT_MOC_LITERAL(13, 131, 10), // "add_quarto"
QT_MOC_LITERAL(14, 142, 6), // "numero"
QT_MOC_LITERAL(15, 149, 3), // "PAX"
QT_MOC_LITERAL(16, 153, 11), // "tipo_quarto"
QT_MOC_LITERAL(17, 165, 10), // "disponivel"
QT_MOC_LITERAL(18, 176, 11), // "edit_quarto"
QT_MOC_LITERAL(19, 188, 13), // "remove_quarto"
QT_MOC_LITERAL(20, 202, 17), // "remove_all_quarto"
QT_MOC_LITERAL(21, 220, 11), // "add_cliente"
QT_MOC_LITERAL(22, 232, 3), // "cpf"
QT_MOC_LITERAL(23, 236, 14), // "telefone_movel"
QT_MOC_LITERAL(24, 251, 13), // "telefone_fixo"
QT_MOC_LITERAL(25, 265, 8), // "Endereco"
QT_MOC_LITERAL(26, 274, 8), // "endereco"
QT_MOC_LITERAL(27, 283, 12), // "edit_cliente"
QT_MOC_LITERAL(28, 296, 14), // "remove_cliente"
QT_MOC_LITERAL(29, 311, 18), // "remove_all_cliente"
QT_MOC_LITERAL(30, 330, 11), // "add_hospede"
QT_MOC_LITERAL(31, 342, 10), // "cliente_ID"
QT_MOC_LITERAL(32, 353, 15), // "data_nascimento"
QT_MOC_LITERAL(33, 369, 12), // "edit_hospede"
QT_MOC_LITERAL(34, 382, 14), // "remove_hospede"
QT_MOC_LITERAL(35, 397, 18), // "remove_all_hospede"
QT_MOC_LITERAL(36, 416, 11), // "add_reserva"
QT_MOC_LITERAL(37, 428, 15), // "vector<QString>"
QT_MOC_LITERAL(38, 444, 9), // "quarto_ID"
QT_MOC_LITERAL(39, 454, 11), // "data_inicio"
QT_MOC_LITERAL(40, 466, 8), // "data_fim"
QT_MOC_LITERAL(41, 475, 16), // "hospedes_adultos"
QT_MOC_LITERAL(42, 492, 17), // "hospedes_criancas"
QT_MOC_LITERAL(43, 510, 12), // "edit_reserva"
QT_MOC_LITERAL(44, 523, 16), // "confirma_reserva"
QT_MOC_LITERAL(45, 540, 15), // "cancela_reserva"
QT_MOC_LITERAL(46, 556, 15), // "no_show_reserva"
QT_MOC_LITERAL(47, 572, 15), // "checkin_reserva"
QT_MOC_LITERAL(48, 588, 11), // "vector<int>"
QT_MOC_LITERAL(49, 600, 11), // "hospedes_ID"
QT_MOC_LITERAL(50, 612, 9), // "date_time"
QT_MOC_LITERAL(51, 622, 16), // "checkout_reserva"
QT_MOC_LITERAL(52, 639, 14), // "remove_reserva"
QT_MOC_LITERAL(53, 654, 18), // "remove_all_reserva"
QT_MOC_LITERAL(54, 673, 10), // "update_xml"
QT_MOC_LITERAL(55, 684, 5), // "clear"
QT_MOC_LITERAL(56, 690, 16) // "external_updated"

    },
    "Pousada\0push_log\0\0line\0origin\0updated\0"
    "add_tipo_quarto\0nome\0descricao\0"
    "edit_tipo_quarto\0ID\0remove_tipo_quarto\0"
    "remove_all_tipo_quarto\0add_quarto\0"
    "numero\0PAX\0tipo_quarto\0disponivel\0"
    "edit_quarto\0remove_quarto\0remove_all_quarto\0"
    "add_cliente\0cpf\0telefone_movel\0"
    "telefone_fixo\0Endereco\0endereco\0"
    "edit_cliente\0remove_cliente\0"
    "remove_all_cliente\0add_hospede\0"
    "cliente_ID\0data_nascimento\0edit_hospede\0"
    "remove_hospede\0remove_all_hospede\0"
    "add_reserva\0vector<QString>\0quarto_ID\0"
    "data_inicio\0data_fim\0hospedes_adultos\0"
    "hospedes_criancas\0edit_reserva\0"
    "confirma_reserva\0cancela_reserva\0"
    "no_show_reserva\0checkin_reserva\0"
    "vector<int>\0hospedes_ID\0date_time\0"
    "checkout_reserva\0remove_reserva\0"
    "remove_all_reserva\0update_xml\0clear\0"
    "external_updated"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Pousada[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      30,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,  164,    2, 0x06 /* Public */,
       5,    0,  169,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    2,  170,    2, 0x0a /* Public */,
       9,    3,  175,    2, 0x0a /* Public */,
      11,    1,  182,    2, 0x0a /* Public */,
      12,    0,  185,    2, 0x0a /* Public */,
      13,    5,  186,    2, 0x0a /* Public */,
      18,    6,  197,    2, 0x0a /* Public */,
      19,    1,  210,    2, 0x0a /* Public */,
      20,    0,  213,    2, 0x0a /* Public */,
      21,    5,  214,    2, 0x0a /* Public */,
      27,    6,  225,    2, 0x0a /* Public */,
      28,    1,  238,    2, 0x0a /* Public */,
      29,    0,  241,    2, 0x0a /* Public */,
      30,    4,  242,    2, 0x0a /* Public */,
      33,    5,  251,    2, 0x0a /* Public */,
      34,    1,  262,    2, 0x0a /* Public */,
      35,    0,  265,    2, 0x0a /* Public */,
      36,    6,  266,    2, 0x0a /* Public */,
      43,    3,  279,    2, 0x0a /* Public */,
      44,    1,  286,    2, 0x0a /* Public */,
      45,    1,  289,    2, 0x0a /* Public */,
      46,    1,  292,    2, 0x0a /* Public */,
      47,    3,  295,    2, 0x0a /* Public */,
      51,    2,  302,    2, 0x0a /* Public */,
      52,    1,  307,    2, 0x0a /* Public */,
      53,    0,  310,    2, 0x0a /* Public */,
      54,    0,  311,    2, 0x0a /* Public */,
      55,    0,  312,    2, 0x0a /* Public */,
      56,    0,  313,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    3,    4,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    7,    8,
    QMetaType::Void, QMetaType::Int, QMetaType::QString, QMetaType::QString,   10,    7,    8,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::Int, QMetaType::QString, QMetaType::Bool,   14,    8,   15,   16,   17,
    QMetaType::Void, QMetaType::Int, QMetaType::QString, QMetaType::QString, QMetaType::Int, QMetaType::QString, QMetaType::Bool,   10,   14,    8,   15,   16,   17,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, 0x80000000 | 25,    7,   22,   23,   24,   26,
    QMetaType::Void, QMetaType::Int, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, 0x80000000 | 25,   10,    7,   22,   23,   24,   26,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QDate, QMetaType::QString,   31,    7,   32,   22,
    QMetaType::Void, QMetaType::Int, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QDate,   10,   31,    7,   22,   32,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 37, QMetaType::QDate, QMetaType::QDate, QMetaType::Int, QMetaType::Int,   31,   38,   39,   40,   41,   42,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int,   10,   41,   42,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 48, QMetaType::QDateTime,   10,   49,   50,
    QMetaType::Void, QMetaType::Int, QMetaType::QDateTime,   10,   50,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Pousada::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Pousada *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->push_log((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 1: _t->updated(); break;
        case 2: _t->add_tipo_quarto((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 3: _t->edit_tipo_quarto((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3]))); break;
        case 4: _t->remove_tipo_quarto((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->remove_all_tipo_quarto(); break;
        case 6: _t->add_quarto((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])),(*reinterpret_cast< bool(*)>(_a[5]))); break;
        case 7: _t->edit_quarto((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< QString(*)>(_a[5])),(*reinterpret_cast< bool(*)>(_a[6]))); break;
        case 8: _t->remove_quarto((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->remove_all_quarto(); break;
        case 10: _t->add_cliente((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])),(*reinterpret_cast< Endereco(*)>(_a[5]))); break;
        case 11: _t->edit_cliente((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])),(*reinterpret_cast< QString(*)>(_a[5])),(*reinterpret_cast< Endereco(*)>(_a[6]))); break;
        case 12: _t->remove_cliente((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->remove_all_cliente(); break;
        case 14: _t->add_hospede((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QDate(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4]))); break;
        case 15: _t->edit_hospede((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])),(*reinterpret_cast< QDate(*)>(_a[5]))); break;
        case 16: _t->remove_hospede((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->remove_all_hospede(); break;
        case 18: _t->add_reserva((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< vector<QString>(*)>(_a[2])),(*reinterpret_cast< QDate(*)>(_a[3])),(*reinterpret_cast< QDate(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6]))); break;
        case 19: _t->edit_reserva((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 20: _t->confirma_reserva((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 21: _t->cancela_reserva((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 22: _t->no_show_reserva((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 23: _t->checkin_reserva((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< vector<int>(*)>(_a[2])),(*reinterpret_cast< QDateTime(*)>(_a[3]))); break;
        case 24: _t->checkout_reserva((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QDateTime(*)>(_a[2]))); break;
        case 25: _t->remove_reserva((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 26: _t->remove_all_reserva(); break;
        case 27: _t->update_xml(); break;
        case 28: _t->clear(); break;
        case 29: _t->external_updated(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (Pousada::*)(QString , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Pousada::push_log)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (Pousada::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Pousada::updated)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Pousada::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_Pousada.data,
    qt_meta_data_Pousada,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Pousada::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Pousada::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Pousada.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Pousada::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 30)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 30;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 30)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 30;
    }
    return _id;
}

// SIGNAL 0
void Pousada::push_log(QString _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Pousada::updated()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
