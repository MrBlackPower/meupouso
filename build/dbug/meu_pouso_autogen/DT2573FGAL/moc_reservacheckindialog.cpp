/****************************************************************************
** Meta object code from reading C++ file 'reservacheckindialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "window/reservacheckindialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'reservacheckindialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ReservaCheckinDialog_t {
    QByteArrayData data[22];
    char stringdata0[414];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ReservaCheckinDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ReservaCheckinDialog_t qt_meta_stringdata_ReservaCheckinDialog = {
    {
QT_MOC_LITERAL(0, 0, 20), // "ReservaCheckinDialog"
QT_MOC_LITERAL(1, 21, 15), // "reserva_checkin"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 10), // "reserva_ID"
QT_MOC_LITERAL(4, 49, 11), // "vector<int>"
QT_MOC_LITERAL(5, 61, 8), // "hospedes"
QT_MOC_LITERAL(6, 70, 8), // "datetime"
QT_MOC_LITERAL(7, 79, 19), // "update_current_time"
QT_MOC_LITERAL(8, 99, 15), // "update_hospedes"
QT_MOC_LITERAL(9, 115, 22), // "on_radioButton_clicked"
QT_MOC_LITERAL(10, 138, 24), // "on_radioButton_2_clicked"
QT_MOC_LITERAL(11, 163, 25), // "on_dt_current_dateChanged"
QT_MOC_LITERAL(12, 189, 4), // "date"
QT_MOC_LITERAL(13, 194, 24), // "on_dt_insert_dateChanged"
QT_MOC_LITERAL(14, 219, 38), // "on_lst_hospedes_disp_itemDoub..."
QT_MOC_LITERAL(15, 258, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(16, 275, 4), // "item"
QT_MOC_LITERAL(17, 280, 39), // "on_lst_hospedes_selec_itemDou..."
QT_MOC_LITERAL(18, 320, 26), // "on_btn_add_hospede_clicked"
QT_MOC_LITERAL(19, 347, 21), // "on_buttonBox_accepted"
QT_MOC_LITERAL(20, 369, 21), // "on_buttonBox_rejected"
QT_MOC_LITERAL(21, 391, 22) // "on_btn_refresh_clicked"

    },
    "ReservaCheckinDialog\0reserva_checkin\0"
    "\0reserva_ID\0vector<int>\0hospedes\0"
    "datetime\0update_current_time\0"
    "update_hospedes\0on_radioButton_clicked\0"
    "on_radioButton_2_clicked\0"
    "on_dt_current_dateChanged\0date\0"
    "on_dt_insert_dateChanged\0"
    "on_lst_hospedes_disp_itemDoubleClicked\0"
    "QListWidgetItem*\0item\0"
    "on_lst_hospedes_selec_itemDoubleClicked\0"
    "on_btn_add_hospede_clicked\0"
    "on_buttonBox_accepted\0on_buttonBox_rejected\0"
    "on_btn_refresh_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ReservaCheckinDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,   79,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    0,   86,    2, 0x0a /* Public */,
       8,    0,   87,    2, 0x0a /* Public */,
       9,    0,   88,    2, 0x08 /* Private */,
      10,    0,   89,    2, 0x08 /* Private */,
      11,    1,   90,    2, 0x08 /* Private */,
      13,    1,   93,    2, 0x08 /* Private */,
      14,    1,   96,    2, 0x08 /* Private */,
      17,    1,   99,    2, 0x08 /* Private */,
      18,    0,  102,    2, 0x08 /* Private */,
      19,    0,  103,    2, 0x08 /* Private */,
      20,    0,  104,    2, 0x08 /* Private */,
      21,    0,  105,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int, 0x80000000 | 4, QMetaType::QDateTime,    3,    5,    6,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QDate,   12,
    QMetaType::Void, QMetaType::QDate,   12,
    QMetaType::Void, 0x80000000 | 15,   16,
    QMetaType::Void, 0x80000000 | 15,   16,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ReservaCheckinDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ReservaCheckinDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->reserva_checkin((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< vector<int>(*)>(_a[2])),(*reinterpret_cast< QDateTime(*)>(_a[3]))); break;
        case 1: _t->update_current_time(); break;
        case 2: _t->update_hospedes(); break;
        case 3: _t->on_radioButton_clicked(); break;
        case 4: _t->on_radioButton_2_clicked(); break;
        case 5: _t->on_dt_current_dateChanged((*reinterpret_cast< const QDate(*)>(_a[1]))); break;
        case 6: _t->on_dt_insert_dateChanged((*reinterpret_cast< const QDate(*)>(_a[1]))); break;
        case 7: _t->on_lst_hospedes_disp_itemDoubleClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 8: _t->on_lst_hospedes_selec_itemDoubleClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 9: _t->on_btn_add_hospede_clicked(); break;
        case 10: _t->on_buttonBox_accepted(); break;
        case 11: _t->on_buttonBox_rejected(); break;
        case 12: _t->on_btn_refresh_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (ReservaCheckinDialog::*)(int , vector<int> , QDateTime );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ReservaCheckinDialog::reserva_checkin)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ReservaCheckinDialog::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_ReservaCheckinDialog.data,
    qt_meta_data_ReservaCheckinDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ReservaCheckinDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ReservaCheckinDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ReservaCheckinDialog.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int ReservaCheckinDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void ReservaCheckinDialog::reserva_checkin(int _t1, vector<int> _t2, QDateTime _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
