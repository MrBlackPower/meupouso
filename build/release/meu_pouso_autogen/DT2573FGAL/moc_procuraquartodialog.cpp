/****************************************************************************
** Meta object code from reading C++ file 'procuraquartodialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "window/procuraquartodialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'procuraquartodialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ProcuraQuartoDialog_t {
    QByteArrayData data[20];
    char stringdata0[348];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ProcuraQuartoDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ProcuraQuartoDialog_t qt_meta_stringdata_ProcuraQuartoDialog = {
    {
QT_MOC_LITERAL(0, 0, 19), // "ProcuraQuartoDialog"
QT_MOC_LITERAL(1, 20, 12), // "chose_quarto"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 2), // "id"
QT_MOC_LITERAL(4, 37, 6), // "numero"
QT_MOC_LITERAL(5, 44, 3), // "PAX"
QT_MOC_LITERAL(6, 48, 27), // "on_dt_lower_userDateChanged"
QT_MOC_LITERAL(7, 76, 4), // "date"
QT_MOC_LITERAL(8, 81, 28), // "on_dt_higher_userDateChanged"
QT_MOC_LITERAL(9, 110, 25), // "on_txt_numero_textChanged"
QT_MOC_LITERAL(10, 136, 37), // "on_cbx_tipo_quarto_currentTex..."
QT_MOC_LITERAL(11, 174, 4), // "arg1"
QT_MOC_LITERAL(12, 179, 20), // "on_chk_lower_clicked"
QT_MOC_LITERAL(13, 200, 21), // "on_chk_higher_clicked"
QT_MOC_LITERAL(14, 222, 21), // "on_buttonBox_accepted"
QT_MOC_LITERAL(15, 244, 21), // "on_buttonBox_rejected"
QT_MOC_LITERAL(16, 266, 32), // "on_lst_quartos_itemDoubleClicked"
QT_MOC_LITERAL(17, 299, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(18, 316, 4), // "item"
QT_MOC_LITERAL(19, 321, 26) // "on_lst_quartos_itemClicked"

    },
    "ProcuraQuartoDialog\0chose_quarto\0\0id\0"
    "numero\0PAX\0on_dt_lower_userDateChanged\0"
    "date\0on_dt_higher_userDateChanged\0"
    "on_txt_numero_textChanged\0"
    "on_cbx_tipo_quarto_currentTextChanged\0"
    "arg1\0on_chk_lower_clicked\0"
    "on_chk_higher_clicked\0on_buttonBox_accepted\0"
    "on_buttonBox_rejected\0"
    "on_lst_quartos_itemDoubleClicked\0"
    "QListWidgetItem*\0item\0on_lst_quartos_itemClicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ProcuraQuartoDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,   69,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    1,   76,    2, 0x08 /* Private */,
       8,    1,   79,    2, 0x08 /* Private */,
       9,    0,   82,    2, 0x08 /* Private */,
      10,    1,   83,    2, 0x08 /* Private */,
      12,    0,   86,    2, 0x08 /* Private */,
      13,    0,   87,    2, 0x08 /* Private */,
      14,    0,   88,    2, 0x08 /* Private */,
      15,    0,   89,    2, 0x08 /* Private */,
      16,    1,   90,    2, 0x08 /* Private */,
      19,    1,   93,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::Int,    3,    4,    5,

 // slots: parameters
    QMetaType::Void, QMetaType::QDate,    7,
    QMetaType::Void, QMetaType::QDate,    7,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 17,   18,
    QMetaType::Void, 0x80000000 | 17,   18,

       0        // eod
};

void ProcuraQuartoDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ProcuraQuartoDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->chose_quarto((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 1: _t->on_dt_lower_userDateChanged((*reinterpret_cast< const QDate(*)>(_a[1]))); break;
        case 2: _t->on_dt_higher_userDateChanged((*reinterpret_cast< const QDate(*)>(_a[1]))); break;
        case 3: _t->on_txt_numero_textChanged(); break;
        case 4: _t->on_cbx_tipo_quarto_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->on_chk_lower_clicked(); break;
        case 6: _t->on_chk_higher_clicked(); break;
        case 7: _t->on_buttonBox_accepted(); break;
        case 8: _t->on_buttonBox_rejected(); break;
        case 9: _t->on_lst_quartos_itemDoubleClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 10: _t->on_lst_quartos_itemClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (ProcuraQuartoDialog::*)(QString , QString , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProcuraQuartoDialog::chose_quarto)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ProcuraQuartoDialog::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_ProcuraQuartoDialog.data,
    qt_meta_data_ProcuraQuartoDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ProcuraQuartoDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ProcuraQuartoDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ProcuraQuartoDialog.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int ProcuraQuartoDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void ProcuraQuartoDialog::chose_quarto(QString _t1, QString _t2, int _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
