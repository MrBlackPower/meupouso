/****************************************************************************
** Meta object code from reading C++ file 'clientedialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "window/clientedialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'clientedialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ClienteDialog_t {
    QByteArrayData data[24];
    char stringdata0[449];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ClienteDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ClienteDialog_t qt_meta_stringdata_ClienteDialog = {
    {
QT_MOC_LITERAL(0, 0, 13), // "ClienteDialog"
QT_MOC_LITERAL(1, 14, 14), // "create_cliente"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 4), // "nome"
QT_MOC_LITERAL(4, 35, 3), // "cpf"
QT_MOC_LITERAL(5, 39, 14), // "telefone_movel"
QT_MOC_LITERAL(6, 54, 13), // "telefone_fixo"
QT_MOC_LITERAL(7, 68, 8), // "Endereco"
QT_MOC_LITERAL(8, 77, 8), // "endereco"
QT_MOC_LITERAL(9, 86, 12), // "edit_cliente"
QT_MOC_LITERAL(10, 99, 2), // "ID"
QT_MOC_LITERAL(11, 102, 14), // "remove_cliente"
QT_MOC_LITERAL(12, 117, 23), // "on_txt_nome_textChanged"
QT_MOC_LITERAL(13, 141, 33), // "on_txt_telefone_movel_textCha..."
QT_MOC_LITERAL(14, 175, 4), // "arg1"
QT_MOC_LITERAL(15, 180, 32), // "on_txt_telefone_fixo_textChanged"
QT_MOC_LITERAL(16, 213, 31), // "on_txt_endereco_rua_textChanged"
QT_MOC_LITERAL(17, 245, 22), // "on_txt_CEP_textChanged"
QT_MOC_LITERAL(18, 268, 34), // "on_txt_endereco_bairro_textCh..."
QT_MOC_LITERAL(19, 303, 34), // "on_txt_endereco_estado_textCh..."
QT_MOC_LITERAL(20, 338, 30), // "on_cbx_pais_currentTextChanged"
QT_MOC_LITERAL(21, 369, 21), // "on_buttonBox_accepted"
QT_MOC_LITERAL(22, 391, 34), // "on_txt_endereco_cidade_textCh..."
QT_MOC_LITERAL(23, 426, 22) // "on_txt_cpf_textChanged"

    },
    "ClienteDialog\0create_cliente\0\0nome\0"
    "cpf\0telefone_movel\0telefone_fixo\0"
    "Endereco\0endereco\0edit_cliente\0ID\0"
    "remove_cliente\0on_txt_nome_textChanged\0"
    "on_txt_telefone_movel_textChanged\0"
    "arg1\0on_txt_telefone_fixo_textChanged\0"
    "on_txt_endereco_rua_textChanged\0"
    "on_txt_CEP_textChanged\0"
    "on_txt_endereco_bairro_textChanged\0"
    "on_txt_endereco_estado_textChanged\0"
    "on_cbx_pais_currentTextChanged\0"
    "on_buttonBox_accepted\0"
    "on_txt_endereco_cidade_textChanged\0"
    "on_txt_cpf_textChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ClienteDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    5,   84,    2, 0x06 /* Public */,
       9,    6,   95,    2, 0x06 /* Public */,
      11,    1,  108,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      12,    0,  111,    2, 0x08 /* Private */,
      13,    1,  112,    2, 0x08 /* Private */,
      15,    1,  115,    2, 0x08 /* Private */,
      16,    0,  118,    2, 0x08 /* Private */,
      17,    1,  119,    2, 0x08 /* Private */,
      18,    0,  122,    2, 0x08 /* Private */,
      19,    0,  123,    2, 0x08 /* Private */,
      20,    1,  124,    2, 0x08 /* Private */,
      21,    0,  127,    2, 0x08 /* Private */,
      22,    0,  128,    2, 0x08 /* Private */,
      23,    1,  129,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, 0x80000000 | 7,    3,    4,    5,    6,    8,
    QMetaType::Void, QMetaType::Int, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, 0x80000000 | 7,   10,    3,    4,    5,    6,    8,
    QMetaType::Void, QMetaType::Int,   10,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,

       0        // eod
};

void ClienteDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ClienteDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->create_cliente((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])),(*reinterpret_cast< Endereco(*)>(_a[5]))); break;
        case 1: _t->edit_cliente((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])),(*reinterpret_cast< QString(*)>(_a[5])),(*reinterpret_cast< Endereco(*)>(_a[6]))); break;
        case 2: _t->remove_cliente((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_txt_nome_textChanged(); break;
        case 4: _t->on_txt_telefone_movel_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->on_txt_telefone_fixo_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->on_txt_endereco_rua_textChanged(); break;
        case 7: _t->on_txt_CEP_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 8: _t->on_txt_endereco_bairro_textChanged(); break;
        case 9: _t->on_txt_endereco_estado_textChanged(); break;
        case 10: _t->on_cbx_pais_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 11: _t->on_buttonBox_accepted(); break;
        case 12: _t->on_txt_endereco_cidade_textChanged(); break;
        case 13: _t->on_txt_cpf_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (ClienteDialog::*)(QString , QString , QString , QString , Endereco );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ClienteDialog::create_cliente)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (ClienteDialog::*)(int , QString , QString , QString , QString , Endereco );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ClienteDialog::edit_cliente)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (ClienteDialog::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ClienteDialog::remove_cliente)) {
                *result = 2;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ClienteDialog::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_ClienteDialog.data,
    qt_meta_data_ClienteDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ClienteDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ClienteDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ClienteDialog.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int ClienteDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
    return _id;
}

// SIGNAL 0
void ClienteDialog::create_cliente(QString _t1, QString _t2, QString _t3, QString _t4, Endereco _t5)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ClienteDialog::edit_cliente(int _t1, QString _t2, QString _t3, QString _t4, QString _t5, Endereco _t6)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)), const_cast<void*>(reinterpret_cast<const void*>(&_t6)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ClienteDialog::remove_cliente(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
