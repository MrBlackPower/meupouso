/****************************************************************************
** Meta object code from reading C++ file 'meupousolog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "meupousolog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'meupousolog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_meupousolog_t {
    QByteArrayData data[12];
    char stringdata0[94];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_meupousolog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_meupousolog_t qt_meta_stringdata_meupousolog = {
    {
QT_MOC_LITERAL(0, 0, 11), // "meupousolog"
QT_MOC_LITERAL(1, 12, 10), // "repush_log"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 4), // "line"
QT_MOC_LITERAL(4, 29, 6), // "origin"
QT_MOC_LITERAL(5, 36, 9), // "push_feed"
QT_MOC_LITERAL(6, 46, 8), // "push_log"
QT_MOC_LITERAL(7, 55, 9), // "do_backup"
QT_MOC_LITERAL(8, 65, 8), // "Pousada*"
QT_MOC_LITERAL(9, 74, 7), // "pousada"
QT_MOC_LITERAL(10, 82, 4), // "file"
QT_MOC_LITERAL(11, 87, 6) // "backup"

    },
    "meupousolog\0repush_log\0\0line\0origin\0"
    "push_feed\0push_log\0do_backup\0Pousada*\0"
    "pousada\0file\0backup"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_meupousolog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x06 /* Public */,
       1,    2,   60,    2, 0x06 /* Public */,
       5,    1,   65,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    2,   68,    2, 0x0a /* Public */,
       6,    1,   73,    2, 0x2a /* Public | MethodCloned */,
       7,    2,   76,    2, 0x0a /* Public */,
       7,    1,   81,    2, 0x2a /* Public | MethodCloned */,
      11,    2,   84,    2, 0x0a /* Public */,
      11,    1,   89,    2, 0x2a /* Public | MethodCloned */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    3,    4,
    QMetaType::Void, QMetaType::QString,    3,

 // slots: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    3,    4,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Bool, 0x80000000 | 8, QMetaType::QString,    9,   10,
    QMetaType::Bool, 0x80000000 | 8,    9,
    QMetaType::Bool, 0x80000000 | 8, QMetaType::QString,    9,   10,
    QMetaType::Bool, 0x80000000 | 8,    9,

       0        // eod
};

void meupousolog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<meupousolog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->repush_log(); break;
        case 1: _t->repush_log((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 2: _t->push_feed((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->push_log((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 4: _t->push_log((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: { bool _r = _t->do_backup((*reinterpret_cast< Pousada*(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 6: { bool _r = _t->do_backup((*reinterpret_cast< Pousada*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 7: { bool _r = _t->backup((*reinterpret_cast< Pousada*(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 8: { bool _r = _t->backup((*reinterpret_cast< Pousada*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Pousada* >(); break;
            }
            break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Pousada* >(); break;
            }
            break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Pousada* >(); break;
            }
            break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Pousada* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (meupousolog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&meupousolog::repush_log)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (meupousolog::*)(QString , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&meupousolog::repush_log)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (meupousolog::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&meupousolog::push_feed)) {
                *result = 2;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject meupousolog::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_meupousolog.data,
    qt_meta_data_meupousolog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *meupousolog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *meupousolog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_meupousolog.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int meupousolog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void meupousolog::repush_log()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void meupousolog::repush_log(QString _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void meupousolog::push_feed(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
