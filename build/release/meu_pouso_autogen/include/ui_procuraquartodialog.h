/********************************************************************************
** Form generated from reading UI file 'procuraquartodialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROCURAQUARTODIALOG_H
#define UI_PROCURAQUARTODIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_ProcuraQuartoDialog
{
public:
    QDialogButtonBox *buttonBox;
    QTextEdit *txt_numero;
    QGroupBox *groupBox;
    QListWidget *lst_quartos;
    QLabel *label_6;
    QLabel *label_4;
    QLabel *label_13;
    QDateEdit *dt_lower;
    QDateEdit *dt_higher;
    QLabel *label_12;
    QCheckBox *chk_lower;
    QCheckBox *chk_higher;
    QComboBox *cbx_tipo_quarto;
    QLabel *label_7;

    void setupUi(QDialog *ProcuraQuartoDialog)
    {
        if (ProcuraQuartoDialog->objectName().isEmpty())
            ProcuraQuartoDialog->setObjectName(QString::fromUtf8("ProcuraQuartoDialog"));
        ProcuraQuartoDialog->resize(621, 556);
        buttonBox = new QDialogButtonBox(ProcuraQuartoDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(10, 510, 601, 32));
        QFont font;
        font.setFamily(QString::fromUtf8("URW Bookman L"));
        font.setPointSize(15);
        font.setBold(true);
        font.setWeight(75);
        buttonBox->setFont(font);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        txt_numero = new QTextEdit(ProcuraQuartoDialog);
        txt_numero->setObjectName(QString::fromUtf8("txt_numero"));
        txt_numero->setGeometry(QRect(90, 10, 531, 30));
        QFont font1;
        font1.setPointSize(12);
        font1.setBold(false);
        font1.setWeight(50);
        txt_numero->setFont(font1);
        groupBox = new QGroupBox(ProcuraQuartoDialog);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 130, 601, 371));
        groupBox->setFont(font);
        lst_quartos = new QListWidget(groupBox);
        lst_quartos->setObjectName(QString::fromUtf8("lst_quartos"));
        lst_quartos->setGeometry(QRect(0, 30, 601, 341));
        label_6 = new QLabel(ProcuraQuartoDialog);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(0, 50, 121, 27));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_6->sizePolicy().hasHeightForWidth());
        label_6->setSizePolicy(sizePolicy);
        label_6->setFont(font);
        label_4 = new QLabel(ProcuraQuartoDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(0, 10, 91, 27));
        sizePolicy.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy);
        label_4->setFont(font);
        label_13 = new QLabel(ProcuraQuartoDialog);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setGeometry(QRect(380, 50, 41, 31));
        QFont font2;
        font2.setFamily(QString::fromUtf8("URW Bookman L"));
        font2.setPointSize(12);
        font2.setUnderline(true);
        label_13->setFont(font2);
        dt_lower = new QDateEdit(ProcuraQuartoDialog);
        dt_lower->setObjectName(QString::fromUtf8("dt_lower"));
        dt_lower->setGeometry(QRect(170, 50, 171, 30));
        QFont font3;
        font3.setFamily(QString::fromUtf8("URW Bookman L"));
        font3.setPointSize(12);
        dt_lower->setFont(font3);
        dt_higher = new QDateEdit(ProcuraQuartoDialog);
        dt_higher->setObjectName(QString::fromUtf8("dt_higher"));
        dt_higher->setGeometry(QRect(420, 50, 171, 30));
        dt_higher->setFont(font3);
        label_12 = new QLabel(ProcuraQuartoDialog);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setGeometry(QRect(140, 50, 31, 31));
        label_12->setFont(font2);
        chk_lower = new QCheckBox(ProcuraQuartoDialog);
        chk_lower->setObjectName(QString::fromUtf8("chk_lower"));
        chk_lower->setGeometry(QRect(340, 50, 20, 23));
        chk_lower->setLayoutDirection(Qt::RightToLeft);
        chk_lower->setIconSize(QSize(20, 20));
        chk_higher = new QCheckBox(ProcuraQuartoDialog);
        chk_higher->setObjectName(QString::fromUtf8("chk_higher"));
        chk_higher->setGeometry(QRect(600, 50, 20, 23));
        chk_higher->setLayoutDirection(Qt::RightToLeft);
        chk_higher->setIconSize(QSize(20, 20));
        cbx_tipo_quarto = new QComboBox(ProcuraQuartoDialog);
        cbx_tipo_quarto->setObjectName(QString::fromUtf8("cbx_tipo_quarto"));
        cbx_tipo_quarto->setGeometry(QRect(64, 90, 551, 31));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(cbx_tipo_quarto->sizePolicy().hasHeightForWidth());
        cbx_tipo_quarto->setSizePolicy(sizePolicy1);
        cbx_tipo_quarto->setMinimumSize(QSize(0, 20));
        label_7 = new QLabel(ProcuraQuartoDialog);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(0, 90, 121, 27));
        sizePolicy.setHeightForWidth(label_7->sizePolicy().hasHeightForWidth());
        label_7->setSizePolicy(sizePolicy);
        label_7->setFont(font);

        retranslateUi(ProcuraQuartoDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ProcuraQuartoDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ProcuraQuartoDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ProcuraQuartoDialog);
    } // setupUi

    void retranslateUi(QDialog *ProcuraQuartoDialog)
    {
        ProcuraQuartoDialog->setWindowTitle(QApplication::translate("ProcuraQuartoDialog", "Dialog", nullptr));
        groupBox->setTitle(QApplication::translate("ProcuraQuartoDialog", "Quartos", nullptr));
        label_6->setText(QApplication::translate("ProcuraQuartoDialog", "Dispon\303\255vel", nullptr));
        label_4->setText(QApplication::translate("ProcuraQuartoDialog", "N\303\272mero", nullptr));
        label_13->setText(QApplication::translate("ProcuraQuartoDialog", "at\303\251", nullptr));
        label_12->setText(QApplication::translate("ProcuraQuartoDialog", "de", nullptr));
        chk_lower->setText(QString());
        chk_higher->setText(QString());
        label_7->setText(QApplication::translate("ProcuraQuartoDialog", "Tipo", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ProcuraQuartoDialog: public Ui_ProcuraQuartoDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROCURAQUARTODIALOG_H
