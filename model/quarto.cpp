#include "quarto.h"

int Quarto::QuartoElemento::CURR_ID = 0;

Quarto::Quarto(int ID, QString numero, QString descricao, int PAX, TipoQuarto* tipo, bool disponivel) : quarto({ID,numero,descricao,PAX,tipo,disponivel}) , e("QUARTO")
{
    e.SetAttribute("ID",quarto.ID);
    e.SetAttribute("NUMERO",quarto.numero.toStdString().data());
    e.SetAttribute("DESCRICAO",quarto.descricao.toStdString().data());
    e.SetAttribute("DISPONIBILIDADE",quarto.disponivel ? "1" : "0");
    e.SetAttribute("PAX",QString::asprintf("%d",quarto.PAX).toStdString().data());

    e.SetAttribute("TIPO_DE_QUARTO_NOME",QString::asprintf("%s",quarto.tipo_quarto->get_nome().toStdString().data()).toStdString().data());
    e.SetAttribute("TIPO_DE_QUARTO_ID",QString::asprintf("%d",quarto.tipo_quarto->get_ID()).toStdString().data());
}

Quarto::Quarto(QString numero, QString descricao, int PAX, TipoQuarto* tipo, bool disponivel) : quarto({numero,descricao,PAX,tipo,disponivel}) , e("QUARTO")
{
    e.SetAttribute("ID",quarto.ID);
    e.SetAttribute("NUMERO",quarto.numero.toStdString().data());
    e.SetAttribute("DESCRICAO",quarto.descricao.toStdString().data());
    e.SetAttribute("DISPONIBILIDADE",quarto.disponivel ? "1" : "0");
    e.SetAttribute("PAX",QString::asprintf("%d",quarto.PAX).toStdString().data());

    e.SetAttribute("TIPO_DE_QUARTO_NOME",QString::asprintf("%s",quarto.tipo_quarto->get_nome().toStdString().data()).toStdString().data());
    e.SetAttribute("TIPO_DE_QUARTO_ID",QString::asprintf("%d",quarto.tipo_quarto->get_ID()).toStdString().data());
}

Quarto::~Quarto(){

}

bool Quarto::operator==(Quarto* q){
    return quarto.ID == q->get_ID();
}

bool Quarto::operator==(const int& i){
    return quarto.ID == i;
}

int Quarto::get_ID(){
    return quarto.ID;
}

QString Quarto::get_numero(){
    return quarto.numero;
}

void Quarto::set_numero(QString numero){
    quarto.numero = numero;

    e.RemoveAttribute("NUMERO");
    e.SetAttribute("NUMERO",numero.toStdString().data());
}

QString Quarto::get_descricao(){
    return quarto.descricao;
}

void Quarto::set_descricao(QString descricao){
    quarto.descricao = descricao;

    e.RemoveAttribute("DESCRICAO");
    e.SetAttribute("DESCRICAO",descricao.toStdString().data());
}

int Quarto::get_PAX(){
    return  quarto.PAX;
}
void Quarto::set_PAX(int PAX){
    quarto.PAX = PAX;

    e.RemoveAttribute("PAX");
    e.SetAttribute("PAX",QString::asprintf("%d",PAX).toStdString().data());
}

bool Quarto::get_disponibilidade(){
    return quarto.disponivel;
}

void Quarto::set_disponibilidade(bool disp){
    quarto.disponivel = disp;

    e.RemoveAttribute("DISPONIBILIDADE");
    e.SetAttribute("DISPONIBILIDADE",disp ? "1" : "0");
}

TipoQuarto* Quarto::get_tipo_quarto(){
    return quarto.tipo_quarto;
}

void Quarto::set_tipo_quarto(TipoQuarto* quarto){
    if(quarto != NULL){
        this->quarto.tipo_quarto = quarto;

        e.RemoveAttribute("TIPO_DE_QUARTO_ID");
        e.RemoveAttribute("TIPO_DE_QUARTO_NOME");

        e.SetAttribute("TIPO_DE_QUARTO_NOME",QString::asprintf("%s",quarto->get_nome().toStdString().data()).toStdString().data());
        e.SetAttribute("TIPO_DE_QUARTO_ID",QString::asprintf("%d",quarto->get_ID()).toStdString().data());
    }
}

TiXmlNode* Quarto::get_xml_node(){
    return e.Clone();
}

Quarto* Quarto::get_reference(){
    return this;
}

Quarto::QuartoElemento::QuartoElemento(int ID, QString numero, QString descricao, int PAX, TipoQuarto* tipo, bool disponivel) :
    ID(ID), numero(numero), descricao(descricao), PAX(PAX), disponivel(disponivel)
{
    tipo_quarto = tipo;

    if(ID >= CURR_ID)
        CURR_ID = ID + 1;
}

Quarto::QuartoElemento::QuartoElemento(QString numero, QString descricao, int PAX, TipoQuarto* tipo, bool disponivel) :
    ID(CURR_ID++), numero(numero), descricao(descricao), PAX(PAX), disponivel(disponivel)
{
    tipo_quarto = tipo;
}

Quarto::QuartoElemento::QuartoElemento(const QuartoElemento& e) :
    ID(e.ID), numero(e.numero), descricao(e.descricao), PAX(e.PAX), disponivel(e.disponivel)
{
    tipo_quarto = e.tipo_quarto;
}

const Quarto::QuartoElemento& Quarto::QuartoElemento::operator=(const QuartoElemento& e){
    return {e};
}
