#include "hospede.h"

int Hospede::HospedeElemento::CURR_ID = 0;

Hospede::Hospede(Cliente* cliente, QString nome, QString data_nascimento, QString cpf) : hospede(cliente,nome,QDate::fromString(data_nascimento,"dd/MM/yyyy"),cpf), e("HOSPEDE")
{
    e.SetAttribute("ID",hospede.ID);
    e.SetAttribute("NOME",hospede.nome.toStdString().data());
    e.SetAttribute("DATA_NASCIMENTO",hospede.data_nascimento.toString("dd/MM/yyyy").toStdString().data());
    e.SetAttribute("CPF",hospede.cpf.toStdString().data());

    e.SetAttribute("CLIENTE_ID",cliente->get_ID());
    e.SetAttribute("CLIENTE_NOME",cliente->get_nome().toStdString().data());
}

Hospede::Hospede(int ID, Cliente* cliente, QString nome, QDate data_nascimento, QString cpf) : hospede(ID,cliente,nome,data_nascimento,cpf), e("HOSPEDE")
{
    e.SetAttribute("ID",hospede.ID);
    e.SetAttribute("NOME",hospede.nome.toStdString().data());
    e.SetAttribute("DATA_NASCIMENTO",hospede.data_nascimento.toString("dd/MM/yyyy").toStdString().data());
    e.SetAttribute("CPF",hospede.cpf.toStdString().data());

    e.SetAttribute("CLIENTE_ID",cliente->get_ID());
    e.SetAttribute("CLIENTE_NOME",cliente->get_nome().toStdString().data());
}

Hospede::Hospede(Cliente* cliente, QString nome, QDate data_nascimento, QString cpf) : hospede(cliente,nome,data_nascimento,cpf), e("HOSPEDE")
{
    e.SetAttribute("ID",hospede.ID);
    e.SetAttribute("NOME",hospede.nome.toStdString().data());
    e.SetAttribute("DATA_NASCIMENTO",hospede.data_nascimento.toString("dd/MM/yyyy").toStdString().data());
    e.SetAttribute("CPF",hospede.cpf.toStdString().data());

    e.SetAttribute("CLIENTE_ID",cliente->get_ID());
    e.SetAttribute("CLIENTE_NOME",cliente->get_nome().toStdString().data());
}

Hospede::~Hospede(){

}

int Hospede::get_ID(){
    return hospede.ID;
}

QString Hospede::get_nome(){
    return hospede.nome;
}

void Hospede::set_nome(QString nome){
    hospede.nome = nome;

    e.RemoveAttribute("NOME");
    e.SetAttribute("NOME",hospede.nome.toStdString().data());
}

QString Hospede::get_data_nascimento(){
    return hospede.data_nascimento.toString("dd/MM/yyyy");
}

void Hospede::set_data_nascimento(QString data_nascimento){
    hospede.data_nascimento = QDate::fromString(data_nascimento,"dd/MM/yyyy");

    e.RemoveAttribute("DATA_NASCIMENTO");
    e.SetAttribute("DATA_NASCIMENTO",hospede.data_nascimento.toString("dd/MM/yyyy").toStdString().data());
}

void Hospede::set_data_nascimento(QDate data_nascimento){
    hospede.data_nascimento = data_nascimento;

    e.RemoveAttribute("DATA_NASCIMENTO");
    e.SetAttribute("DATA_NASCIMENTO",hospede.data_nascimento.toString("dd/MM/yyyy").toStdString().data());
}

QString Hospede::get_cpf(){
    return hospede.cpf;
}

void Hospede::set_cpf(QString cpf){
    hospede.cpf = cpf;

    e.RemoveAttribute("CPF");
    e.SetAttribute("CPF",hospede.cpf.toStdString().data());
}

Cliente* Hospede::get_cliente(){
    return hospede.hospede_cliente;
}

void Hospede::set_cliente(Cliente* cliente){
    hospede.hospede_cliente = cliente;

    e.RemoveAttribute("CLIENTE_ID");
    e.RemoveAttribute("CLIENTE_NOME");
    e.SetAttribute("CLIENTE_ID",cliente->get_ID());
    e.SetAttribute("CLIENTE_NOME",cliente->get_nome().toStdString().data());
}

bool Hospede::operator==(Hospede* h){
    return hospede.ID == h->get_ID();
}
bool Hospede::operator==(const int& i){
    return hospede.ID == i;
}

TiXmlNode* Hospede::get_xml_node(){
    return e.Clone();
}

Hospede::HospedeElemento::HospedeElemento(int ID, Cliente* cliente, QString nome, QDate data_nascimento, QString cpf) : ID(ID), nome(nome), data_nascimento(data_nascimento), cpf(cpf), hospede_cliente(cliente)
{
    if(ID >= CURR_ID)
        CURR_ID = ID + 1;
}

Hospede::HospedeElemento::HospedeElemento(Cliente* cliente, QString nome, QDate data_nascimento, QString cpf) : ID(CURR_ID++), nome(nome), data_nascimento(data_nascimento), cpf(cpf), hospede_cliente(cliente)
{

}

Hospede::HospedeElemento::HospedeElemento(const HospedeElemento& e) : ID(e.ID), nome(e.nome), data_nascimento(e.data_nascimento), cpf(e.cpf), hospede_cliente(e.hospede_cliente)
{

}

const Hospede::HospedeElemento& Hospede::HospedeElemento::operator=(const HospedeElemento& e){
    return {e};
}


bool Hospede::HospedeElemento::operator==(const HospedeElemento& e){
    return e.ID == ID;
}
