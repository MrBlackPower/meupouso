#ifndef ENDERECO_H
#define ENDERECO_H

#include <QString>
#include <vector>
#include <tinyxml/tinyxml.h>

using namespace std;

class Endereco
{
public:
    /***
    * Elemento - Endereco
    **/
    class EnderecoElemento{
    friend class Endereco;
    public:

        const int ID;
        QString rua;
        QString CEP;
        QString bairro;
        QString cidade;
        QString estado;
        QString pais;
    private:
        EnderecoElemento(QString rua, QString CEP, QString bairro, QString cidade, QString estado, QString pais);
        EnderecoElemento(const EnderecoElemento& e);

        bool operator==(const EnderecoElemento& e);

        static int CURR_ID;
    };



    /***
     * GETS & SETS
    ***/

    int get_ID();

    QString get_rua();
    void set_rua(QString rua);

    QString get_CEP();
    void set_CEP(QString CEP);

    QString get_bairro();
    void set_bairro(QString bairro);

    QString get_cidade();
    void set_cidade(QString cidade);

    QString get_estado();
    void set_estado(QString estado);

    QString get_pais();
    void set_pais(QString pais);

    QString to_string();

    TiXmlNode* get_xml_node();

    Endereco(QString rua, QString CEP, QString bairro, QString cidade, QString estado, QString pais);
    ~Endereco();

    const Endereco& operator=(const Endereco& e);

private:
    EnderecoElemento endereco;
    TiXmlElement e;

};

#endif // ENDERECO_H
