#ifndef RESERVA_H
#define RESERVA_H

#include <QString>
#include <QDate>
#include <QDateTime>
#include <tinyxml/tinyxml.h>

#include "quarto.h"
#include "cliente.h"
#include "hospede.h"

class Reserva
{
public:
    enum ReservaStatus{
        NAO_CONFIRMADA,
        CONFIRMADA,
        CHECK_IN,
        CHECK_OUT,
        CANCELADA,
        NO_SHOW
    };

    /***
    * Elemento - Reserva
    **/
    class ReservaElemento{
    friend class Reserva;
    public:
    private:
        const int ID;
        QDate data_inicio;
        QDate data_fim;
        int hospedes_adultos;
        int hospedes_criancas;

        bool has_check_in;
        QDateTime data_check_in;
        bool has_check_out;
        QDateTime data_check_out;

        ReservaStatus status;

        Cliente* cliente;
        vector<Quarto*> quarto;

        vector<Hospede*> hospedes;

        const ReservaElemento& operator=(const ReservaElemento& e);


        ReservaElemento(Cliente* cliente, vector<Quarto*> quarto, QDate data_inicio, QDate data_fim, int hospedes_adultos, int hospedes_criancas);
        ReservaElemento(int ID, Cliente* cliente, vector<Quarto*> quarto, QDate data_inicio, QDate data_fim, int hospedes_adultos, int hospedes_criancas,
                        bool has_check_in, QDateTime check_in, bool has_check_out, QDateTime check_out, ReservaStatus status, vector<Hospede*> hospedes);
        ReservaElemento(Cliente* cliente, vector<Quarto*> quarto, QDate data_inicio, QDate data_fim, int hospedes_adultos, int hospedes_criancas,
                        bool has_check_in, QDateTime check_in, bool has_check_out, QDateTime check_out, ReservaStatus status, vector<Hospede*> hospedes);

        bool operator==(const ReservaElemento& e);

        static int CURR_ID;
    };

    Reserva(Cliente* cliente, vector<Quarto*> quarto, QString data_inicio, QString data_fim, int hospedes_adultos, int hospedes_criancas);
    Reserva(Cliente* cliente, vector<Quarto*> quarto, QDate data_inicio, QDate data_fim, int hospedes_adultos, int hospedes_criancas);
    Reserva(int ID, Cliente* cliente, vector<Quarto*> quarto, QDate data_inicio, QDate data_fim, int hospedes_adultos, int hospedes_criancas,
            bool has_check_in, QDateTime check_in, bool has_check_out, QDateTime check_out, ReservaStatus status, vector<Hospede*> hospede);
    Reserva(Cliente* cliente, vector<Quarto*> quarto, QDate data_inicio, QDate data_fim, int hospedes_adultos, int hospedes_criancas,
            bool has_check_in, QDateTime check_in, bool has_check_out, QDateTime check_out, ReservaStatus status, vector<Hospede*> hospedes);
    ~Reserva();

    ReservaStatus get_status();

    static QString status_to_qstring(ReservaStatus status);
    static ReservaStatus qstring_to_status(QString status);

    bool confirm();

    bool cancel();

    bool no_show();

    bool check_in(vector<Hospede*> hospedes, QDateTime date_time);

    bool check_out(QDateTime date_time);

    bool has_quarto(QString id);

    bool has_quarto(int id);

    /***
     * GETS & SETS
    ***/
    int get_ID();

    Cliente* get_cliente();
    void set_cliente(Cliente* cliente);

    vector<Quarto*> get_quarto();
    vector<int> get_quarto_IDs();
    QString get_quartos_string();
    void set_quarto(vector<Quarto*> quarto);

    int get_PAX();

    QDate get_data_inicio();
    void set_data_inicio(QDate data_inicio);

    QDate get_data_fim();
    void set_data_fim(QDate data_fim);

    int get_hospedes();

    vector<int> get_hospedes_list();

    int get_hospedes_adultos();
    void set_hospedes_adultos(int hospedes_adultos);

    int get_hospedes_criancas();
    void set_hospedes_criancas(int hospedes_criancas);

    QDateTime get_data_check_in();
    void set_data_check_in(QDateTime data_check_in);

    QDateTime get_data_check_out();
    void set_data_check_out(QDateTime data_check_out);

    TiXmlNode* get_xml_node();

    bool checked_in();

    bool checked_out();

    bool operator==(Reserva* c);
    bool operator==(const int& i);

private:
    ReservaElemento reserva;
    TiXmlElement e;
};

#endif // RESERVA_H
