#include "endereco.h"

int Endereco::EnderecoElemento::CURR_ID = 0;

Endereco::Endereco(QString rua, QString CEP, QString bairro, QString cidade, QString estado, QString pais) : endereco(rua,CEP,bairro,cidade,estado,pais), e("ENDERECO")
{
    e.SetAttribute("RUA",endereco.rua.toStdString().data());
    e.SetAttribute("CEP",endereco.CEP.toStdString().data());
    e.SetAttribute("BAIRRO",endereco.bairro.toStdString().data());
    e.SetAttribute("CIDADE",endereco.cidade.toStdString().data());
    e.SetAttribute("ESTADO",endereco.estado.toStdString().data());
    e.SetAttribute("PAIS",endereco.pais.toStdString().data());
}

Endereco::~Endereco(){

}

const Endereco& Endereco::operator=(const Endereco& e){
    return {e};
}


int Endereco::get_ID(){
    return  endereco.ID;
}

QString Endereco::get_rua(){
    return endereco.rua;
}
void Endereco::set_rua(QString rua){
    endereco.rua = rua;

    e.RemoveAttribute("RUA");
    e.SetAttribute("RUA",endereco.rua.toStdString().data());
}

QString Endereco::get_CEP(){
    return endereco.CEP;
}

void Endereco::set_CEP(QString CEP){
    endereco.CEP = CEP;

    e.RemoveAttribute("CEP");
    e.SetAttribute("CEP",endereco.CEP.toStdString().data());
}

QString Endereco::get_bairro(){
    return endereco.bairro;
}

void Endereco::set_bairro(QString bairro){
    endereco.bairro = bairro;

    e.RemoveAttribute("BAIRRO");
    e.SetAttribute("BAIRRO",endereco.bairro.toStdString().data());
}

QString Endereco::get_cidade(){
    return endereco.cidade;
}

void Endereco::set_cidade(QString cidade){
    endereco.cidade = cidade;

    e.RemoveAttribute("CIDADE");
    e.SetAttribute("CIDADE",endereco.cidade.toStdString().data());
}

QString Endereco::get_estado(){
    return endereco.estado;
}

void Endereco::set_estado(QString estado){
    endereco.estado = estado;

    e.RemoveAttribute("ESTADO");
    e.SetAttribute("ESTADO",endereco.estado.toStdString().data());
}

QString Endereco::get_pais(){
    return endereco.pais;
}

void Endereco::set_pais(QString pais){
    endereco.pais = pais;

    e.RemoveAttribute("PAIS");
    e.SetAttribute("PAIS",endereco.pais.toStdString().data());
}

QString Endereco::to_string(){
    return QString::asprintf("%s, %s, %s - %s (%s) %s", endereco.rua.toStdString().data(), endereco.bairro.toStdString().data(), endereco.cidade.toStdString().data(), endereco.estado.toStdString().data(),
                             endereco.CEP.toStdString().data(), endereco.pais.toStdString().data());
}

TiXmlNode* Endereco::get_xml_node(){
    return e.Clone();
}

Endereco::EnderecoElemento::EnderecoElemento(QString rua, QString CEP, QString bairro, QString cidade, QString estado, QString pais) :
    ID(CURR_ID++), rua(rua), CEP(CEP), bairro(bairro), cidade(cidade), estado(estado), pais(pais)
{

}

Endereco::EnderecoElemento::EnderecoElemento(const EnderecoElemento& e) :
    ID(e.ID), rua(e.rua), CEP(e.CEP), bairro(e.bairro), cidade(e.cidade), estado(e.estado), pais(e.pais)
{

}

bool Endereco::EnderecoElemento::operator==(const EnderecoElemento& e){
    return ID == e.ID;
}
