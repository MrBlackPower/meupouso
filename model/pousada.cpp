#include "pousada.h"

Pousada::Pousada(QString nome) : pousada(nome)
{
    doc = new TiXmlDocument();
}

Pousada::PousadaElemento::PousadaElemento(QString nome) : nome(nome){

}

std::ostream& operator<<(std::ostream& os, const Pousada& pousada){
    TiXmlPrinter printer;
    printer.SetIndent( "    " );

    pousada.doc->Accept( &printer );

    string s_xml = printer.CStr();

    os << s_xml << endl;
    return os;
}

Pousada& Pousada::add_tipo_quarto(QString nome, QString descricao){
    pousada.tipos_quarto.push_back(new TipoQuarto{nome,descricao});

    update_xml();

    emit updated();

    return *this;
}

Pousada& Pousada::add_tipo_quarto(int tipo_quarto_ID, QString nome, QString descricao){
    pousada.tipos_quarto.push_back(new TipoQuarto{tipo_quarto_ID,nome,descricao});

    update_xml();

    emit updated();

    return *this;
}

Pousada& Pousada::edit_tipo_quarto(int ID, QString nome, QString descricao){
    //FINDS TIPO QUARTO
    TipoQuarto* t = NULL;

    for(int i = 0; i < pousada.tipos_quarto.size() && t == NULL; i++)
        if(pousada.tipos_quarto.operator[](i)->operator==(ID))
            t = pousada.tipos_quarto.operator[](i);

    if(t != NULL){
        t->set_nome(nome);
        t->set_descricao(descricao);

        emit updated();
    } else {
        emit push_log("EDIÇÃO DE TIPO DE QUARTO FALHOU " + nome ,pousada.nome);
    }

    update_xml();

    return *this;
}

Pousada& Pousada::add_quarto(QString numero, QString descricao, int PAX, QString tipo_quarto_ID, bool disponivel){
    //FINDS TIPO QUARTO
    TipoQuarto* t = NULL;

    for(int i = 0; i < pousada.tipos_quarto.size() && t == NULL; i++)
        if(pousada.tipos_quarto.operator[](i)->operator==(tipo_quarto_ID.toInt()))
            t = pousada.tipos_quarto.operator[](i);


    if(t != NULL){
        pousada.quartos.push_back(new Quarto{numero,descricao,PAX,t,disponivel});

        emit updated();
    } else {
        emit push_log("ADIÇÃO DE QUARTO FALHOU " + numero ,pousada.nome);
    }

    update_xml();

    return *this;
}

Pousada& Pousada::add_quarto(int quarto_ID, QString numero, QString descricao, int PAX, QString tipo_quarto, bool disponivel){
    //FINDS TIPO QUARTO
    TipoQuarto* t = NULL;

    for(int i = 0; i < pousada.tipos_quarto.size() && t == NULL; i++)
        if(pousada.tipos_quarto.operator[](i)->operator==(tipo_quarto.toInt()))
            t = pousada.tipos_quarto.operator[](i);


    if(t != NULL){
        pousada.quartos.push_back(new Quarto{quarto_ID,numero,descricao,PAX,t,disponivel});

        emit updated();
    } else {
        emit push_log("ADIÇÃO DE QUARTO FALHOU " + numero ,pousada.nome);
    }

    update_xml();

    return *this;
}

Pousada& Pousada::edit_quarto(int ID, QString numero, QString descricao, int PAX, QString tipo_quarto, bool disponivel){
    //FINDS QUARTO
    Quarto* t = NULL;

    for(int i = 0; i < pousada.quartos.size() && t == NULL; i++)
        if(pousada.quartos.operator[](i)->operator==(ID))
            t = pousada.quartos.operator[](i);

    //FINDS TIPO QUARTO
    TipoQuarto* tt = NULL;

    for(int i = 0; i < pousada.tipos_quarto.size() && tt == NULL; i++)
        if(pousada.tipos_quarto.operator[](i)->operator==(tipo_quarto.toInt()))
            tt = pousada.tipos_quarto.operator[](i);

    if(tt == NULL)
        return *this;

    if(t != NULL){
        t->set_numero(numero);
        t->set_descricao(descricao);
        t->set_PAX(PAX);
        t->set_disponibilidade(disponivel);
        t->set_tipo_quarto(tt);

        update_xml();

        emit updated();
    } else {
        emit push_log("EDIÇÃO DE TIPO DE QUARTO FALHOU " + numero ,pousada.nome);
    }

    return *this;
}

Pousada& Pousada::remove_quarto(int ID){
    //FINDS QUARTO
    for (int i = 0; i < pousada.quartos.size(); i++) {
        if(pousada.quartos.operator[](i)->operator==(ID)){
            pousada.quartos.erase(pousada.quartos.begin() + i );

            update_xml();

            emit updated();

            return *this;
        }
    }

    return *this;
}

Pousada& Pousada::remove_all_quarto(){
    //FINDS QUARTO
    for (int i = 0; i < pousada.quartos.size(); i++) {
        delete  pousada.quartos.operator[](i);

        pousada.quartos.erase(pousada.quartos.begin() + i );

        i--;
    }

    return *this;
}

Pousada& Pousada::add_cliente(QString nome, QString cpf, QString telefone_movel, QString telefone_fixo, Endereco endereco){
    if(!QStringFormatHelper::in_format("NNN.NNN.NNN-NN",cpf)){
        emit push_log("ADIÇÃO DE CLIENTE FALHOU <CPF INVÁLIDO> " + nome ,pousada.nome);
        return *this;
    }    
    if(!(QStringFormatHelper::in_format("(NN)NNNN-NNNN",telefone_movel) or QStringFormatHelper::in_format("(NN)NNNNN-NNNN",telefone_movel))){
        emit push_log("ADIÇÃO DE CLIENTE FALHOU <TELEFONE MOVEL INVÁLIDO> " + nome ,pousada.nome);
        return *this;
    }    
    if(!(QStringFormatHelper::in_format("(NN)NNNN-NNNN",telefone_fixo) or QStringFormatHelper::in_format("(NN)NNNNN-NNNN",telefone_fixo))){
        emit push_log("ADIÇÃO DE CLIENTE FALHOU <TELEFONE FIXO INVÁLIDO> " + nome ,pousada.nome);
        return *this;
    }
    if(!(QStringFormatHelper::in_format("NNNNN-NNN",endereco.get_CEP()) or endereco.get_CEP().isEmpty())){
        emit push_log("ADIÇÃO DE CLIENTE FALHOU <CEP INVÁLIDO> " + nome ,pousada.nome);
        return *this;
    }

    pousada.clientes.push_back(new Cliente{nome,cpf,telefone_movel,telefone_fixo,endereco});

    update_xml();

    emit updated();

    return *this;
}

Pousada& Pousada::add_cliente(int cliente_ID, QString nome, QString cpf, QString telefone_movel, QString telefone_fixo, Endereco endereco){
    if(!QStringFormatHelper::in_format("NNN.NNN.NNN-NN",cpf)){
        emit push_log("ADIÇÃO DE CLIENTE FALHOU <CPF INVÁLIDO> " + nome ,pousada.nome);
        return *this;
    }
    if(!(QStringFormatHelper::in_format("(NN)NNNN-NNNN",telefone_movel) or QStringFormatHelper::in_format("(NN)NNNNN-NNNN",telefone_movel))){
        emit push_log("ADIÇÃO DE CLIENTE FALHOU <TELEFONE MOVEL INVÁLIDO> " + nome ,pousada.nome);
        return *this;
    }
    if(!(QStringFormatHelper::in_format("(NN)NNNN-NNNN",telefone_fixo) or QStringFormatHelper::in_format("(NN)NNNNN-NNNN",telefone_fixo))){
        emit push_log("ADIÇÃO DE CLIENTE FALHOU <TELEFONE FIXO INVÁLIDO> " + nome ,pousada.nome);
        return *this;
    }
    if(!(QStringFormatHelper::in_format("NNNNN-NNN",endereco.get_CEP()) or endereco.get_CEP().isEmpty())){
        emit push_log("ADIÇÃO DE CLIENTE FALHOU <CEP INVÁLIDO> " + nome ,pousada.nome);
        return *this;
    }

    pousada.clientes.push_back(new Cliente{cliente_ID,nome,cpf,telefone_movel,telefone_fixo,endereco});

    update_xml();

    emit updated();

    return *this;
}

Pousada& Pousada::edit_cliente(int ID, QString nome, QString cpf, QString telefone_movel, QString telefone_fixo, Endereco endereco){
    if(!QStringFormatHelper::in_format("NNN.NNN.NNN-NN",cpf)){
        emit push_log("EDIÇÃO DE CLIENTE FALHOU <CPF INVÁLIDO> " + nome ,pousada.nome);
    }
    if(!(QStringFormatHelper::in_format("(NN)NNNN-NNNN",telefone_movel) or QStringFormatHelper::in_format("(NN)NNNNN-NNNN",telefone_movel))){
        emit push_log("EDIÇÃO DE CLIENTE FALHOU <TELEFONE MOVEL INVÁLIDO> " + nome ,pousada.nome);
    }
    if(!(QStringFormatHelper::in_format("(NN)NNNN-NNNN",telefone_fixo) or QStringFormatHelper::in_format("(NN)NNNNN-NNNN",telefone_fixo))){
        emit push_log("EDIÇÃO DE CLIENTE FALHOU <TELEFONE FIXO INVÁLIDO> " + nome ,pousada.nome);
    }    
    if(!(QStringFormatHelper::in_format("NNNNN-NNN",endereco.get_CEP()))){
        emit push_log("EDIÇÃO DE CLIENTE FALHOU <CEP INVÁLIDO> " + nome ,pousada.nome);
    }

    Cliente* c = NULL;

    for (int i = 0; i < pousada.clientes.size() && c == NULL; i++) {
        if(pousada.clientes.operator[](i)->operator==(ID))
            c = pousada.clientes.operator[](i);
    }

    if(c == NULL){
        emit push_log("EDIÇÃO DE CLIENTE FALHOU <> " + nome ,pousada.nome);

        return *this;
    }

    c->set_nome(nome);
    c->set_cpf(cpf);
    c->set_endereco(endereco);
    c->set_telefone_fixo(telefone_fixo);
    c->set_telefone_movel(telefone_movel);

    update_xml();

    emit updated();

    return *this;
}

Pousada& Pousada::remove_cliente(int ID){
    //FINDS CLIENTE
    for (int i = 0; i < pousada.clientes.size(); i++) {
        if(pousada.clientes.operator[](i)->operator==(ID)){
            pousada.clientes.erase(pousada.clientes.begin() + i );

            update_xml();

            emit updated();

            return *this;
        }
    }

    return *this;
}

Pousada& Pousada::remove_all_cliente(){
    for (int i = 0; i < pousada.clientes.size(); i++) {
        delete pousada.clientes.operator[](i);

        pousada.clientes.erase(pousada.clientes.begin() + i );

        i--;
    }

    return *this;
}

Pousada& Pousada::add_hospede(QString cliente_ID, QString nome, QDate data_nascimento, QString cpf){
    if(!QStringFormatHelper::in_format("NNN.NNN.NNN-NN",cpf)){
        emit push_log("ADIÇÃO DE HOSPEDE FALHOU <CPF INVÁLIDO> " + nome ,pousada.nome);
        return *this;
    }

    //FINDS_CLIENTE
    Cliente* c = NULL;

    for(int i = 0; i < pousada.clientes.size(); i++)
        if(pousada.clientes.operator[](i)->operator==(cliente_ID.toInt()))
            c = pousada.clientes.operator[](i);

    if(c == NULL){
        emit push_log("ADIÇÃO DE HOSPEDE FALHOU <> " + nome ,pousada.nome);

        return *this;
    }

    pousada.hospedes.push_back(new Hospede{c,nome,data_nascimento,cpf});

    update_xml();

    emit updated();

    return *this;
}

Pousada& Pousada::add_hospede(int hospede_ID, QString cliente_ID, QString nome, QDate data_nascimento, QString cpf){
    if(!QStringFormatHelper::in_format("NNN.NNN.NNN-NN",cpf)){
        emit push_log("ADIÇÃO DE HOSPEDE FALHOU <CPF INVÁLIDO> " + nome ,pousada.nome);
        return *this;
    }

    //FINDS_CLIENTE
    Cliente* c = NULL;

    for(int i = 0; i < pousada.clientes.size(); i++)
        if(pousada.clientes.operator[](i)->operator==(cliente_ID.toInt()))
            c = pousada.clientes.operator[](i);

    if(c == NULL){
        emit push_log("ADIÇÃO DE HOSPEDE FALHOU <> " + nome ,pousada.nome);

        return *this;
    }

    pousada.hospedes.push_back(new Hospede{hospede_ID,c,nome,data_nascimento,cpf});

    update_xml();

    emit updated();

    return *this;
}

Pousada& Pousada::edit_hospede(int ID, QString cliente_ID, QString nome, QString cpf, QDate data_nascimento){
    //FINDS CLIENTE
    for (int i = 0; i < pousada.hospedes.size(); i++) {
        if(pousada.hospedes.operator[](i)->operator==(ID)){
            Hospede* h = pousada.hospedes.operator[](i);

            h->set_cpf(cpf);
            h->set_nome(nome);
            h->set_data_nascimento(data_nascimento);

            if(cliente_ID.toInt() != h->get_cliente()->get_ID()){
                //UPDATE CLIENTE
                Cliente* c = get_cliente(cliente_ID.toInt());

                if(c == NULL){
                    emit push_log("EDIÇÃO DE HOSPEDE FALHOU <> " + nome ,pousada.nome);

                    return *this;
                }

                h->set_cliente(c);
            }

            update_xml();

            emit updated();

            return *this;
        }
    }

    emit push_log("EDIÇÃO DE HOSPEDE FALHOU <> " + nome ,pousada.nome);

    return *this;
}

Pousada& Pousada::remove_hospede(int ID){
    //FINDS HOSPEDE
    for (int i = 0; i < pousada.hospedes.size(); i++) {
        if(pousada.hospedes.operator[](i)->operator==(ID)){
            pousada.hospedes.erase(pousada.hospedes.begin() + i );

            update_xml();

            emit updated();

            return *this;
        }
    }

    return *this;
}

Pousada& Pousada::remove_all_hospede(){
    for (int i = 0; i < pousada.hospedes.size(); i++) {
        delete  pousada.hospedes.operator[](i);

        pousada.hospedes.erase(pousada.hospedes.begin() + i );

        i--;
    }

    return *this;
}

Pousada& Pousada::add_reserva(QString cliente_ID, vector<QString> quarto_ID, QDate data_inicio, QDate data_fim, int hospedes_adultos, int hospedes_criancas){

    //FINDS_CLIENTE
    Cliente* c = NULL;

    for(int i = 0; i < pousada.clientes.size(); i++)
        if(pousada.clientes.operator[](i)->operator==(cliente_ID.toInt()))
            c = pousada.clientes.operator[](i);

    if(c == NULL){
        emit push_log("ADIÇÃO DE HOSPEDE FALHOU <> ",pousada.nome);

        return *this;
    }

    //FINDS_QUARTO
    vector<Quarto*> qs;

    for(QString id : quarto_ID){
        Quarto* q = NULL;
        for(int i = 0; i < pousada.quartos.size() && !q; i++){
            if(pousada.quartos.operator[](i)->operator==(id.toInt())){
                q = pousada.quartos.operator[](i);
            }
        }

        if(q){
            qs.push_back(q);
        }
    }

    if(qs.empty()){
        emit push_log("ADIÇÃO DE RESERVA FALHOU <> ",pousada.nome);

        return *this;
    }

    pousada.reservas.push_back(new Reserva{c,qs,data_inicio,data_fim,hospedes_adultos,hospedes_criancas});

    update_xml();

    emit updated();

    return *this;
}

Pousada& Pousada::add_reserva(int reserva_ID, QString cliente_ID, vector<QString> quarto_ID, QDate data_inicio, QDate data_fim, int hospedes_adultos, int hospedes_criancas,
                     bool has_check_in, QDateTime check_in, bool has_check_out, QDateTime check_out, Reserva::ReservaStatus status, vector<Hospede*> hospedes){
    //FINDS_CLIENTE
    Cliente* c = NULL;

    for(int i = 0; i < pousada.clientes.size(); i++)
        if(pousada.clientes.operator[](i)->operator==(cliente_ID.toInt()))
            c = pousada.clientes.operator[](i);

    if(c == NULL){
        emit push_log("ADIÇÃO DE HOSPEDE FALHOU <> ",pousada.nome);

        return *this;
    }

    //FINDS_QUARTO
    vector<Quarto*> qs;
    for(QString id : quarto_ID){
        Quarto* q = get_quarto(id.toInt());

        if(q){
            qs.push_back(q);
        } else {
            emit push_log("ADIÇÃO DE RESERVA FALHOU <> ",pousada.nome);

            return *this;
        }
    }

    if(qs.empty()){
        emit push_log("ADIÇÃO DE RESERVA FALHOU <> ",pousada.nome);

        return *this;
    }

    pousada.reservas.push_back(new Reserva{reserva_ID,c,qs,data_inicio,data_fim,hospedes_adultos,hospedes_criancas,has_check_in,check_in,has_check_out,check_out,status,hospedes});

    update_xml();

    emit updated();

    return *this;
}

Pousada& Pousada::edit_reserva(int ID, int hospedes_adultos, int hospedes_criancas){
    //FINDS RESERVA
    for (int i = 0; i < pousada.reservas.size(); i++) {
        if(pousada.reservas.operator[](i)->operator==(ID)){
            Reserva* r = pousada.reservas.operator[](i);

            r->set_hospedes_adultos(hospedes_adultos);
            r->set_hospedes_criancas(hospedes_criancas);

            update_xml();

            emit updated();

            return *this;
        }
    }

    emit push_log("EDIÇÃO DE RESERVA FALHOU <> ",pousada.nome);

    return *this;
}

Pousada& Pousada::confirma_reserva(int ID){
    //FINDS RESERVA
    for (int i = 0; i < pousada.reservas.size(); i++) {
        if(pousada.reservas.operator[](i)->operator==(ID)){
            Reserva* r = pousada.reservas.operator[](i);

            r->confirm();

            update_xml();

            emit updated();

            return *this;
        }
    }

    emit push_log("CONFIRMAÇÃO DE HOSPEDE FALHOU <> ",pousada.nome);

    return *this;
}

Pousada& Pousada::cancela_reserva(int ID){
    //FINDS RESERVA
    for (int i = 0; i < pousada.reservas.size(); i++) {
        if(pousada.reservas.operator[](i)->operator==(ID)){
            Reserva* r = pousada.reservas.operator[](i);

            r->cancel();

            update_xml();

            emit updated();

            return *this;
        }
    }

    emit push_log("CANCELAMENTO DE RESERVA FALHOU <> ",pousada.nome);

    return *this;
}

Pousada& Pousada::no_show_reserva(int ID){
    //FINDS RESERVA
    for (int i = 0; i < pousada.reservas.size(); i++) {
        if(pousada.reservas.operator[](i)->operator==(ID)){
            Reserva* r = pousada.reservas.operator[](i);

            r->no_show();

            update_xml();

            emit updated();

            return *this;
        }
    }

    emit push_log("NO SHOW DE RESERVA FALHOU <> ",pousada.nome);

    return *this;
}

Pousada& Pousada::checkin_reserva(int ID, vector<int>hospedes_ID, QDateTime date_time){
    //FINDS RESERVA
    for (int i = 0; i < pousada.reservas.size(); i++) {
        if(pousada.reservas.operator[](i)->operator==(ID)){
            Reserva* r = pousada.reservas.operator[](i);

            vector<Hospede*> hospedes;
            vector<int> hospedes_cliente = get_hospede_cliente_id(r->get_cliente()->get_ID());
            for(int curr: hospedes_ID){
                bool found = false;
                for(int i = 0; i < hospedes_cliente.size() && !found; i++){
                    if(curr == hospedes_cliente.operator[](i)){
                        found = true;
                        hospedes.push_back(get_hospede(curr));
                    }
                }

                if(!found){

                    emit push_log("CONFIRMAÇÃO DE HOSPEDE FALHOU <> ",pousada.nome);

                    return *this;
                }
            }

            r->check_in(hospedes,date_time);

            update_xml();

            emit updated();

            return *this;
        }
    }

    emit push_log("CONFIRMAÇÃO DE HOSPEDE FALHOU <> ",pousada.nome);

    return *this;
}

Pousada& Pousada::checkout_reserva(int ID, QDateTime date_time){
    //FINDS RESERVA
    for (int i = 0; i < pousada.reservas.size(); i++) {
        if(pousada.reservas.operator[](i)->operator==(ID)){
            Reserva* r = pousada.reservas.operator[](i);

            r->check_out(date_time);

            update_xml();

            emit updated();

            return *this;
        }
    }

    emit push_log("CONFIRMAÇÃO DE HOSPEDE FALHOU <> ",pousada.nome);

    return *this;
}

Pousada& Pousada::remove_reserva(int ID){
    //FINDS RESERVA
    for (int i = 0; i < pousada.reservas.size(); i++) {
        if(pousada.reservas.operator[](i)->operator==(ID)){
            pousada.reservas.erase(pousada.reservas.begin() + i );

            update_xml();

            emit updated();

            return *this;
        }
    }

    return *this;
}

Pousada& Pousada::remove_all_reserva(){
    for (int i = 0; i < pousada.reservas.size(); i++) {
        delete  pousada.reservas.operator[](i);

        pousada.reservas.erase(pousada.reservas.begin() + i );

        i--;
    }

    return *this;
}

string& operator<<(string& os, const Pousada& pousada){
    TiXmlPrinter printer;
    printer.SetIndent( "    " );

    pousada.doc->Accept( &printer );

    string s_xml = printer.CStr();

    os  = os + s_xml;
    return os;
}


bool Pousada::load(QString file){
    clear();

    TiXmlDocument doc(file.toStdString().data());
    doc.LoadFile();
    TiXmlHandle hDoc(&doc);
    TiXmlElement* pElem;
    TiXmlHandle hRoot(0);


    TiXmlElement* subElem = NULL;
    TiXmlElement* subsubElem = NULL;

    bool no_quarto = false;
    bool no_hospede = false;
    bool no_reserva = false;

    // block: MEU_POUSO
    pElem = hDoc.FirstChildElement().Element();
    // should always have a valid root but handle gracefully if it does

    if (!pElem)
        return false;

    // save this for later
    hRoot = TiXmlHandle(pElem);

    // block: TIPOS_DE_QUARTO
    subElem = hRoot.FirstChild( "TIPOS_DE_QUARTO" ).Element();

    if(!subElem){
        no_quarto = true;
        no_reserva = true;
    }else{
        if(subElem->NoChildren()){
            no_quarto = true;
            no_reserva = true;
        }

        for( subsubElem = subElem->FirstChildElement("TIPO_DE_QUARTO"); subsubElem; subsubElem=subsubElem->NextSiblingElement()){
            int id = 0;
            string t_id = subsubElem->Attribute("ID",&id);
            string t_nome = subsubElem->Attribute("NOME");
            string t_desc = subsubElem->Attribute("DESCRICAO");

            this->add_tipo_quarto(id,QString(t_nome.data()),QString(t_desc.data()));
        }
    }

    // block: CLIENTES
    subElem = hRoot.FirstChild( "CLIENTES" ).Element();

    if(!subElem){
        no_hospede = true;
        no_reserva = true;
    }else{
        if(subElem->NoChildren()){
            no_hospede = true;
            no_reserva = true;
        }

        for( subsubElem = subElem->FirstChildElement("CLIENTE"); subsubElem; subsubElem=subsubElem->NextSiblingElement()){
            int id = 0;
            string t_id = subsubElem->Attribute("ID",&id);
            string t_nome = subsubElem->Attribute("NOME");
            string t_cpf = subsubElem->Attribute("CPF");
            string t_tl_movel = subsubElem->Attribute("TELEFONE_MOVEL");
            string t_tl_fixo = subsubElem->Attribute("TELEFONE_FIXO");

            TiXmlElement* subsubsubElem = subsubElem->FirstChildElement("ENDERECO");

            string t_d_rua = subsubsubElem->Attribute("RUA");
            string t_d_bairro = subsubsubElem->Attribute("BAIRRO");
            string t_d_cep = subsubsubElem->Attribute("CEP");
            string t_d_cidade = subsubsubElem->Attribute("CIDADE");
            string t_d_estado = subsubsubElem->Attribute("ESTADO");
            string t_d_pais = subsubsubElem->Attribute("PAIS");

            this->add_cliente(id,QString(t_nome.data()),QString(t_cpf.data()),QString(t_tl_movel.data()),QString(t_tl_fixo.data())
                              ,Endereco(QString(t_d_rua.data()),QString(t_d_cep.data()),t_d_bairro.data(),QString(t_d_cidade.data()),QString(t_d_estado.data()),QString(t_d_pais.data())));
        }
    }


    // block: QUARTOS
    if(!no_quarto){
        subElem = hRoot.FirstChild( "QUARTOS" ).Element();

        if(!subElem){
            no_reserva = true;
        }else{
            if(subElem->NoChildren()){
                no_reserva = true;
            }

            for( subsubElem = subElem->FirstChildElement("QUARTO"); subsubElem; subsubElem=subsubElem->NextSiblingElement()){
                int id = 0;
                string t_id = subsubElem->Attribute("ID",&id);
                string t_nome = subsubElem->Attribute("NUMERO");
                string t_desc = subsubElem->Attribute("DESCRICAO");
                string t_PAX = subsubElem->Attribute("PAX");
                string t_disp = subsubElem->Attribute("DISPONIBILIDADE");
                string t_tipo = subsubElem->Attribute("TIPO_DE_QUARTO_ID");

                this->add_quarto(id,QString(t_nome.data()),QString(t_desc.data()),QString(t_PAX.data()).toInt(),QString(t_tipo.data()),QString(t_disp.data()).toInt() == 1? true : false);
            }
        }
    }

    // block: HOSPEDES
    if(!no_hospede){
        subElem = hRoot.FirstChild( "HOSPEDES" ).Element();
        if(subElem){
            for( subsubElem = subElem->FirstChildElement("HOSPEDE"); subsubElem; subsubElem=subsubElem->NextSiblingElement()){
                int id = 0;
                string t_id = subsubElem->Attribute("ID",&id);
                string t_nome = subsubElem->Attribute("NOME");
                string t_data = subsubElem->Attribute("DATA_NASCIMENTO");
                string t_cpf = subsubElem->Attribute("CPF");
                string t_cliente = subsubElem->Attribute("CLIENTE_ID");

                this->add_hospede(id,QString(t_cliente.data()),QString(t_nome.data()),QDate::fromString(QString(t_data.data()),"dd/MM/yyyy"),QString(t_cpf.data()));
            }
        }
    }

    // block: RESERVAS
    if(!no_reserva){
        subElem = hRoot.FirstChild( "RESERVAS" ).Element();
        if(subElem){
            for( subsubElem = subElem->FirstChildElement("RESERVA"); subsubElem; subsubElem=subsubElem->NextSiblingElement()){
                int id = 0;


                string t_id = subsubElem->Attribute("ID",&id);
                string t_cliente = subsubElem->Attribute("CLIENTE_ID");

                vector<QString> t_quarto;
                TiXmlElement * subsubsubElem;
                for(subsubsubElem = subsubElem->FirstChildElement("QUARTO"); subsubsubElem; subsubsubElem=subsubsubElem->NextSiblingElement("QUARTO")){
                    string id = "";
                    int q_id = 0;
                    id = subsubsubElem->Attribute("QUARTO_ID",&q_id);

                    if(id != ""){
                        t_quarto.push_back(QString(id.data()));
                    }

                }


                string t_hospedes_adultos = subsubElem->Attribute("HOSPEDES_ADULTOS");
                string t_hospedes_criancas = subsubElem->Attribute("HOSPEDES_CRIANCAS");

                string t_data_inicio = subsubElem->Attribute("DATA_INICIO");
                string t_data_fim = subsubElem->Attribute("DATA_FIM");

                QString t_has_checkin = subsubElem->Attribute("HAS_CHECK_IN");
                string t_data_checkin = subsubElem->Attribute("DATA_CHECK_IN");
                QString t_has_checkout = subsubElem->Attribute("HAS_CHECK_OUT");
                string t_data_checkout = subsubElem->Attribute("DATA_CHECK_OUT");

                string t_status = subsubElem->Attribute("STATUS");

                vector<Hospede*> guests;
                for( subsubsubElem = subsubElem->FirstChildElement("HOSPEDE"); subsubsubElem; subsubsubElem=subsubsubElem->NextSiblingElement("HOSPEDE")){
                    int id = -1;
                    subsubsubElem->Attribute("ID",&id);

                    if(id != -1){
                        guests.push_back(get_hospede(id));
                    }

                }

                this->add_reserva(id,QString(t_cliente.data()),t_quarto,
                                  QDate::fromString(QString(t_data_inicio.data()),"dd/MM/yyyy"),QDate::fromString(QString(t_data_fim.data()),"dd/MM/yyyy"),
                                  QString(t_hospedes_adultos.data()).toInt(),QString(t_hospedes_criancas.data()).toInt(),
                                  t_has_checkin.toInt()==1?true:false,QDateTime::fromString(QString(t_data_checkin.data()),"dd/MM/yyyy hh-mm-ss"),
                                  t_has_checkout.toInt()==1?true:false,QDateTime::fromString(QString(t_data_checkout.data()),"dd/MM/yyyy hh-mm-ss"),
                                  Reserva::qstring_to_status(QString(t_status.data())),guests);
            }
        }
    }

    update_xml();

    emit updated();
}

void Pousada::get_tipos_quarto(vector<QString>* ID, vector<QString>* nome, vector<QString>* descricao, QString nome_filtro){
    for (auto e : pousada.tipos_quarto) {
        if(nome_filtro != ""){
            if(e->get_nome().contains(nome_filtro)){
                ID->emplace_back(QString::asprintf("%d",e->get_ID()));
                nome->emplace_back(e->get_nome());
                descricao->emplace_back(e->get_descricao());
            }
        } else {
            ID->emplace_back(QString::asprintf("%d",e->get_ID()));
            nome->emplace_back(e->get_nome());
            descricao->emplace_back(e->get_descricao());
        }
    }
}

void Pousada::get_quartos(vector<QString>* ID, vector<QString>* numero, vector<QString>* PAX, vector<QString>* disponivel, vector<QString>* tipo_quarto, vector<QString>* descricao,
                      QString nome_filtro, QString tipo_quarto_filtro){
    vector<int> quartos;

    for (int i = 0; i < pousada.quartos.size(); i++) {
        if(pousada.quartos.operator[](i)->get_numero().contains(nome_filtro) || nome_filtro == "")
            if(pousada.quartos.operator[](i)->get_tipo_quarto()->get_nome().contains(tipo_quarto_filtro) || tipo_quarto_filtro == "")
                quartos.emplace_back(i);
    }

    for(int i : quartos){
        ID->emplace_back(QString::asprintf("%d",pousada.quartos.operator[](i)->get_ID()));
        numero->emplace_back(pousada.quartos.operator[](i)->get_numero());
        PAX->emplace_back(QString::asprintf("%d",pousada.quartos.operator[](i)->get_PAX()).toStdString().data());
        disponivel->emplace_back(pousada.quartos.operator[](i)->get_disponibilidade()? "DISPONÍVEL" : "NÂO DISPONÍVEL");
        descricao->emplace_back(pousada.quartos.operator[](i)->get_descricao());
        tipo_quarto->emplace_back(pousada.quartos.operator[](i)->get_tipo_quarto()->get_nome());
    }
}

void Pousada::get_quartos(vector<QString>* ID, vector<QString>* numero, vector<QString>* PAX, vector<QString>* disponivel, vector<QString>* tipo_quarto, vector<QString>* descricao,
                 QString nome_filtro, QString tipo_quarto_filtro, QDate disponivel_inicio, QDate disponivel_fim){
    vector<int> quartos;

    for (int i = 0; i < pousada.quartos.size(); i++) {
        if(pousada.quartos.operator[](i)->get_numero().contains(nome_filtro) || nome_filtro == ""){
            if(pousada.quartos.operator[](i)->get_tipo_quarto()->get_nome().contains(tipo_quarto_filtro) || tipo_quarto_filtro == ""){
                //CHECKS AVAILABILITY THEN
                if(quarto_disponivel(pousada.quartos.operator[](i)->get_ID(),disponivel_inicio,disponivel_fim))
                    quartos.emplace_back(i);
            }
        }
    }

    for(int i : quartos){
        ID->emplace_back(QString::asprintf("%d",pousada.quartos.operator[](i)->get_ID()));
        numero->emplace_back(pousada.quartos.operator[](i)->get_numero());
        PAX->emplace_back(QString::asprintf("%d",pousada.quartos.operator[](i)->get_PAX()).toStdString().data());
        disponivel->emplace_back(pousada.quartos.operator[](i)->get_disponibilidade()? "DISPONÍVEL" : "NÂO DISPONÍVEL");
        descricao->emplace_back(pousada.quartos.operator[](i)->get_descricao());
        tipo_quarto->emplace_back(pousada.quartos.operator[](i)->get_tipo_quarto()->get_nome());
    }
}

void Pousada::get_clientes(vector<QString>* ID, vector<QString>* nome, vector<QString>* cpf, vector<QString>* telefone_movel, vector<QString>* telefone_fixo, vector<QString>* endereco,
                           QString nome_filtro, QString cpf_filtro){
    vector<int> clientes;

    for (int i = 0; i < pousada.clientes.size(); i++) {
        if(pousada.clientes.operator[](i)->get_nome().contains(nome_filtro) || nome_filtro == "")
            if(pousada.clientes.operator[](i)->get_cpf().contains(cpf_filtro) || cpf_filtro == "")
                clientes.emplace_back(i);
    }

    for(int i : clientes){
        ID->emplace_back(QString::asprintf("%d",pousada.clientes.operator[](i)->get_ID()));
        nome->emplace_back(pousada.clientes.operator[](i)->get_nome());
        cpf->emplace_back(pousada.clientes.operator[](i)->get_cpf());
        telefone_movel->emplace_back(pousada.clientes.operator[](i)->get_telefone_movel());
        telefone_fixo->emplace_back(pousada.clientes.operator[](i)->get_telefone_fixo());
        endereco->emplace_back(pousada.clientes.operator[](i)->get_endereco().to_string());
    }
}

void Pousada::get_hospedes(vector<QString>* ID, vector<QString>* nome, vector<QString>* cpf, vector<QString>* data_nascimento, vector<QString>* cliente_nome,
                          QString nome_filtro, QString cpf_filtro){
    vector<int> hospedes;

    for (int i = 0; i < pousada.hospedes.size(); i++) {
        if(pousada.hospedes.operator[](i)->get_nome().contains(nome_filtro) || nome_filtro == "")
            if(pousada.hospedes.operator[](i)->get_cpf().contains(cpf_filtro) || cpf_filtro == "")
                hospedes.emplace_back(i);
    }

    for(int i : hospedes){
        ID->emplace_back(QString::asprintf("%d",pousada.hospedes.operator[](i)->get_ID()));
        nome->emplace_back(pousada.hospedes.operator[](i)->get_nome());
        cpf->emplace_back(pousada.hospedes.operator[](i)->get_cpf());
        data_nascimento->emplace_back(pousada.hospedes.operator[](i)->get_data_nascimento());
        cliente_nome->emplace_back(pousada.hospedes.operator[](i)->get_cliente()->get_nome());
    }
}



void Pousada::get_reserva(vector<QString>* ID, vector<QString>* cliente, vector<QString>* quarto, vector<QString>* status, vector<QString>* data_inicio, vector<QString>* data_fim,
                 vector<QString>* hospedes_adultos, vector<QString>* hospedes_criancas, vector<QString>* has_checkin, vector<QString>* checkin, vector<QString>* has_checkout,
                 vector<QString>* checkout, vector<QString>* hospedes, QString cliente_filto, QString quarto_filtro, tuple<bool,QDate,QDate> inicio, tuple<bool,QDate,QDate> fim, pair<bool,Reserva::ReservaStatus> r_status){
    vector<int> reservas;

    for (int i = 0; i < pousada.reservas.size(); i++) {
        if(pousada.reservas.operator[](i)->get_cliente()->get_nome().contains(cliente_filto) || cliente_filto == "")
            if(pousada.reservas.operator[](i)->has_quarto(quarto_filtro) || quarto_filtro == "")
                if((get<0>(inicio) && (pousada.reservas.operator[](i)->get_data_inicio() >= get<1>(inicio) && pousada.reservas.operator[](i)->get_data_inicio() <= get<2>(inicio))) or !get<0>(inicio))
                    if((get<0>(fim) && (pousada.reservas.operator[](i)->get_data_fim() >= get<1>(fim) && pousada.reservas.operator[](i)->get_data_fim() <= get<2>(fim))) or !get<0>(fim))
                        if((r_status.first && (pousada.reservas.operator[](i)->get_status() == r_status.second)) || !r_status.first)
                            reservas.emplace_back(i);
    }

    for(int i : reservas){
        ID->emplace_back(QString::asprintf("%d",pousada.reservas.operator[](i)->get_ID()));
        cliente->emplace_back(pousada.reservas.operator[](i)->get_cliente()->get_nome());
        quarto->emplace_back(pousada.reservas.operator[](i)->get_quartos_string());
        status->emplace_back(Reserva::status_to_qstring(pousada.reservas.operator[](i)->get_status()));
        data_inicio->emplace_back(pousada.reservas.operator[](i)->get_data_inicio().toString("dd/MM/yyyy"));
        data_fim->emplace_back(pousada.reservas.operator[](i)->get_data_fim().toString("dd/MM/yyyy"));
        hospedes_adultos->emplace_back(QString::asprintf("%d",pousada.reservas.operator[](i)->get_hospedes_adultos()));
        hospedes_criancas->emplace_back(QString::asprintf("%d",pousada.reservas.operator[](i)->get_hospedes_criancas()));
        has_checkin->emplace_back(pousada.reservas.operator[](i)->checked_in()? "SIM" : "NÃO");
        checkin->emplace_back(pousada.reservas.operator[](i)->get_data_check_in().toString("dd/MM/yyyy hh-mm-ss"));
        has_checkout->emplace_back(pousada.reservas.operator[](i)->checked_out()? "SIM" : "NÃO");
        checkout->emplace_back(pousada.reservas.operator[](i)->get_data_check_out().toString("dd/MM/yyyy hh-mm-ss"));

        QString hosps;
        for(int h : pousada.reservas.operator[](i)->get_hospedes_list())
            hosps += QString::asprintf("%s //",get_hospede(h)->get_nome().toStdString().data());
        hospedes->emplace_back(QString(hosps));
    }
}

TipoQuarto* Pousada::get_tipo_quarto(int i){
    for (int j = 0; j < pousada.tipos_quarto.size(); j++) {
        TipoQuarto*  e = pousada.tipos_quarto.operator[](j);
        if(e->operator==(i))
            return e;
    }

    return NULL;
}

vector<pair<QString,int>> Pousada::get_tipos_quarto(){
    vector<pair<QString,int>> r;

    for(int i = 0; i < pousada.tipos_quarto.size(); i++){
        TipoQuarto* t = pousada.tipos_quarto.operator[](i);
        r.emplace_back(pair<QString,int>{t->get_nome(),t->get_ID()});
    }

    return r;
}

bool Pousada::has_quartos(){
    return !pousada.quartos.empty();
}

bool Pousada::has_tipos_quartos(){
    return !pousada.tipos_quarto.empty();
}

bool Pousada::has_clientes(){
    return !pousada.clientes.empty();
}

bool Pousada::has_hospedes(){
    return !pousada.hospedes.empty();
}

bool Pousada::has_reservas(){
    return !pousada.reservas.empty();
}

Quarto* Pousada::get_quarto(int i){
    for (int j = 0; j < pousada.quartos.size(); j++) {
        Quarto*  e = pousada.quartos.operator[](j);
        if(e->operator==(i))
            return e;
    }

    return NULL;
}

Cliente* Pousada::get_cliente(int i){
    for (int j = 0; j < pousada.clientes.size(); j++) {
        Cliente*  e = pousada.clientes.operator[](j);
        if(e->operator==(i))
            return e;
    }

    return NULL;
}

Hospede* Pousada::get_hospede(int i){
    for (int j = 0; j < pousada.hospedes.size(); j++) {
        Hospede*  e = pousada.hospedes.operator[](j);
        if(e->operator==(i))
            return e;
    }

    return NULL;
}

Reserva* Pousada::get_reserva(int i){
    for (int j = 0; j < pousada.reservas.size(); j++) {
        Reserva*  e = pousada.reservas.operator[](j);
        if(e->operator==(i))
            return e;
    }

    return NULL;
}

bool Pousada::quartos_disponivel(vector<QString> ID, QDate disponivel_inicio, QDate disponivel_fim){
    for(QString i : ID)
        if(!quarto_disponivel(i.toInt(),disponivel_inicio,disponivel_fim))
            return false;

    return true;
}

bool Pousada::quartos_disponivel(vector<int> ID, QDate disponivel_inicio, QDate disponivel_fim){
    for(int i : ID)
        if(!quarto_disponivel(i,disponivel_inicio,disponivel_fim))
            return false;

    return true;
}

bool Pousada::quarto_disponivel(int ID, QDate disponivel_inicio, QDate disponivel_fim){
    bool available  = true;

    vector<int> reservas = get_reserva_quarto_id(ID);

    for(int j = 0; j < reservas.size() && available; j++){
        int aux = reservas.operator[](j);
        Reserva* r = get_reserva(reservas.operator[](j));

        if(disponivel_inicio == r->get_data_inicio()
                or (disponivel_inicio > r->get_data_inicio() && disponivel_inicio < r->get_data_fim())
                or (disponivel_inicio < r->get_data_inicio() && disponivel_fim > r->get_data_inicio()))
            available = false;
    }

    return available;
}

vector<tuple<int,QString,QString,QString,QString>> Pousada::get_confirmations(){
    vector<tuple<int,QString,QString,QString,QString>> ans;
    for(int i = 0; i < pousada.reservas.size(); i++){
        Reserva* r = pousada.reservas.operator[](i);
        if(r->get_status() == Reserva::NAO_CONFIRMADA){
            ans.emplace_back(tuple<int,QString,QString,QString,QString>{r->get_ID(),r->get_cliente()->get_nome(),r->get_quartos_string()
                        ,r->get_data_inicio().toString("dd/MM/yyyy"),r->get_data_fim().toString("dd/MM/yyyy")});
        }
    }

    return ans;
}

vector<tuple<int,QString,QString,QString,QString>> Pousada::get_check_ins(){
    vector<tuple<int,QString,QString,QString,QString>> ans;
    for(int i = 0; i < pousada.reservas.size(); i++){
        Reserva* r = pousada.reservas.operator[](i);
        if(r->get_data_inicio() == QDate::currentDate() && r->get_status() == Reserva::CONFIRMADA){
            ans.emplace_back(tuple<int,QString,QString,QString,QString>{r->get_ID(),r->get_cliente()->get_nome(),r->get_quartos_string()
                        ,r->get_data_inicio().toString("dd/MM/yyyy"),r->get_data_fim().toString("dd/MM/yyyy")});
        }
    }

    return ans;
}

vector<tuple<int,QString,QString,QString,QString>> Pousada::get_check_outs(){
    vector<tuple<int,QString,QString,QString,QString>> ans;
    for(int i = 0; i < pousada.reservas.size(); i++){
        Reserva* r = pousada.reservas.operator[](i);
        if(r->get_data_fim() == QDate::currentDate() && r->get_status() == Reserva::CHECK_IN){
            ans.emplace_back(tuple<int,QString,QString,QString,QString>{r->get_ID(),r->get_cliente()->get_nome(),r->get_quartos_string()
                        ,r->get_data_inicio().toString("dd/MM/yyyy"),r->get_data_fim().toString("dd/MM/yyyy")});
        }
    }

    return ans;
}

vector<int> Pousada::get_quarto_tipo_id(int tipo){
    vector<int> r;

    for(Quarto* q : pousada.quartos)
        if(q->get_tipo_quarto()->operator==(tipo))
            r.push_back(q->get_ID());

    return r;
}

vector<QString> Pousada::get_quarto_tipo(int tipo){
    vector<QString> r;

    for(Quarto* q : pousada.quartos)
        if(q->get_tipo_quarto()->operator==(tipo))
            r.push_back(q->get_numero());

    return r;
}

bool Pousada::has_quarto_tipo(int tipo){
    for(Quarto* q : pousada.quartos)
        if(q->get_tipo_quarto()->operator==(tipo))
            return true;

    return false;
}

vector<int> Pousada::get_hospede_cliente_id(int cliente){
    vector<int> r;

    for(Hospede* h : pousada.hospedes)
        if(h->get_cliente()->operator==(cliente))
            r.push_back(h->get_ID());

    return r;
}

vector<QString> Pousada::get_hospede_cliente(int cliente){
    vector<QString> r;

    for(Hospede* h : pousada.hospedes)
        if(h->get_cliente()->operator==(cliente))
            r.push_back(h->get_nome());

    return r;
}

bool Pousada::has_hospede_cliente(int cliente){
    for(Hospede* h : pousada.hospedes)
        if(h->get_cliente()->operator==(cliente))
            return true;

    return false;
}

vector<int> Pousada::get_reserva_cliente_id(int cliente){
    vector<int> r_;

    for(Reserva* r : pousada.reservas){
        if(r->get_cliente()->operator==(cliente)){
            r_.push_back(r->get_ID());
        }
    }

    return r_;
}

vector<QString> Pousada::get_reserva_cliente(int cliente){
    vector<QString> r_;

    for(Reserva* r : pousada.reservas){
        if(r->get_cliente()->operator==(cliente)){
            r_.push_back(QString::asprintf("[%d] (%s) - (%s)",r->get_ID(),r->get_data_inicio().toString("dd/MM/yyyy").toStdString().data()
                                           ,r->get_data_fim().toString("dd/MM/yyyy").toStdString().data()));
        }
    }

    return r_;
}

bool Pousada::has_reserva_cliente(int cliente){
    for(Reserva* r : pousada.reservas)
        if(r->get_cliente()->operator==(cliente))
            return true;

    return false;
}

vector<int> Pousada::get_reserva_quarto_id(int quarto){
    vector<int> r_;

    for(Reserva* r : pousada.reservas){
        if(r->has_quarto(quarto)){
            r_.push_back(r->get_ID());
        }
    }

    return r_;
}

vector<QString> Pousada::get_reserva_quarto(int quarto){
    vector<QString> r_;

    for(Reserva* r : pousada.reservas){
        if(r->has_quarto(quarto)){
            r_.push_back(QString::asprintf("[%d] (%s) - (%s)",r->get_ID(),r->get_data_inicio().toString("dd/MM/yyyy").toStdString().data()
                                           ,r->get_data_fim().toString("dd/MM/yyyy").toStdString().data()));
        }
    }

    return r_;
}

bool Pousada::has_reserva_quarto(int quarto){
    for(Reserva* r : pousada.reservas)
        if(r->has_quarto(quarto))
            return true;

    return false;
}

Pousada& Pousada::remove_tipo_quarto(int ID){
    for (int i = 0; i < pousada.tipos_quarto.size(); i++) {
        if(pousada.tipos_quarto.operator[](i)->operator==(ID)){
            //REMOVE QUARTOS THIS TIPOQUARTO
            if(has_quarto_tipo(pousada.tipos_quarto.operator[](i)->get_ID())){
                for(int j = 0; j < pousada.quartos.size(); j++){
                    Quarto* q = pousada.quartos.operator[](j);
                    if(q->get_tipo_quarto()->operator==(ID)){
                        pousada.quartos.erase(pousada.quartos.begin() + j);

                        j--;
                    }
                }
            }

            pousada.tipos_quarto.erase(pousada.tipos_quarto.begin() + i);

            update_xml();

            emit updated();

            return *this;
        }
    }

    return *this;
}

Pousada& Pousada::remove_all_tipo_quarto(){
    for (int i = 0; i < pousada.tipos_quarto.size(); i++) {
        delete  pousada.tipos_quarto.operator[](i);

        pousada.tipos_quarto.erase(pousada.tipos_quarto.begin() + i );

        i--;
    }

    return *this;
}

void Pousada::update_xml(){
    doc->Clear();

    TiXmlComment * comment;
    string s;
    TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "", "" );
    doc->LinkEndChild( decl );

    TiXmlElement * root = new TiXmlElement("POUSADA");
    doc->LinkEndChild( root );

    comment = new TiXmlComment();
    s = " XML para POUSADA " + pousada.nome.toStdString() + " ";
    comment->SetValue(s.c_str());

    root->LinkEndChild( comment );

    //CLIENTES
    TiXmlElement * clientes = new TiXmlElement("CLIENTES");
    root->LinkEndChild( clientes );

    for(auto a : pousada.clientes)
        clientes->LinkEndChild(a->get_xml_node());

    //HOSPEDES
    TiXmlElement * hospedes = new TiXmlElement("HOSPEDES");
    root->LinkEndChild( hospedes );

    for(auto a : pousada.hospedes)
        hospedes->LinkEndChild(a->get_xml_node());

    //RESERVAS
    TiXmlElement * reservas = new TiXmlElement("RESERVAS");
    root->LinkEndChild( reservas );

    for(auto a : pousada.reservas)
        reservas->LinkEndChild(a->get_xml_node());

    //QUARTOS
    TiXmlElement * quartos = new TiXmlElement("QUARTOS");
    root->LinkEndChild( quartos );

    for(auto a : pousada.quartos)
        quartos->LinkEndChild(a->get_xml_node());

    //TIPOS DE QUARTOS
    TiXmlElement * tipos_quarto = new TiXmlElement("TIPOS_DE_QUARTO");
    root->LinkEndChild( tipos_quarto );

    for(auto a : pousada.tipos_quarto)
        tipos_quarto->LinkEndChild(a->get_xml_node());

}

void Pousada::clear(){
    remove_all_reserva();
    remove_all_hospede();
    remove_all_cliente();
    remove_all_quarto();
    remove_all_tipo_quarto();
}

void Pousada::external_updated(){
    emit updated();
}

Pousada::~Pousada(){
    remove_all_reserva();
    remove_all_hospede();
    remove_all_cliente();
    remove_all_quarto();
    remove_all_tipo_quarto();
    delete doc;
}
