#include "tipoquarto.h"

int TipoQuarto::TipoQuartoElemento::CURR_ID = 0;

TipoQuarto::TipoQuarto(QString nome, QString descricao) :  quarto({nome,descricao}), e("TIPO_DE_QUARTO")
{
    e.SetAttribute("ID",quarto.ID);
    e.SetAttribute("NOME",quarto.nome.toStdString().data());
    e.SetAttribute("DESCRICAO",quarto.descricao.toStdString().data());
}

TipoQuarto::TipoQuarto(int ID, QString nome, QString descricao) :  quarto({ID,nome,descricao}), e("TIPO_DE_QUARTO")
{
    e.SetAttribute("ID",quarto.ID);
    e.SetAttribute("NOME",quarto.nome.toStdString().data());
    e.SetAttribute("DESCRICAO",quarto.descricao.toStdString().data());
}

TipoQuarto::~TipoQuarto(){

}

int TipoQuarto::get_ID(){
    return quarto.ID;
}

QString TipoQuarto::get_nome(){
    return quarto.nome;
}

void TipoQuarto::set_nome(QString nome){
    quarto.nome = nome;

    e.RemoveAttribute("NOME");
    e.SetAttribute("NOME",nome.toStdString().data());
}

QString TipoQuarto::get_descricao(){
    return quarto.descricao;
}

void TipoQuarto::set_descricao(QString descricao){
    quarto.descricao = descricao;

    e.RemoveAttribute("DESCRICAO");
    e.SetAttribute("DESCRICAO",descricao.toStdString().data());
}

TiXmlNode* TipoQuarto::get_xml_node(){
    return e.Clone();
}

bool TipoQuarto::operator==(const TipoQuarto& e){
    return e.quarto.ID == this->quarto.ID;
}

bool TipoQuarto::operator==(const QString& s){
    return quarto.nome == s;
}

bool TipoQuarto::operator==(const int& i){
    return i == quarto.ID;
}

TipoQuarto::TipoQuartoElemento::TipoQuartoElemento(QString nome, QString descricao) : ID(CURR_ID++), nome(nome), descricao(descricao)
{

}

TipoQuarto::TipoQuartoElemento::TipoQuartoElemento(int ID, QString nome, QString descricao) : ID(ID), nome(nome), descricao(descricao)
{
    if(ID >= CURR_ID)
        CURR_ID = ID + 1;
}

TipoQuarto::TipoQuartoElemento::TipoQuartoElemento(const TipoQuartoElemento& e) : ID(e.ID), nome(e.nome), descricao(e.descricao)
{

}

const TipoQuarto::TipoQuartoElemento& TipoQuarto::TipoQuartoElemento::operator=(const TipoQuartoElemento& e){
    return {e};
}

bool TipoQuarto::TipoQuartoElemento::operator==(const TipoQuartoElemento& e){
    if(ID == e.ID)
        return true;

    return false;
}
