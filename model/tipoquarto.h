#ifndef TIPOQUARTO_H
#define TIPOQUARTO_H

#include <QString>
#include <vector>
#include <tinyxml/tinyxml.h>

#define TIPO_QUARTO_STRING "TIPO_DE_QUARTO"

using namespace std;

class TipoQuarto
{
public:
    /***
    * Elemento - Tipo Quarto
    **/
    class TipoQuartoElemento{
    friend class TipoQuarto;
    public:
    private:
        const int ID;
        QString nome;
        QString descricao;
        TipoQuartoElemento(int ID, QString nome, QString descricao);
        TipoQuartoElemento(QString nome, QString descricao);
        TipoQuartoElemento(const TipoQuartoElemento& e);

        const TipoQuartoElemento& operator=(const TipoQuartoElemento& e);

        bool operator==(const TipoQuartoElemento& e);

        static int CURR_ID;
    };

    TipoQuarto(int ID, QString nome, QString descricao);
    TipoQuarto(QString nome, QString descricao);
    ~TipoQuarto();

    /***
     * GETS & SETS
    ***/
    int get_ID();

    QString get_nome();
    void set_nome(QString nome);

    QString get_descricao();
    void set_descricao(QString descricao);

    bool has_quarto_tipo();

    TiXmlNode* get_xml_node();

    bool operator==(const TipoQuarto& e);
    bool operator==(const QString& s);
    bool operator==(const int& i);


private:

    TiXmlElement e;
    TipoQuartoElemento quarto;
};

#endif // TIPOQUARTO_H
