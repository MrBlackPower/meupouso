#ifndef CLIENTE_H
#define CLIENTE_H

#include <QString>
#include <vector>
#include <tinyxml/tinyxml.h>

#include "endereco.h"

using namespace std;

class Reserva;

class Cliente
{
public:
    /***
    * Elemento - Cliente
    **/
    class ClienteElemento{
    friend class Cliente;
    public:
    private:
        const int ID;
        QString nome;
        QString cpf;
        QString telefone_movel;
        QString telefone_fixo;

        Endereco endereco;

        ClienteElemento(int ID, QString nome, QString cpf, QString telefone_movel, QString telefone_fixo, Endereco endereco);
        ClienteElemento(QString nome, QString cpf, QString telefone_movel, QString telefone_fixo, Endereco endereco);
        ClienteElemento(const ClienteElemento& e);

        const ClienteElemento& operator=(const ClienteElemento& e);

        bool operator==(const ClienteElemento& e);

        static int CURR_ID;
    };

    void add_reserva(Reserva* r);

    vector<Reserva*> get_reservas();

    bool operator==(Cliente* c);
    bool operator==(const int& i);

    /***
     * GETS & SETS
    ***/
    int get_ID();

    QString get_nome();
    void set_nome(QString nome);

    QString get_cpf();
    void set_cpf(QString cpf);

    QString get_telefone_movel();
    void set_telefone_movel(QString telefone_movel);

    QString get_telefone_fixo();
    void set_telefone_fixo(QString telefone_fixo);

    Endereco get_endereco();
    void set_endereco(QString rua, QString CEP, QString bairro, QString cidade, QString estado, QString pais);
    void set_endereco(Endereco endereco);

    TiXmlNode* get_xml_node();

    Cliente(int ID, QString nome, QString cpf, QString telefone_movel, QString telefone_fix, Endereco enderecoo);
    Cliente(QString nome, QString cpf, QString telefone_movel, QString telefone_fix, Endereco enderecoo);
    ~Cliente();

private:
    ClienteElemento cliente;

    TiXmlElement e;
};

#endif // CLIENTE_H
