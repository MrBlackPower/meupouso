#ifndef QUARTO_H
#define QUARTO_H

#include <QString>
#include <vector>
#include <tinyxml/tinyxml.h>

#include "tipoquarto.h"

using namespace std;

class Quarto
{
public:
    /***
    * Elemento - Quarto
    **/
    class QuartoElemento{
    friend class Quarto;
    public:
    private:
        const int ID;
        QString numero;
        QString descricao;
        int PAX;
        bool disponivel;

        TipoQuarto* tipo_quarto;

        QuartoElemento(int ID, QString nome, QString descricao, int PAX, TipoQuarto* tipo, bool disponivel);
        QuartoElemento(QString nome, QString descricao, int PAX, TipoQuarto* tipo, bool disponivel);
        QuartoElemento(const QuartoElemento& e);

        const QuartoElemento& operator=(const QuartoElemento& e);

        bool operator==(const QuartoElemento& e);

        static int CURR_ID;
    };


    Quarto(int ID, QString numero, QString descricao, int PAX, TipoQuarto* tipo, bool disponivel);
    Quarto(QString numero, QString descricao, int PAX, TipoQuarto* tipo, bool disponivel);
    ~Quarto();

    bool operator==(Quarto* q);
    bool operator==(const int& i);

    /***
     * GETS & SETS
    ***/
    int get_ID();

    QString get_numero();
    void set_numero(QString numero);

    QString get_descricao();
    void set_descricao(QString descricao);

    int get_PAX();
    void set_PAX(int PAX);

    bool get_disponibilidade();
    void set_disponibilidade(bool disp);

    TipoQuarto* get_tipo_quarto();
    void set_tipo_quarto(TipoQuarto* quarto);

    TiXmlNode* get_xml_node();

    Quarto* get_reference();

private:
    TiXmlElement e;
    QuartoElemento quarto;
};

#endif // QUARTO_H
