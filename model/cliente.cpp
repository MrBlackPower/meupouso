#include "cliente.h"
int Cliente::ClienteElemento::CURR_ID = 0;

Cliente::Cliente(QString nome, QString cpf, QString telefone_movel, QString telefone_fixo, Endereco endereco) : cliente(nome,cpf,telefone_movel,telefone_fixo,endereco), e("CLIENTE")
{
    e.SetAttribute("ID",cliente.ID);
    e.SetAttribute("NOME",cliente.nome.toStdString().data());
    e.SetAttribute("CPF",cliente.cpf.toStdString().data());
    e.SetAttribute("TELEFONE_MOVEL",cliente.telefone_movel.toStdString().data());
    e.SetAttribute("TELEFONE_FIXO",cliente.telefone_fixo.toStdString().data());

    e.LinkEndChild(cliente.endereco.get_xml_node());
}

Cliente::Cliente(int ID, QString nome, QString cpf, QString telefone_movel, QString telefone_fixo, Endereco endereco) : cliente(ID,nome,cpf,telefone_movel,telefone_fixo,endereco), e("CLIENTE")
{
    e.SetAttribute("ID",cliente.ID);
    e.SetAttribute("NOME",cliente.nome.toStdString().data());
    e.SetAttribute("CPF",cliente.cpf.toStdString().data());
    e.SetAttribute("TELEFONE_MOVEL",cliente.telefone_movel.toStdString().data());
    e.SetAttribute("TELEFONE_FIXO",cliente.telefone_fixo.toStdString().data());

    e.LinkEndChild(cliente.endereco.get_xml_node());
}

Cliente::~Cliente(){

}

int Cliente::get_ID(){
    return cliente.ID;
}

bool Cliente::operator==(Cliente* c){
    return c->get_ID() == cliente.ID;
}

bool Cliente::operator==(const int& i){
    return cliente.ID == i;
}

QString Cliente::get_nome(){
    return cliente.nome;
}

void Cliente::set_nome(QString nome){
    cliente.nome = nome;

    e.RemoveAttribute("NOME");
    e.SetAttribute("NOME",cliente.nome.toStdString().data());
}

QString Cliente::get_cpf(){
    return cliente.cpf;
}

void Cliente::set_cpf(QString cpf){
    cliente.cpf = cpf;

    e.RemoveAttribute("CPF");
    e.SetAttribute("CPF",cliente.cpf.toStdString().data());
}

QString Cliente::get_telefone_movel(){
    return cliente.telefone_movel;
}

void Cliente::set_telefone_movel(QString telefone_movel){
    cliente.telefone_movel = telefone_movel;

    e.RemoveAttribute("TELEFONE_MOVEL");
    e.SetAttribute("TELEFONE_MOVEL",cliente.telefone_movel.toStdString().data());
}

QString Cliente::get_telefone_fixo(){
    return cliente.telefone_fixo;
}

void Cliente::set_telefone_fixo(QString telefone_fixo){
    cliente.telefone_fixo = telefone_fixo;

    e.RemoveAttribute("TELEFONE_FIXO");
    e.SetAttribute("TELEFONE_FIXO",cliente.telefone_fixo.toStdString().data());
}

Endereco Cliente::get_endereco(){
    return cliente.endereco;
}

void Cliente::set_endereco(QString rua, QString CEP, QString bairro, QString cidade, QString estado, QString pais){
    e.Clear();

    cliente.endereco.set_rua(rua);
    cliente.endereco.set_CEP(CEP);
    cliente.endereco.set_bairro(bairro);
    cliente.endereco.set_cidade(cidade);
    cliente.endereco.set_estado(estado);
    cliente.endereco.set_pais(pais);


    e.LinkEndChild(cliente.endereco.get_xml_node());
}

void Cliente::set_endereco(Endereco endereco){
    set_endereco(endereco.get_rua(),endereco.get_CEP(),endereco.get_bairro(),endereco.get_cidade(),endereco.get_estado(),endereco.get_pais());
}


TiXmlNode* Cliente::get_xml_node(){
    return  e.Clone();
}

Cliente::ClienteElemento::ClienteElemento(int ID, QString nome, QString cpf, QString telefone_movel, QString telefone_fixo, Endereco endereco) :
    ID(ID), nome(nome), cpf(cpf), telefone_movel(telefone_movel), telefone_fixo(telefone_fixo), endereco(endereco)
{
    if(ID >= CURR_ID)
        CURR_ID = ID + 1;
}

Cliente::ClienteElemento::ClienteElemento(QString nome, QString cpf, QString telefone_movel, QString telefone_fixo, Endereco endereco) :
    ID(CURR_ID++), nome(nome), cpf(cpf), telefone_movel(telefone_movel), telefone_fixo(telefone_fixo), endereco(endereco)
{

}

Cliente::ClienteElemento::ClienteElemento(const ClienteElemento& e) : ID(e.ID), endereco(e.endereco)
{
    nome = e.nome;
    cpf = e.cpf;
    telefone_fixo = e.telefone_fixo;
    telefone_movel = e.telefone_movel;
}

const Cliente::ClienteElemento& Cliente::ClienteElemento::operator=(const ClienteElemento& e){
    return {e};
}

bool Cliente::ClienteElemento::operator==(const ClienteElemento& e){
    return e.ID == ID;
}
