#ifndef HOSPEDE_H
#define HOSPEDE_H

#include <QString>
#include <QDate>
#include <vector>
#include <tinyxml/tinyxml.h>
#include "cliente.h"

using namespace std;

class Hospede
{
public:
    /***
    * Elemento - Reserva
    **/
    class HospedeElemento{
    friend class Hospede;
    public:
    private:
        const int ID;
        QString nome;
        QDate data_nascimento;
        QString cpf;

        Cliente* hospede_cliente;

        HospedeElemento(int ID, Cliente* cliente, QString nome, QDate data_nascimento, QString cpf);
        HospedeElemento(Cliente* cliente, QString nome, QDate data_nascimento, QString cpf);
        HospedeElemento(const HospedeElemento& e);
        
        const HospedeElemento& operator=(const HospedeElemento& e);

        bool operator==(const HospedeElemento& e);

        static int CURR_ID;
    };

    Hospede(Cliente* cliente, QString nome, QString data_nascimento, QString cpf);
    Hospede(int ID, Cliente* cliente, QString nome, QDate data_nascimento, QString cpf);
    Hospede(Cliente* cliente, QString nome, QDate data_nascimento, QString cpf);
    ~Hospede();

    /***
     * GETS & SETS
    ***/
    int get_ID();

    QString get_nome();
    void set_nome(QString nome);

    QString get_data_nascimento();
    void set_data_nascimento(QString data_nascimento);
    void set_data_nascimento(QDate data_nascimento);

    QString get_cpf();
    void set_cpf(QString cpf);

    Cliente* get_cliente();
    void set_cliente(Cliente* cliente);

    bool operator==(Hospede* h);
    bool operator==(const int& i);

    TiXmlNode* get_xml_node();

private:
    HospedeElemento hospede;

    TiXmlElement e;
};

#endif // HOSPEDE_H
