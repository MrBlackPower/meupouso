#ifndef POUSADA_H
#define POUSADA_H

#include <QString>
#include <QObject>
#include <vector>
#include <iostream>
#include <tinyxml/tinyxml.h>

#include "quarto.h"
#include "cliente.h"
#include "reserva.h"
#include "hospede.h"
#include "helper/qstringformathelper.h"

#define NOME_POUSADA "POUSADA DAS ROSAS"

using namespace std;

class Pousada : public QObject
{
Q_OBJECT
public:
    /***
    * Elemento - Quarto
    **/
    class PousadaElemento{
    friend class Pousada;
    public:
        QString nome;

        vector<TipoQuarto*> tipos_quarto;
        vector<Quarto*> quartos;
        vector<Reserva*> reservas;
        vector<Cliente*> clientes;
        vector<Hospede*> hospedes;

    private:

        PousadaElemento(QString nome);
    };

    Pousada(QString nome = NOME_POUSADA);
    ~Pousada();

    friend std::ostream& operator<<(std::ostream& os, const Pousada& pousada);
    friend string& operator<<(string& os, const Pousada& pousada);

    bool load(QString file);

    void get_tipos_quarto(vector<QString>* ID, vector<QString>* nome, vector<QString>* descricao, QString nome_filtro);

    void get_quartos(vector<QString>* ID, vector<QString>* numero, vector<QString>* PAX, vector<QString>* disponivel, vector<QString>* tipo_quarto, vector<QString>* descricao,
                          QString nome_filtro, QString tipo_quarto_filtro);

    void get_quartos(vector<QString>* ID, vector<QString>* numero, vector<QString>* PAX, vector<QString>* disponivel, vector<QString>* tipo_quarto, vector<QString>* descricao,
                          QString nome_filtro, QString tipo_quarto_filtro, QDate disponivel_inicio, QDate disponivel_fim);

    void get_clientes(vector<QString>* ID, vector<QString>* nome, vector<QString>* cpf, vector<QString>* telefone_movel, vector<QString>* telefone_fixo, vector<QString>* endereco,
                      QString nome_filtro, QString cpf_filtro);

    void get_hospedes(vector<QString>* ID, vector<QString>* nome, vector<QString>* cpf, vector<QString>* data_nascimento, vector<QString>* cliente_nome,
                      QString nome_filtro, QString cpf_filtro);

    void get_reserva(vector<QString>* ID, vector<QString>* cliente, vector<QString>* quarto,vector<QString>* status, vector<QString>* data_inicio, vector<QString>* data_fim,
                     vector<QString>* hospedes_adultos, vector<QString>* hospedes_criancas, vector<QString>* has_checkin, vector<QString>* checkin, vector<QString>* has_checkout,
                     vector<QString>* checkout, vector<QString>* hospedes, QString cliente_filto, QString quarto_filtro, tuple<bool,QDate,QDate> inicio, tuple<bool,QDate,QDate> fim, pair<bool,Reserva::ReservaStatus> r_status);

    TipoQuarto* get_tipo_quarto(int i);

    vector<pair<QString,int>> get_tipos_quarto();

    bool has_quartos();

    bool has_tipos_quartos();

    bool has_clientes();

    bool has_hospedes();

    bool has_reservas();

    Quarto* get_quarto(int i);

    Cliente* get_cliente(int i);

    Hospede* get_hospede(int i);

    Reserva* get_reserva(int i);

    bool quarto_disponivel(int ID, QDate disponivel_inicio, QDate disponivel_fim);

    bool quartos_disponivel(vector<QString> ID, QDate disponivel_inicio, QDate disponivel_fim);

    bool quartos_disponivel(vector<int> ID, QDate disponivel_inicio, QDate disponivel_fim);

    vector<tuple<int,QString,QString,QString,QString>> get_confirmations();

    vector<tuple<int,QString,QString,QString,QString>> get_check_ins();

    vector<tuple<int,QString,QString,QString,QString>> get_check_outs();

    //QUARTO VS TIPOQUARTO

    vector<int> get_quarto_tipo_id(int tipo);

    vector<QString> get_quarto_tipo(int tipo);

    bool has_quarto_tipo(int tipo);


    //HOSPEDE VS CLIENTE

    vector<int> get_hospede_cliente_id(int cliente);

    vector<QString> get_hospede_cliente(int cliente);

    bool has_hospede_cliente(int cliente);


    //RESERVA VS (CLIENTE & QUARTO)

    vector<int> get_reserva_cliente_id(int cliente);

    vector<QString> get_reserva_cliente(int cliente);

    bool has_reserva_cliente(int cliente);

    vector<int> get_reserva_quarto_id(int quarto);

    vector<QString> get_reserva_quarto(int quarto);

    bool has_reserva_quarto(int quarto);


public slots:

    Pousada& add_tipo_quarto(QString nome, QString descricao);

    Pousada& edit_tipo_quarto(int ID, QString nome, QString descricao);

    Pousada& remove_tipo_quarto(int ID);

    Pousada& remove_all_tipo_quarto();

    Pousada& add_quarto(QString numero, QString descricao, int PAX, QString tipo_quarto, bool disponivel);

    Pousada& edit_quarto(int ID, QString numero, QString descricao, int PAX, QString tipo_quarto, bool disponivel);

    Pousada& remove_quarto(int ID);

    Pousada& remove_all_quarto();

    Pousada& add_cliente(QString nome, QString cpf, QString telefone_movel, QString telefone_fixo, Endereco endereco);

    Pousada& edit_cliente(int ID, QString nome, QString cpf, QString telefone_movel, QString telefone_fixo, Endereco endereco);

    Pousada& remove_cliente(int ID);

    Pousada& remove_all_cliente();

    Pousada& add_hospede(QString cliente_ID, QString nome, QDate data_nascimento, QString cpf);

    Pousada& edit_hospede(int ID, QString cliente_ID, QString nome, QString cpf, QDate data_nascimento);

    Pousada& remove_hospede(int ID);

    Pousada& remove_all_hospede();

    Pousada& add_reserva(QString cliente_ID, vector<QString> quarto_ID, QDate data_inicio, QDate data_fim, int hospedes_adultos, int hospedes_criancas);

    Pousada& edit_reserva(int ID, int hospedes_adultos, int hospedes_criancas);

    Pousada& confirma_reserva(int ID);

    Pousada& cancela_reserva(int ID);

    Pousada& no_show_reserva(int ID);

    Pousada& checkin_reserva(int ID, vector<int>hospedes_ID, QDateTime date_time);

    Pousada& checkout_reserva(int ID, QDateTime date_time);

    Pousada& remove_reserva(int ID);

    Pousada& remove_all_reserva();

    void update_xml();

    void clear();

    void external_updated();

signals:
    void push_log(QString line, QString origin);

    void updated();

private:
    Pousada& add_tipo_quarto(int tipo_quarto_ID, QString nome, QString descricao);
    
    Pousada& add_quarto(int quarto_ID, QString numero, QString descricao, int PAX, QString tipo_quarto, bool disponivel);
    
    Pousada& add_cliente(int cliente_ID, QString nome, QString cpf, QString telefone_movel, QString telefone_fixo, Endereco endereco);
    
    Pousada& add_hospede(int hospede_ID, QString cliente_ID, QString nome, QDate data_nascimento, QString cpf);
    
    Pousada& add_reserva(int reserva_ID, QString cliente_ID, vector<QString> quarto_ID, QDate data_inicio, QDate data_fim, int hospedes_adultos, int hospedes_criancas,
                         bool has_check_in, QDateTime check_in, bool has_check_out, QDateTime check_out, Reserva::ReservaStatus status, vector<Hospede*> hospedes);

    PousadaElemento pousada;
    TiXmlDocument* doc;
};

#endif // POUSADA_H
