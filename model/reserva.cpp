#include "reserva.h"

int Reserva::ReservaElemento::CURR_ID = 0;

Reserva::Reserva(Cliente* cliente, vector<Quarto*> quarto, QString data_inicio, QString data_fim, int hospedes_adultos, int hospedes_criancas) :
    reserva(cliente,quarto,QDate::fromString(data_inicio,"dd/MM/yyyy"),QDate::fromString(data_fim,"dd/MM/yyyy"),hospedes_adultos,hospedes_criancas), e("RESERVA")
{
    e.SetAttribute("ID",reserva.ID);
    e.SetAttribute("CLIENTE_ID",cliente->get_ID());
    e.SetAttribute("CLIENTE_NOME",cliente->get_nome().toStdString().data());

    for(Quarto* q : quarto){
        TiXmlElement* aux = new TiXmlElement("QUARTO");
        aux->SetAttribute("QUARTO_ID",q->get_ID());
        aux->SetAttribute("QUARTO_NUMERO",q->get_numero().toStdString().data());

        e.LinkEndChild(aux);
    }

    e.SetAttribute("HOSPEDES_ADULTOS",hospedes_adultos);
    e.SetAttribute("HOSPEDES_CRIANCAS",hospedes_criancas);

    e.SetAttribute("DATA_INICIO",reserva.data_inicio.toString("dd/MM/yyyy").toStdString().data());
    e.SetAttribute("DATA_FIM",reserva.data_fim.toString("dd/MM/yyyy").toStdString().data());

    e.SetAttribute("DATA_CHECK_IN",reserva.data_check_in.toString("dd/MM/yyyy hh-mm-ss").toStdString().data());
    e.SetAttribute("HAS_CHECK_IN",reserva.has_check_in?"1":"0");
    e.SetAttribute("DATA_CHECK_OUT",reserva.data_check_out.toString("dd/MM/yyyy hh-mm-ss").toStdString().data());
    e.SetAttribute("HAS_CHECK_OUT",reserva.has_check_out?"1":"0");

    e.SetAttribute("STATUS",status_to_qstring(reserva.status).toStdString().data());
}
Reserva::Reserva(Cliente* cliente, vector<Quarto*> quarto, QDate data_inicio, QDate data_fim, int hospedes_adultos, int hospedes_criancas) :
    reserva(cliente,quarto,data_inicio,data_fim,hospedes_adultos,hospedes_criancas), e("RESERVA")
{
    e.SetAttribute("ID",reserva.ID);
    e.SetAttribute("CLIENTE_ID",cliente->get_ID());
    e.SetAttribute("CLIENTE_NOME",cliente->get_nome().toStdString().data());


    for(Quarto* q : quarto){
        TiXmlElement* aux = new TiXmlElement("QUARTO");
        aux->SetAttribute("QUARTO_ID",q->get_ID());
        aux->SetAttribute("QUARTO_NUMERO",q->get_numero().toStdString().data());

        e.LinkEndChild(aux);
    }

    e.SetAttribute("HOSPEDES_ADULTOS",hospedes_adultos);
    e.SetAttribute("HOSPEDES_CRIANCAS",hospedes_criancas);

    e.SetAttribute("DATA_INICIO",reserva.data_inicio.toString("dd/MM/yyyy").toStdString().data());
    e.SetAttribute("DATA_FIM",reserva.data_fim.toString("dd/MM/yyyy").toStdString().data());

    e.SetAttribute("DATA_CHECK_IN",reserva.data_check_in.toString("dd/MM/yyyy hh-mm-ss").toStdString().data());
    e.SetAttribute("HAS_CHECK_IN",reserva.has_check_in?"1":"0");
    e.SetAttribute("DATA_CHECK_OUT",reserva.data_check_out.toString("dd/MM/yyyy hh-mm-ss").toStdString().data());
    e.SetAttribute("HAS_CHECK_OUT",reserva.has_check_out?"1":"0");

    e.SetAttribute("STATUS",status_to_qstring(reserva.status).toStdString().data());
}

Reserva::Reserva(Cliente* cliente, vector<Quarto*> quarto, QDate data_inicio, QDate data_fim, int hospedes_adultos, int hospedes_criancas,
        bool has_check_in, QDateTime check_in, bool has_check_out, QDateTime check_out, ReservaStatus status, vector<Hospede*> hospedes) :
    reserva(cliente,quarto,data_inicio,data_fim,hospedes_adultos,hospedes_criancas,has_check_in,check_in,has_check_out,check_out,status,hospedes), e("RESERVA")
{
    e.SetAttribute("ID",reserva.ID);
    e.SetAttribute("CLIENTE_ID",cliente->get_ID());
    e.SetAttribute("CLIENTE_NOME",cliente->get_nome().toStdString().data());

    for(Quarto* q : quarto){
        TiXmlElement* aux = new TiXmlElement("QUARTO");
        aux->SetAttribute("QUARTO_ID",q->get_ID());
        aux->SetAttribute("QUARTO_NUMERO",q->get_numero().toStdString().data());

        e.LinkEndChild(aux);
    }

    e.SetAttribute("HOSPEDES_ADULTOS",hospedes_adultos);
    e.SetAttribute("HOSPEDES_CRIANCAS",hospedes_criancas);

    e.SetAttribute("DATA_INICIO",reserva.data_inicio.toString("dd/MM/yyyy").toStdString().data());
    e.SetAttribute("DATA_FIM",reserva.data_fim.toString("dd/MM/yyyy").toStdString().data());

    e.SetAttribute("DATA_CHECK_IN",reserva.data_check_in.toString("dd/MM/yyyy hh-mm-ss").toStdString().data());
    e.SetAttribute("HAS_CHECK_IN",reserva.has_check_in?"1":"0");
    e.SetAttribute("DATA_CHECK_OUT",reserva.data_check_out.toString("dd/MM/yyyy hh-mm-ss").toStdString().data());
    e.SetAttribute("HAS_CHECK_OUT",reserva.has_check_out?"1":"0");

    e.SetAttribute("STATUS",status_to_qstring(reserva.status).toStdString().data());
    //ADDS HOSPEDES
    for(Hospede* hosp : hospedes){
        int id = hosp->get_ID();
        QString nome = hosp->get_nome();

        TiXmlElement* aux = new TiXmlElement("HOSPEDE");
        aux->SetAttribute("ID",id);
        aux->SetAttribute("NOME",nome.toStdString().data());

        e.LinkEndChild(aux);
    }
}

Reserva::Reserva(int ID, Cliente* cliente, vector<Quarto*> quarto, QDate data_inicio, QDate data_fim, int hospedes_adultos, int hospedes_criancas,
        bool has_check_in, QDateTime check_in, bool has_check_out, QDateTime check_out, ReservaStatus status, vector<Hospede*> hospede) :
    reserva(ID,cliente,quarto,data_inicio,data_fim,hospedes_adultos,hospedes_criancas,has_check_in,check_in,has_check_out,check_out,status,hospede), e("RESERVA")
{
    e.SetAttribute("ID",reserva.ID);
    e.SetAttribute("CLIENTE_ID",cliente->get_ID());
    e.SetAttribute("CLIENTE_NOME",cliente->get_nome().toStdString().data());

    for(Quarto* q : quarto){
        TiXmlElement* aux = new TiXmlElement("QUARTO");
        aux->SetAttribute("QUARTO_ID",q->get_ID());
        aux->SetAttribute("QUARTO_NUMERO",q->get_numero().toStdString().data());

        e.LinkEndChild(aux);
    }

    e.SetAttribute("HOSPEDES_ADULTOS",hospedes_adultos);
    e.SetAttribute("HOSPEDES_CRIANCAS",hospedes_criancas);

    e.SetAttribute("DATA_INICIO",reserva.data_inicio.toString("dd/MM/yyyy").toStdString().data());
    e.SetAttribute("DATA_FIM",reserva.data_fim.toString("dd/MM/yyyy").toStdString().data());

    e.SetAttribute("DATA_CHECK_IN",reserva.data_check_in.toString("dd/MM/yyyy hh-mm-ss").toStdString().data());
    e.SetAttribute("HAS_CHECK_IN",reserva.has_check_in?"1":"0");
    e.SetAttribute("DATA_CHECK_OUT",reserva.data_check_out.toString("dd/MM/yyyy hh-mm-ss").toStdString().data());
    e.SetAttribute("HAS_CHECK_OUT",reserva.has_check_out?"1":"0");

    e.SetAttribute("STATUS",status_to_qstring(reserva.status).toStdString().data());

    //ADDS HOSPEDES
    for(Hospede* hosp : hospede){
        int id = hosp->get_ID();
        QString nome = hosp->get_nome();

        TiXmlElement* aux = new TiXmlElement("HOSPEDE");
        aux->SetAttribute("ID",id);
        aux->SetAttribute("NOME",nome.toStdString().data());

        e.LinkEndChild(aux);
    }
}

Reserva::~Reserva(){

}

Reserva::ReservaStatus Reserva::get_status(){
    return reserva.status;
}

QString Reserva::status_to_qstring(ReservaStatus status){
    switch (status) {
    case NAO_CONFIRMADA:
        return "NAO_CONFIRMADA";
        break;
    case CONFIRMADA:
        return "CONFIRMADA";
        break;
    case CHECK_IN:
        return "CHECK_IN";
        break;
    case CHECK_OUT:
        return "CHECK_OUT";
        break;
    case CANCELADA:
        return "CANCELADA";
        break;
    }

    return "NO_SHOW";
}

Reserva::ReservaStatus Reserva::qstring_to_status(QString status){
    if(status == "NAO_CONFIRMADA")
        return NAO_CONFIRMADA;
    if(status == "CONFIRMADA")
        return CONFIRMADA;
    if(status == "CHECK_IN")
        return CHECK_IN;
    if(status == "CHECK_OUT")
        return CHECK_OUT;
    if(status == "CANCELADA")
        return CANCELADA;

    return NO_SHOW;
}

bool Reserva::confirm(){
    if(reserva.status == NAO_CONFIRMADA){
        reserva.status = CONFIRMADA;

        e.SetAttribute("STATUS",status_to_qstring(reserva.status).toStdString().data());

        return true;
    }
    return false;
}

bool Reserva::cancel(){
    if(reserva.status != CANCELADA && reserva.status != CHECK_OUT){
        reserva.status = CANCELADA;
        return true;
    }

    return false;
}

bool Reserva::no_show(){
    if(reserva.status != CANCELADA && reserva.status != NO_SHOW && reserva.status != CHECK_OUT){
        reserva.status = NO_SHOW;
        return true;
    }

    return false;
}

bool Reserva::check_in(vector<Hospede*> hospedes, QDateTime date_time){
    if(reserva.status == CONFIRMADA){
        reserva.has_check_in = true;
        reserva.data_check_in = date_time;
        reserva.hospedes = hospedes;

        reserva.status = CHECK_IN;

        e.RemoveAttribute("HAS_CHECK_IN");
        e.RemoveAttribute("DATA_CHECK_IN");
        e.RemoveAttribute("STATUS");
        e.SetAttribute("HAS_CHECK_IN",reserva.has_check_in?"1":"0");
        e.SetAttribute("DATA_CHECK_IN",reserva.data_check_in.toString("dd/MM/yyyy hh-mm-ss").toStdString().data());
        e.SetAttribute("STATUS",status_to_qstring(reserva.status).toStdString().data());

        if(!e.NoChildren()){
            e.Clear();
        }

        //ADDS QUARTOS
        for(Quarto* q : reserva.quarto){
            TiXmlElement* aux = new TiXmlElement("QUARTO");
            aux->SetAttribute("QUARTO_ID",q->get_ID());
            aux->SetAttribute("QUARTO_NUMERO",q->get_numero().toStdString().data());

            e.LinkEndChild(aux);
        }

        //ADDS HOSPEDES
        for(Hospede* hosp : hospedes){
            int id = hosp->get_ID();
            QString nome = hosp->get_nome();

            TiXmlElement* aux = new TiXmlElement("HOSPEDE");
            aux->SetAttribute("ID",id);
            aux->SetAttribute("NOME",nome.toStdString().data());

            e.LinkEndChild(aux);
        }

        return true;
    }
    return false;
}

bool Reserva::check_out(QDateTime date_time){
    if(reserva.status == CHECK_IN){
        reserva.has_check_out = true;
        reserva.data_check_out = date_time;

        reserva.status = CHECK_OUT;

        e.RemoveAttribute("HAS_CHECK_OUT");
        e.RemoveAttribute("DATA_CHECK_OUT");
        e.RemoveAttribute("STATUS");
        e.SetAttribute("HAS_CHECK_OUT",reserva.has_check_out?"1":"0");
        e.SetAttribute("DATA_CHECK_OUT",reserva.data_check_out.toString("dd/MM/yyyy hh-mm-ss").toStdString().data());
        e.SetAttribute("STATUS",status_to_qstring(reserva.status).toStdString().data());

        return true;
    }
    return false;
}

bool Reserva::has_quarto(QString id){
    bool found = false;
    for(int i = 0; i < reserva.quarto.size() && !found; i++){
        Quarto* q = reserva.quarto.operator[](i);

        if(q->get_ID() == id.toInt())
            found = true;
    }

    return found;
}

bool Reserva::has_quarto(int id){
    bool found = false;
    for(int i = 0; i < reserva.quarto.size() && !found; i++){
        Quarto* q = reserva.quarto.operator[](i);

        if(q->get_ID() == id)
            found = true;
    }

    return found;
}

int Reserva::get_ID(){
    return reserva.ID;
}

Cliente* Reserva::get_cliente(){
    return reserva.cliente;
}
void Reserva::set_cliente(Cliente* cliente){
    if(cliente != NULL)
        reserva.cliente = cliente;

    e.RemoveAttribute("CLIENTE_ID");
    e.RemoveAttribute("CLIENTE_NOME");
    e.SetAttribute("CLIENTE_ID",cliente->get_ID());
    e.SetAttribute("CLIENTE_NOME",cliente->get_nome().toStdString().data());
}

vector<Quarto*> Reserva::get_quarto(){
    return reserva.quarto;
}
vector<int> Reserva::get_quarto_IDs(){
    vector<int> r;

    for(Quarto* q : reserva.quarto){
        r.push_back(q->get_ID());
    }

    return r;
}

QString Reserva::get_quartos_string(){
    QString aux;

    for(Quarto* q : reserva.quarto){
        aux += QString::asprintf("%s ",q->get_numero().toStdString().data());
    }

    return aux;
}
void Reserva::set_quarto(vector<Quarto*> quarto){
    if(!quarto.empty())
        reserva.quarto = quarto;

    e.RemoveAttribute("QUARTO_ID");
    e.RemoveAttribute("QUARTO_NUMERO");
    for(Quarto* q : quarto){
        TiXmlElement* aux = new TiXmlElement("QUARTO");
        aux->SetAttribute("QUARTO_ID",q->get_ID());
        aux->SetAttribute("QUARTO_NUMERO",q->get_numero().toStdString().data());

        e.LinkEndChild(aux);
    }
}


int Reserva::get_PAX(){
    int r = 0;

    for(Quarto* q : reserva.quarto)
        r += q->get_PAX();

    return r;
}

QDate Reserva::get_data_inicio(){
    return reserva.data_inicio;
}

void Reserva::set_data_inicio(QDate data_inicio){
    if(data_inicio > reserva.data_fim){
        reserva.data_fim = data_inicio;
        e.RemoveAttribute("DATA_FIM");
        e.SetAttribute("DATA_FIM",reserva.data_fim.toString("dd/MM/yyyy").toStdString().data());
    }

    reserva.data_inicio = data_inicio;

    e.RemoveAttribute("DATA_INICIO");
    e.SetAttribute("DATA_INICIO",reserva.data_inicio.toString("dd/MM/yyyy").toStdString().data());
}

QDate Reserva::get_data_fim(){
    return reserva.data_fim;
}

void Reserva::set_data_fim(QDate data_fim){
    if(reserva.data_inicio > data_fim){
        reserva.data_inicio = data_fim;

        e.RemoveAttribute("DATA_INICIO");
        e.SetAttribute("DATA_INICIO",reserva.data_inicio.toString("dd/MM/yyyy").toStdString().data());
    }

    reserva.data_inicio = data_fim;
    e.RemoveAttribute("DATA_FIM");
    e.SetAttribute("DATA_FIM",reserva.data_fim.toString("dd/MM/yyyy").toStdString().data());
}

int Reserva::get_hospedes(){
    return reserva.hospedes_adultos + reserva.hospedes_criancas;
}

vector<int> Reserva::get_hospedes_list(){
    vector<int> h;

    for(Hospede* hosp : reserva.hospedes){
        h.push_back(hosp->get_ID());
    }

    return h;
}

int Reserva::get_hospedes_adultos(){
    return reserva.hospedes_adultos;
}

void Reserva::set_hospedes_adultos(int hospedes_adultos){
    reserva.hospedes_adultos = hospedes_adultos;

    e.RemoveAttribute("HOSPEDES_ADULTOS");
    e.SetAttribute("HOSPEDES_ADULTOS",hospedes_adultos);
}

int Reserva::get_hospedes_criancas(){
    return reserva.hospedes_criancas;
}

void Reserva::set_hospedes_criancas(int hospedes_criancas){
    reserva.hospedes_criancas = hospedes_criancas;

    e.RemoveAttribute("HOSPEDES_CRIANCAS");
    e.SetAttribute("HOSPEDES_CRIANCAS",hospedes_criancas);
}

QDateTime Reserva::get_data_check_in(){
    return reserva.data_check_in;
}

void Reserva::set_data_check_in(QDateTime data_check_in){
    reserva.data_check_in = data_check_in;

    e.RemoveAttribute("DATA_CHECK_IN");
    e.SetAttribute("DATA_CHECK_IN",reserva.data_check_in.toString("dd/MM/yyyy").toStdString().data());
}

QDateTime Reserva::get_data_check_out(){
    return  reserva.data_check_out;
}

void Reserva::set_data_check_out(QDateTime data_check_out){
    reserva.data_check_out = data_check_out;

    e.RemoveAttribute("DATA_CHECK_OUT");
    e.SetAttribute("DATA_CHECK_OUT",reserva.data_check_out.toString("dd/MM/yyyy").toStdString().data());
}

TiXmlNode* Reserva::get_xml_node(){
    return e.Clone();
}

bool Reserva::checked_in(){
    return reserva.has_check_in;
}

bool Reserva::checked_out(){
    return reserva.has_check_out;
}

bool Reserva::operator==(Reserva* c){
    return reserva.ID == c->get_ID();
}

bool Reserva::operator==(const int& i){
    return reserva.ID == i;
}

const Reserva::ReservaElemento& Reserva::ReservaElemento::operator=(const ReservaElemento& e){
    return ReservaElemento{e.cliente,e.quarto,e.data_inicio,e.data_fim,e.hospedes_adultos,e.hospedes_criancas,e.has_check_in,e.data_check_in,e.has_check_out,e.data_check_out,e.status,e.hospedes};
}

Reserva::ReservaElemento::ReservaElemento(Cliente* cliente, vector<Quarto*> quarto, QDate data_inicio, QDate data_fim, int hospedes_adultos,
int hospedes_criancas) : ID(CURR_ID++), cliente(cliente), quarto(quarto), data_inicio(data_inicio), data_fim(data_fim), hospedes_adultos(hospedes_adultos), hospedes_criancas(hospedes_criancas)
{
    has_check_in = false;
    has_check_out = false;
    status = NAO_CONFIRMADA;
}

Reserva::ReservaElemento::ReservaElemento(Cliente* cliente, vector<Quarto*> quarto, QDate data_inicio, QDate data_fim, int hospedes_adultos,
int hospedes_criancas, bool has_check_in, QDateTime check_in, bool has_check_out, QDateTime check_out, ReservaStatus status, vector<Hospede*> hospedes) : ID(CURR_ID++),
cliente(cliente),quarto(quarto), data_inicio(data_inicio), data_fim(data_fim), hospedes_adultos(hospedes_adultos), hospedes_criancas(hospedes_criancas),
has_check_in(has_check_in), has_check_out(has_check_out), data_check_in(check_in), data_check_out(check_out), status(status), hospedes(hospedes)
{

}

Reserva::ReservaElemento::ReservaElemento(int ID, Cliente* cliente, vector<Quarto*> quarto, QDate data_inicio, QDate data_fim, int hospedes_adultos,
int hospedes_criancas, bool has_check_in, QDateTime check_in, bool has_check_out, QDateTime check_out, ReservaStatus status, vector<Hospede*> hospedes) : ID(ID),
cliente(cliente),quarto(quarto), data_inicio(data_inicio), data_fim(data_fim), hospedes_adultos(hospedes_adultos), hospedes_criancas(hospedes_criancas),
has_check_in(has_check_in), has_check_out(has_check_out), data_check_in(check_in), data_check_out(check_out), status(status), hospedes(hospedes)
{
    if(ID >= CURR_ID)
        CURR_ID = ID + 1;
}

bool Reserva::ReservaElemento::operator==(const ReservaElemento& e){
    return e.ID == ID;
}
