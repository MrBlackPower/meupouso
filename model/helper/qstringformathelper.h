#ifndef QSTRINGFORMATHELPER_H
#define QSTRINGFORMATHELPER_H

#include <QString>
#include <vector>

using namespace std;

class QStringFormatHelper
{
public:
    static bool in_format(QString format, QString txt);

    static bool only_numbers(QString txt);

    static bool only_letters(QString txt);
};

#endif // QSTRINGFORMATHELPER_H
