#include "qstringformathelper.h"

bool QStringFormatHelper::in_format(QString format, QString txt){
    bool r = true;

    for(int i = 0; i < txt.size() && r; i++){
        if(format.size() <= i){
            r = false;
        } else {
            QChar c_from_txt = txt.operator[](i);
            QChar c_from_format = txt.operator[](i);

            QString letters = "abcdefghijklmnopqrstuvxywzABCDEFGHIJKLMNOPQRSTUVXYWZ";
            QString numbers = "0123456789";

            //NUMBER - FORMAT CHAR ("N")
            //LETTER - FORMAT CHAR ("L")
            //OTHERWISE - LETTER FROM TXT HAS TO BE EQUAL TO LETTER FROM FORMAT
            r = ((c_from_format == "N" && numbers.contains(c_from_txt)) || (c_from_format == "L" && letters.contains(c_from_txt)) || (c_from_txt == c_from_format))? true : false;
        }
    }

    return r;
}

bool QStringFormatHelper::only_numbers(QString txt){
    QString numbers = "0123456789";

    for(int i = 0; i < txt.size(); i++){
        if(!numbers.contains(txt.operator[](i)))
            return false;
    }

    return true;
}

bool QStringFormatHelper::only_letters(QString txt){
    QString letters = "abcdefghijklmnopqrstuvxywzABCDEFGHIJKLMNOPQRSTUVXYWZ";

    for(int i = 0; i < txt.size(); i++){
        if(!letters.contains(txt.operator[](i)))
            return false;
    }

    return true;
}
