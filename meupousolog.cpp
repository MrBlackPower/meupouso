#include "meupousolog.h"

meupousolog::meupousolog(QString log_file, QString backup_file) : log_file(log_file), backup_file(backup_file)
{
    vector<QString> folders = meupousolog::loadRaw(BACKUP_PASTA_DEF);

    if(!folders.empty()){
        folder = folders.operator[](0);

        this->log_file = folder + "/" + LOG_FILE;
        this->backup_file = folder + "/" + BACKUP_DIR;

    }
}

void meupousolog::push_log(QString line, QString origin){
    QString t = QTime::currentTime().toString("hh:mm:ss");
    QString a = QString::asprintf("[ %s : %s ] - %s",origin.toStdString().data(),t.toStdString().data(),line.toStdString().data());

    log.emplace_back(a);

    emit repush_log();
    emit repush_log(line,origin);
    emit push_feed(a);
}

string meupousolog::str(){
    string s = "";

    int min = (log.size() > STR_MAXIMUM_LENGTH)? log.size() - STR_MAXIMUM_LENGTH : 0 ;

    for(int i = min; i < log.size(); i++){
        QString l = log.operator[](i);
        s +=  l.toStdString() + "\n";
    }

    return s;
}


bool meupousolog::do_backup(Pousada* pousada, QString file){
    string s = "";
    s << *pousada;

    if (file == ""){
        QString date_time = QDateTime::currentDateTime().toString("dd_MM_yy_hh_mm_ss");

        saveRaw(s,QString::asprintf(backup_file.toStdString().data(),date_time.toStdString().data()).toStdString().data());
        saveRaw(s,QString::asprintf(backup_file.toStdString().data(),"latest").toStdString().data());

        push_log(QString::asprintf("%s",s.data()), "MEU POUSO");
        push_log(QString::asprintf(backup_file.toStdString().data(),date_time.toStdString().data()).toStdString().data(),"MEUPOUSO");
        push_log(QString::asprintf("BACKUP REALIZADO EM %s", date_time.toStdString().data()),"MEU POUSO");
    } else {
        QString date_time = QDateTime::currentDateTime().toString("dd_MM_yy_hh_mm_ss");
        saveRaw(s,file.toStdString().data());

        push_log(QString::asprintf("%s",s.data()), "MEU POUSO");
        push_log(QString::asprintf(file.toStdString().data(),date_time.toStdString().data()).toStdString().data(),"MEUPOUSO");
        push_log(QString::asprintf("BACKUP REALIZADO EM %s", file.toStdString().data()),"MEU POUSO");
    }
}

bool meupousolog::backup(Pousada* pousada, QString file){
    if(file == "")
        file = QString::asprintf(backup_file.toStdString().data(),"latest");

    return pousada->load(file);
}

bool meupousolog::saveRaw(string raw, const char *fileName){
    FILE *file;
    QString aux(fileName);
    if(!(aux.right(aux.lastIndexOf("."))).contains("xml"))
        aux = QString::asprintf(fileName) + ".xml";

    file = fopen(aux.toStdString().data(),"w+");

    if(file == NULL)
        return false;

    fprintf(file,"%s\n", raw.data());

    fclose(file);

    return true;
}
bool meupousolog::saveRaw_raw(string raw, const char *fileName){
    FILE *file;
    QString aux(fileName);
    file = fopen(aux.toStdString().data(),"w+");

    if(file == NULL)
        return false;

    fprintf(file,"%s", raw.data());

    fclose(file);

    return true;
}

vector<QString> meupousolog::loadRaw(const char *fileName){
    FILE *file;
    vector<QString> r;

    file = fopen(fileName,"r");

    if(file == NULL){
        return r;
    }

    char line[100];

    while(fgets(line, 80, file) != NULL){
        r.emplace_back(QString{line});
    }

    fclose(file);

    return r;
}

void meupousolog::setFolder(){
    QString dir = QFileDialog::getExistingDirectory();

    if(dir != ""){
        folder = dir;

        log_file = folder + "/" + LOG_FILE;
        backup_file = folder + "/" + BACKUP_DIR;

        saveRaw_raw(folder.toStdString().data(),BACKUP_PASTA_DEF);
    }
}
