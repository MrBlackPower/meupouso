#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QPrinter>
#include <QMainWindow>
#include <QDateTime>
#include <QTimer>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QFileDialog>

#include <iostream>

#include "meupousolog.h"
#include "model/pousada.h"
#include "window/tipoquartodialog.h"
#include "window/quartodialog.h"
#include "window/clientedialog.h"
#include "window/hospededialog.h"
#include "window/reservadialog.h"
#include "window/reservacheckindialog.h"
#include "window/reservacheckoutdialog.h"
#include "window/confirmdialog.h"

#define REFRESH_RATE 500
#define BACKUP_RATE 3600000

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void update_time();

    void update_tables();

    void update_tipoquarto_list();

    void do_backup();

    void update_log();

    void confirma_reserva(QString id);


private slots:
    void on_btn_add_tipoquarto_clicked();

    void on_btn_edit_tipoquarto_clicked();

    void on_txt_tipo_quarto_nome_textChanged();

    void on_btn_remove_tipoquarto_clicked();

    void on_table_tipoquarto_itemDoubleClicked(QTableWidgetItem *item);

    void on_btn_add_quarto_clicked();

    void on_btn_remove_quarto_clicked();

    void on_btn_edit_quarto_clicked();

    void on_cbx_quarto_tipo_quarto_filtro_currentTextChanged(const QString &arg1);

    void on_btn_add_cliente_clicked();

    void on_btn_remove_cliente_clicked();

    void on_btn_edit_cliente_clicked();

    void on_txt_cliente_cpf_textChanged();

    void on_table_quarto_cellPressed(int row, int column);

    void on_table_tipoquarto_cellPressed(int row, int column);

    void on_table_cliente_cellPressed(int row, int column);

    void on_btn_add_hospede_clicked();

    void on_btn_remove_hospede_clicked();

    void on_btn_edit_hospede_clicked();

    void on_table_quarto_itemDoubleClicked(QTableWidgetItem *item);

    void on_txt_cliente_nome_textChanged();

    void on_txt_hospede_nome_textChanged();

    void on_txt_hospede_cpf_textChanged();

    void on_txt_cliente_textChanged();

    void on_txt_quarto_textChanged();

    void on_chk_inicio_lower_clicked();

    void on_chk_inicio_higher_clicked();

    void on_dt_inicio_lower_userDateChanged(const QDate &date);

    void on_dt_inicio_higher_dateChanged(const QDate &date);

    void on_tb_confirmations_cellClicked(int row, int column);

    void on_tb_confirmations_cellDoubleClicked(int row, int column);

    void on_tb_check_ins_cellClicked(int row, int column);

    void on_tb_check_ins_cellDoubleClicked(int row, int column);

    void on_tb_check_outs_cellClicked(int row, int column);

    void on_tb_check_outs_cellDoubleClicked(int row, int column);

    void on_btn_confirma_clicked();

    void on_btn_checkin_clicked();

    void on_btn_add_reserva_inicial_clicked();

    void on_btn_add_reserva_clicked();

    void on_btn_remove_reserva_clicked();

    void on_btn_edit_reserva_clicked();

    void on_btn_checkout_clicked();

    void on_table_reserva_itemClicked(QTableWidgetItem *item);

    void on_table_reserva_itemDoubleClicked(QTableWidgetItem *item);

    void on_txt_quarto_numero_textChanged(const QString &arg1);

    void on_chk_disponivel_lower_clicked();

    void on_chk_disponivel_higher_clicked();

    void on_dt_disponivel_lower_userDateChanged(const QDate &date);

    void on_dt_disponivel_higher_userDateChanged(const QDate &date);

    void on_act_backup_triggered();

    void on_act_carregar_triggered();

    void on_act_sair_triggered();

    void on_act_salvar_triggered();

    void on_table_cliente_itemDoubleClicked(QTableWidgetItem *item);

    void on_table_hospede_itemDoubleClicked(QTableWidgetItem *item);

    void on_table_hospede_cellPressed(int row, int column);

    void on_tb_confirmations_itemDoubleClicked(QTableWidgetItem *item);

    void on_tb_check_ins_itemDoubleClicked(QTableWidgetItem *item);

    void on_tb_check_outs_itemDoubleClicked(QTableWidgetItem *item);

    void on_btn_print_reserva_clicked();

    void on_cbx_status_reserva_currentTextChanged(const QString &arg1);

    void on_MainWindow_destroyed();

    void on_btn_update_toggled(bool checked);

    void on_act_past_backup_triggered();

private:
    Pousada pousada;
    meupousolog log;

    //TIPO_QUARTO
    int last_tipo_quarto_row_ID;

    QString tipo_quarto_nome;

    //QUARTO
    int last_quarto_row_ID;

    QString quarto_numero;
    QString quarto_tipo;

    bool using_disponivel;
    QDate data_inicio;
    QDate data_fim;

    //CLIENTE
    int last_cliente_row_ID;

    QString cliente_nome;
    QString cliente_cpf;

    //HOSPEDE
    int last_hospede_row_ID;

    QString hospede_nome;
    QString hospede_cpf;

    //RESERVA
    int last_reserva_row_ID;

    QString reserva_cliente;
    QString reserva_quarto;

    bool using_data_inicio;
    QDate data_inicio_higher;
    QDate data_inicio_lower;

    bool using_data_fim;
    QDate data_fim_higher;
    QDate data_fim_lower;

    bool using_status;
    Reserva::ReservaStatus reserva_status;

    QTimer* refresh_rate;
    QTimer* backup_rate;

    //RESERVA _ MAIN WINDOW
    int last_reserva_confirm_ID;
    int last_reserva_check_in_ID;
    int last_reserva_check_out_ID;

    QString lock_filename;
    FILE* lock;
    bool locked;

    void print_reservas(QTableWidget* tbl);

    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
