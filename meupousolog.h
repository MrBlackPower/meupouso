#ifndef MEUPOUSOLOG_H
#define MEUPOUSOLOG_H

#include <QObject>
#include <vector>
#include <QTime>
#include <QDateTime>
#include <string>
#include <QFileDialog>

#include "model/pousada.h"

#define LOG_FILE "log.xml"
#define BACKUP_DIR "meupouso_%s.xml"
#define STR_MAXIMUM_LENGTH 5000
#define BACKUP_PASTA_DEF "C:\\Users\\Rosas\\Documents\\MeuPouso\\meupouso\\backup_pasta.xml"

using namespace std;

class meupousolog : public QObject
{
Q_OBJECT
public:
    meupousolog(QString log_file = LOG_FILE, QString backup_file = BACKUP_DIR);

    string str();

    static bool saveRaw(string raw, const char *fileName);

    static bool saveRaw_raw(string raw, const char *fileName);

    static vector<QString> loadRaw(const char *fileName);

    void setFolder();

    QString folder;
public slots:
    void push_log(QString line, QString origin = "ANONYMOUS");

    bool do_backup(Pousada* pousada, QString file = "");

    bool backup(Pousada* pousada, QString file = "");

signals:
    void repush_log();
    void repush_log(QString line, QString origin);
    void push_feed(QString line);

private:
    vector<QString> log;

    QString log_file;
    QString backup_file;
};

#endif // MEUPOUSOLOG_H
