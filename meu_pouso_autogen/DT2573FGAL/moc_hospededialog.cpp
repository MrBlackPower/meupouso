/****************************************************************************
** Meta object code from reading C++ file 'hospededialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../window/hospededialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'hospededialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_HospedeDialog_t {
    QByteArrayData data[19];
    char stringdata0[272];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_HospedeDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_HospedeDialog_t qt_meta_stringdata_HospedeDialog = {
    {
QT_MOC_LITERAL(0, 0, 13), // "HospedeDialog"
QT_MOC_LITERAL(1, 14, 14), // "create_hospede"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 10), // "cliente_ID"
QT_MOC_LITERAL(4, 41, 4), // "nome"
QT_MOC_LITERAL(5, 46, 15), // "data_nascimento"
QT_MOC_LITERAL(6, 62, 3), // "cpf"
QT_MOC_LITERAL(7, 66, 12), // "edit_hospede"
QT_MOC_LITERAL(8, 79, 2), // "ID"
QT_MOC_LITERAL(9, 82, 14), // "remove_hospede"
QT_MOC_LITERAL(10, 97, 13), // "chose_cliente"
QT_MOC_LITERAL(11, 111, 31), // "on_btn_procurar_cliente_clicked"
QT_MOC_LITERAL(12, 143, 23), // "on_txt_nome_textChanged"
QT_MOC_LITERAL(13, 167, 22), // "on_txt_cpf_textChanged"
QT_MOC_LITERAL(14, 190, 4), // "arg1"
QT_MOC_LITERAL(15, 195, 27), // "on_dateEdit_userDateChanged"
QT_MOC_LITERAL(16, 223, 4), // "date"
QT_MOC_LITERAL(17, 228, 21), // "on_buttonBox_accepted"
QT_MOC_LITERAL(18, 250, 21) // "on_buttonBox_rejected"

    },
    "HospedeDialog\0create_hospede\0\0cliente_ID\0"
    "nome\0data_nascimento\0cpf\0edit_hospede\0"
    "ID\0remove_hospede\0chose_cliente\0"
    "on_btn_procurar_cliente_clicked\0"
    "on_txt_nome_textChanged\0on_txt_cpf_textChanged\0"
    "arg1\0on_dateEdit_userDateChanged\0date\0"
    "on_buttonBox_accepted\0on_buttonBox_rejected"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_HospedeDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    4,   64,    2, 0x06 /* Public */,
       7,    4,   73,    2, 0x06 /* Public */,
       9,    1,   82,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    2,   85,    2, 0x0a /* Public */,
      11,    0,   90,    2, 0x08 /* Private */,
      12,    0,   91,    2, 0x08 /* Private */,
      13,    1,   92,    2, 0x08 /* Private */,
      15,    1,   95,    2, 0x08 /* Private */,
      17,    0,   98,    2, 0x08 /* Private */,
      18,    0,   99,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QDate, QMetaType::QString,    3,    4,    5,    6,
    QMetaType::Void, QMetaType::Int, QMetaType::QString, QMetaType::QDate, QMetaType::QString,    8,    4,    5,    6,
    QMetaType::Void, QMetaType::Int,    8,

 // slots: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    8,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::QDate,   16,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void HospedeDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<HospedeDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->create_hospede((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QDate(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4]))); break;
        case 1: _t->edit_hospede((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QDate(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4]))); break;
        case 2: _t->remove_hospede((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->chose_cliente((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 4: _t->on_btn_procurar_cliente_clicked(); break;
        case 5: _t->on_txt_nome_textChanged(); break;
        case 6: _t->on_txt_cpf_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->on_dateEdit_userDateChanged((*reinterpret_cast< const QDate(*)>(_a[1]))); break;
        case 8: _t->on_buttonBox_accepted(); break;
        case 9: _t->on_buttonBox_rejected(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (HospedeDialog::*)(QString , QString , QDate , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&HospedeDialog::create_hospede)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (HospedeDialog::*)(int , QString , QDate , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&HospedeDialog::edit_hospede)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (HospedeDialog::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&HospedeDialog::remove_hospede)) {
                *result = 2;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject HospedeDialog::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_HospedeDialog.data,
    qt_meta_data_HospedeDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *HospedeDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *HospedeDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_HospedeDialog.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int HospedeDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void HospedeDialog::create_hospede(QString _t1, QString _t2, QDate _t3, QString _t4)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void HospedeDialog::edit_hospede(int _t1, QString _t2, QDate _t3, QString _t4)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void HospedeDialog::remove_hospede(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
