/****************************************************************************
** Meta object code from reading C++ file 'reservadialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../window/reservadialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'reservadialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ReservaDialog_t {
    QByteArrayData data[34];
    char stringdata0[562];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ReservaDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ReservaDialog_t qt_meta_stringdata_ReservaDialog = {
    {
QT_MOC_LITERAL(0, 0, 13), // "ReservaDialog"
QT_MOC_LITERAL(1, 14, 14), // "create_reserva"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 10), // "cliente_ID"
QT_MOC_LITERAL(4, 41, 9), // "quarto_ID"
QT_MOC_LITERAL(5, 51, 11), // "data_inicio"
QT_MOC_LITERAL(6, 63, 8), // "data_fim"
QT_MOC_LITERAL(7, 72, 16), // "hospedes_adultos"
QT_MOC_LITERAL(8, 89, 17), // "hospedes_criancas"
QT_MOC_LITERAL(9, 107, 12), // "edit_reserva"
QT_MOC_LITERAL(10, 120, 2), // "ID"
QT_MOC_LITERAL(11, 123, 14), // "remove_reserva"
QT_MOC_LITERAL(12, 138, 13), // "choose_quarto"
QT_MOC_LITERAL(13, 152, 13), // "quarto_numero"
QT_MOC_LITERAL(14, 166, 3), // "PAX"
QT_MOC_LITERAL(15, 170, 14), // "choose_cliente"
QT_MOC_LITERAL(16, 185, 12), // "cliente_nome"
QT_MOC_LITERAL(17, 198, 16), // "confirma_reserva"
QT_MOC_LITERAL(18, 215, 8), // "check_in"
QT_MOC_LITERAL(19, 224, 9), // "check_out"
QT_MOC_LITERAL(20, 234, 31), // "on_btn_procurar_cliente_clicked"
QT_MOC_LITERAL(21, 266, 28), // "on_dt_inicio_userDateChanged"
QT_MOC_LITERAL(22, 295, 4), // "date"
QT_MOC_LITERAL(23, 300, 25), // "on_dt_fim_userDateChanged"
QT_MOC_LITERAL(24, 326, 30), // "on_btn_procurar_quarto_clicked"
QT_MOC_LITERAL(25, 357, 36), // "on_spn_hospedes_adultos_value..."
QT_MOC_LITERAL(26, 394, 4), // "arg1"
QT_MOC_LITERAL(27, 399, 37), // "on_spn_hospedes_criancas_valu..."
QT_MOC_LITERAL(28, 437, 21), // "on_buttonBox_accepted"
QT_MOC_LITERAL(29, 459, 21), // "on_buttonBox_rejected"
QT_MOC_LITERAL(30, 481, 22), // "on_btn_confirm_clicked"
QT_MOC_LITERAL(31, 504, 22), // "on_btn_checkin_clicked"
QT_MOC_LITERAL(32, 527, 23), // "on_btn_checkout_clicked"
QT_MOC_LITERAL(33, 551, 10) // "update_btn"

    },
    "ReservaDialog\0create_reserva\0\0cliente_ID\0"
    "quarto_ID\0data_inicio\0data_fim\0"
    "hospedes_adultos\0hospedes_criancas\0"
    "edit_reserva\0ID\0remove_reserva\0"
    "choose_quarto\0quarto_numero\0PAX\0"
    "choose_cliente\0cliente_nome\0"
    "confirma_reserva\0check_in\0check_out\0"
    "on_btn_procurar_cliente_clicked\0"
    "on_dt_inicio_userDateChanged\0date\0"
    "on_dt_fim_userDateChanged\0"
    "on_btn_procurar_quarto_clicked\0"
    "on_spn_hospedes_adultos_valueChanged\0"
    "arg1\0on_spn_hospedes_criancas_valueChanged\0"
    "on_buttonBox_accepted\0on_buttonBox_rejected\0"
    "on_btn_confirm_clicked\0on_btn_checkin_clicked\0"
    "on_btn_checkout_clicked\0update_btn"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ReservaDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    6,  114,    2, 0x06 /* Public */,
       9,    3,  127,    2, 0x06 /* Public */,
      11,    1,  134,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      12,    3,  137,    2, 0x0a /* Public */,
      15,    2,  144,    2, 0x0a /* Public */,
      17,    0,  149,    2, 0x0a /* Public */,
      18,    0,  150,    2, 0x0a /* Public */,
      19,    0,  151,    2, 0x0a /* Public */,
      20,    0,  152,    2, 0x08 /* Private */,
      21,    1,  153,    2, 0x08 /* Private */,
      23,    1,  156,    2, 0x08 /* Private */,
      24,    0,  159,    2, 0x08 /* Private */,
      25,    1,  160,    2, 0x08 /* Private */,
      27,    1,  163,    2, 0x08 /* Private */,
      28,    0,  166,    2, 0x08 /* Private */,
      29,    0,  167,    2, 0x08 /* Private */,
      30,    0,  168,    2, 0x08 /* Private */,
      31,    0,  169,    2, 0x08 /* Private */,
      32,    0,  170,    2, 0x08 /* Private */,
      33,    0,  171,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QDate, QMetaType::QDate, QMetaType::Int, QMetaType::Int,    3,    4,    5,    6,    7,    8,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int,   10,    7,    8,
    QMetaType::Void, QMetaType::Int,   10,

 // slots: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::Int,    4,   13,   14,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    3,   16,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QDate,   22,
    QMetaType::Void, QMetaType::QDate,   22,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   26,
    QMetaType::Void, QMetaType::QString,   26,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ReservaDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ReservaDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->create_reserva((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QDate(*)>(_a[3])),(*reinterpret_cast< QDate(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6]))); break;
        case 1: _t->edit_reserva((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 2: _t->remove_reserva((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->choose_quarto((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 4: _t->choose_cliente((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 5: _t->confirma_reserva(); break;
        case 6: _t->check_in(); break;
        case 7: _t->check_out(); break;
        case 8: _t->on_btn_procurar_cliente_clicked(); break;
        case 9: _t->on_dt_inicio_userDateChanged((*reinterpret_cast< const QDate(*)>(_a[1]))); break;
        case 10: _t->on_dt_fim_userDateChanged((*reinterpret_cast< const QDate(*)>(_a[1]))); break;
        case 11: _t->on_btn_procurar_quarto_clicked(); break;
        case 12: _t->on_spn_hospedes_adultos_valueChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 13: _t->on_spn_hospedes_criancas_valueChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 14: _t->on_buttonBox_accepted(); break;
        case 15: _t->on_buttonBox_rejected(); break;
        case 16: _t->on_btn_confirm_clicked(); break;
        case 17: _t->on_btn_checkin_clicked(); break;
        case 18: _t->on_btn_checkout_clicked(); break;
        case 19: _t->update_btn(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (ReservaDialog::*)(QString , QString , QDate , QDate , int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ReservaDialog::create_reserva)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (ReservaDialog::*)(int , int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ReservaDialog::edit_reserva)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (ReservaDialog::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ReservaDialog::remove_reserva)) {
                *result = 2;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ReservaDialog::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_ReservaDialog.data,
    qt_meta_data_ReservaDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ReservaDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ReservaDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ReservaDialog.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int ReservaDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 20)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 20;
    }
    return _id;
}

// SIGNAL 0
void ReservaDialog::create_reserva(QString _t1, QString _t2, QDate _t3, QDate _t4, int _t5, int _t6)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)), const_cast<void*>(reinterpret_cast<const void*>(&_t6)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ReservaDialog::edit_reserva(int _t1, int _t2, int _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ReservaDialog::remove_reserva(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
