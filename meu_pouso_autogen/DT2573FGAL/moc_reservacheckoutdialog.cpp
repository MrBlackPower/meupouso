/****************************************************************************
** Meta object code from reading C++ file 'reservacheckoutdialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../window/reservacheckoutdialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'reservacheckoutdialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ReservaCheckoutDialog_t {
    QByteArrayData data[14];
    char stringdata0[244];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ReservaCheckoutDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ReservaCheckoutDialog_t qt_meta_stringdata_ReservaCheckoutDialog = {
    {
QT_MOC_LITERAL(0, 0, 21), // "ReservaCheckoutDialog"
QT_MOC_LITERAL(1, 22, 16), // "reserva_checkout"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 10), // "reserva_ID"
QT_MOC_LITERAL(4, 51, 8), // "datetime"
QT_MOC_LITERAL(5, 60, 19), // "update_current_time"
QT_MOC_LITERAL(6, 80, 15), // "update_hospedes"
QT_MOC_LITERAL(7, 96, 22), // "on_radioButton_clicked"
QT_MOC_LITERAL(8, 119, 24), // "on_radioButton_2_clicked"
QT_MOC_LITERAL(9, 144, 24), // "on_dt_insert_dateChanged"
QT_MOC_LITERAL(10, 169, 4), // "date"
QT_MOC_LITERAL(11, 174, 25), // "on_dt_current_dateChanged"
QT_MOC_LITERAL(12, 200, 21), // "on_buttonBox_accepted"
QT_MOC_LITERAL(13, 222, 21) // "on_buttonBox_rejected"

    },
    "ReservaCheckoutDialog\0reserva_checkout\0"
    "\0reserva_ID\0datetime\0update_current_time\0"
    "update_hospedes\0on_radioButton_clicked\0"
    "on_radioButton_2_clicked\0"
    "on_dt_insert_dateChanged\0date\0"
    "on_dt_current_dateChanged\0"
    "on_buttonBox_accepted\0on_buttonBox_rejected"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ReservaCheckoutDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   59,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,   64,    2, 0x0a /* Public */,
       6,    0,   65,    2, 0x0a /* Public */,
       7,    0,   66,    2, 0x08 /* Private */,
       8,    0,   67,    2, 0x08 /* Private */,
       9,    1,   68,    2, 0x08 /* Private */,
      11,    1,   71,    2, 0x08 /* Private */,
      12,    0,   74,    2, 0x08 /* Private */,
      13,    0,   75,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::QDateTime,    3,    4,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QDate,   10,
    QMetaType::Void, QMetaType::QDate,   10,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ReservaCheckoutDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ReservaCheckoutDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->reserva_checkout((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QDateTime(*)>(_a[2]))); break;
        case 1: _t->update_current_time(); break;
        case 2: _t->update_hospedes(); break;
        case 3: _t->on_radioButton_clicked(); break;
        case 4: _t->on_radioButton_2_clicked(); break;
        case 5: _t->on_dt_insert_dateChanged((*reinterpret_cast< const QDate(*)>(_a[1]))); break;
        case 6: _t->on_dt_current_dateChanged((*reinterpret_cast< const QDate(*)>(_a[1]))); break;
        case 7: _t->on_buttonBox_accepted(); break;
        case 8: _t->on_buttonBox_rejected(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (ReservaCheckoutDialog::*)(int , QDateTime );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ReservaCheckoutDialog::reserva_checkout)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ReservaCheckoutDialog::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_ReservaCheckoutDialog.data,
    qt_meta_data_ReservaCheckoutDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ReservaCheckoutDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ReservaCheckoutDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ReservaCheckoutDialog.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int ReservaCheckoutDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void ReservaCheckoutDialog::reserva_checkout(int _t1, QDateTime _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
