/****************************************************************************
** Meta object code from reading C++ file 'procuraclientedialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../window/procuraclientedialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'procuraclientedialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ProcuraClienteDialog_t {
    QByteArrayData data[13];
    char stringdata0[219];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ProcuraClienteDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ProcuraClienteDialog_t qt_meta_stringdata_ProcuraClienteDialog = {
    {
QT_MOC_LITERAL(0, 0, 20), // "ProcuraClienteDialog"
QT_MOC_LITERAL(1, 21, 13), // "chose_cliente"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 2), // "ID"
QT_MOC_LITERAL(4, 39, 4), // "nome"
QT_MOC_LITERAL(5, 44, 23), // "on_txt_nome_textChanged"
QT_MOC_LITERAL(6, 68, 22), // "on_txt_cpf_textChanged"
QT_MOC_LITERAL(7, 91, 21), // "on_buttonBox_accepted"
QT_MOC_LITERAL(8, 113, 21), // "on_buttonBox_rejected"
QT_MOC_LITERAL(9, 135, 27), // "on_lst_clientes_itemClicked"
QT_MOC_LITERAL(10, 163, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(11, 180, 4), // "item"
QT_MOC_LITERAL(12, 185, 33) // "on_lst_clientes_itemDoubleCli..."

    },
    "ProcuraClienteDialog\0chose_cliente\0\0"
    "ID\0nome\0on_txt_nome_textChanged\0"
    "on_txt_cpf_textChanged\0on_buttonBox_accepted\0"
    "on_buttonBox_rejected\0on_lst_clientes_itemClicked\0"
    "QListWidgetItem*\0item\0"
    "on_lst_clientes_itemDoubleClicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ProcuraClienteDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   49,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,   54,    2, 0x08 /* Private */,
       6,    0,   55,    2, 0x08 /* Private */,
       7,    0,   56,    2, 0x08 /* Private */,
       8,    0,   57,    2, 0x08 /* Private */,
       9,    1,   58,    2, 0x08 /* Private */,
      12,    1,   61,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    3,    4,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 10,   11,
    QMetaType::Void, 0x80000000 | 10,   11,

       0        // eod
};

void ProcuraClienteDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ProcuraClienteDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->chose_cliente((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 1: _t->on_txt_nome_textChanged(); break;
        case 2: _t->on_txt_cpf_textChanged(); break;
        case 3: _t->on_buttonBox_accepted(); break;
        case 4: _t->on_buttonBox_rejected(); break;
        case 5: _t->on_lst_clientes_itemClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 6: _t->on_lst_clientes_itemDoubleClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (ProcuraClienteDialog::*)(QString , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProcuraClienteDialog::chose_cliente)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ProcuraClienteDialog::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_ProcuraClienteDialog.data,
    qt_meta_data_ProcuraClienteDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ProcuraClienteDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ProcuraClienteDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ProcuraClienteDialog.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int ProcuraClienteDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void ProcuraClienteDialog::chose_cliente(QString _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
