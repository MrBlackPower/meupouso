/****************************************************************************
** Meta object code from reading C++ file 'quartodialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../window/quartodialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'quartodialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QuartoDialog_t {
    QByteArrayData data[19];
    char stringdata0[287];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QuartoDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QuartoDialog_t qt_meta_stringdata_QuartoDialog = {
    {
QT_MOC_LITERAL(0, 0, 12), // "QuartoDialog"
QT_MOC_LITERAL(1, 13, 13), // "create_quarto"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 6), // "numero"
QT_MOC_LITERAL(4, 35, 9), // "descricao"
QT_MOC_LITERAL(5, 45, 3), // "PAX"
QT_MOC_LITERAL(6, 49, 7), // "tipo_ID"
QT_MOC_LITERAL(7, 57, 10), // "disponivel"
QT_MOC_LITERAL(8, 68, 11), // "edit_quarto"
QT_MOC_LITERAL(9, 80, 2), // "ID"
QT_MOC_LITERAL(10, 83, 13), // "remove_quarto"
QT_MOC_LITERAL(11, 97, 28), // "on_txt_descricao_textChanged"
QT_MOC_LITERAL(12, 126, 23), // "on_txt_nome_textChanged"
QT_MOC_LITERAL(13, 150, 21), // "on_buttonBox_accepted"
QT_MOC_LITERAL(14, 172, 38), // "on_cbx_tipo_quarto_currentInd..."
QT_MOC_LITERAL(15, 211, 4), // "arg1"
QT_MOC_LITERAL(16, 216, 24), // "on_chk_disp_stateChanged"
QT_MOC_LITERAL(17, 241, 23), // "on_spn_PAX_valueChanged"
QT_MOC_LITERAL(18, 265, 21) // "on_buttonBox_rejected"

    },
    "QuartoDialog\0create_quarto\0\0numero\0"
    "descricao\0PAX\0tipo_ID\0disponivel\0"
    "edit_quarto\0ID\0remove_quarto\0"
    "on_txt_descricao_textChanged\0"
    "on_txt_nome_textChanged\0on_buttonBox_accepted\0"
    "on_cbx_tipo_quarto_currentIndexChanged\0"
    "arg1\0on_chk_disp_stateChanged\0"
    "on_spn_PAX_valueChanged\0on_buttonBox_rejected"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QuartoDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    5,   64,    2, 0x06 /* Public */,
       8,    6,   75,    2, 0x06 /* Public */,
      10,    1,   88,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      11,    0,   91,    2, 0x08 /* Private */,
      12,    0,   92,    2, 0x08 /* Private */,
      13,    0,   93,    2, 0x08 /* Private */,
      14,    1,   94,    2, 0x08 /* Private */,
      16,    1,   97,    2, 0x08 /* Private */,
      17,    1,  100,    2, 0x08 /* Private */,
      18,    0,  103,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::Int, QMetaType::QString, QMetaType::Bool,    3,    4,    5,    6,    7,
    QMetaType::Void, QMetaType::Int, QMetaType::QString, QMetaType::QString, QMetaType::Int, QMetaType::QString, QMetaType::Bool,    9,    3,    4,    5,    6,    7,
    QMetaType::Void, QMetaType::Int,    9,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void, QMetaType::Int,   15,
    QMetaType::Void, QMetaType::Int,   15,
    QMetaType::Void,

       0        // eod
};

void QuartoDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QuartoDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->create_quarto((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])),(*reinterpret_cast< bool(*)>(_a[5]))); break;
        case 1: _t->edit_quarto((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< QString(*)>(_a[5])),(*reinterpret_cast< bool(*)>(_a[6]))); break;
        case 2: _t->remove_quarto((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_txt_descricao_textChanged(); break;
        case 4: _t->on_txt_nome_textChanged(); break;
        case 5: _t->on_buttonBox_accepted(); break;
        case 6: _t->on_cbx_tipo_quarto_currentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->on_chk_disp_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->on_spn_PAX_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->on_buttonBox_rejected(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QuartoDialog::*)(QString , QString , int , QString , bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QuartoDialog::create_quarto)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QuartoDialog::*)(int , QString , QString , int , QString , bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QuartoDialog::edit_quarto)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QuartoDialog::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QuartoDialog::remove_quarto)) {
                *result = 2;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject QuartoDialog::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_QuartoDialog.data,
    qt_meta_data_QuartoDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QuartoDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QuartoDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QuartoDialog.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int QuartoDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void QuartoDialog::create_quarto(QString _t1, QString _t2, int _t3, QString _t4, bool _t5)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QuartoDialog::edit_quarto(int _t1, QString _t2, QString _t3, int _t4, QString _t5, bool _t6)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)), const_cast<void*>(reinterpret_cast<const void*>(&_t6)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QuartoDialog::remove_quarto(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
