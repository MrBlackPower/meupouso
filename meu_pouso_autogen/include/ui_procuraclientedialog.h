/********************************************************************************
** Form generated from reading UI file 'procuraclientedialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROCURACLIENTEDIALOG_H
#define UI_PROCURACLIENTEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_ProcuraClienteDialog
{
public:
    QDialogButtonBox *buttonBox;
    QLabel *label_4;
    QLabel *label_6;
    QTextEdit *txt_nome;
    QTextEdit *txt_cpf;
    QGroupBox *groupBox;
    QListWidget *lst_clientes;

    void setupUi(QDialog *ProcuraClienteDialog)
    {
        if (ProcuraClienteDialog->objectName().isEmpty())
            ProcuraClienteDialog->setObjectName(QString::fromUtf8("ProcuraClienteDialog"));
        ProcuraClienteDialog->resize(507, 510);
        buttonBox = new QDialogButtonBox(ProcuraClienteDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(10, 470, 491, 32));
        QFont font;
        font.setFamily(QString::fromUtf8("URW Bookman L"));
        font.setPointSize(15);
        font.setBold(true);
        font.setWeight(75);
        buttonBox->setFont(font);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        label_4 = new QLabel(ProcuraClienteDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(0, 10, 59, 27));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy);
        label_4->setFont(font);
        label_6 = new QLabel(ProcuraClienteDialog);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(10, 50, 51, 27));
        sizePolicy.setHeightForWidth(label_6->sizePolicy().hasHeightForWidth());
        label_6->setSizePolicy(sizePolicy);
        label_6->setFont(font);
        txt_nome = new QTextEdit(ProcuraClienteDialog);
        txt_nome->setObjectName(QString::fromUtf8("txt_nome"));
        txt_nome->setGeometry(QRect(70, 10, 431, 30));
        QFont font1;
        font1.setPointSize(12);
        font1.setBold(false);
        font1.setWeight(50);
        txt_nome->setFont(font1);
        txt_cpf = new QTextEdit(ProcuraClienteDialog);
        txt_cpf->setObjectName(QString::fromUtf8("txt_cpf"));
        txt_cpf->setGeometry(QRect(70, 50, 431, 30));
        txt_cpf->setFont(font1);
        groupBox = new QGroupBox(ProcuraClienteDialog);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 90, 491, 371));
        groupBox->setFont(font);
        lst_clientes = new QListWidget(groupBox);
        lst_clientes->setObjectName(QString::fromUtf8("lst_clientes"));
        lst_clientes->setGeometry(QRect(0, 30, 491, 341));

        retranslateUi(ProcuraClienteDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ProcuraClienteDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ProcuraClienteDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ProcuraClienteDialog);
    } // setupUi

    void retranslateUi(QDialog *ProcuraClienteDialog)
    {
        ProcuraClienteDialog->setWindowTitle(QApplication::translate("ProcuraClienteDialog", "Dialog", nullptr));
        label_4->setText(QApplication::translate("ProcuraClienteDialog", "Nome", nullptr));
        label_6->setText(QApplication::translate("ProcuraClienteDialog", "CPF", nullptr));
        groupBox->setTitle(QApplication::translate("ProcuraClienteDialog", "Clientes", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ProcuraClienteDialog: public Ui_ProcuraClienteDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROCURACLIENTEDIALOG_H
