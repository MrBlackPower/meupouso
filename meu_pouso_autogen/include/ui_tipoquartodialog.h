/********************************************************************************
** Form generated from reading UI file 'tipoquartodialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TIPOQUARTODIALOG_H
#define UI_TIPOQUARTODIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TipoQuartoDialog
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QTextEdit *txt_nome;
    QTextEdit *txt_descricao;
    QLabel *label;
    QLabel *label_3;
    QLabel *lbl_quarto;
    QListWidget *lst_quartos;

    void setupUi(QDialog *TipoQuartoDialog)
    {
        if (TipoQuartoDialog->objectName().isEmpty())
            TipoQuartoDialog->setObjectName(QString::fromUtf8("TipoQuartoDialog"));
        TipoQuartoDialog->resize(626, 745);
        buttonBox = new QDialogButtonBox(TipoQuartoDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(10, 700, 601, 32));
        QFont font;
        font.setFamily(QString::fromUtf8("URW Bookman L"));
        font.setPointSize(15);
        font.setBold(false);
        font.setWeight(50);
        buttonBox->setFont(font);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        formLayoutWidget = new QWidget(TipoQuartoDialog);
        formLayoutWidget->setObjectName(QString::fromUtf8("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(0, 10, 610, 681));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setSizeConstraint(QLayout::SetNoConstraint);
        formLayout->setFieldGrowthPolicy(QFormLayout::ExpandingFieldsGrow);
        formLayout->setRowWrapPolicy(QFormLayout::DontWrapRows);
        formLayout->setLabelAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        formLayout->setFormAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        formLayout->setContentsMargins(0, 0, 0, 0);
        txt_nome = new QTextEdit(formLayoutWidget);
        txt_nome->setObjectName(QString::fromUtf8("txt_nome"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(txt_nome->sizePolicy().hasHeightForWidth());
        txt_nome->setSizePolicy(sizePolicy);
        txt_nome->setMinimumSize(QSize(495, 0));
        txt_nome->setMaximumSize(QSize(600, 40));
        QFont font1;
        font1.setFamily(QString::fromUtf8("URW Bookman L"));
        font1.setPointSize(12);
        txt_nome->setFont(font1);

        formLayout->setWidget(0, QFormLayout::FieldRole, txt_nome);

        txt_descricao = new QTextEdit(formLayoutWidget);
        txt_descricao->setObjectName(QString::fromUtf8("txt_descricao"));
        txt_descricao->setMinimumSize(QSize(495, 430));
        txt_descricao->setSizeIncrement(QSize(495, 500));
        txt_descricao->setBaseSize(QSize(0, 500));
        QFont font2;
        font2.setPointSize(12);
        font2.setBold(false);
        font2.setWeight(50);
        txt_descricao->setFont(font2);

        formLayout->setWidget(1, QFormLayout::FieldRole, txt_descricao);

        label = new QLabel(formLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("URW Bookman L"));
        font3.setPointSize(15);
        font3.setBold(true);
        font3.setWeight(75);
        label->setFont(font3);

        formLayout->setWidget(1, QFormLayout::LabelRole, label);

        label_3 = new QLabel(formLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font3);

        formLayout->setWidget(0, QFormLayout::LabelRole, label_3);

        lbl_quarto = new QLabel(formLayoutWidget);
        lbl_quarto->setObjectName(QString::fromUtf8("lbl_quarto"));
        lbl_quarto->setFont(font3);

        formLayout->setWidget(2, QFormLayout::LabelRole, lbl_quarto);

        lst_quartos = new QListWidget(formLayoutWidget);
        lst_quartos->setObjectName(QString::fromUtf8("lst_quartos"));

        formLayout->setWidget(2, QFormLayout::FieldRole, lst_quartos);


        retranslateUi(TipoQuartoDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), TipoQuartoDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), TipoQuartoDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(TipoQuartoDialog);
    } // setupUi

    void retranslateUi(QDialog *TipoQuartoDialog)
    {
        TipoQuartoDialog->setWindowTitle(QApplication::translate("TipoQuartoDialog", "Dialog", nullptr));
        label->setText(QApplication::translate("TipoQuartoDialog", "Descri\303\247\303\243o", nullptr));
        label_3->setText(QApplication::translate("TipoQuartoDialog", "Nome", nullptr));
        lbl_quarto->setText(QApplication::translate("TipoQuartoDialog", "Quartos", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TipoQuartoDialog: public Ui_TipoQuartoDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TIPOQUARTODIALOG_H
