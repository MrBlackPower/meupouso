/********************************************************************************
** Form generated from reading UI file 'clientedialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLIENTEDIALOG_H
#define UI_CLIENTEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ClienteDialog
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QTextEdit *txt_nome;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QListWidget *lst_hospedes;
    QLabel *lbl_hospedes;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QComboBox *cbx_pais;
    QTextEdit *txt_endereco_rua;
    QTextEdit *txt_endereco_bairro;
    QTextEdit *txt_endereco_estado;
    QLineEdit *txt_cpf;
    QLineEdit *txt_telefone_movel;
    QLineEdit *txt_telefone_fixo;
    QLineEdit *txt_CEP;
    QLabel *label_10;
    QTextEdit *txt_endereco_cidade;

    void setupUi(QDialog *ClienteDialog)
    {
        if (ClienteDialog->objectName().isEmpty())
            ClienteDialog->setObjectName(QString::fromUtf8("ClienteDialog"));
        ClienteDialog->resize(686, 661);
        buttonBox = new QDialogButtonBox(ClienteDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(10, 620, 671, 31));
        QFont font;
        font.setFamily(QString::fromUtf8("URW Bookman L"));
        font.setPointSize(15);
        buttonBox->setFont(font);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        formLayoutWidget = new QWidget(ClienteDialog);
        formLayoutWidget->setObjectName(QString::fromUtf8("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(10, 10, 731, 601));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(formLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("URW Bookman L"));
        font1.setPointSize(15);
        font1.setBold(true);
        font1.setWeight(75);
        label->setFont(font1);

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        txt_nome = new QTextEdit(formLayoutWidget);
        txt_nome->setObjectName(QString::fromUtf8("txt_nome"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(txt_nome->sizePolicy().hasHeightForWidth());
        txt_nome->setSizePolicy(sizePolicy);
        txt_nome->setMinimumSize(QSize(495, 0));
        txt_nome->setMaximumSize(QSize(495, 40));
        txt_nome->setFont(font);

        formLayout->setWidget(0, QFormLayout::FieldRole, txt_nome);

        label_2 = new QLabel(formLayoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font1);

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        label_3 = new QLabel(formLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font1);

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        label_4 = new QLabel(formLayoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font1);

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        lst_hospedes = new QListWidget(formLayoutWidget);
        lst_hospedes->setObjectName(QString::fromUtf8("lst_hospedes"));
        lst_hospedes->setMaximumSize(QSize(495, 16777215));
        lst_hospedes->setFont(font);

        formLayout->setWidget(11, QFormLayout::FieldRole, lst_hospedes);

        lbl_hospedes = new QLabel(formLayoutWidget);
        lbl_hospedes->setObjectName(QString::fromUtf8("lbl_hospedes"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(lbl_hospedes->sizePolicy().hasHeightForWidth());
        lbl_hospedes->setSizePolicy(sizePolicy1);
        lbl_hospedes->setMaximumSize(QSize(150, 16777215));
        lbl_hospedes->setFont(font1);
        lbl_hospedes->setScaledContents(false);
        lbl_hospedes->setWordWrap(true);

        formLayout->setWidget(11, QFormLayout::LabelRole, lbl_hospedes);

        label_5 = new QLabel(formLayoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font1);

        formLayout->setWidget(4, QFormLayout::LabelRole, label_5);

        label_6 = new QLabel(formLayoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setFont(font1);

        formLayout->setWidget(5, QFormLayout::LabelRole, label_6);

        label_7 = new QLabel(formLayoutWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setFont(font1);

        formLayout->setWidget(6, QFormLayout::LabelRole, label_7);

        label_8 = new QLabel(formLayoutWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setFont(font1);

        formLayout->setWidget(9, QFormLayout::LabelRole, label_8);

        label_9 = new QLabel(formLayoutWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setFont(font1);

        formLayout->setWidget(10, QFormLayout::LabelRole, label_9);

        cbx_pais = new QComboBox(formLayoutWidget);
        cbx_pais->setObjectName(QString::fromUtf8("cbx_pais"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(20);
        sizePolicy2.setHeightForWidth(cbx_pais->sizePolicy().hasHeightForWidth());
        cbx_pais->setSizePolicy(sizePolicy2);
        cbx_pais->setMinimumSize(QSize(0, 40));
        cbx_pais->setMaximumSize(QSize(495, 16777215));
        cbx_pais->setFont(font);

        formLayout->setWidget(10, QFormLayout::FieldRole, cbx_pais);

        txt_endereco_rua = new QTextEdit(formLayoutWidget);
        txt_endereco_rua->setObjectName(QString::fromUtf8("txt_endereco_rua"));
        sizePolicy.setHeightForWidth(txt_endereco_rua->sizePolicy().hasHeightForWidth());
        txt_endereco_rua->setSizePolicy(sizePolicy);
        txt_endereco_rua->setMinimumSize(QSize(495, 0));
        txt_endereco_rua->setMaximumSize(QSize(495, 40));
        txt_endereco_rua->setFont(font);

        formLayout->setWidget(4, QFormLayout::FieldRole, txt_endereco_rua);

        txt_endereco_bairro = new QTextEdit(formLayoutWidget);
        txt_endereco_bairro->setObjectName(QString::fromUtf8("txt_endereco_bairro"));
        sizePolicy.setHeightForWidth(txt_endereco_bairro->sizePolicy().hasHeightForWidth());
        txt_endereco_bairro->setSizePolicy(sizePolicy);
        txt_endereco_bairro->setMinimumSize(QSize(495, 0));
        txt_endereco_bairro->setMaximumSize(QSize(495, 40));
        txt_endereco_bairro->setFont(font);

        formLayout->setWidget(6, QFormLayout::FieldRole, txt_endereco_bairro);

        txt_endereco_estado = new QTextEdit(formLayoutWidget);
        txt_endereco_estado->setObjectName(QString::fromUtf8("txt_endereco_estado"));
        sizePolicy.setHeightForWidth(txt_endereco_estado->sizePolicy().hasHeightForWidth());
        txt_endereco_estado->setSizePolicy(sizePolicy);
        txt_endereco_estado->setMinimumSize(QSize(495, 0));
        txt_endereco_estado->setMaximumSize(QSize(495, 40));
        txt_endereco_estado->setFont(font);

        formLayout->setWidget(9, QFormLayout::FieldRole, txt_endereco_estado);

        txt_cpf = new QLineEdit(formLayoutWidget);
        txt_cpf->setObjectName(QString::fromUtf8("txt_cpf"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(txt_cpf->sizePolicy().hasHeightForWidth());
        txt_cpf->setSizePolicy(sizePolicy3);
        txt_cpf->setMinimumSize(QSize(0, 40));
        txt_cpf->setMaximumSize(QSize(495, 16777215));
        txt_cpf->setFont(font);

        formLayout->setWidget(1, QFormLayout::FieldRole, txt_cpf);

        txt_telefone_movel = new QLineEdit(formLayoutWidget);
        txt_telefone_movel->setObjectName(QString::fromUtf8("txt_telefone_movel"));
        sizePolicy3.setHeightForWidth(txt_telefone_movel->sizePolicy().hasHeightForWidth());
        txt_telefone_movel->setSizePolicy(sizePolicy3);
        txt_telefone_movel->setMinimumSize(QSize(0, 40));
        txt_telefone_movel->setMaximumSize(QSize(495, 16777215));
        txt_telefone_movel->setFont(font);

        formLayout->setWidget(2, QFormLayout::FieldRole, txt_telefone_movel);

        txt_telefone_fixo = new QLineEdit(formLayoutWidget);
        txt_telefone_fixo->setObjectName(QString::fromUtf8("txt_telefone_fixo"));
        sizePolicy3.setHeightForWidth(txt_telefone_fixo->sizePolicy().hasHeightForWidth());
        txt_telefone_fixo->setSizePolicy(sizePolicy3);
        txt_telefone_fixo->setMinimumSize(QSize(0, 40));
        txt_telefone_fixo->setMaximumSize(QSize(495, 16777215));
        txt_telefone_fixo->setFont(font);

        formLayout->setWidget(3, QFormLayout::FieldRole, txt_telefone_fixo);

        txt_CEP = new QLineEdit(formLayoutWidget);
        txt_CEP->setObjectName(QString::fromUtf8("txt_CEP"));
        sizePolicy3.setHeightForWidth(txt_CEP->sizePolicy().hasHeightForWidth());
        txt_CEP->setSizePolicy(sizePolicy3);
        txt_CEP->setMinimumSize(QSize(0, 40));
        txt_CEP->setMaximumSize(QSize(495, 16777215));
        txt_CEP->setFont(font);

        formLayout->setWidget(5, QFormLayout::FieldRole, txt_CEP);

        label_10 = new QLabel(formLayoutWidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setFont(font1);

        formLayout->setWidget(8, QFormLayout::LabelRole, label_10);

        txt_endereco_cidade = new QTextEdit(formLayoutWidget);
        txt_endereco_cidade->setObjectName(QString::fromUtf8("txt_endereco_cidade"));
        sizePolicy.setHeightForWidth(txt_endereco_cidade->sizePolicy().hasHeightForWidth());
        txt_endereco_cidade->setSizePolicy(sizePolicy);
        txt_endereco_cidade->setMinimumSize(QSize(495, 0));
        txt_endereco_cidade->setMaximumSize(QSize(495, 40));
        txt_endereco_cidade->setFont(font);

        formLayout->setWidget(8, QFormLayout::FieldRole, txt_endereco_cidade);


        retranslateUi(ClienteDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ClienteDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ClienteDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ClienteDialog);
    } // setupUi

    void retranslateUi(QDialog *ClienteDialog)
    {
        ClienteDialog->setWindowTitle(QApplication::translate("ClienteDialog", "Dialog", nullptr));
        label->setText(QApplication::translate("ClienteDialog", "Nome", nullptr));
        label_2->setText(QApplication::translate("ClienteDialog", "CPF", nullptr));
        label_3->setText(QApplication::translate("ClienteDialog", "Telefone M\303\263vel", nullptr));
        label_4->setText(QApplication::translate("ClienteDialog", "Telefone Fixo", nullptr));
        lbl_hospedes->setText(QApplication::translate("ClienteDialog", "H\303\263spedes Registrados", nullptr));
        label_5->setText(QApplication::translate("ClienteDialog", "Rua", nullptr));
        label_6->setText(QApplication::translate("ClienteDialog", "CEP", nullptr));
        label_7->setText(QApplication::translate("ClienteDialog", "Bairro", nullptr));
        label_8->setText(QApplication::translate("ClienteDialog", "Estado", nullptr));
        label_9->setText(QApplication::translate("ClienteDialog", "Pais", nullptr));
        txt_cpf->setInputMask(QApplication::translate("ClienteDialog", "000.000.000-00", nullptr));
        txt_telefone_movel->setInputMask(QApplication::translate("ClienteDialog", "(00)00000-0000", nullptr));
        txt_telefone_fixo->setInputMask(QApplication::translate("ClienteDialog", "(00)0000-0000", nullptr));
        txt_CEP->setInputMask(QApplication::translate("ClienteDialog", "00000-00", nullptr));
        label_10->setText(QApplication::translate("ClienteDialog", "Cidade", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ClienteDialog: public Ui_ClienteDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLIENTEDIALOG_H
