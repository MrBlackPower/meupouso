/********************************************************************************
** Form generated from reading UI file 'reservacheckindialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RESERVACHECKINDIALOG_H
#define UI_RESERVACHECKINDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDateTimeEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ReservaCheckinDialog
{
public:
    QDialogButtonBox *buttonBox;
    QLabel *label_4;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QTextEdit *txt_reserva;
    QTextEdit *txt_quarto;
    QTextEdit *txt_cliente;
    QLabel *label_8;
    QTextEdit *txt_PAX;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QDateTimeEdit *dt_current;
    QDateTimeEdit *dt_insert;
    QLabel *label_9;
    QWidget *gridLayoutWidget_2;
    QGridLayout *gridLayout_2;
    QLabel *label_11;
    QLabel *label_10;
    QLabel *label;
    QPushButton *btn_add_hospede;
    QListWidget *lst_hospedes_disp;
    QListWidget *lst_hospedes_selec;
    QPushButton *btn_refresh;

    void setupUi(QDialog *ReservaCheckinDialog)
    {
        if (ReservaCheckinDialog->objectName().isEmpty())
            ReservaCheckinDialog->setObjectName(QString::fromUtf8("ReservaCheckinDialog"));
        ReservaCheckinDialog->resize(625, 706);
        buttonBox = new QDialogButtonBox(ReservaCheckinDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(10, 660, 601, 40));
        buttonBox->setMinimumSize(QSize(0, 40));
        QFont font;
        font.setFamily(QString::fromUtf8("URW Bookman L"));
        font.setPointSize(15);
        font.setBold(true);
        font.setWeight(75);
        buttonBox->setFont(font);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        label_4 = new QLabel(ReservaCheckinDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 0, 81, 40));
        label_4->setFont(font);
        formLayoutWidget = new QWidget(ReservaCheckinDialog);
        formLayoutWidget->setObjectName(QString::fromUtf8("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(10, 130, 601, 180));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label_5 = new QLabel(formLayoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font);

        formLayout->setWidget(0, QFormLayout::LabelRole, label_5);

        label_6 = new QLabel(formLayoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setFont(font);

        formLayout->setWidget(1, QFormLayout::LabelRole, label_6);

        label_7 = new QLabel(formLayoutWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setFont(font);

        formLayout->setWidget(2, QFormLayout::LabelRole, label_7);

        txt_reserva = new QTextEdit(formLayoutWidget);
        txt_reserva->setObjectName(QString::fromUtf8("txt_reserva"));
        txt_reserva->setEnabled(false);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(txt_reserva->sizePolicy().hasHeightForWidth());
        txt_reserva->setSizePolicy(sizePolicy);
        txt_reserva->setMinimumSize(QSize(495, 0));
        txt_reserva->setMaximumSize(QSize(495, 40));
        QFont font1;
        font1.setFamily(QString::fromUtf8("URW Bookman L"));
        font1.setPointSize(12);
        txt_reserva->setFont(font1);
        txt_reserva->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(0, QFormLayout::FieldRole, txt_reserva);

        txt_quarto = new QTextEdit(formLayoutWidget);
        txt_quarto->setObjectName(QString::fromUtf8("txt_quarto"));
        txt_quarto->setEnabled(false);
        sizePolicy.setHeightForWidth(txt_quarto->sizePolicy().hasHeightForWidth());
        txt_quarto->setSizePolicy(sizePolicy);
        txt_quarto->setMinimumSize(QSize(495, 0));
        txt_quarto->setMaximumSize(QSize(495, 40));
        txt_quarto->setFont(font1);
        txt_quarto->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(1, QFormLayout::FieldRole, txt_quarto);

        txt_cliente = new QTextEdit(formLayoutWidget);
        txt_cliente->setObjectName(QString::fromUtf8("txt_cliente"));
        txt_cliente->setEnabled(false);
        sizePolicy.setHeightForWidth(txt_cliente->sizePolicy().hasHeightForWidth());
        txt_cliente->setSizePolicy(sizePolicy);
        txt_cliente->setMinimumSize(QSize(495, 0));
        txt_cliente->setMaximumSize(QSize(495, 40));
        txt_cliente->setFont(font1);
        txt_cliente->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(2, QFormLayout::FieldRole, txt_cliente);

        label_8 = new QLabel(formLayoutWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setFont(font);

        formLayout->setWidget(3, QFormLayout::LabelRole, label_8);

        txt_PAX = new QTextEdit(formLayoutWidget);
        txt_PAX->setObjectName(QString::fromUtf8("txt_PAX"));
        txt_PAX->setEnabled(false);
        sizePolicy.setHeightForWidth(txt_PAX->sizePolicy().hasHeightForWidth());
        txt_PAX->setSizePolicy(sizePolicy);
        txt_PAX->setMinimumSize(QSize(495, 0));
        txt_PAX->setMaximumSize(QSize(495, 40));
        txt_PAX->setFont(font1);
        txt_PAX->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(3, QFormLayout::FieldRole, txt_PAX);

        gridLayoutWidget = new QWidget(ReservaCheckinDialog);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 40, 601, 80));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        radioButton = new QRadioButton(gridLayoutWidget);
        radioButton->setObjectName(QString::fromUtf8("radioButton"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("URW Bookman L"));
        font2.setPointSize(15);
        radioButton->setFont(font2);
        radioButton->setAutoFillBackground(false);
        radioButton->setChecked(true);

        gridLayout->addWidget(radioButton, 0, 0, 1, 1);

        radioButton_2 = new QRadioButton(gridLayoutWidget);
        radioButton_2->setObjectName(QString::fromUtf8("radioButton_2"));
        radioButton_2->setFont(font2);

        gridLayout->addWidget(radioButton_2, 0, 1, 1, 1);

        dt_current = new QDateTimeEdit(gridLayoutWidget);
        dt_current->setObjectName(QString::fromUtf8("dt_current"));
        dt_current->setCalendarPopup(true);

        gridLayout->addWidget(dt_current, 1, 0, 1, 1);

        dt_insert = new QDateTimeEdit(gridLayoutWidget);
        dt_insert->setObjectName(QString::fromUtf8("dt_insert"));
        dt_insert->setCalendarPopup(true);

        gridLayout->addWidget(dt_insert, 1, 1, 1, 1);

        label_9 = new QLabel(ReservaCheckinDialog);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(20, 330, 111, 40));
        label_9->setFont(font);
        gridLayoutWidget_2 = new QWidget(ReservaCheckinDialog);
        gridLayoutWidget_2->setObjectName(QString::fromUtf8("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(10, 360, 601, 291));
        gridLayout_2 = new QGridLayout(gridLayoutWidget_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        label_11 = new QLabel(gridLayoutWidget_2);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("URW Bookman L"));
        font3.setPointSize(14);
        font3.setBold(false);
        font3.setItalic(true);
        font3.setWeight(50);
        label_11->setFont(font3);

        gridLayout_2->addWidget(label_11, 0, 3, 1, 1);

        label_10 = new QLabel(gridLayoutWidget_2);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setFont(font3);

        gridLayout_2->addWidget(label_10, 0, 1, 1, 1);

        label = new QLabel(gridLayoutWidget_2);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font4;
        font4.setFamily(QString::fromUtf8("URW Gothic L"));
        font4.setPointSize(20);
        font4.setBold(true);
        font4.setItalic(true);
        font4.setWeight(75);
        label->setFont(font4);

        gridLayout_2->addWidget(label, 1, 2, 1, 1);

        btn_add_hospede = new QPushButton(gridLayoutWidget_2);
        btn_add_hospede->setObjectName(QString::fromUtf8("btn_add_hospede"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(btn_add_hospede->sizePolicy().hasHeightForWidth());
        btn_add_hospede->setSizePolicy(sizePolicy1);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/add.png"), QSize(), QIcon::Normal, QIcon::Off);
        btn_add_hospede->setIcon(icon);
        btn_add_hospede->setIconSize(QSize(40, 40));

        gridLayout_2->addWidget(btn_add_hospede, 1, 0, 1, 1);

        lst_hospedes_disp = new QListWidget(gridLayoutWidget_2);
        lst_hospedes_disp->setObjectName(QString::fromUtf8("lst_hospedes_disp"));

        gridLayout_2->addWidget(lst_hospedes_disp, 1, 1, 1, 1);

        lst_hospedes_selec = new QListWidget(gridLayoutWidget_2);
        lst_hospedes_selec->setObjectName(QString::fromUtf8("lst_hospedes_selec"));

        gridLayout_2->addWidget(lst_hospedes_selec, 1, 3, 1, 1);

        btn_refresh = new QPushButton(gridLayoutWidget_2);
        btn_refresh->setObjectName(QString::fromUtf8("btn_refresh"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(btn_refresh->sizePolicy().hasHeightForWidth());
        btn_refresh->setSizePolicy(sizePolicy2);
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/repeat.png"), QSize(), QIcon::Normal, QIcon::Off);
        btn_refresh->setIcon(icon1);
        btn_refresh->setIconSize(QSize(40, 40));

        gridLayout_2->addWidget(btn_refresh, 0, 0, 1, 1);


        retranslateUi(ReservaCheckinDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ReservaCheckinDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ReservaCheckinDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ReservaCheckinDialog);
    } // setupUi

    void retranslateUi(QDialog *ReservaCheckinDialog)
    {
        ReservaCheckinDialog->setWindowTitle(QApplication::translate("ReservaCheckinDialog", "Dialog", nullptr));
        label_4->setText(QApplication::translate("ReservaCheckinDialog", "Hor\303\241rio", nullptr));
        label_5->setText(QApplication::translate("ReservaCheckinDialog", "Reserva", nullptr));
        label_6->setText(QApplication::translate("ReservaCheckinDialog", "Quarto", nullptr));
        label_7->setText(QApplication::translate("ReservaCheckinDialog", "Cliente", nullptr));
        label_8->setText(QApplication::translate("ReservaCheckinDialog", "PAX", nullptr));
        radioButton->setText(QApplication::translate("ReservaCheckinDialog", "Hor\303\241rio Atual", nullptr));
        radioButton_2->setText(QApplication::translate("ReservaCheckinDialog", "Inserir", nullptr));
        label_9->setText(QApplication::translate("ReservaCheckinDialog", "H\303\263spedes", nullptr));
        label_11->setText(QApplication::translate("ReservaCheckinDialog", "Selecionados", nullptr));
        label_10->setText(QApplication::translate("ReservaCheckinDialog", "Dispon\303\255veis", nullptr));
        label->setText(QApplication::translate("ReservaCheckinDialog", ">>", nullptr));
        btn_add_hospede->setText(QString());
        btn_refresh->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ReservaCheckinDialog: public Ui_ReservaCheckinDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RESERVACHECKINDIALOG_H
