/********************************************************************************
** Form generated from reading UI file 'hospededialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HOSPEDEDIALOG_H
#define UI_HOSPEDEDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_HospedeDialog
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QTextEdit *txt_nome;
    QLabel *label_2;
    QLabel *label_3;
    QDateEdit *dateEdit;
    QLabel *label_4;
    QTextEdit *txt_cliente;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *btn_procurar_cliente;
    QLineEdit *txt_cpf;

    void setupUi(QDialog *HospedeDialog)
    {
        if (HospedeDialog->objectName().isEmpty())
            HospedeDialog->setObjectName(QString::fromUtf8("HospedeDialog"));
        HospedeDialog->resize(737, 317);
        buttonBox = new QDialogButtonBox(HospedeDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(10, 270, 711, 41));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(buttonBox->sizePolicy().hasHeightForWidth());
        buttonBox->setSizePolicy(sizePolicy);
        QFont font;
        font.setFamily(QString::fromUtf8("URW Bookman L"));
        font.setPointSize(15);
        buttonBox->setFont(font);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        formLayoutWidget = new QWidget(HospedeDialog);
        formLayoutWidget->setObjectName(QString::fromUtf8("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(10, 10, 712, 258));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setLabelAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        formLayout->setFormAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(formLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("URW Bookman L"));
        font1.setPointSize(15);
        font1.setBold(true);
        font1.setWeight(75);
        label->setFont(font1);

        formLayout->setWidget(3, QFormLayout::LabelRole, label);

        txt_nome = new QTextEdit(formLayoutWidget);
        txt_nome->setObjectName(QString::fromUtf8("txt_nome"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(txt_nome->sizePolicy().hasHeightForWidth());
        txt_nome->setSizePolicy(sizePolicy1);
        txt_nome->setMinimumSize(QSize(495, 0));
        txt_nome->setMaximumSize(QSize(495, 40));
        QFont font2;
        font2.setFamily(QString::fromUtf8("URW Bookman L"));
        font2.setPointSize(12);
        txt_nome->setFont(font2);

        formLayout->setWidget(3, QFormLayout::FieldRole, txt_nome);

        label_2 = new QLabel(formLayoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font1);

        formLayout->setWidget(4, QFormLayout::LabelRole, label_2);

        label_3 = new QLabel(formLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font1);

        formLayout->setWidget(5, QFormLayout::LabelRole, label_3);

        dateEdit = new QDateEdit(formLayoutWidget);
        dateEdit->setObjectName(QString::fromUtf8("dateEdit"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(dateEdit->sizePolicy().hasHeightForWidth());
        dateEdit->setSizePolicy(sizePolicy2);
        dateEdit->setMinimumSize(QSize(495, 40));
        dateEdit->setMinimumDateTime(QDateTime(QDate(1900, 9, 21), QTime(0, 0, 0)));
        dateEdit->setCalendarPopup(true);

        formLayout->setWidget(5, QFormLayout::FieldRole, dateEdit);

        label_4 = new QLabel(formLayoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font1);

        formLayout->setWidget(0, QFormLayout::LabelRole, label_4);

        txt_cliente = new QTextEdit(formLayoutWidget);
        txt_cliente->setObjectName(QString::fromUtf8("txt_cliente"));
        txt_cliente->setEnabled(false);
        sizePolicy1.setHeightForWidth(txt_cliente->sizePolicy().hasHeightForWidth());
        txt_cliente->setSizePolicy(sizePolicy1);
        txt_cliente->setMinimumSize(QSize(495, 0));
        txt_cliente->setMaximumSize(QSize(495, 40));
        txt_cliente->setFont(font2);
        txt_cliente->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(0, QFormLayout::FieldRole, txt_cliente);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btn_procurar_cliente = new QPushButton(formLayoutWidget);
        btn_procurar_cliente->setObjectName(QString::fromUtf8("btn_procurar_cliente"));
        QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(btn_procurar_cliente->sizePolicy().hasHeightForWidth());
        btn_procurar_cliente->setSizePolicy(sizePolicy3);
        btn_procurar_cliente->setMaximumSize(QSize(200, 16777215));
        btn_procurar_cliente->setLayoutDirection(Qt::RightToLeft);
        btn_procurar_cliente->setAutoFillBackground(false);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/search.png"), QSize(), QIcon::Normal, QIcon::Off);
        btn_procurar_cliente->setIcon(icon);
        btn_procurar_cliente->setIconSize(QSize(40, 40));
        btn_procurar_cliente->setFlat(false);

        horizontalLayout->addWidget(btn_procurar_cliente);


        formLayout->setLayout(1, QFormLayout::FieldRole, horizontalLayout);

        txt_cpf = new QLineEdit(formLayoutWidget);
        txt_cpf->setObjectName(QString::fromUtf8("txt_cpf"));
        sizePolicy.setHeightForWidth(txt_cpf->sizePolicy().hasHeightForWidth());
        txt_cpf->setSizePolicy(sizePolicy);
        txt_cpf->setMinimumSize(QSize(0, 40));
        txt_cpf->setMaximumSize(QSize(495, 16777215));
        txt_cpf->setFont(font);

        formLayout->setWidget(4, QFormLayout::FieldRole, txt_cpf);


        retranslateUi(HospedeDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), HospedeDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), HospedeDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(HospedeDialog);
    } // setupUi

    void retranslateUi(QDialog *HospedeDialog)
    {
        HospedeDialog->setWindowTitle(QApplication::translate("HospedeDialog", "Dialog", nullptr));
        label->setText(QApplication::translate("HospedeDialog", "Nome", nullptr));
        label_2->setText(QApplication::translate("HospedeDialog", "CPF", nullptr));
        label_3->setText(QApplication::translate("HospedeDialog", "Data de Nascimento", nullptr));
        label_4->setText(QApplication::translate("HospedeDialog", "Cliente", nullptr));
        btn_procurar_cliente->setText(QString());
        txt_cpf->setInputMask(QApplication::translate("HospedeDialog", "000.000.000-00", nullptr));
    } // retranslateUi

};

namespace Ui {
    class HospedeDialog: public Ui_HospedeDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HOSPEDEDIALOG_H
