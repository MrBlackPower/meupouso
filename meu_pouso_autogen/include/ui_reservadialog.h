/********************************************************************************
** Form generated from reading UI file 'reservadialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RESERVADIALOG_H
#define UI_RESERVADIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ReservaDialog
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label_4;
    QTextEdit *txt_cliente;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *btn_procurar_cliente;
    QLabel *label_7;
    QLabel *label_6;
    QDateEdit *dt_inicio;
    QDateEdit *dt_fim;
    QLabel *label_5;
    QTextEdit *txt_quarto;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *btn_procurar_quarto;
    QLabel *label_9;
    QLabel *label_8;
    QTextEdit *txt_PAX;
    QSpinBox *spn_hospedes_adultos;
    QLabel *label_10;
    QSpinBox *spn_hospedes_criancas;
    QPushButton *btn_checkin;
    QPushButton *btn_confirm;
    QPushButton *btn_checkout;

    void setupUi(QDialog *ReservaDialog)
    {
        if (ReservaDialog->objectName().isEmpty())
            ReservaDialog->setObjectName(QString::fromUtf8("ReservaDialog"));
        ReservaDialog->resize(668, 626);
        buttonBox = new QDialogButtonBox(ReservaDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(0, 580, 661, 40));
        buttonBox->setMinimumSize(QSize(0, 40));
        QFont font;
        font.setFamily(QString::fromUtf8("URW Bookman L"));
        font.setPointSize(15);
        font.setBold(true);
        font.setWeight(75);
        buttonBox->setFont(font);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        formLayoutWidget = new QWidget(ReservaDialog);
        formLayoutWidget->setObjectName(QString::fromUtf8("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(0, 0, 661, 426));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(formLayoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font);

        formLayout->setWidget(0, QFormLayout::LabelRole, label_4);

        txt_cliente = new QTextEdit(formLayoutWidget);
        txt_cliente->setObjectName(QString::fromUtf8("txt_cliente"));
        txt_cliente->setEnabled(false);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(txt_cliente->sizePolicy().hasHeightForWidth());
        txt_cliente->setSizePolicy(sizePolicy);
        txt_cliente->setMinimumSize(QSize(495, 0));
        txt_cliente->setMaximumSize(QSize(495, 40));
        QFont font1;
        font1.setFamily(QString::fromUtf8("URW Bookman L"));
        font1.setPointSize(12);
        txt_cliente->setFont(font1);
        txt_cliente->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(0, QFormLayout::FieldRole, txt_cliente);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btn_procurar_cliente = new QPushButton(formLayoutWidget);
        btn_procurar_cliente->setObjectName(QString::fromUtf8("btn_procurar_cliente"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(btn_procurar_cliente->sizePolicy().hasHeightForWidth());
        btn_procurar_cliente->setSizePolicy(sizePolicy1);
        btn_procurar_cliente->setMaximumSize(QSize(200, 16777215));
        btn_procurar_cliente->setLayoutDirection(Qt::RightToLeft);
        btn_procurar_cliente->setAutoFillBackground(false);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/search.png"), QSize(), QIcon::Normal, QIcon::Off);
        btn_procurar_cliente->setIcon(icon);
        btn_procurar_cliente->setIconSize(QSize(40, 40));
        btn_procurar_cliente->setFlat(false);

        horizontalLayout->addWidget(btn_procurar_cliente);


        formLayout->setLayout(1, QFormLayout::FieldRole, horizontalLayout);

        label_7 = new QLabel(formLayoutWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setFont(font);

        formLayout->setWidget(3, QFormLayout::LabelRole, label_7);

        label_6 = new QLabel(formLayoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setFont(font);

        formLayout->setWidget(2, QFormLayout::LabelRole, label_6);

        dt_inicio = new QDateEdit(formLayoutWidget);
        dt_inicio->setObjectName(QString::fromUtf8("dt_inicio"));
        dt_inicio->setMinimumSize(QSize(0, 40));
        dt_inicio->setFont(font1);
        dt_inicio->setCalendarPopup(true);

        formLayout->setWidget(2, QFormLayout::FieldRole, dt_inicio);

        dt_fim = new QDateEdit(formLayoutWidget);
        dt_fim->setObjectName(QString::fromUtf8("dt_fim"));
        dt_fim->setMinimumSize(QSize(0, 40));
        dt_fim->setFont(font1);
        dt_fim->setCalendarPopup(true);

        formLayout->setWidget(3, QFormLayout::FieldRole, dt_fim);

        label_5 = new QLabel(formLayoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font);

        formLayout->setWidget(4, QFormLayout::LabelRole, label_5);

        txt_quarto = new QTextEdit(formLayoutWidget);
        txt_quarto->setObjectName(QString::fromUtf8("txt_quarto"));
        txt_quarto->setEnabled(false);
        sizePolicy.setHeightForWidth(txt_quarto->sizePolicy().hasHeightForWidth());
        txt_quarto->setSizePolicy(sizePolicy);
        txt_quarto->setMinimumSize(QSize(495, 0));
        txt_quarto->setMaximumSize(QSize(495, 40));
        txt_quarto->setFont(font1);
        txt_quarto->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(4, QFormLayout::FieldRole, txt_quarto);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        btn_procurar_quarto = new QPushButton(formLayoutWidget);
        btn_procurar_quarto->setObjectName(QString::fromUtf8("btn_procurar_quarto"));
        sizePolicy1.setHeightForWidth(btn_procurar_quarto->sizePolicy().hasHeightForWidth());
        btn_procurar_quarto->setSizePolicy(sizePolicy1);
        btn_procurar_quarto->setMaximumSize(QSize(200, 16777215));
        btn_procurar_quarto->setLayoutDirection(Qt::RightToLeft);
        btn_procurar_quarto->setAutoFillBackground(false);
        btn_procurar_quarto->setIcon(icon);
        btn_procurar_quarto->setIconSize(QSize(40, 40));
        btn_procurar_quarto->setFlat(false);

        horizontalLayout_2->addWidget(btn_procurar_quarto);


        formLayout->setLayout(5, QFormLayout::FieldRole, horizontalLayout_2);

        label_9 = new QLabel(formLayoutWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setFont(font);
        label_9->setWordWrap(true);

        formLayout->setWidget(7, QFormLayout::LabelRole, label_9);

        label_8 = new QLabel(formLayoutWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setFont(font);

        formLayout->setWidget(6, QFormLayout::LabelRole, label_8);

        txt_PAX = new QTextEdit(formLayoutWidget);
        txt_PAX->setObjectName(QString::fromUtf8("txt_PAX"));
        txt_PAX->setEnabled(false);
        txt_PAX->setMinimumSize(QSize(0, 40));

        formLayout->setWidget(6, QFormLayout::FieldRole, txt_PAX);

        spn_hospedes_adultos = new QSpinBox(formLayoutWidget);
        spn_hospedes_adultos->setObjectName(QString::fromUtf8("spn_hospedes_adultos"));
        spn_hospedes_adultos->setMinimumSize(QSize(0, 40));

        formLayout->setWidget(7, QFormLayout::FieldRole, spn_hospedes_adultos);

        label_10 = new QLabel(formLayoutWidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setFont(font);
        label_10->setWordWrap(true);

        formLayout->setWidget(8, QFormLayout::LabelRole, label_10);

        spn_hospedes_criancas = new QSpinBox(formLayoutWidget);
        spn_hospedes_criancas->setObjectName(QString::fromUtf8("spn_hospedes_criancas"));
        spn_hospedes_criancas->setMinimumSize(QSize(0, 40));

        formLayout->setWidget(8, QFormLayout::FieldRole, spn_hospedes_criancas);

        btn_checkin = new QPushButton(ReservaDialog);
        btn_checkin->setObjectName(QString::fromUtf8("btn_checkin"));
        btn_checkin->setEnabled(false);
        btn_checkin->setGeometry(QRect(0, 480, 661, 46));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/checked.png"), QSize(), QIcon::Normal, QIcon::Off);
        btn_checkin->setIcon(icon1);
        btn_checkin->setIconSize(QSize(40, 40));
        btn_confirm = new QPushButton(ReservaDialog);
        btn_confirm->setObjectName(QString::fromUtf8("btn_confirm"));
        btn_confirm->setEnabled(false);
        btn_confirm->setGeometry(QRect(0, 430, 661, 46));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/checked_1.png"), QSize(), QIcon::Normal, QIcon::Off);
        btn_confirm->setIcon(icon2);
        btn_confirm->setIconSize(QSize(40, 40));
        btn_checkout = new QPushButton(ReservaDialog);
        btn_checkout->setObjectName(QString::fromUtf8("btn_checkout"));
        btn_checkout->setEnabled(false);
        btn_checkout->setGeometry(QRect(0, 530, 661, 46));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/close.png"), QSize(), QIcon::Normal, QIcon::Off);
        btn_checkout->setIcon(icon3);
        btn_checkout->setIconSize(QSize(40, 40));

        retranslateUi(ReservaDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ReservaDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ReservaDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ReservaDialog);
    } // setupUi

    void retranslateUi(QDialog *ReservaDialog)
    {
        ReservaDialog->setWindowTitle(QApplication::translate("ReservaDialog", "Dialog", nullptr));
        label_4->setText(QApplication::translate("ReservaDialog", "Cliente", nullptr));
        btn_procurar_cliente->setText(QString());
        label_7->setText(QApplication::translate("ReservaDialog", "Fim", nullptr));
        label_6->setText(QApplication::translate("ReservaDialog", "Inicio", nullptr));
        label_5->setText(QApplication::translate("ReservaDialog", "Quarto", nullptr));
        btn_procurar_quarto->setText(QString());
        label_9->setText(QApplication::translate("ReservaDialog", "H\303\263spedes Adultos", nullptr));
        label_8->setText(QApplication::translate("ReservaDialog", "PAX", nullptr));
        label_10->setText(QApplication::translate("ReservaDialog", "H\303\263spedes Crian\303\247as", nullptr));
        btn_checkin->setText(QString());
        btn_confirm->setText(QString());
        btn_checkout->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ReservaDialog: public Ui_ReservaDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RESERVADIALOG_H
