#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    //VERIFY IF LOCK EXISTS
    FILE* f;
    lock_filename = QDir::currentPath().toStdString().data();
    lock_filename += "/rosas.dat";
    lock_filename.replace("/","\\");

    f = fopen(lock_filename.toStdString().data(),"r");
    log.push_log(lock_filename.toStdString().data());

    if(f){
        fclose(f);
        qApp->closeAllWindows();
        log.push_log("LOCK FOUND");
        locked = true;
        qApp->exit();
    } else {
        locked = false;
        log.push_log("LOCK CREATED");
        fclose(f);
        lock = fopen(lock_filename.toStdString().data(),"w+");

        fprintf(lock,"BLOCK");

        fclose(lock);
    }


    ui->setupUi(this);
    update_tables();

    using_status = false;
    reserva_status = Reserva::NAO_CONFIRMADA;

    QDateTime t;
    t = t.currentDateTime();

    QString time = t.time().toString();
    QString date = t.date().toString("dd - MMMM - yyyy");
    date.replace("/","de");
    ui->lbl_time->setText(time);
    ui->lbl_date->setText(date);

    refresh_rate = new QTimer();
    refresh_rate->setInterval(REFRESH_RATE);
    QObject::connect(refresh_rate,SIGNAL(timeout()),this,SLOT(update_time()));

    backup_rate = new QTimer();
    backup_rate->setInterval(BACKUP_RATE);
    QObject::connect(backup_rate,SIGNAL(timeout()),this,SLOT(do_backup()));
    QObject::connect(&log,SIGNAL(repush_log()),this,SLOT(update_log()));

    QObject::connect(&pousada,SIGNAL(updated()),this,SLOT(update_tables()));
    QObject::connect(&pousada,SIGNAL(updated()),this,SLOT(update_tipoquarto_list()));

    //REFRESH BUTTON
    QObject::connect(ui->btn_update,SIGNAL(clicked()),this,SLOT(update_tables()));
    QObject::connect(ui->btn_update,SIGNAL(clicked()),this,SLOT(do_backup()));

    refresh_rate->start();
    backup_rate->start();

    quarto_tipo = "";
    using_data_fim = false;
    using_data_inicio = false;

    last_reserva_check_in_ID = -1;
    last_reserva_check_out_ID = -1;
    last_reserva_confirm_ID = -1;
    last_hospede_row_ID = -1;
    last_tipo_quarto_row_ID = -1;
    last_quarto_row_ID = -1;
    last_cliente_row_ID = -1;
    last_reserva_row_ID = -1;

    //SETS CURRENT YEAR
    ui->dt_inicio_lower->setDate(QDate::currentDate());
    ui->dt_inicio_higher->setDate(QDate::currentDate());
    ui->dt_disponivel_lower->setDate(QDate::currentDate());
    ui->dt_disponivel_higher->setDate(QDate::currentDate());

    ui->cbx_status_reserva->clear();
    ui->cbx_status_reserva->addItem("NENHUM");
    ui->cbx_status_reserva->addItem("NAO_CONFIRMADA");
    ui->cbx_status_reserva->addItem("CONFIRMADA");
    ui->cbx_status_reserva->addItem("CHECK_IN");
    ui->cbx_status_reserva->addItem("CHECK_OUT");

    log.backup(&pousada);
}

void MainWindow::update_time(){
    QDateTime t;
    t = t.currentDateTime();

    QString time = t.time().toString();
    QString date = t.date().toString("dd / MMMM / yyyy");
    date.replace("/","de");
    ui->lbl_time->setText(time);
    ui->lbl_date->setText(date);

    if(locked)
        qApp->exit();
}

void MainWindow::update_tables(){
    //MAIN WINDOW LISTS
    vector<tuple<int,QString,QString,QString,QString>> confirmations = pousada.get_confirmations();
    vector<tuple<int,QString,QString,QString,QString>> check_ins = pousada.get_check_ins();
    vector<tuple<int,QString,QString,QString,QString>> check_outs = pousada.get_check_outs();

    if(!confirmations.empty())
        ui->btn_confirma->setEnabled(true);
    else
        ui->btn_confirma->setEnabled(false);


    if(!check_ins.empty())
        ui->btn_checkin->setEnabled(true);
    else
        ui->btn_checkin->setEnabled(false);


    if(!check_outs.empty())
        ui->btn_checkout->setEnabled(true);
    else
        ui->btn_checkout->setEnabled(false);

    int i = 0;

    ui->tb_confirmations->setColumnCount(5);
    ui->tb_confirmations->setHorizontalHeaderItem(0, new QTableWidgetItem("ID"));
    ui->tb_confirmations->setHorizontalHeaderItem(1, new QTableWidgetItem("Nome"));
    ui->tb_confirmations->setHorizontalHeaderItem(2, new QTableWidgetItem("Quarto"));
    ui->tb_confirmations->setHorizontalHeaderItem(3, new QTableWidgetItem("Inicio"));
    ui->tb_confirmations->setHorizontalHeaderItem(4, new QTableWidgetItem("Fim"));
    ui->tb_confirmations->setRowCount(confirmations.size());
    for(auto t : confirmations){
        ui->tb_confirmations->setItem(i,0,new QTableWidgetItem(QString::asprintf("%d",get<0>(t))));
        ui->tb_confirmations->setItem(i,1,new QTableWidgetItem(get<1>(t)));
        ui->tb_confirmations->setItem(i,2,new QTableWidgetItem(get<2>(t)));
        ui->tb_confirmations->setItem(i,3,new QTableWidgetItem(get<3>(t)));
        ui->tb_confirmations->setItem(i,4,new QTableWidgetItem(get<4>(t)));
        i++;
    }
    ui->tb_check_ins->setColumnCount(5);
    ui->tb_check_ins->setHorizontalHeaderItem(0, new QTableWidgetItem("ID"));
    ui->tb_check_ins->setHorizontalHeaderItem(1, new QTableWidgetItem("Nome"));
    ui->tb_check_ins->setHorizontalHeaderItem(2, new QTableWidgetItem("Quarto"));
    ui->tb_check_ins->setHorizontalHeaderItem(3, new QTableWidgetItem("Inicio"));
    ui->tb_check_ins->setHorizontalHeaderItem(4, new QTableWidgetItem("Fim"));
    ui->tb_check_ins->setRowCount(check_ins.size());
    for(auto t : check_ins){
        ui->tb_check_ins->setItem(i,0,new QTableWidgetItem(QString::asprintf("%d",get<0>(t))));
        ui->tb_check_ins->setItem(i,1,new QTableWidgetItem(get<1>(t)));
        ui->tb_check_ins->setItem(i,2,new QTableWidgetItem(get<2>(t)));
        ui->tb_check_ins->setItem(i,3,new QTableWidgetItem(get<3>(t)));
        ui->tb_check_ins->setItem(i,4,new QTableWidgetItem(get<4>(t)));
        i++;
    }
    ui->tb_check_outs->setColumnCount(5);
    ui->tb_check_outs->setHorizontalHeaderItem(0, new QTableWidgetItem("ID"));
    ui->tb_check_outs->setHorizontalHeaderItem(1, new QTableWidgetItem("Nome"));
    ui->tb_check_outs->setHorizontalHeaderItem(2, new QTableWidgetItem("Quarto"));
    ui->tb_check_outs->setHorizontalHeaderItem(3, new QTableWidgetItem("Inicio"));
    ui->tb_check_outs->setHorizontalHeaderItem(4, new QTableWidgetItem("Fim"));
    ui->tb_check_outs->setRowCount(check_outs.size());
    for(auto t : check_outs){
        ui->tb_check_outs->setItem(i,0,new QTableWidgetItem(QString::asprintf("%d",get<0>(t))));
        ui->tb_check_outs->setItem(i,1,new QTableWidgetItem(get<1>(t)));
        ui->tb_check_outs->setItem(i,2,new QTableWidgetItem(get<2>(t)));
        ui->tb_check_outs->setItem(i,3,new QTableWidgetItem(get<3>(t)));
        ui->tb_check_outs->setItem(i,4,new QTableWidgetItem(get<4>(t)));
        i++;
    }

    //TIPO QUARTO
    QTableWidget* tipos_quarto = ui->table_tipoquarto;
    tipos_quarto->setColumnCount(3);

    tipos_quarto->setHorizontalHeaderItem(0, new QTableWidgetItem("ID"));
    tipos_quarto->setHorizontalHeaderItem(1, new QTableWidgetItem("Nome"));
    tipos_quarto->setHorizontalHeaderItem(2, new QTableWidgetItem("Descrição"));

    vector<QString> tp_IDS;
    vector<QString> tp_nomes;
    vector<QString> tp_descs;

    pousada.get_tipos_quarto(&tp_IDS,&tp_nomes,&tp_descs,tipo_quarto_nome);
    tipos_quarto->setRowCount(tp_IDS.size());

    for (int i = 0; i < tp_IDS.size(); i++) {
        tipos_quarto->setItem(i,0,new QTableWidgetItem(tp_IDS.operator[](i)));
        tipos_quarto->setItem(i,1,new QTableWidgetItem(tp_nomes.operator[](i)));
        tipos_quarto->setItem(i,2,new QTableWidgetItem(tp_descs.operator[](i)));
    }

    //QUARTO
    QTableWidget* quartos = ui->table_quarto;
    quartos->setColumnCount(6);

    quartos->setHorizontalHeaderItem(0, new QTableWidgetItem("ID"));
    quartos->setHorizontalHeaderItem(1, new QTableWidgetItem("Número"));
    quartos->setHorizontalHeaderItem(2, new QTableWidgetItem("PAX"));
    quartos->setHorizontalHeaderItem(3, new QTableWidgetItem("Disponível"));
    quartos->setHorizontalHeaderItem(4, new QTableWidgetItem("Tipo de Quarto"));
    quartos->setHorizontalHeaderItem(5, new QTableWidgetItem("Descrição"));

    vector<QString> q_IDS;
    vector<QString> q_numeros;
    vector<QString> q_PAXs;
    vector<QString> q_disps;
    vector<QString> q_tipos_de_quartos;
    vector<QString> q_descs;

    if(!using_disponivel)
        pousada.get_quartos(&q_IDS,&q_numeros,&q_PAXs,&q_disps,&q_tipos_de_quartos,&q_descs,quarto_numero,quarto_tipo);
    else
        pousada.get_quartos(&q_IDS,&q_numeros,&q_PAXs,&q_disps,&q_tipos_de_quartos,&q_descs,quarto_numero,quarto_tipo,data_inicio,data_fim);

    quartos->setRowCount(q_IDS.size());

    for (int i = 0; i < q_IDS.size(); i++) {
        quartos->setItem(i,0,new QTableWidgetItem(q_IDS.operator[](i)));
        quartos->setItem(i,1,new QTableWidgetItem(q_numeros.operator[](i)));
        quartos->setItem(i,2,new QTableWidgetItem(q_PAXs.operator[](i)));
        quartos->setItem(i,3,new QTableWidgetItem(q_disps.operator[](i)));
        quartos->setItem(i,4,new QTableWidgetItem(q_tipos_de_quartos.operator[](i)));
        quartos->setItem(i,5,new QTableWidgetItem(q_descs.operator[](i)));
    }

    //CLIENTE
    QTableWidget* clientes = ui->table_cliente;
    clientes->setColumnCount(6);

    clientes->setHorizontalHeaderItem(0, new QTableWidgetItem("ID"));
    clientes->setHorizontalHeaderItem(1, new QTableWidgetItem("Nome"));
    clientes->setHorizontalHeaderItem(2, new QTableWidgetItem("CPF"));
    clientes->setHorizontalHeaderItem(3, new QTableWidgetItem("Telefone Móvel"));
    clientes->setHorizontalHeaderItem(4, new QTableWidgetItem("Telefone Fixo"));
    clientes->setHorizontalHeaderItem(5, new QTableWidgetItem("Endereço"));

    vector<QString> c_IDS;
    vector<QString> c_nomes;
    vector<QString> c_cpfs;
    vector<QString> c_telefones_movel;
    vector<QString> c_telefones_fixo;
    vector<QString> c_enderecos;

    pousada.get_clientes(&c_IDS,&c_nomes,&c_cpfs,&c_telefones_movel,&c_telefones_fixo,&c_enderecos,cliente_nome,cliente_cpf);
    clientes->setRowCount(c_IDS.size());

    for (int i = 0; i < c_IDS.size(); i++) {
        clientes->setItem(i,0,new QTableWidgetItem(c_IDS.operator[](i)));
        clientes->setItem(i,1,new QTableWidgetItem(c_nomes.operator[](i)));
        clientes->setItem(i,2,new QTableWidgetItem(c_cpfs.operator[](i)));
        clientes->setItem(i,3,new QTableWidgetItem(c_telefones_movel.operator[](i)));
        clientes->setItem(i,4,new QTableWidgetItem(c_telefones_fixo.operator[](i)));
        clientes->setItem(i,5,new QTableWidgetItem(c_enderecos.operator[](i)));
    }

    //HOSPEDE
    QTableWidget* hospedes = ui->table_hospede;
    hospedes->setColumnCount(5);

    hospedes->setHorizontalHeaderItem(0, new QTableWidgetItem("ID"));
    hospedes->setHorizontalHeaderItem(1, new QTableWidgetItem("Nome"));
    hospedes->setHorizontalHeaderItem(2, new QTableWidgetItem("CPF"));
    hospedes->setHorizontalHeaderItem(3, new QTableWidgetItem("Data de Nascimento"));
    hospedes->setHorizontalHeaderItem(4, new QTableWidgetItem("Cliente Associado"));

    vector<QString> h_IDS;
    vector<QString> h_nomes;
    vector<QString> h_cpfs;
    vector<QString> h_data_nascimento;
    vector<QString> h_cliente;

    pousada.get_hospedes(&h_IDS,&h_nomes,&h_cpfs,&h_data_nascimento,&h_cliente,hospede_nome,hospede_cpf);
    hospedes->setRowCount(h_IDS.size());

    for (int i = 0; i < h_IDS.size(); i++) {
        hospedes->setItem(i,0,new QTableWidgetItem(h_IDS.operator[](i)));
        hospedes->setItem(i,1,new QTableWidgetItem(h_nomes.operator[](i)));
        hospedes->setItem(i,2,new QTableWidgetItem(h_cpfs.operator[](i)));
        hospedes->setItem(i,3,new QTableWidgetItem(h_data_nascimento.operator[](i)));
        hospedes->setItem(i,4,new QTableWidgetItem(h_cliente.operator[](i)));
    }

    //RESERVA
    QTableWidget* reservas = ui->table_reserva;
    reservas->setColumnCount(13);

    reservas->setHorizontalHeaderItem(0, new QTableWidgetItem("ID"));
    reservas->setHorizontalHeaderItem(1, new QTableWidgetItem("Cliente"));
    reservas->setHorizontalHeaderItem(2, new QTableWidgetItem("Quarto"));
    reservas->setHorizontalHeaderItem(3, new QTableWidgetItem("Status"));
    reservas->setHorizontalHeaderItem(4, new QTableWidgetItem("Início"));
    reservas->setHorizontalHeaderItem(5, new QTableWidgetItem("Fim"));
    reservas->setHorizontalHeaderItem(6, new QTableWidgetItem("Hóspedes Adultos"));
    reservas->setHorizontalHeaderItem(7, new QTableWidgetItem("Hóspedes Crianças"));
    reservas->setHorizontalHeaderItem(8, new QTableWidgetItem("Fez Check-in"));
    reservas->setHorizontalHeaderItem(9, new QTableWidgetItem("Data Check-in"));
    reservas->setHorizontalHeaderItem(10, new QTableWidgetItem("Fez Check-out"));
    reservas->setHorizontalHeaderItem(11, new QTableWidgetItem("Data Check-out"));
    reservas->setHorizontalHeaderItem(12, new QTableWidgetItem("Hóspedes"));

    vector<QString> r_IDS;
    vector<QString> r_clientes;
    vector<QString> r_quartos;
    vector<QString> r_status;
    vector<QString> r_data_inicio;
    vector<QString> r_data_fim;
    vector<QString> r_hospedes_adultos;
    vector<QString> r_hospedes_criancas;
    vector<QString> r_has_checkin;
    vector<QString> r_checkin;
    vector<QString> r_has_checkout;
    vector<QString> r_checkout;
    vector<QString> r_hospedes;


    pousada.get_reserva(&r_IDS,&r_clientes,&r_quartos,&r_status,&r_data_inicio,&r_data_fim,&r_hospedes_adultos,&r_hospedes_criancas,&r_has_checkin,&r_checkin,&r_has_checkout,&r_checkout,&r_hospedes,
                        reserva_cliente,reserva_quarto,tuple<bool,QDate,QDate>{using_data_inicio,data_inicio_lower,data_inicio_higher},tuple<bool,QDate,QDate>{using_data_fim,data_fim_lower,data_fim_higher},pair<bool,Reserva::ReservaStatus>{using_status,reserva_status});
    reservas->setRowCount(r_IDS.size());

    for (int i = 0; i < r_IDS.size(); i++) {
        reservas->setItem(i,0,new QTableWidgetItem(r_IDS.operator[](i)));
        reservas->setItem(i,1,new QTableWidgetItem(r_clientes.operator[](i)));
        reservas->setItem(i,2,new QTableWidgetItem(r_quartos.operator[](i)));
        reservas->setItem(i,3,new QTableWidgetItem(r_status.operator[](i)));
        reservas->setItem(i,4,new QTableWidgetItem(r_data_inicio.operator[](i)));
        reservas->setItem(i,5,new QTableWidgetItem(r_data_fim.operator[](i)));
        reservas->setItem(i,6,new QTableWidgetItem(r_hospedes_adultos.operator[](i)));
        reservas->setItem(i,7,new QTableWidgetItem(r_hospedes_criancas.operator[](i)));
        reservas->setItem(i,8,new QTableWidgetItem(r_has_checkin.operator[](i)));
        reservas->setItem(i,9,new QTableWidgetItem(r_checkin.operator[](i)));
        reservas->setItem(i,10,new QTableWidgetItem(r_has_checkout.operator[](i)));
        reservas->setItem(i,11,new QTableWidgetItem(r_checkout.operator[](i)));
        reservas->setItem(i,12,new QTableWidgetItem(r_hospedes.operator[](i)));
    }
}

void MainWindow::update_tipoquarto_list(){
    vector<pair<QString,int>> tps_qrts = pousada.get_tipos_quarto();
    ui->cbx_quarto_tipo_quarto_filtro->clear();
    ui->cbx_quarto_tipo_quarto_filtro->addItem("NENHUM");
    if(quarto_tipo == ""){
        for(pair<QString,int> txt : tps_qrts)
            ui->cbx_quarto_tipo_quarto_filtro->addItem(txt.first);
    } else {
        ui->cbx_quarto_tipo_quarto_filtro->addItem(quarto_tipo);

        for(pair<QString,int> txt : tps_qrts)
            if(txt.first != quarto_tipo)
                ui->cbx_quarto_tipo_quarto_filtro->addItem(txt.first);
    }
}

void MainWindow::do_backup(){
    log.do_backup(&pousada);
}

void MainWindow::update_log(){
    ui->txt_log->clear();
    ui->txt_log->setText(QString::asprintf("%s",log.str().data()));
}

void MainWindow::confirma_reserva(QString id){
    Reserva* r = pousada.get_reserva(id.toInt());

    if(!r)
        return;

    if(r->get_status() == Reserva::NAO_CONFIRMADA)
        r->confirm();

    update_tables();
}

MainWindow::~MainWindow()
{
    do_backup();

    QDir dir;
    dir.remove(lock_filename);

    delete backup_rate;
    delete refresh_rate;
    delete ui;
}

void MainWindow::on_btn_add_tipoquarto_clicked()
{
    TipoQuartoDialog* d = new TipoQuartoDialog(this);

    QObject::connect(d,SIGNAL(create_tipo_quarto(QString,QString)),&pousada,SLOT(add_tipo_quarto(QString,QString)));

    d->show();
}

void MainWindow::on_btn_edit_tipoquarto_clicked()
{
    TipoQuartoDialog* d = new TipoQuartoDialog(&pousada,pousada.get_tipo_quarto(last_tipo_quarto_row_ID),true,this);

    QObject::connect(d,SIGNAL(edit_tipo_quarto(int,QString,QString)),&pousada,SLOT(edit_tipo_quarto(int,QString,QString)));

    d->show();
}

void MainWindow::on_txt_tipo_quarto_nome_textChanged()
{
    tipo_quarto_nome = ui->txt_tipo_quarto_nome->text();

    update_tables();
}

void MainWindow::on_btn_remove_tipoquarto_clicked()
{
    if(last_tipo_quarto_row_ID != -1){
        TipoQuartoDialog* d = new TipoQuartoDialog(&pousada,pousada.get_tipo_quarto(last_tipo_quarto_row_ID),false,this);

        QObject::connect(d,SIGNAL(remove_tipo_quarto(int)),&pousada,SLOT(remove_tipo_quarto(int)));

        last_tipo_quarto_row_ID = -1;

        d->show();
    }
}

void MainWindow::on_table_tipoquarto_itemDoubleClicked(QTableWidgetItem *item)
{
    TipoQuartoDialog* d = new TipoQuartoDialog(&pousada,pousada.get_tipo_quarto(last_tipo_quarto_row_ID),true,this);

    QObject::connect(d,SIGNAL(edit_tipo_quarto(int,QString,QString)),&pousada,SLOT(edit_tipo_quarto(int,QString,QString)));

    d->show();
}

void MainWindow::on_btn_add_quarto_clicked()
{
    QuartoDialog* d = new QuartoDialog(&pousada,this);

    QObject::connect(d,SIGNAL(create_quarto(QString,QString,int,QString,bool)),&pousada,SLOT(add_quarto(QString,QString,int,QString,bool)));

    d->show();
}

void MainWindow::on_btn_remove_quarto_clicked()
{
    if(last_quarto_row_ID != 1){
        QuartoDialog* d = new QuartoDialog(&pousada,pousada.get_quarto(last_quarto_row_ID),false,this);

        QObject::connect(d,SIGNAL(remove_quarto(int)),&pousada,SLOT(remove_quarto(int)));

        last_quarto_row_ID = -1;

        d->show();
    }
}

void MainWindow::on_btn_edit_quarto_clicked()
{
    QuartoDialog* d = new QuartoDialog(&pousada,pousada.get_quarto(last_quarto_row_ID),true,this);

    QObject::connect(d,SIGNAL(edit_quarto(int,QString,QString,int,QString,bool)),&pousada,SLOT(edit_quarto(int,QString,QString,int,QString,bool)));

    d->show();
}

void MainWindow::on_cbx_quarto_tipo_quarto_filtro_currentTextChanged(const QString &arg1)
{
    if(ui->cbx_quarto_tipo_quarto_filtro->currentText() != "NENHUM")
        quarto_tipo = ui->cbx_quarto_tipo_quarto_filtro->currentText();
    else
        quarto_tipo = "";

    update_tables();
}

void MainWindow::on_btn_add_cliente_clicked()
{
    ClienteDialog* d = new ClienteDialog(this);

    QObject::connect(d,SIGNAL(create_cliente(QString,QString,QString,QString,Endereco)),&pousada,SLOT(add_cliente(QString,QString,QString,QString,Endereco)));

    d->show();
}

void MainWindow::on_btn_remove_cliente_clicked()
{
    if(last_cliente_row_ID != -1){
        ClienteDialog* d = new ClienteDialog(pousada.get_cliente(last_cliente_row_ID),false,this);

        QObject::connect(d,SIGNAL(remove_cliente(int)),&pousada,SLOT(remove_cliente(int)));

        last_cliente_row_ID = -1;

        d->show();
    }
}

void MainWindow::on_btn_edit_cliente_clicked()
{
    ClienteDialog* d = new ClienteDialog(pousada.get_cliente(last_cliente_row_ID),true,this);

    QObject::connect(d,SIGNAL(edit_cliente(int,QString,QString,QString,QString,Endereco)),&pousada,SLOT(edit_cliente(int,QString,QString,QString,QString,Endereco)));

    d->show();
}

void MainWindow::on_txt_cliente_cpf_textChanged()
{
    cliente_cpf = ui->txt_cliente_cpf->toPlainText();

    update_tables();
}

void MainWindow::on_table_quarto_cellPressed(int row, int column)
{
    last_quarto_row_ID = ui->table_quarto->item(row,0)->text().toInt();
}

void MainWindow::on_table_tipoquarto_cellPressed(int row, int column)
{
    last_tipo_quarto_row_ID = ui->table_tipoquarto->item(row,0)->text().toInt();
}

void MainWindow::on_table_cliente_cellPressed(int row, int column)
{
    last_cliente_row_ID = ui->table_cliente->item(row,0)->text().toInt();
}

void MainWindow::on_btn_add_hospede_clicked()
{
    HospedeDialog* d = new HospedeDialog(&pousada);

    QObject::connect(d,SIGNAL(create_hospede(QString,QString,QDate,QString)),&pousada,SLOT(add_hospede(QString,QString,QDate,QString)));

    d->show();
}

void MainWindow::on_btn_remove_hospede_clicked()
{
    if(last_hospede_row_ID != -1){
        HospedeDialog* d = new HospedeDialog(&pousada,false,pousada.get_hospede(last_hospede_row_ID));

        QObject::connect(d,SIGNAL(remove_hospede(int)),&pousada,SLOT(remove_hospede(int)));

        last_hospede_row_ID = -1;

        d->show();
    }
}

void MainWindow::on_btn_edit_hospede_clicked()
{
    HospedeDialog* d = new HospedeDialog(&pousada,true,pousada.get_hospede(last_hospede_row_ID));

    QObject::connect(d,SIGNAL(edit_hospede(int,QString,QString,QDate,QString)),&pousada,SLOT(edit_hospede(int,QString,QString,QDate,QString)));

    d->show();
}

void MainWindow::on_table_quarto_itemDoubleClicked(QTableWidgetItem *item)
{
    QuartoDialog* d = new QuartoDialog(&pousada,pousada.get_quarto(last_quarto_row_ID),true,this);

    QObject::connect(d,SIGNAL(edit_quarto(int,QString,QString,int,QString,bool)),&pousada,SLOT(edit_quarto(int,QString,QString,int,QString,bool)));

    d->show();
}

void MainWindow::on_txt_cliente_nome_textChanged()
{
    cliente_nome = ui->txt_cliente_nome->toPlainText();

    update_tables();
}

void MainWindow::on_txt_hospede_nome_textChanged()
{
    hospede_nome = ui->txt_hospede_nome->toPlainText();

    update_tables();
}

void MainWindow::on_txt_hospede_cpf_textChanged()
{
    hospede_cpf = ui->txt_hospede_cpf->toPlainText();

    update_tables();
}

void MainWindow::on_txt_cliente_textChanged()
{
    reserva_cliente = ui->txt_cliente->toPlainText();

    update_tables();
}

void MainWindow::on_txt_quarto_textChanged()
{
    reserva_quarto = ui->txt_quarto->toPlainText();

    update_tables();
}

void MainWindow::on_chk_inicio_lower_clicked()
{
    using_data_inicio = ui->chk_inicio_lower->isChecked();

    ui->chk_inicio_lower->setChecked(using_data_inicio);

    update_tables();
}

void MainWindow::on_chk_inicio_higher_clicked()
{
    using_data_inicio = ui->chk_inicio_lower->isChecked();

    ui->chk_inicio_lower->setChecked(using_data_inicio);

    update_tables();
}

void MainWindow::on_dt_inicio_lower_userDateChanged(const QDate &date)
{
    if(ui->dt_inicio_lower->date() > data_inicio_higher)
        data_inicio_higher = ui->dt_inicio_lower->date();

    data_inicio_lower = ui->dt_inicio_lower->date();

    update_tables();
}

void MainWindow::on_dt_inicio_higher_dateChanged(const QDate &date)
{
    if(ui->dt_inicio_higher->date() < data_inicio_lower)
        data_inicio_lower = ui->dt_inicio_lower->date();

    data_inicio_higher = ui->dt_inicio_higher->date();

    update_tables();
}

void MainWindow::on_tb_confirmations_cellClicked(int row, int column)
{
    last_reserva_confirm_ID = ui->tb_confirmations->item(row,0)->text().toInt();
}

void MainWindow::on_tb_confirmations_cellDoubleClicked(int row, int column)
{
    if(last_reserva_confirm_ID != -1){
        Reserva* reserva = pousada.get_reserva(last_reserva_confirm_ID);

        if(reserva->get_status() == Reserva::NAO_CONFIRMADA){
            ConfirmDialog* d = new ConfirmDialog(QString::asprintf("%d",reserva->get_ID()),QString::asprintf("Confirmar reserva do cliente '%s'? Com começo em %s e fim em %s no quarto(s) [%s].",reserva->get_cliente()->get_nome().toStdString().data(),
                                                                   reserva->get_data_inicio().toString("dd/MM/yyyy").toStdString().data(),reserva->get_data_fim().toString("dd/MM/yyyy").toStdString().data(),
                                                                   reserva->get_quartos_string().toStdString().data()));

            QObject::connect(d,SIGNAL(accept_dialog(QString)),this,SLOT(confirma_reserva(QString)));
        }
    }
}

void MainWindow::on_tb_check_ins_cellClicked(int row, int column)
{
    last_reserva_check_in_ID = ui->tb_check_ins->item(row,0)->text().toInt();
}

void MainWindow::on_tb_check_ins_cellDoubleClicked(int row, int column)
{
    if(last_reserva_check_in_ID != -1){
        Reserva* reserva = pousada.get_reserva(last_reserva_check_in_ID);

        if(reserva->get_status() == Reserva::CONFIRMADA){
            ReservaCheckinDialog* d = new ReservaCheckinDialog(&pousada,reserva,this);

            QObject::connect(d,SIGNAL(reserva_checkin(int,vector<int>,QDateTime)),&pousada,SLOT(checkin_reserva(int,vector<int>,QDateTime)));

            d->show();
        }
    }
}

void MainWindow::on_tb_check_outs_cellClicked(int row, int column)
{
    last_reserva_check_out_ID = ui->tb_check_outs->item(row,0)->text().toInt();
}

void MainWindow::on_tb_check_outs_cellDoubleClicked(int row, int column)
{
    if(last_reserva_check_out_ID != -1){
        Reserva* reserva = pousada.get_reserva(last_reserva_check_out_ID);

        if(reserva->get_status() == Reserva::CONFIRMADA){
            ReservaCheckoutDialog* d = new ReservaCheckoutDialog(&pousada,reserva,this);

            QObject::connect(d,SIGNAL(reserva_checkout(int,QDateTime)),&pousada,SLOT(checkout_reserva(int,QDateTime)));

            d->show();
        }
    }
}

void MainWindow::on_btn_confirma_clicked()
{
    if(last_reserva_confirm_ID != -1){
        Reserva* reserva = pousada.get_reserva(last_reserva_confirm_ID);

        if(reserva->get_status() == Reserva::NAO_CONFIRMADA){
            ConfirmDialog* d = new ConfirmDialog(QString::asprintf("%d",reserva->get_ID()),QString::asprintf("Confirmar reserva do cliente '%s'? Com começo em %s e fim em %s no quarto %s.",reserva->get_cliente()->get_nome().toStdString().data(),
                                                                   reserva->get_data_inicio().toString("dd/MM/yyyy").toStdString().data(),reserva->get_data_fim().toString("dd/MM/yyyy").toStdString().data(),
                                                                   reserva->get_quartos_string().toStdString().data()));

            QObject::connect(d,SIGNAL(accept_dialog(QString)),this,SLOT(confirma_reserva(QString)));

            d->show();
        }
    }
}

void MainWindow::on_btn_checkin_clicked()
{
    if(last_reserva_check_in_ID != -1){
        Reserva* reserva = pousada.get_reserva(last_reserva_check_in_ID);

        if(reserva->get_status() == Reserva::CONFIRMADA){
            ReservaCheckinDialog* d = new ReservaCheckinDialog(&pousada,reserva,this);

            QObject::connect(d,SIGNAL(reserva_checkin(int,vector<int>,QDateTime)),&pousada,SLOT(checkin_reserva(int,vector<int>,QDateTime)));

            d->show();
        }
    }
}

void MainWindow::on_btn_add_reserva_inicial_clicked()
{
    ReservaDialog* d = new ReservaDialog(&pousada);

    QObject::connect(d,SIGNAL(create_reserva(QString,vector<QString>,QDate,QDate,int,int)),&pousada,SLOT(add_reserva(QString,vector<QString>,QDate,QDate,int,int)));

    d->show();
}

void MainWindow::on_btn_add_reserva_clicked()
{
    ReservaDialog* d = new ReservaDialog(&pousada);

    QObject::connect(d,SIGNAL(create_reserva(QString,vector<QString>,QDate,QDate,int,int)),&pousada,SLOT(add_reserva(QString,vector<QString>,QDate,QDate,int,int)));

    d->show();
}

void MainWindow::on_btn_remove_reserva_clicked()
{
    if(last_reserva_row_ID != -1){
        ReservaDialog* d = new ReservaDialog(&pousada,false,pousada.get_reserva(last_reserva_row_ID));

        QObject::connect(d,SIGNAL(remove_reserva(int)),&pousada,SLOT(remove_reserva(int)));

        last_reserva_row_ID = -1;

        d->show();
    }
}

void MainWindow::on_btn_edit_reserva_clicked()
{
    ReservaDialog* d = new ReservaDialog(&pousada,true,pousada.get_reserva(last_reserva_row_ID));

    QObject::connect(d,SIGNAL(edit_reserva(int,int,int)),&pousada,SLOT(edit_reserva(int,int,int)));

    d->show();
}

void MainWindow::on_btn_checkout_clicked()
{
    if(last_reserva_check_out_ID != -1){
        Reserva* reserva = pousada.get_reserva(last_reserva_check_out_ID);

        if(reserva->get_status() == Reserva::CHECK_IN){
            ReservaCheckoutDialog* d = new ReservaCheckoutDialog(&pousada,reserva,this);

            QObject::connect(d,SIGNAL(reserva_checkout(int,QDateTime)),&pousada,SLOT(checkout_reserva(int,QDateTime)));

            d->show();
        }
    }
}

void MainWindow::on_table_reserva_itemClicked(QTableWidgetItem *item)
{
    last_reserva_row_ID = ui->table_reserva->item(item->row(),0)->text().toInt();
}

void MainWindow::on_table_reserva_itemDoubleClicked(QTableWidgetItem *item)
{
    ReservaDialog* d = new ReservaDialog(&pousada,true,pousada.get_reserva(last_reserva_row_ID));

    QObject::connect(d,SIGNAL(edit_reserva(int,int,int)),&pousada,SLOT(edit_reserva(int,int,int)));

    d->show();
}

void MainWindow::on_txt_quarto_numero_textChanged(const QString &arg1)
{
    quarto_numero = ui->txt_quarto_numero->text();

    update_tables();
}

void MainWindow::on_chk_disponivel_lower_clicked()
{
    using_disponivel = ui->chk_disponivel_lower->isChecked();

    ui->chk_disponivel_higher->setChecked(using_disponivel);

    update_tables();
}

void MainWindow::on_chk_disponivel_higher_clicked()
{
    using_disponivel = ui->chk_disponivel_higher->isChecked();

    ui->chk_disponivel_lower->setChecked(using_disponivel);

    update_tables();
}

void MainWindow::on_dt_disponivel_lower_userDateChanged(const QDate &date)
{
    if(date > data_fim)
        data_fim = date;

    data_inicio = date;

    update_tables();
}

void MainWindow::on_dt_disponivel_higher_userDateChanged(const QDate &date)
{
    if(date < data_inicio)
        data_inicio = date;

    data_fim = date;

    update_tables();
}

void MainWindow::on_act_backup_triggered()
{
    log.backup(&pousada);
}

void MainWindow::on_act_carregar_triggered()
{
    QString file = QFileDialog::getOpenFileName(this, tr("Open XML"),"/home/","*.xml");

    log.backup(&pousada,file);
}

void MainWindow::on_act_sair_triggered()
{
    log.do_backup(&pousada);

    this->deleteLater();
}

void MainWindow::on_act_salvar_triggered()
{
    QString file = QFileDialog::getSaveFileName(this,tr("Save XML"),"/home/");

    log.do_backup(&pousada,file);
}

void MainWindow::on_table_cliente_itemDoubleClicked(QTableWidgetItem *item)
{
    ClienteDialog* d = new ClienteDialog(pousada.get_cliente(last_cliente_row_ID),true,this);

    QObject::connect(d,SIGNAL(edit_cliente(int,QString,QString,QString,QString,Endereco)),&pousada,SLOT(edit_cliente(int,QString,QString,QString,QString,Endereco)));

    d->show();
}

void MainWindow::on_table_hospede_itemDoubleClicked(QTableWidgetItem *item)
{
    HospedeDialog* d = new HospedeDialog(&pousada,true,pousada.get_hospede(last_hospede_row_ID));

    QObject::connect(d,SIGNAL(edit_hospede(int,QString,QString,QString,QDate)),&pousada,SLOT(edit_hospede(int,QString,QString,QString,QDate)));

    d->show();
}

void MainWindow::on_table_hospede_cellPressed(int row, int column)
{
    last_hospede_row_ID = ui->table_hospede->item(row,0)->text().toInt();
}

void MainWindow::on_tb_confirmations_itemDoubleClicked(QTableWidgetItem *item)
{
    if(last_reserva_confirm_ID != -1){
        Reserva* reserva = pousada.get_reserva(last_reserva_confirm_ID);

        if(reserva->get_status() == Reserva::NAO_CONFIRMADA){
            ConfirmDialog* d = new ConfirmDialog(QString::asprintf("%d",reserva->get_ID()),QString::asprintf("Confirmar reserva do cliente '%s'? Com começo em %s e fim em %s no quarto(s) [%s].",reserva->get_cliente()->get_nome().toStdString().data(),
                                                                   reserva->get_data_inicio().toString("dd/MM/yyyy").toStdString().data(),reserva->get_data_fim().toString("dd/MM/yyyy").toStdString().data(),
                                                                   reserva->get_quartos_string().toStdString().data()));

            QObject::connect(d,SIGNAL(accept_dialog(QString)),this,SLOT(confirma_reserva(QString)));

            d->show();
        }
    }
}

void MainWindow::on_tb_check_ins_itemDoubleClicked(QTableWidgetItem *item)
{
    if(last_reserva_check_in_ID != -1){
        Reserva* reserva = pousada.get_reserva(last_reserva_check_in_ID);

        if(reserva->get_status() == Reserva::CONFIRMADA){
            ReservaCheckinDialog* d = new ReservaCheckinDialog(&pousada,reserva,this);

            QObject::connect(d,SIGNAL(reserva_checkin(int,vector<int>,QDateTime)),&pousada,SLOT(checkin_reserva(int,vector<int>,QDateTime)));

            d->show();
        }
    }
}

void MainWindow::on_tb_check_outs_itemDoubleClicked(QTableWidgetItem *item)
{
    if(last_reserva_check_out_ID != -1){
        Reserva* reserva = pousada.get_reserva(last_reserva_check_out_ID);

        if(reserva->get_status() == Reserva::CHECK_IN){
            ReservaCheckoutDialog* d = new ReservaCheckoutDialog(&pousada,reserva,this);

            QObject::connect(d,SIGNAL(reserva_checkout(int,QDateTime)),&pousada,SLOT(checkout_reserva(int,QDateTime)));
            QObject::connect(d,SIGNAL(reserva_checkout(int,QDateTime)),this,SLOT(update_tables()));

            d->show();
        }
    }
}

void MainWindow::on_btn_print_reserva_clicked()
{
    print_reservas(ui->table_reserva);
}

void MainWindow::print_reservas(QTableWidget* tbl){
    QPrinter printer(QPrinter::PrinterResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setPaperSize(QPrinter::A4);
    printer.setOrientation(QPrinter::Landscape);

    QString strFile = "C:\\Users\\Rosas\\Desktop\\meu_pouso.pdf";

    printer.setOutputFileName(strFile);

    QTextDocument doc;

    QString text("<table align='center' cellpadding='4'><thead>");
    text.append("<tr>");
    for (int i = 0; i < tbl->columnCount(); i++) {
        if(i == 1 || i == 2 || i == 3 || i == 4 || i == 5|| i == 6 || i == 7){
            text.append("<th>").append(tbl->horizontalHeaderItem(i)->data(Qt::DisplayRole).toString()).append("</th>");
        }
    }
    text.append("</tr></thead>");
    text.append("<tbody>");
    for (int i = 0; i < tbl->rowCount(); i++) {
        text.append("<tr>");
        for (int j = 0; j < tbl->columnCount(); j++) {
            if(j == 1 || j == 2 || j == 3 || j == 4 || j == 5|| j == 6 || j == 7){
                QTableWidgetItem *item = tbl->item(i, j);
                if (!item || item->text().isEmpty()) {
                    tbl->setItem(i, j, new QTableWidgetItem("0"));
                }
                text.append("<td>").append(tbl->item(i, j)->text()).append("</td>");
            }
        }
        text.append("</tr>");
    }
    text.append("</tbody></table>");
    doc.setHtml(text);
    doc.setPageSize(printer.pageRect().size());
    doc.print(&printer);
}

void MainWindow::on_cbx_status_reserva_currentTextChanged(const QString &arg1)
{
    if(arg1 == "NENHUM"){
        using_status = false;
    } else {
        using_status = true;
        reserva_status = Reserva::qstring_to_status(arg1);
    }

    update_tables();
}

void MainWindow::on_MainWindow_destroyed()
{
    do_backup();
}

void MainWindow::on_btn_update_toggled(bool checked)
{
    log.backup(&pousada);
}

void MainWindow::on_act_past_backup_triggered()
{
    log.setFolder();
}
