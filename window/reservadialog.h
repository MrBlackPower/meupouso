#ifndef RESERVADIALOG_H
#define RESERVADIALOG_H

#include <QDialog>

#include "../model/pousada.h"
#include "../model/reserva.h"

#include "procuraclientedialog.h"
#include "procuraquartodialog.h"

#include "confirmdialog.h"
#include "reservacheckindialog.h"
#include "reservacheckoutdialog.h"

namespace Ui {
class ReservaDialog;
}

class ReservaDialog : public QDialog
{
    Q_OBJECT

public:

    enum ReservaDialogMode{
        CREATE,
        EDIT,
        REMOVE
    };

    explicit ReservaDialog(Pousada* pousada, QWidget *parent = nullptr);
    explicit ReservaDialog(Pousada* pousada, bool edit, Reserva* reserva, QWidget *parent = nullptr);
    ~ReservaDialog();

public slots:

    void choose_quarto(QString quarto_ID, QString quarto_numero, int PAX);

    void choose_cliente(QString cliente_ID, QString cliente_nome);

    void update_list();

    void confirma_reserva();

    void check_in();

    void check_out();

signals:

    void create_reserva(QString cliente_ID, vector<QString> quarto_ID, QDate data_inicio, QDate data_fim, int hospedes_adultos, int hospedes_criancas);

    void edit_reserva(int ID, int hospedes_adultos, int hospedes_criancas);

    void remove_reserva(int ID);

private slots:
    void on_btn_procurar_cliente_clicked();

    void on_dt_inicio_userDateChanged(const QDate &date);

    void on_dt_fim_userDateChanged(const QDate &date);

    void on_btn_procurar_quarto_clicked();

    void on_spn_hospedes_adultos_valueChanged(const QString &arg1);

    void on_spn_hospedes_criancas_valueChanged(const QString &arg1);

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_btn_confirm_clicked();

    void on_btn_checkin_clicked();

    void on_btn_checkout_clicked();

    void update_btn();

    void on_lst_quartos_itemDoubleClicked(QListWidgetItem *item);

    void on_btn_cancelar_clicked();

    void on_btn_no_show_clicked();

private:
    Ui::ReservaDialog *ui;

    ReservaDialogMode modo;

    Pousada* pousada;

    Reserva* reserva;

    QString cliente_ID;
    QString cliente_nome;

    QString get_quartos_nome();

    vector<QString> quarto_ID;
    vector<int> quarto_PAX;
    vector<QString> quarto_numero;
    int quarto_PAX_t;

    QDate data_inicio;
    QDate data_fim;

    int hospedes_adultos;
    int hospedes_criancas;

    bool data_modificada;
};

#endif // RESERVADIALOG_H
