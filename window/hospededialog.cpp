#include "hospededialog.h"
#include "ui_hospededialog.h"

HospedeDialog::HospedeDialog(Pousada* p, QWidget *parent) :
    QDialog(parent),  ui(new Ui::HospedeDialog), pousada(p)
{
    ui->setupUi(this);

    modo = CREATE;

    h = NULL;

    data_nascimento = ui->dateEdit->date();

    cliente_chosen = false;

    if(!p->has_clientes())
        deleteLater();
}

HospedeDialog::HospedeDialog(Pousada* p, Cliente* c, QWidget *parent) :
    QDialog(parent),  ui(new Ui::HospedeDialog), pousada(p), cliente_ID(c->get_ID()), cliente_nome(c->get_nome())
{
    ui->setupUi(this);

    modo = CREATE;

    cliente_chosen = true;

    h = NULL;
    ui->txt_cliente->setText(cliente_nome);

    if(!p->has_clientes())
        deleteLater();
}

HospedeDialog::HospedeDialog(Pousada* p, bool edit, Hospede* hospede, QWidget *parent) :
    QDialog(parent),  ui(new Ui::HospedeDialog), pousada(p)
{
    ui->setupUi(this);

    cliente_chosen = true;

    h = hospede;

    nome = hospede->get_nome();
    cpf = hospede->get_cpf();
    data_nascimento = QDate::fromString(hospede->get_data_nascimento(),"dd/MM/yyyy");

    cliente_nome = hospede->get_cliente()->get_nome();
    cliente_ID = hospede->get_cliente()->get_ID();

    ui->txt_nome->setText(nome);
    ui->txt_cpf->setText(cpf);
    ui->dateEdit->setDate(data_nascimento);
    ui->txt_cliente->setText(cliente_nome);

    if(edit){
        modo = EDIT;

    }else{
        ui->btn_procurar_cliente->setEnabled(false);
        ui->txt_cpf->setEnabled(false);
        ui->dateEdit->setEnabled(false);
        ui->txt_cliente->setEnabled(false);
        ui->txt_nome->setEnabled(false);

        modo = REMOVE;
    }

    if(!p->has_clientes())
        deleteLater();
}

HospedeDialog::~HospedeDialog()
{
    delete ui;
}
void HospedeDialog::chose_cliente(QString ID, QString nome){
    cliente_ID = ID;
    cliente_nome = nome;
    cliente_chosen = true;

    ui->txt_cliente->setText(cliente_nome);
}

void HospedeDialog::on_btn_procurar_cliente_clicked()
{
    ProcuraClienteDialog* d = new ProcuraClienteDialog(pousada,this);

    QObject::connect(d,SIGNAL(chose_cliente(QString,QString)),this,SLOT(chose_cliente(QString,QString)));

    d->show();
}

void HospedeDialog::on_txt_nome_textChanged()
{
    nome = ui->txt_nome->toPlainText();
}

void HospedeDialog::on_txt_cpf_textChanged(const QString &arg1)
{
    cpf = ui->txt_cpf->text();
}

void HospedeDialog::on_dateEdit_userDateChanged(const QDate &date)
{
    data_nascimento = ui->dateEdit->dateTime().date();
}

void HospedeDialog::on_buttonBox_accepted()
{
    switch (modo) {
    case CREATE:
        if(cliente_chosen){
            emit create_hospede(cliente_ID,nome,data_nascimento,cpf);
        }else {
            InformDialog* d = new InformDialog("Hóspede não criado, selecionar cliente à ser associoado.");

            d->show();
        }
        break;
    case EDIT:
        emit edit_hospede(h->get_ID(),cliente_ID,nome,cpf,data_nascimento);
        break;
    case REMOVE:
        emit remove_hospede(h->get_ID());
    }

    deleteLater();
}

void HospedeDialog::on_buttonBox_rejected()
{
    deleteLater();
}
