#include "procuraclientedialog.h"
#include "ui_procuraclientedialog.h"

ProcuraClienteDialog::ProcuraClienteDialog(Pousada* p,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProcuraClienteDialog), p(p)
{
    ui->setupUi(this);

    nome = "";
    cpf = "";
    current_ID = "";
    current_nome = "";

    if(p == NULL or !p->has_clientes()){
        deleteLater();
    }else{
        update_list();
    }
}

void ProcuraClienteDialog::update_list(){
    ui->lst_clientes->clear();

    vector<QString> c_IDS;
    vector<QString> c_nome;
    vector<QString> c_cpf;
    vector<QString> c_tel1;
    vector<QString> c_tel2;
    vector<QString> c_endereco;
    p->get_clientes(&c_IDS,&c_nome,&c_cpf,&c_tel1,&c_tel2,&c_endereco,nome,cpf);

    for(int i = 0; i < c_IDS.size(); i++)
        ui->lst_clientes->addItem(QString::asprintf("%s %s %s",c_IDS.operator[](i).toStdString().data(),c_nome.operator[](i).remove(" ").toStdString().data()
                ,c_cpf.operator[](i).remove(" ").toStdString().data()));
}

ProcuraClienteDialog::~ProcuraClienteDialog()
{
    delete ui;
}

void ProcuraClienteDialog::on_txt_nome_textChanged()
{
    nome = ui->txt_nome->toPlainText();

    update_list();
}

void ProcuraClienteDialog::on_txt_cpf_textChanged()
{
    cpf = ui->txt_cpf->toPlainText();

    update_list();
}

void ProcuraClienteDialog::on_buttonBox_accepted()
{
    if(current_ID != "" && current_nome != "")
        emit chose_cliente(current_ID,current_nome);

    deleteLater();
}

void ProcuraClienteDialog::on_buttonBox_rejected()
{
    deleteLater();
}

void ProcuraClienteDialog::on_lst_clientes_itemClicked(QListWidgetItem *item)
{
    QString s = ui->lst_clientes->currentItem()->text();
    QTextStream ss(&s);

    ss >> current_ID >> current_nome;
}

void ProcuraClienteDialog::on_lst_clientes_itemDoubleClicked(QListWidgetItem *item)
{
    if(current_ID != "" && current_nome != "")
        emit chose_cliente(current_ID,current_nome);

    deleteLater();
}
