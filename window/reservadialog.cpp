#include "reservadialog.h"
#include "ui_reservadialog.h"

ReservaDialog::ReservaDialog(Pousada* pousada, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ReservaDialog), pousada(pousada), quarto_PAX(0)
{
    ui->setupUi(this);

    data_modificada = false;
    reserva = NULL;

    modo = CREATE;

    ui->btn_no_show->setEnabled(false);
    ui->btn_cancelar->setEnabled(false);

    ui->txt_status->setText("NAO_CONFIRMADA");

    ui->dt_inicio->setDate(QDate::currentDate());
    ui->dt_fim->setDate(QDate::currentDate());
    data_inicio = ui->dt_inicio->date();
    data_fim = ui->dt_fim->date();

    ui->btn_confirm->setEnabled(false);
    ui->btn_checkin->setEnabled(false);
    ui->btn_checkout->setEnabled(false);

    hospedes_adultos = 0;
    hospedes_criancas = 0;
    quarto_PAX_t = 0;

    if(pousada == NULL)
        deleteLater();
    else if (!pousada->has_clientes() or !pousada->has_quartos())
        deleteLater();
}

ReservaDialog::ReservaDialog(Pousada* pousada, bool edit, Reserva* reserva, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ReservaDialog), pousada(pousada), reserva(reserva), quarto_PAX(0)
{
    ui->setupUi(this);

    data_modificada = false;
    quarto_PAX_t = 0;


    if(pousada == NULL  || reserva == NULL){
        deleteLater();

    } else if (!pousada->has_clientes() or !pousada->has_quartos()){
        deleteLater();

    } else {
        cliente_ID = reserva->get_cliente()->get_ID();
        cliente_nome = reserva->get_cliente()->get_nome();

        vector<Quarto*> qs = reserva->get_quarto();
        for(Quarto* q : qs){
            quarto_ID.push_back(QString(q->get_ID()));
            quarto_numero.push_back(q->get_numero());
            quarto_PAX.push_back(q->get_PAX());

            quarto_PAX_t += q->get_PAX();
        }

        data_inicio = reserva->get_data_inicio();
        data_fim = reserva->get_data_fim();
        hospedes_adultos = reserva->get_hospedes_adultos();
        hospedes_criancas = reserva->get_hospedes_criancas();

        ui->txt_PAX->setText(QString::asprintf("%d",quarto_PAX_t));
        ui->txt_cliente->setText(cliente_nome);
        ui->dt_inicio->setDate(data_inicio);
        ui->dt_fim->setDate(data_fim);
        ui->spn_hospedes_adultos->setValue(hospedes_adultos);
        ui->spn_hospedes_criancas->setValue(hospedes_criancas);

        ui->txt_status->setText(Reserva::status_to_qstring(reserva->get_status()));

        if(edit){
            modo = EDIT;

            ui->btn_procurar_quarto->setEnabled(false);
            ui->btn_procurar_cliente->setEnabled(false);
            ui->dt_fim->setEnabled(false);
            ui->dt_inicio->setEnabled(false);

            if(reserva->get_status() != Reserva::NAO_CONFIRMADA){
                ui->spn_hospedes_adultos->setEnabled(false);
                ui->spn_hospedes_criancas->setEnabled(false);
                ui->lst_quartos->setEnabled(false);
            }

            if(reserva->get_status() == Reserva::NAO_CONFIRMADA){
                ui->btn_confirm->setEnabled(true);
            } else if(reserva->get_status() == Reserva::CONFIRMADA){
                ui->btn_checkin->setEnabled(true);
            }else if(reserva->get_status() == Reserva::CHECK_IN){
                ui->btn_checkout->setEnabled(true);
            }

        }else{
            modo = REMOVE;

            ui->btn_no_show->setEnabled(false);
            ui->btn_cancelar->setEnabled(false);

            ui->dt_fim->setEnabled(false);
            ui->dt_inicio->setEnabled(false);
            ui->btn_procurar_quarto->setEnabled(false);
            ui->btn_procurar_cliente->setEnabled(false);
            ui->spn_hospedes_adultos->setEnabled(false);
            ui->spn_hospedes_criancas->setEnabled(false);

            ui->btn_confirm->setEnabled(false);
            ui->btn_checkin->setEnabled(false);
            ui->btn_checkout->setEnabled(false);
        }
    }

    update_list();
}

ReservaDialog::~ReservaDialog()
{
    delete ui;
}

void ReservaDialog::choose_quarto(QString quarto_ID, QString quarto_numero, int PAX){
    this->quarto_ID.push_back(quarto_ID);
    this->quarto_numero.push_back(quarto_numero);
    this->quarto_PAX.push_back(PAX);

    quarto_PAX_t += PAX;

    ui->txt_PAX->setText(QString::asprintf("%d",quarto_PAX_t));

    update_list();
}

QString ReservaDialog::get_quartos_nome(){
    QString aux;

    for(QString n : quarto_numero)
        aux += QString::asprintf("%s ",n.toStdString().data());

    return aux;
}

void ReservaDialog::choose_cliente(QString cliente_ID, QString cliente_nome){
    this->cliente_ID = cliente_ID;
    this->cliente_nome = cliente_nome;

    ui->txt_cliente->setText(cliente_nome);
}

void ReservaDialog::update_list(){
    ui->lst_quartos->clear();

    for(int i = 0; i < quarto_numero.size(); i++)
        ui->lst_quartos->addItem(quarto_numero.operator[](i));
}

void ReservaDialog::check_in(){
    deleteLater();
}

void ReservaDialog::check_out(){
    deleteLater();
}

void ReservaDialog::confirma_reserva(){
    reserva->confirm();

    pousada->update_xml();

    pousada->external_updated();

    deleteLater();
}

void ReservaDialog::on_btn_procurar_cliente_clicked()
{
    ProcuraClienteDialog* d = new ProcuraClienteDialog(pousada);

    QObject::connect(d,SIGNAL(chose_cliente(QString,QString)),this,SLOT(choose_cliente(QString,QString)));

    d->show();
}

void ReservaDialog::on_dt_inicio_userDateChanged(const QDate &date)
{
    data_modificada = true;
    if(data_fim < date){
        data_fim = date;
        ui->dt_fim->setDate(data_fim);
    }

    data_inicio = date;
}

void ReservaDialog::on_dt_fim_userDateChanged(const QDate &date)
{
    data_modificada = true;
    if(data_inicio > date){
        data_inicio = date;
        ui->dt_inicio->setDate(data_inicio);
    }

    data_fim = date;
}

void ReservaDialog::on_btn_procurar_quarto_clicked()
{
    ProcuraQuartoDialog* d;

    if(!data_modificada)
        d = new ProcuraQuartoDialog(quarto_ID,pousada,this);
    else
        d = new ProcuraQuartoDialog(quarto_ID,pousada,data_inicio,data_fim,this);

    QObject::connect(d,SIGNAL(chose_quarto(QString,QString,int)),this,SLOT(choose_quarto(QString,QString,int)));

    d->show();
}

void ReservaDialog::on_spn_hospedes_adultos_valueChanged(const QString &arg1)
{
    if((ui->spn_hospedes_adultos->value() + hospedes_criancas) <= quarto_PAX_t){
        hospedes_adultos = ui->spn_hospedes_adultos->value();
    } else{
        ui->spn_hospedes_adultos->setValue(hospedes_adultos);
    }
}

void ReservaDialog::on_spn_hospedes_criancas_valueChanged(const QString &arg1)
{
    if((ui->spn_hospedes_criancas->value() + hospedes_adultos) <= quarto_PAX_t){
        hospedes_criancas = ui->spn_hospedes_criancas->value();
    } else{
        ui->spn_hospedes_criancas->setValue(hospedes_criancas);
    }
}

void ReservaDialog::on_buttonBox_accepted()
{
    switch (modo) {
    case CREATE:
        if(pousada->quartos_disponivel(quarto_ID, data_inicio, data_fim)){
            emit create_reserva(cliente_ID,quarto_ID,data_inicio,data_fim,hospedes_adultos,hospedes_criancas);

        }else{
            InformDialog* d = new InformDialog("Quarto não disponível nas datas selecionadas.");

            d->show();

            deleteLater();
        }
        break;
    case EDIT:
        emit edit_reserva(reserva->get_ID(),hospedes_adultos,hospedes_criancas);
        break;
    case REMOVE:
        emit remove_reserva(reserva->get_ID());
        break;
    }

    deleteLater();
}

void ReservaDialog::on_buttonBox_rejected()
{
    deleteLater();
}

void ReservaDialog::on_btn_confirm_clicked()
{
    if(modo == EDIT){
        if(reserva->get_status() == Reserva::NAO_CONFIRMADA){
            ConfirmDialog* d = new ConfirmDialog(QString::asprintf("Confirmar reserva do cliente '%s'? Com começo em %s e fim em %s no quarto(s) [%s].",reserva->get_cliente()->get_nome().toStdString().data(),
                                                                   reserva->get_data_inicio().toString("dd/MM/yyyy").toStdString().data(),reserva->get_data_fim().toString("dd/MM/yyyy").toStdString().data(),
                                                                   reserva->get_quartos_string().toStdString().data()),this);

            QObject::connect(d,SIGNAL(accept_dialog()),this,SLOT(confirma_reserva()));

            d->show();
        }
    }
}

void ReservaDialog::on_btn_checkin_clicked()
{
    if(modo == EDIT){
        if(reserva->get_status() == Reserva::CONFIRMADA){
            ReservaCheckinDialog* d = new ReservaCheckinDialog(pousada,reserva,this);

            QObject::connect(d,SIGNAL(reserva_checkin(int,vector<int>,QDateTime)),pousada,SLOT(checkin_reserva(int,vector<int>,QDateTime)));
            QObject::connect(d,SIGNAL(reserva_checkin(int,vector<int>,QDateTime)),this,SLOT(check_in()));

            d->show();
        }
    }
}

void ReservaDialog::on_btn_checkout_clicked()
{
    if(modo == EDIT){
        if(reserva->get_status() == Reserva::CHECK_IN){
            ReservaCheckoutDialog* d = new ReservaCheckoutDialog(pousada,reserva,this);

            QObject::connect(d,SIGNAL(reserva_checkout(int,QDateTime)),pousada,SLOT(checkout_reserva(int,QDateTime)));
            QObject::connect(d,SIGNAL(reserva_checkout(int,QDateTime)),this,SLOT(check_out()));

            d->show();
        }
    }
}


void ReservaDialog::update_btn(){
    ui->btn_checkin->setEnabled(false);
    ui->btn_checkout->setEnabled(false);
    ui->btn_confirm->setEnabled(false);

    if(reserva->get_status() == Reserva::NAO_CONFIRMADA){
        ui->btn_confirm->setEnabled(true);
    } else if(reserva->get_status() == Reserva::CONFIRMADA){
        ui->btn_checkin->setEnabled(true);
    }else if(reserva->get_status() == Reserva::CHECK_IN){
        ui->btn_checkout->setEnabled(true);
    }
}

void ReservaDialog::on_lst_quartos_itemDoubleClicked(QListWidgetItem *item)
{
    int i = ui->lst_quartos->currentRow();

    if(i < quarto_ID.size()){
        quarto_ID.erase(quarto_ID.begin() + i);
        quarto_PAX.erase(quarto_PAX.begin() + i);
        quarto_numero.erase(quarto_numero.begin() + i);

        quarto_PAX_t = 0;
        for(int i : quarto_PAX)
            quarto_PAX_t += i;
    }

    QString s(quarto_PAX_t);
    ui->txt_PAX->setText(s);
    update_list();
}

void ReservaDialog::on_btn_cancelar_clicked()
{
    pousada->cancela_reserva(reserva->get_ID());

    deleteLater();
}

void ReservaDialog::on_btn_no_show_clicked()
{
    pousada->no_show_reserva(reserva->get_ID());

    deleteLater();
}
