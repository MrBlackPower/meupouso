#ifndef PROCURAQUARTODIALOG_H
#define PROCURAQUARTODIALOG_H

#include <QDialog>
#include <QString>
#include <QDate>
#include <QTextStream>
#include <QListWidget>

#include "../model/pousada.h"

#include "../model/quarto.h"

namespace Ui {
class ProcuraQuartoDialog;
}

class ProcuraQuartoDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ProcuraQuartoDialog(vector<QString> quartos_id, Pousada* p, QWidget *parent = nullptr);
    explicit ProcuraQuartoDialog(vector<QString> quartos_id, Pousada* p, QDate inicio, QDate fim, QWidget *parent = nullptr);
    ~ProcuraQuartoDialog();

signals:

    void chose_quarto(QString id, QString numero, int PAX);

private slots:
    void on_dt_lower_userDateChanged(const QDate &date);

    void on_dt_higher_userDateChanged(const QDate &date);

    void on_txt_numero_textChanged();

    void on_cbx_tipo_quarto_currentTextChanged(const QString &arg1);

    void on_chk_lower_clicked();

    void on_chk_higher_clicked();

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_lst_quartos_itemDoubleClicked(QListWidgetItem *item);

    void on_lst_quartos_itemClicked(QListWidgetItem *item);

private:

    void update_list();

    Ui::ProcuraQuartoDialog *ui;

    Pousada* pousada;

    QString quarto_ID;
    QString quarto_numero;
    QString quarto_PAX;
    QString quarto_tipo;

    vector<QString> quartos_set;

    bool quarto_set;

    vector<pair<QString,int>> tipos_quarto;

    QDate disponivel_inicio;
    QDate disponivel_fim;
    bool disponivel_checked;
};

#endif // PROCURAQUARTODIALOG_H
