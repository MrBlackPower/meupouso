#include "reservacheckindialog.h"
#include "ui_reservacheckindialog.h"

ReservaCheckinDialog::ReservaCheckinDialog(Pousada* p, Reserva* r, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ReservaCheckinDialog), reserva(r), pousada(p)
{
    ui->setupUi(this);

    refresh_rate = new QTimer();
    refresh_rate->setInterval(REFRESH_RATE);
    refresh_rate->start();
    QObject::connect(refresh_rate,SIGNAL(timeout()),this,SLOT(update_current_time()));

    using_current = true;
    checked_in = 0;

    update_hospedes();

    if(!r or !p){
        deleteLater();
    }else{
        ui->txt_PAX->setText(QString::asprintf("%d",reserva->get_PAX()));
        ui->txt_quarto->setText(reserva->get_quartos_string());
        ui->txt_cliente->setText(reserva->get_cliente()->get_nome());
        ui->txt_reserva->setText(QString::asprintf("%d",reserva->get_ID()));
    }
}

ReservaCheckinDialog::~ReservaCheckinDialog()
{
    delete ui;
    delete refresh_rate;
}

void ReservaCheckinDialog::update_current_time(){
    ui->dt_current->setDateTime(QDateTime::currentDateTime());
}

void ReservaCheckinDialog::update_hospedes(){
    vector<int> hospedes_IDS = pousada->get_hospede_cliente_id(reserva->get_cliente()->get_ID());
    vector<QString> hospedes = pousada->get_hospede_cliente(reserva->get_cliente()->get_ID());

    all_hospedes.clear();
    ui->lst_hospedes_disp->clear();

    if(hospedes.size() != hospedes_IDS.size())
        return;

    int i = 0;
    int found = 0;
    for(int id : hospedes_IDS){
        all_hospedes.push_back(pair<int,QString>{id,hospedes.operator[](i)});

        bool found_this = false;
        if(found != selected_hospedes.size()){
            for(auto a : selected_hospedes){
                if(a.first == id){
                    found_this = true;
                    found++;
                    break;
                }
            }
        }

        if(!found_this){
            ui->lst_hospedes_disp->addItem(QString::asprintf("%d %s",id,hospedes.operator[](i).remove(" ").toStdString().data()));
        }

        i++;
    }
}

void ReservaCheckinDialog::on_radioButton_clicked()
{
    using_current = ui->radioButton->isChecked();

    ui->radioButton_2->setChecked(!using_current);
    check_in_date = ui->dt_current->dateTime();
}

void ReservaCheckinDialog::on_radioButton_2_clicked()
{
    using_current = !ui->radioButton_2->isChecked();

    ui->radioButton->setChecked(using_current);
    check_in_date = ui->dt_insert->dateTime();
}

void ReservaCheckinDialog::on_dt_current_dateChanged(const QDate &date)
{
    if(using_current)
        check_in_date = ui->dt_current->dateTime();
}

void ReservaCheckinDialog::on_dt_insert_dateChanged(const QDate &date)
{
    if(!using_current)
        check_in_date = ui->dt_insert->dateTime();
}

void ReservaCheckinDialog::on_lst_hospedes_disp_itemDoubleClicked(QListWidgetItem *item)
{
    if(selected_hospedes.size() < reserva->get_hospedes()){
        QString s(ui->lst_hospedes_disp->currentItem()->text());
        QTextStream ss(&s);

        int id;
        QString nome;

        ss >> id >> nome;

        ui->lst_hospedes_disp->removeItemWidget(item);

        selected_hospedes.emplace_back(pair<int,QString>{id,nome});

        ui->lst_hospedes_selec->addItem(QString::asprintf("%d %s",id,nome.remove(" ").toStdString().data()));

        update_hospedes();
    } else {
        InformDialog* d = new InformDialog("Numero máximo de hospedes atingido.");

        d->show();
    }
}

void ReservaCheckinDialog::on_lst_hospedes_selec_itemDoubleClicked(QListWidgetItem *item)
{
    QString s(ui->lst_hospedes_selec->currentItem()->text());
    QTextStream ss(&s);

    int id;
    QString nome;

    ss >> id >> nome;

    ui->lst_hospedes_selec->takeItem(ui->lst_hospedes_selec->currentRow());

    bool found = false;
    for(int i = 0; i < selected_hospedes.size() && !found; i++){
        if(id == selected_hospedes.operator[](i).first){
            found = true;
            selected_hospedes.erase(selected_hospedes.begin()+i);
        }
    }

    if(!found){
        deleteLater();
        return;
    }

    update_hospedes();
}

void ReservaCheckinDialog::on_btn_add_hospede_clicked()
{
    HospedeDialog* d = new HospedeDialog(pousada,reserva->get_cliente(),this);

    QObject::connect(d,SIGNAL(create_hospede(QString,QString,QDate,QString)),pousada,SLOT(add_hospede(QString,QString,QDate,QString)));
    QObject::connect(d,SIGNAL(create_hospede(QString,QString,QDate,QString)),this,SLOT(on_btn_refresh_clicked()));

    d->show();
}

void ReservaCheckinDialog::on_buttonBox_accepted()
{
    if(selected_hospedes.size() <= reserva->get_hospedes()){
        vector<int> hs;

        for(pair<int,QString> s : selected_hospedes){
            hs.push_back(s.first);
        }

        emit reserva_checkin(reserva->get_ID(),hs, check_in_date);

        if(selected_hospedes.size() < reserva->get_hospedes()){
            InformDialog* d = new InformDialog("ALERTA NO CHECKIN: quantidade de hóspedes da reserva superior à quantidade de hóspedes selecionados no checkin.",InformDialog::ERROR,this);

            d->show();
        }

        deleteLater();
    } else {
        InformDialog* d = new InformDialog("ERRO NO CHECKIN: quantidade de hóspedes da reserva diferente da quantidade de hóspedes selecionados no checkin.",InformDialog::ERROR,this);

        d->show();
    }
}

void ReservaCheckinDialog::on_buttonBox_rejected()
{
    deleteLater();
}

void ReservaCheckinDialog::on_btn_refresh_clicked()
{
    update_hospedes();
    update_current_time();
}
