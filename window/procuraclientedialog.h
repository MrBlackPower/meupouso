#ifndef PROCURACLIENTEDIALOG_H
#define PROCURACLIENTEDIALOG_H

#include <QDialog>
#include <QTextStream>
#include <QListWidget>

#include "../model/pousada.h"
#include "../model/cliente.h"

namespace Ui {
class ProcuraClienteDialog;
}

class ProcuraClienteDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ProcuraClienteDialog(Pousada* p,QWidget *parent = nullptr);
    ~ProcuraClienteDialog();

signals:

    void chose_cliente(QString ID, QString nome);

private slots:
    void on_txt_nome_textChanged();

    void on_txt_cpf_textChanged();

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_lst_clientes_itemClicked(QListWidgetItem *item);

    void on_lst_clientes_itemDoubleClicked(QListWidgetItem *item);

private:
    void update_list();

    Ui::ProcuraClienteDialog *ui;

    QString nome;
    QString cpf;

    QString current_ID;
    QString current_nome;

    Pousada* p;
};

#endif // PROCURACLIENTEDIALOG_H
