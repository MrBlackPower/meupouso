#include "reservacheckoutdialog.h"
#include "ui_reservacheckoutdialog.h"

ReservaCheckoutDialog::ReservaCheckoutDialog(Pousada* p, Reserva* r, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ReservaCheckoutDialog), reserva(r), pousada(p)
{
    ui->setupUi(this);

    using_current = true;

    refresh_rate = new QTimer();
    refresh_rate->setInterval(REFRESH_RATE);
    refresh_rate->start();
    QObject::connect(refresh_rate,SIGNAL(timeout()),this,SLOT(update_current_time()));

    if(!r or !p)
        deleteLater();
    else{
        update_hospedes(r);
    }
}

void ReservaCheckoutDialog::update_hospedes(Reserva* r){
    vector<int> hospedes_IDS = pousada->get_hospede_cliente_id(reserva->get_cliente()->get_ID());
    vector<QString> hospedes = pousada->get_hospede_cliente(reserva->get_cliente()->get_ID());


    if(hospedes.size() != hospedes_IDS.size())
        return;

    int i = 0;
    vector<int> list = r->get_hospedes_list();
    for(int id : list){
        bool found = false;
        int aux = -1;
        for(int j = 0; j < hospedes_IDS.size() && !found; j++) {
            if(id == hospedes_IDS.operator[](j)){
                found = true;
                aux = j;
            }
        }

        if(found && aux != -1){
            ui->lst_hospedes->addItem(QString::asprintf("%d %s",id,hospedes.operator[](aux).toStdString().data()));
        }
        i++;
    }
}

ReservaCheckoutDialog::~ReservaCheckoutDialog()
{
    delete refresh_rate;

    delete ui;
}
void ReservaCheckoutDialog::update_current_time(){
    ui->dt_current->setDateTime(QDateTime::currentDateTime());
}

void ReservaCheckoutDialog::on_radioButton_clicked()
{
    using_current = ui->radioButton->isChecked();

    ui->radioButton_2->setChecked(!using_current);
}

void ReservaCheckoutDialog::on_radioButton_2_clicked()
{
    using_current = !ui->radioButton_2->isChecked();

    ui->radioButton->setChecked(using_current);

    if(using_current)
        current_datetime = ui->dt_current->dateTime();
    else
        current_datetime = ui->dt_insert->dateTime();
}

void ReservaCheckoutDialog::on_dt_insert_dateChanged(const QDate &date)
{
    if(!using_current)
        current_datetime = ui->dt_insert->dateTime();
}

void ReservaCheckoutDialog::on_dt_current_dateChanged(const QDate &date)
{
    if(using_current)
        current_datetime = ui->dt_current->dateTime();
}

void ReservaCheckoutDialog::on_buttonBox_accepted()
{
    emit reserva_checkout(reserva->get_ID(),current_datetime);
}

void ReservaCheckoutDialog::on_buttonBox_rejected()
{

}
