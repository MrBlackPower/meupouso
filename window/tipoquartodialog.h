#ifndef TIPOQUARTODIALOG_H
#define TIPOQUARTODIALOG_H

#include <QDialog>

#include "model/tipoquarto.h"
#include "model/quarto.h"
#include "model/pousada.h"

namespace Ui {
class TipoQuartoDialog;
}

class TipoQuartoDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TipoQuartoDialog(QWidget *parent = nullptr);
    explicit TipoQuartoDialog(Pousada* pousada,TipoQuarto* tipo, bool edit, QWidget *parent = nullptr);
    ~TipoQuartoDialog();

    enum TipoQuartoDialogMode{
        CREATE,
        EDIT,
        REMOVE
    };

signals:

    void create_tipo_quarto(QString nome, QString descricao);

    void edit_tipo_quarto(int ID, QString nome, QString descricao);

    void remove_tipo_quarto(int ID);

private slots:
    void on_buttonBox_accepted();

    void on_txt_nome_textChanged();

    void on_txt_descricao_textChanged();

    void on_buttonBox_rejected();

private:
    Pousada* pousada;

    Ui::TipoQuartoDialog *ui;

    QString nome;
    QString descricao;

    int tipo_quarto_id;

    TipoQuartoDialogMode modo;
};

#endif // TIPOQUARTODIALOG_H
