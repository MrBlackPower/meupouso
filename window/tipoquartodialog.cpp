#include "tipoquartodialog.h"
#include "ui_tipoquartodialog.h"

TipoQuartoDialog::TipoQuartoDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TipoQuartoDialog)
{
    ui->setupUi(this);

    this->setWindowTitle("Criar Tipo de Quarto");

    modo = CREATE;

    tipo_quarto_id = 0;
    pousada = NULL;
}

TipoQuartoDialog::TipoQuartoDialog(Pousada* pousada,TipoQuarto* tipo, bool edit, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TipoQuartoDialog)
{
    ui->setupUi(this);

    nome = tipo->get_nome();
    descricao = tipo->get_descricao();
    tipo_quarto_id = tipo->get_ID();

    ui->txt_nome->setText(nome);
    ui->txt_descricao->setText(descricao);

    if(edit){
        modo = EDIT;


        this->setWindowTitle("Editar Tipo de Quarto");
    }else{
        modo = REMOVE;


        this->setWindowTitle("Excluir Tipo de Quarto");

        ui->txt_nome->setEnabled(false);
        ui->txt_descricao->setEnabled(false);
    }
}

TipoQuartoDialog::~TipoQuartoDialog()
{
    delete ui;
}

void TipoQuartoDialog::on_buttonBox_accepted()
{
    switch (modo) {
    case CREATE:
        emit create_tipo_quarto(nome,descricao);
        break;
    case EDIT:
        emit edit_tipo_quarto(tipo_quarto_id,nome,descricao);
        break;
    case REMOVE:
        emit remove_tipo_quarto(tipo_quarto_id);
        break;
    }

    deleteLater();
}

void TipoQuartoDialog::on_txt_nome_textChanged()
{
    nome = ui->txt_nome->toPlainText();
}

void TipoQuartoDialog::on_txt_descricao_textChanged()
{
    descricao = ui->txt_descricao->toPlainText();
}

void TipoQuartoDialog::on_buttonBox_rejected()
{
    deleteLater();
}
