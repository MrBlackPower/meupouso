#include "confirmdialog.h"
#include "ui_confirmdialog.h"

ConfirmDialog::ConfirmDialog(QString text, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfirmDialog), msg()
{
    ui->setupUi(this);
    ui->textBrowser->setText(text);
}
ConfirmDialog::ConfirmDialog(QString msg,QString text, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfirmDialog), msg(msg)
{
    ui->setupUi(this);
    ui->textBrowser->setText(text);
}

ConfirmDialog::~ConfirmDialog()
{
    delete ui;
}

void ConfirmDialog::on_buttonBox_accepted()
{
    emit accept_dialog();

    emit accept_dialog(msg);

    deleteLater();
}

void ConfirmDialog::on_buttonBox_rejected()
{
    emit deny_dialog();

    deleteLater();
}
