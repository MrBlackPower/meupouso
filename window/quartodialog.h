#ifndef QUARTODIALOG_H
#define QUARTODIALOG_H

#include <QDialog>
#include <vector>

#include "../model/tipoquarto.h"
#include "../model/quarto.h"
#include "../model/pousada.h"

using namespace std;

namespace Ui {
class QuartoDialog;
}

class QuartoDialog : public QDialog
{
    Q_OBJECT

public:
    explicit QuartoDialog(Pousada* p, QWidget *parent = nullptr);
    explicit QuartoDialog(Pousada* p, Quarto* q, bool edit, QWidget *parent = nullptr);
    ~QuartoDialog();

    enum QuartoDialogMode{
        CREATE,
        EDIT,
        REMOVE
    };

signals:

    void create_quarto(QString numero, QString descricao, int PAX, QString tipo_ID, bool disponivel);

    void edit_quarto(int ID, QString numero, QString descricao, int PAX, QString tipo_ID, bool disponivel);

    void remove_quarto(int ID);

private slots:
    void on_txt_descricao_textChanged();

    void on_txt_nome_textChanged();

    void on_buttonBox_accepted();

    void on_cbx_tipo_quarto_currentIndexChanged(const QString &arg1);

    void on_chk_disp_stateChanged(int arg1);

    void on_spn_PAX_valueChanged(int arg1);

    void on_buttonBox_rejected();

private:
    Ui::QuartoDialog *ui;

    QuartoDialogMode modo;

    int quarto_id;

    QString numero;
    QString descricao;
    QString tipo;
    QString tipo_id;
    int PAX;
    bool disponibilidade;

    vector<pair<QString,int>> tipos_quarto;
    int qtd_quartos;
    Pousada* pousada;

    pair<QString,int> tipos_quarto_item;
};

#endif // QUARTODIALOG_H
