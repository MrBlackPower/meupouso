#include "clientedialog.h"
#include "ui_clientedialog.h"


ClienteDialog::ClienteDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ClienteDialog)
{
    ui->setupUi(this);

    cliente_id = 0;
    modo = CREATE;
    pais = "Brasil";

    set_countries();
}

ClienteDialog::ClienteDialog(Cliente* c, bool edit, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ClienteDialog)
{
    ui->setupUi(this);

    cliente_id = c->get_ID();
    set_countries(c->get_endereco().get_pais());

    nome = c->get_nome();
    cpf = c->get_cpf();
    telefone_movel = c->get_telefone_movel();
    telefone_fixo = c->get_telefone_fixo();

    Endereco e{c->get_endereco()};

    rua = e.get_rua();
    CEP = e.get_CEP();
    bairro = e.get_bairro();
    cidade = e.get_cidade();
    estado = e.get_estado();
    pais = e.get_pais();

    ui->txt_CEP->setText(CEP);
    ui->txt_endereco_bairro->setText(bairro);
    ui->txt_cpf->setText(cpf);
    ui->txt_nome->setText(nome);
    ui->txt_endereco_rua->setText(rua);
    ui->txt_telefone_fixo->setText(telefone_fixo);
    ui->txt_telefone_movel->setText(telefone_movel);
    ui->txt_endereco_cidade->setText(cidade);
    ui->txt_endereco_estado->setText(estado);

    if(edit){
        modo = EDIT;
    }else{
        ui->txt_CEP->setEnabled(false);
        ui->txt_endereco_bairro->setEnabled(false);
        ui->txt_cpf->setEnabled(false);
        ui->txt_nome->setEnabled(false);
        ui->txt_endereco_rua->setEnabled(false);
        ui->txt_telefone_fixo->setEnabled(false);
        ui->txt_telefone_movel->setEnabled(false);
        ui->txt_endereco_cidade->setEnabled(false);
        ui->txt_endereco_estado->setEnabled(false);
        ui->cbx_pais->setEnabled(false);

        modo = REMOVE;
    }

    int index = ui->cbx_pais->findText(e.get_pais());
    if ( index != -1 ) { // -1 for not found
       ui->cbx_pais->setCurrentIndex(index);
    }
}

ClienteDialog::~ClienteDialog()
{
    delete ui;
}

void ClienteDialog::on_txt_nome_textChanged()
{
    nome = ui->txt_nome->toPlainText();
}

void ClienteDialog::on_txt_telefone_movel_textChanged(const QString &arg1)
{
    telefone_movel = ui->txt_telefone_movel->text();
}

void ClienteDialog::on_txt_telefone_fixo_textChanged(const QString &arg1)
{
    telefone_fixo = ui->txt_telefone_fixo->text();
}

void ClienteDialog::on_txt_endereco_rua_textChanged()
{
    rua = ui->txt_endereco_rua->toPlainText();
}

void ClienteDialog::on_txt_CEP_textChanged(const QString &arg1)
{
    CEP = ui->txt_CEP->text();
}

void ClienteDialog::on_txt_endereco_bairro_textChanged()
{
    bairro = ui->txt_endereco_bairro->toPlainText();
}

void ClienteDialog::on_txt_endereco_estado_textChanged()
{
    estado = ui->txt_endereco_estado->toPlainText();
}

void ClienteDialog::on_cbx_pais_currentTextChanged(const QString &arg1)
{
    pais = ui->cbx_pais->currentText();
}

void ClienteDialog::set_countries(){
    ui->cbx_pais->clear();
    vector<QString> countries = meupousolog::loadRaw("C:\\Users\\Rosas\\Documents\\MeuPouso\\meupouso\\paises.dat");

    for(auto c : countries)
        ui->cbx_pais->addItem(c);

}

void ClienteDialog::set_countries(QString pais){
    ui->cbx_pais->clear();
    vector<QString> countries = meupousolog::loadRaw("C:\\Users\\Rosas\\Documents\\MeuPouso\\meupouso\\paises.dat");

    for(auto c : countries)
        ui->cbx_pais->addItem(c);
}

void ClienteDialog::on_buttonBox_accepted()
{
    switch (modo) {
    case CREATE:
        emit create_cliente(nome, cpf, telefone_movel, telefone_fixo, Endereco{rua,CEP,bairro,cidade,estado,pais});
        break;
    case EDIT:
        emit edit_cliente(cliente_id,nome, cpf, telefone_movel, telefone_fixo, Endereco{rua,CEP,bairro,cidade,estado,pais});
        break;
    case REMOVE:
        emit remove_cliente(cliente_id);
        break;
    }

    deleteLater();
}

void ClienteDialog::on_txt_endereco_cidade_textChanged()
{
    cidade = ui->txt_endereco_cidade->toPlainText();
}

void ClienteDialog::on_txt_cpf_textChanged(const QString &arg1)
{
    cpf = ui->txt_cpf->text();
}
