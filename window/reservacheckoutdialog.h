#ifndef RESERVACHECKOUTDIALOG_H
#define RESERVACHECKOUTDIALOG_H

#include <QDialog>
#include <QTimer>

#include "../model/reserva.h"
#include "../model/pousada.h"

#define REFRESH_RATE 500

namespace Ui {
class ReservaCheckoutDialog;
}

class ReservaCheckoutDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ReservaCheckoutDialog(Pousada* p, Reserva* r, QWidget *parent = nullptr);
    ~ReservaCheckoutDialog();

public slots:

    void update_current_time();

    void update_hospedes(Reserva* r);

signals:

    void reserva_checkout(int reserva_ID, QDateTime datetime);

private slots:
    void on_radioButton_clicked();

    void on_radioButton_2_clicked();

    void on_dt_insert_dateChanged(const QDate &date);

    void on_dt_current_dateChanged(const QDate &date);

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    QTimer* refresh_rate;

    Reserva* reserva;

    Pousada* pousada;

    bool using_current;

    QDateTime current_datetime;

    Ui::ReservaCheckoutDialog *ui;
};

#endif // RESERVACHECKOUTDIALOG_H
