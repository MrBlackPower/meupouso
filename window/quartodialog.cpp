#include "quartodialog.h"
#include "ui_quartodialog.h"

QuartoDialog::QuartoDialog(Pousada* p, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QuartoDialog), tipos_quarto(p->get_tipos_quarto()), qtd_quartos(tipos_quarto.size()), modo(CREATE), pousada(p)
{
    ui->setupUi(this);

    this->setWindowTitle("Criar Quarto");

    numero = "";
    descricao = "";
    disponibilidade = true;
    PAX = 0;
    ui->chk_disp->setChecked(true);

    if(qtd_quartos == 0){
        deleteLater();


    }else {
        tipo = tipos_quarto.operator[](0).first;
        tipo_id = QString::asprintf("%d",tipos_quarto.operator[](0).second);
    }

    for(pair<QString,int> txt : tipos_quarto){
        ui->cbx_tipo_quarto->addItem(txt.first);
    }
}

QuartoDialog::QuartoDialog(Pousada* p, Quarto* q, bool edit, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QuartoDialog), tipos_quarto(p->get_tipos_quarto()), qtd_quartos(tipos_quarto.size()), numero(q->get_numero()), descricao(q->get_descricao()), PAX(q->get_PAX()),
    quarto_id(q->get_ID()),  disponibilidade(q->get_disponibilidade()), pousada(p), tipo_id(q->get_tipo_quarto()->get_ID())
{
    ui->setupUi(this);

    ui->txt_descricao->setText(descricao);
    ui->txt_nome->setText(numero);

    if(qtd_quartos == 0)
        this->close();


    ui->txt_nome->setText(numero);
    ui->txt_descricao->setText(descricao);
    ui->chk_disp->setChecked(disponibilidade);
    ui->spn_PAX->setValue(PAX);

    QString this_txt = q->get_tipo_quarto()->get_nome();


    if(edit){
        modo = EDIT;

        this->setWindowTitle("Editar Quarto");
    }else{
        modo = REMOVE;

        this->setWindowTitle("Excluir Quarto");

        ui->txt_nome->setEnabled(false);
        ui->txt_descricao->setEnabled(false);
        ui->chk_disp->setEnabled(false);
        ui->spn_PAX->setEnabled(false);
    }

    for(pair<QString,int> txt : tipos_quarto){
        ui->cbx_tipo_quarto->addItem(txt.first);
    }

    tipo = (q->get_tipo_quarto()->get_nome());
    tipo_id = q->get_tipo_quarto()->get_ID();

    int index = ui->cbx_tipo_quarto->findText(tipo);
    if ( index != -1 ) { // -1 for not found
       ui->cbx_tipo_quarto->setCurrentIndex(index);
    }
}

QuartoDialog::~QuartoDialog()
{
    delete ui;
}

void QuartoDialog::on_txt_descricao_textChanged()
{
    descricao = ui->txt_descricao->toPlainText();
}

void QuartoDialog::on_txt_nome_textChanged()
{
    numero = ui->txt_nome->toPlainText();
}

void QuartoDialog::on_buttonBox_accepted()
{
    switch (modo) {
    case CREATE:
        emit create_quarto(numero, descricao, PAX, tipo_id, disponibilidade);
        break;
    case EDIT:
        emit edit_quarto(quarto_id,numero, descricao, PAX, tipo_id, disponibilidade);
        break;
    case REMOVE:
        emit remove_quarto(quarto_id);
        break;
    }

    deleteLater();
}

void QuartoDialog::on_cbx_tipo_quarto_currentIndexChanged(const QString &arg1)
{
    if(ui->cbx_tipo_quarto->currentText() == "NENHUM"){
        tipo = "";
        return;
    }

    tipo = ui->cbx_tipo_quarto->currentText();
    tipo_id = QString::asprintf("%d",tipos_quarto.operator[](ui->cbx_tipo_quarto->currentIndex()).second);
}

void QuartoDialog::on_chk_disp_stateChanged(int arg1)
{
    disponibilidade = ui->chk_disp->isChecked();
}

void QuartoDialog::on_spn_PAX_valueChanged(int arg1)
{
    PAX = arg1;
}

void QuartoDialog::on_buttonBox_rejected()
{
    deleteLater();
}
