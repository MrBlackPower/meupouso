#ifndef CONFIRMDIALOG_H
#define CONFIRMDIALOG_H

#include <QDialog>

namespace Ui {
class ConfirmDialog;
}

class ConfirmDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ConfirmDialog(QString text, QWidget *parent = nullptr);
    explicit ConfirmDialog(QString msg,QString text, QWidget *parent = nullptr);
    ~ConfirmDialog();

signals:
    void accept_dialog(QString msg);

    void accept_dialog();

    void deny_dialog();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::ConfirmDialog *ui;

    QString msg;
};

#endif // CONFIRMDIALOG_H
