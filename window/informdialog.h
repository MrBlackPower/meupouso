#ifndef INFORMDIALOG_H
#define INFORMDIALOG_H

#include <QDialog>

namespace Ui {
class InformDialog;
}

class InformDialog : public QDialog
{
    Q_OBJECT

public:
    enum InformDialogMode{
        ERROR,
        WARNING
    };

    explicit InformDialog(QString msg, InformDialogMode mode = WARNING, QWidget *parent = nullptr);
    ~InformDialog();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::InformDialog *ui;
};

#endif // INFORMDIALOG_H
