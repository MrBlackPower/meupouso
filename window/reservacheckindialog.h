#ifndef RESERVACHECKINDIALOG_H
#define RESERVACHECKINDIALOG_H

#include <QDialog>
#include <QTimer>
#include <QDateTime>
#include <QListWidget>

#include "hospededialog.h"
#include "informdialog.h"

#include "../model/pousada.h"
#include "../model/reserva.h"

#define REFRESH_RATE 500

namespace Ui {
class ReservaCheckinDialog;
}

class ReservaCheckinDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ReservaCheckinDialog(Pousada* p, Reserva* r, QWidget *parent = nullptr);
    ~ReservaCheckinDialog();

public slots:
    void update_current_time();

    void update_hospedes();

signals:

    void reserva_checkin(int reserva_ID, vector<int> hospedes, QDateTime datetime);

private slots:
    void on_radioButton_clicked();

    void on_radioButton_2_clicked();

    void on_dt_current_dateChanged(const QDate &date);

    void on_dt_insert_dateChanged(const QDate &date);

    void on_lst_hospedes_disp_itemDoubleClicked(QListWidgetItem *item);

    void on_lst_hospedes_selec_itemDoubleClicked(QListWidgetItem *item);

    void on_btn_add_hospede_clicked();

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_btn_refresh_clicked();

private:
    Ui::ReservaCheckinDialog *ui;

    bool using_current;

    int checked_in;

    vector<pair<int,QString>> all_hospedes;

    vector<pair<int,QString>> selected_hospedes;

    QDateTime check_in_date;

    Pousada* pousada;

    Reserva* reserva;

    QTimer* refresh_rate;
};

#endif // RESERVACHECKINDIALOG_H
