#ifndef CLIENTEDIALOG_H
#define CLIENTEDIALOG_H

#include <QDialog>
#include <QStringList>

#include "model/cliente.h"
#include "model/endereco.h"
#include "meupousolog.h"

namespace Ui {
class ClienteDialog;
}

class ClienteDialog : public QDialog
{
    Q_OBJECT

public:

    enum ClienteDialogMode{
        CREATE,
        EDIT,
        REMOVE
    };

    explicit ClienteDialog(QWidget *parent = nullptr);
    explicit ClienteDialog(Cliente* c, bool edit, QWidget *parent = nullptr);
    ~ClienteDialog();

signals:

    void create_cliente(QString nome, QString cpf, QString telefone_movel, QString telefone_fixo, Endereco endereco);

    void edit_cliente(int ID, QString nome, QString cpf, QString telefone_movel, QString telefone_fixo, Endereco endereco);

    void remove_cliente(int ID);

private slots:
    void on_txt_nome_textChanged();

    void on_txt_telefone_movel_textChanged(const QString &arg1);

    void on_txt_telefone_fixo_textChanged(const QString &arg1);

    void on_txt_endereco_rua_textChanged();

    void on_txt_CEP_textChanged(const QString &arg1);

    void on_txt_endereco_bairro_textChanged();

    void on_txt_endereco_estado_textChanged();

    void on_cbx_pais_currentTextChanged(const QString &arg1);

    void on_buttonBox_accepted();

    void on_txt_endereco_cidade_textChanged();

    void on_txt_cpf_textChanged(const QString &arg1);

private:
    Ui::ClienteDialog *ui;

    ClienteDialogMode modo;

    void set_countries();

    void set_countries(QString pais);

    int cliente_id;

    QString nome;
    QString cpf;
    QString telefone_movel;
    QString telefone_fixo;
    QString rua;
    QString CEP;
    QString bairro;
    QString cidade;
    QString estado;
    QString pais;
};

#endif // CLIENTEDIALOG_H
