#include "informdialog.h"
#include "ui_informdialog.h"

InformDialog::InformDialog(QString msg, InformDialogMode mode, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InformDialog)
{
    ui->setupUi(this);

    ui->textBrowser->setText(msg);

    if(mode == ERROR)
        ui->btn_warning->hide();
    else if (mode == WARNING)
        ui->btn_error->hide();
}

InformDialog::~InformDialog()
{
    delete ui;
}

void InformDialog::on_buttonBox_accepted()
{
    deleteLater();
}
