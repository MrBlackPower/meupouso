#include "procuraquartodialog.h"
#include "ui_procuraquartodialog.h"

ProcuraQuartoDialog::ProcuraQuartoDialog(vector<QString> quartos_id, Pousada* p, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProcuraQuartoDialog), pousada(p), tipos_quarto(p->get_tipos_quarto()), quarto_set(false), quartos_set(quartos_id)
{
    ui->setupUi(this);

    disponivel_checked = false;

    if(pousada == NULL or !pousada->has_quartos())
        deleteLater();

    ui->cbx_tipo_quarto->addItem("NENHUM");
    for(pair<QString,int> txt : tipos_quarto)
        ui->cbx_tipo_quarto->addItem(txt.first);

    update_list();
}

ProcuraQuartoDialog::ProcuraQuartoDialog(vector<QString> quartos_id, Pousada* p, QDate inicio, QDate fim, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProcuraQuartoDialog), pousada(p), tipos_quarto(p->get_tipos_quarto()), quarto_set(false), quartos_set(quartos_id)
{
    ui->setupUi(this);

    disponivel_checked = true;

    disponivel_inicio = inicio;
    disponivel_fim = fim;

    if(inicio > fim){
        disponivel_inicio = fim;
        disponivel_fim = inicio;
    }

    ui->dt_higher->setDate(fim);
    ui->dt_lower->setDate(inicio);

    ui->dt_higher->setEnabled(false);
    ui->dt_lower->setEnabled(false);
    ui->chk_higher->setEnabled(false);
    ui->chk_lower->setEnabled(false);

    ui->chk_higher->setChecked(true);
    ui->chk_lower->setChecked(true);

    if(pousada == NULL or !pousada->has_quartos())
        deleteLater();

    ui->cbx_tipo_quarto->addItem("NENHUM");
    for(pair<QString,int> txt : tipos_quarto)
        ui->cbx_tipo_quarto->addItem(txt.first);

    update_list();
}



void ProcuraQuartoDialog::update_list(){
    ui->lst_quartos->clear();

    vector<QString> q_IDS;
    vector<QString> q_numeros;
    vector<QString> q_PAXs;
    vector<QString> q_disps;
    vector<QString> q_tipos_de_quartos;
    vector<QString> q_descs;

    if(disponivel_checked)
        pousada->get_quartos(&q_IDS,&q_numeros,&q_PAXs,&q_disps,&q_tipos_de_quartos,&q_descs,quarto_numero,quarto_tipo,disponivel_inicio,disponivel_fim);
    else
        pousada->get_quartos(&q_IDS,&q_numeros,&q_PAXs,&q_disps,&q_tipos_de_quartos,&q_descs,quarto_numero,quarto_tipo);

    for (int i = 0; i < q_IDS.size(); i++){
        bool found = false;

        for (int j = 0; j < quartos_set.size() && !found; j++) {
            if(q_IDS.operator[](i) == quartos_set.operator[](j))
                found = true;
        }

        if(!found){
            ui->lst_quartos->addItem(QString::asprintf("%s %s %s %s",q_IDS.operator[](i).toStdString().data(),q_numeros.operator[](i).remove(" ").toStdString().data()
                    ,q_PAXs.operator[](i).toStdString().data(),q_tipos_de_quartos.operator[](i).remove(" ").toStdString().data()));
        }
    }
}

ProcuraQuartoDialog::~ProcuraQuartoDialog()
{
    delete ui;
}

void ProcuraQuartoDialog::on_dt_lower_userDateChanged(const QDate &date)
{
    if(disponivel_fim < ui->dt_lower->date()){
        disponivel_fim = ui->dt_lower->date();
        ui->dt_higher->setDate(disponivel_fim);
    }

    disponivel_inicio = ui->dt_lower->date();
}

void ProcuraQuartoDialog::on_dt_higher_userDateChanged(const QDate &date)
{
    if(disponivel_inicio > ui->dt_higher->date()){
        disponivel_inicio = ui->dt_higher->date();
        ui->dt_lower->setDate(disponivel_fim);
    }

    disponivel_fim = ui->dt_higher->date();
}

void ProcuraQuartoDialog::on_txt_numero_textChanged()
{
    quarto_numero = ui->txt_numero->toPlainText();
}

void ProcuraQuartoDialog::on_cbx_tipo_quarto_currentTextChanged(const QString &arg1)
{
    if(ui->cbx_tipo_quarto->currentText() == "NENHUM")
        quarto_tipo = "";
    else
        quarto_tipo = ui->cbx_tipo_quarto->currentText();
}

void ProcuraQuartoDialog::on_chk_lower_clicked()
{
    disponivel_checked = !disponivel_checked;

    ui->chk_lower->setChecked(disponivel_checked);
    ui->chk_higher->setChecked(disponivel_checked);
}

void ProcuraQuartoDialog::on_chk_higher_clicked()
{
    disponivel_checked = !disponivel_checked;

    ui->chk_lower->setChecked(disponivel_checked);
    ui->chk_higher->setChecked(disponivel_checked);
}

void ProcuraQuartoDialog::on_buttonBox_accepted()
{
    if(quarto_set)
        emit chose_quarto(quarto_ID,quarto_numero,quarto_PAX.toInt());

    deleteLater();
}

void ProcuraQuartoDialog::on_buttonBox_rejected()
{
    deleteLater();
}

void ProcuraQuartoDialog::on_lst_quartos_itemDoubleClicked(QListWidgetItem *item)
{
    if(quarto_set)
        emit chose_quarto(quarto_ID,quarto_numero,quarto_PAX.toInt());

    deleteLater();
}

void ProcuraQuartoDialog::on_lst_quartos_itemClicked(QListWidgetItem *item)
{
    QString s(ui->lst_quartos->currentItem()->text());
    QTextStream ss(&s);

    ss >> quarto_ID >> quarto_numero >> quarto_PAX >> quarto_tipo;

    quarto_set = true;
}
