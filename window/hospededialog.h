#ifndef HOSPEDEDIALOG_H
#define HOSPEDEDIALOG_H

#include <QDialog>

#include "../model/cliente.h"
#include "../model/hospede.h"
#include "../model/pousada.h"

#include "procuraclientedialog.h"

#include "informdialog.h"

namespace Ui {
class HospedeDialog;
}

class HospedeDialog : public QDialog
{
    Q_OBJECT

public:

    enum HospedeDialogMode{
        CREATE,
        EDIT,
        REMOVE
    };

    explicit HospedeDialog(Pousada* p, QWidget *parent = nullptr);
    explicit HospedeDialog(Pousada* p, Cliente* c, QWidget *parent = nullptr);
    explicit HospedeDialog(Pousada* p, bool edit, Hospede* hospede, QWidget *parent = nullptr);
    ~HospedeDialog();

public slots:
    void chose_cliente(QString ID, QString nome);

signals:

    void create_hospede(QString cliente_ID, QString nome, QDate data_nascimento, QString cpf);

    void edit_hospede(int ID,QString cliente_ID, QString nome, QString cpf, QDate data_nascimento);

    void remove_hospede(int ID);

private slots:
    void on_btn_procurar_cliente_clicked();

    void on_txt_nome_textChanged();

    void on_txt_cpf_textChanged(const QString &arg1);

    void on_dateEdit_userDateChanged(const QDate &date);

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Hospede* h;

    QString nome;
    QString cpf;

    bool cliente_chosen;
    QString cliente_nome;
    QString cliente_ID;

    QDate data_nascimento;

    HospedeDialogMode modo;

    Ui::HospedeDialog *ui;

    Pousada* pousada;
};

#endif // HOSPEDEDIALOG_H
