cmake_minimum_required(VERSION 3.8.2)

project(meu_pouso)

# Find includes in the build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
include_directories(window)

# Turn on automatic invocation of the MOC, UIC & RCC
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)

# There may be a way to tell up front if Qt5 is going to be found, but I haven't found
# a foolproof way to do it yet, so settle for the default error message for now.

# Add a compiler flag
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")

# Make this a GUI application on Windows
if(WIN32)
  set(CMAKE_WIN32_EXECUTABLE ON)
endif()

#find_package(TinyXML REQUIRED)
# Find the QtWidgets library
find_package(Qt5 REQUIRED COMPONENTS Widgets PrintSupport)


set(BOOST_INCLUDEDIR "/home/igor/Documentos/boost_1_70_0")
set(BOOST_LIBRARYDIR "/home/igor/Documentos/boost_1_70_0/stage/lib")

find_package(Boost 1.70.0 COMPONENTS date_time )
set(Boost_USE_STATIC_LIBS OFF)
set(Boost_USE_MULTITHREADED ON)

set(TINY_XML
    tinyxml/tinyxml.cpp
    tinyxml/tinyxmlerror.cpp
    tinyxml/tinyxmlparser.cpp
    tinyxml/tinystr.cpp)

set(WINDOWS
    mainwindow.ui
    mainwindow.cpp
    window/tipoquartodialog.cpp
    window/tipoquartodialog.ui
    window/quartodialog.cpp
    window/quartodialog.ui
    window/clientedialog.cpp
    window/clientedialog.ui
    window/hospededialog.cpp
    window/hospededialog.ui
    window/procuraclientedialog.cpp
    window/procuraclientedialog.ui
    window/procuraquartodialog.cpp
    window/procuraquartodialog.ui
    window/reservadialog.cpp
    window/reservadialog.ui
    window/confirmdialog.cpp
    window/confirmdialog.ui
    window/informdialog.cpp
    window/informdialog.ui
    window/reservacheckindialog.cpp
    window/reservacheckindialog.ui
    window/reservacheckoutdialog.cpp
    window/reservacheckoutdialog.ui
    )

set(CLIENT
    model/quarto.cpp
    model/tipoquarto.cpp
    model/cliente.cpp
    model/pousada.cpp
    model/hospede.cpp
    model/endereco.cpp
    model/reserva.cpp
    model/helper/qstringformathelper.cpp
    meupousolog.cpp
    resources.qrc
    ${WINDOWS}
    ${TINY_XML})

# Tell CMake to create the meu_pouso executable
add_executable(meu_pouso main.cpp ${CLIENT})
target_link_libraries(meu_pouso ${Boost_DATE_TIME_LIBRARY})
target_link_libraries(meu_pouso Qt5::Widgets Qt5::PrintSupport)

# Add the Qt5 Widgets for linking
